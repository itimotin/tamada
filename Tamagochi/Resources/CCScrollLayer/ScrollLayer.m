#import "ScrollLayer.h"

#define kMinVelocity              4.0
#define kDecelerationRate         0.45
#define kSetContentOffsetDuration 0.5

@interface ScrollLayer ()
- (void)_decelerate:(ccTime)dt;
- (CGPoint)_constrainedPosition:(CGPoint)p;
- (void)_moveBy:(CGPoint)dx;
- (void)_moveTo:(CGPoint)newPos animated:(BOOL)animated;
@end

@implementation ScrollLayer

@synthesize visibleRect, clipsToBounds, scrollDelegate, scrollingEnabled, directionLock, dragging, sizeItem;

// FYI default anchor point is (0,0) so node space has origin in
// lower-left corner with X to the right and Y upwards.

- (id)init {
    if ((self = [super init])) {
        self.position = CGPointZero;
        winSize = [[CCDirector sharedDirector] winSize];
        self.sizeItem=winSize;
        CGSize defaultSize = [[CCDirector sharedDirector] winSize];
        self.visibleRect = (CGRect) { .origin = CGPointZero, .size = defaultSize };
        self.contentSize  = defaultSize;
        self.scrollingEnabled = YES;
        self.clipsToBounds = YES;
        touchTime=0.1;
    }
    return self;
}



#pragma mark touch handler registration

- (void)onEnter {
    [super onEnter];
    [[[CCDirector sharedDirector] touchDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:NO];
}

- (void)onExit {
    [super onExit];
    [[[CCDirector sharedDirector] touchDispatcher] removeDelegate:self];
}

#pragma mark touch handling

- (void)_clearTrackingState {
    dragStartPoint = CGPointZero;
    prevTimestamp = 0;
    
    direction = CGPointZero;
    velocity = 0;
    
    autoMoving = NO;
    autoMoveToPoint = CGPointZero;
    
    dragging = NO;
    
    [[[CCDirector sharedDirector] scheduler] unscheduleSelector:@selector(_decelerate:) forTarget:self];
}

-(void)verifyButton:(CGPoint)touchLoc
{
    for (int i=1;i<=4;i++)
    {
        if (CGRectContainsPoint(CGRectMake(0, [[self getChildByTag:i] boundingBox].origin.y+self.position.y+ [[[self getChildByTag:i] getChildByTag:100 ]boundingBox].origin.y,  [[[self getChildByTag:i] getChildByTag:100 ]boundingBox].size.width,  [[[self getChildByTag:i] getChildByTag:100 ]boundingBox].size.height), touchLoc))
        {
            [scrollDelegate scrollButtonActive:self button:100 rind:i];
            return;
        }
        else
            if (CGRectContainsPoint(CGRectMake(winSize.width-[[[self getChildByTag:i] getChildByTag:101 ]boundingBox].size.width, [[self getChildByTag:i] boundingBox].origin.y+self.position.y+ [[[self getChildByTag:i] getChildByTag:101 ]boundingBox].origin.y,  [[[self getChildByTag:i] getChildByTag:101 ]boundingBox].size.width,  [[[self getChildByTag:i] getChildByTag:101 ]boundingBox].size.height), touchLoc))
            {
                [scrollDelegate scrollButtonActive:self button:101 rind:i];
                return;
            }
    }
}

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
       CGPoint touchPoint = [[CCDirector sharedDirector] convertToGL:[touch locationInView:touch.view]];
     [self verifyButton:touchPoint];
       if (!self.visible || !self.scrollingEnabled)
        return NO;
  
    
    startPoint = touchPoint;
    
    if (CGRectContainsPoint(self.visibleRect, touchPoint)) {

        [self _clearTrackingState];    
        dragStartPoint = touchPoint;
        prevTimestamp = touch.timestamp;
        
        return YES;
    }
    
    return NO;
}

- (CGPoint)_swipeVectorFromTouches:(UITouch *)touch {
    CGPoint currTouchPoint = [[CCDirector sharedDirector] convertToGL:[touch locationInView:touch.view]];
    CGPoint prevTouchPoint = [[CCDirector sharedDirector] convertToGL:[touch previousLocationInView:touch.view]];
    
    CGPoint swipeVector = ccpSub(currTouchPoint, prevTouchPoint);
    
    switch (directionLock) {
        case kScrollLayerDirectionLockNone:                          break;
        case kScrollLayerDirectionLockHorizontal: swipeVector.y = 0; break;
        case kScrollLayerDirectionLockVertical:   swipeVector.x = 0; break;
    }
    
    NSTimeInterval deltaTime = touch.timestamp - prevTimestamp;
    prevTimestamp = touch.timestamp;
    
    if (deltaTime > 0)
        velocity = (ccpLength(swipeVector) / deltaTime);
    
    direction = ccpNormalize(swipeVector);
    
    return swipeVector;
}

- (void)ccTouchMoved:(UITouch*)touch withEvent:(UIEvent*)event {
    CGPoint touchPoint = [[CCDirector sharedDirector] convertToGL:[touch locationInView:touch.view]];
    if (!self.visible || !self.scrollingEnabled ||  !CGRectContainsPoint(self.visibleRect, touchPoint))
        return;
    
     
     CGPoint swipeVector = [self _swipeVectorFromTouches:touch];
    
    
    

    if (!dragging) {
        dragging = YES;
     //   [self.scrollDelegate scrollLayerWillBeginDragging:self];
    }
    
   
    if ([self tag]!=1)
    {
       
        float distanceY=startPoint.y-touchPoint.y;
        if (distanceY<0) distanceY*=(-1);
        float distanceX=startPoint.x-touchPoint.x;
        if (distanceX<0) distanceX*=(-1);

        if (distanceY>distanceX)
        {
            swipeVector.x=0.0;
            if (distanceY>10){
                //[scrollDelegate scrollLayerCurrentItem:currentItem scroll:self category:s_createNewSprite];
                startPoint=touchPoint;
            }
        }
        
           
    }
    [self _moveBy:swipeVector];
}

- (void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event {
    if ([self tag]!=1)
    {
        [[self getChildByTag:currentItem] stopAllActions];
        [[self getChildByTag:currentItem] setScale:1.0];
    }
    if (!self.visible || !self.scrollingEnabled)
        return;
    
    // [scrollDelegate scrollLayerActive:self active:YES];
     touchTime=0.1;
    [self _swipeVectorFromTouches:touch];
    
    dragging = NO;
    
   
    
    if (velocity > kMinVelocity) {
       // [self.scrollDelegate scrollLayerDidEndDragging:self willDecelerate:YES];
       // [self.scrollDelegate scrollLayerWillBeginDecelerating:self];
        
        [[[CCDirector sharedDirector] scheduler] scheduleSelector:@selector(_decelerate:) forTarget:self interval:0 paused:NO];
    } else {
       // [self.scrollDelegate scrollLayerDidEndDragging:self willDecelerate:NO];
        [self _clearTrackingState];
    }
}

- (void)ccTouchCancelled:(UITouch *)touch withEvent:(UIEvent *)event {
    [self _clearTrackingState];
}

#pragma mark internal

- (void)_decelerate:(ccTime)dt {
    self.position = [self _constrainedPosition:ccpAdd(self.position, ccpMult(direction, velocity * dt))];
   // [self.scrollDelegate scrollLayerDidScroll:self];
    
    BOOL shouldStop = (autoMoving) 
    ? (ccpDistance(autoMoveToPoint, self.position) <= 1)
    : ((velocity *= kDecelerationRate) > kMinVelocity);
    
    if (shouldStop)
        [self _clearTrackingState];
}

- (CGPoint)_constrainedPosition:(CGPoint)p {
  
    CGFloat visibleLeft;
     CGFloat visibleRight ;
    if (directionLock==kScrollLayerDirectionLockVertical)
    {
        visibleLeft= self.visibleRect.size.width/2;
        visibleRight  = self.visibleRect.size.width/2 + self.visibleRect.size.width;
    }
    else
    {
        visibleLeft= 0;
        visibleRight  = 0+ self.visibleRect.size.width;
    }
   
    CGFloat visibleBottom = self.visibleRect.origin.y;
   
    
    CGFloat visibleTop    = self.visibleRect.origin.y + self.visibleRect.size.height;
    
    CGFloat virtualWidth  = self.contentSize.width;
    CGFloat virtualHeight = self.contentSize.height;
    
    if (p.x > visibleLeft)   p.x = visibleLeft;
    if (p.y > visibleBottom) p.y = visibleBottom;
    
    if (p.x + virtualWidth  < visibleRight) p.x = visibleRight - virtualWidth;
    if (p.y + virtualHeight < visibleTop)   p.y = visibleTop   - virtualHeight;
    
    return p;
}

- (void)_moveBy:(CGPoint)dx {
    self.position = [self _constrainedPosition:ccpAdd(self.position, dx)];
   // [scrollDelegate scrollLayerDidScroll:self];
}

- (void)_moveTo:(CGPoint)newPos animated:(BOOL)animated {
    self.position = [self _constrainedPosition:newPos];
   // [scrollDelegate scrollLayerDidScroll:self];
}

#pragma mark Public API

- (void)setContentOffset:(CGPoint)contentOffset animated:(BOOL)animated {
    CGPoint dx;
    if (directionLock==kScrollLayerDirectionLockVertical)
        dx= ccpSub(self.visibleRect.origin, contentOffset);
    else
        dx= contentOffset;
    
    [self _clearTrackingState];
    
    if (!animated) {
        [self _moveBy:dx];
    } else {
        autoMoving = YES;
        autoMoveToPoint = [self _constrainedPosition:ccpAdd(self.position, dx)];
      
        direction = ccpNormalize(dx);
        velocity  = ccpLength(dx) / kSetContentOffsetDuration;
        
        [[[CCDirector sharedDirector] scheduler] scheduleSelector:@selector(_decelerate:) forTarget:self interval:0 paused:NO];
    }
}

-(void)nextPage
{
    [self setContentOffset:CGPointMake(self.position.x-50, 0) animated:YES];
}
-(void)backPage
{
    [self setContentOffset:CGPointMake(self.position.x+50, 0) animated:YES];
}

@end
