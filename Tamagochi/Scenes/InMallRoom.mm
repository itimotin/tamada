//
//  FirstScene.m
//  Tamagochi
//
//  Created by Sergiu Moțipan on 6/4/13.
//
//

#import "InMallRoom.h"

@implementation InMallRoom
{
    int touchObject;
    int timp;
    int timp_2;
    int mouse;
    int oche_mouse;
    int n_mouse;
    int eye_pos;
    float totalTime,yy,xx, tamTouch;
    BOOL repeatTouches;
    float limit;

    CCSprite *btn_shop;
    
    CCSprite *btn_skins;
    CCSprite *btn_eat;
    CCSprite *btn_potions;
   
}



+(CCScene *) scene
{
	CCScene *scene = [CCScene node];
    scene.tag=11;
	InMallRoom *layer = [InMallRoom node];
	[scene addChild: layer];
	return scene;
}


-(id)init
{
    self=[super init];
    if (self)
    {
        [[SimpleAudioEngine sharedEngine] stopAllEffects];
        [[SimpleAudioEngine sharedEngine] setEffectsVolume:[stateTamagochi soundOn]];
        [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume:[stateTamagochi soundOn]];
        [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
        
        [[SimpleAudioEngine sharedEngine] setEffectsVolume:[stateTamagochi soundOn]];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(createBackground:) name:@"showBackgroundAlpha" object:nil];
        [stateTamagochi setRoom:mallRoom];
        mouth=[stateTamagochi mouthState];
        [[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"giggle.wav"];
        [[SimpleAudioEngine sharedEngine] setEffectsVolume:[stateTamagochi soundOn]];
        [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume:[stateTamagochi soundOn]];
        sound1 = [[SimpleAudioEngine sharedEngine] playEffect:@"in_mall_sound.mp3" loop:YES];
        
        winSize = [[CCDirector sharedDirector] winSize];
        self.isTouchEnabled=YES;
        
        CCSprite *background = [CCSprite spriteWithFile:[NSString stringWithFormat:@"bg_inMall%@.png",IMG_PREFIX]];
        background.position = ccp(winSize.width/2, winSize.height/2);
        [background setScale:0.5+0.5*IS_RETINA];
        [self addChild:background z:-12];
        
        if (!IS_IPAD)
            btn_potions = [CCSprite spriteWithFile:@"btn_potions.png"];
        else
            btn_potions = [CCSprite spriteWithFile:@"btn_potions-iPad.png"];
        [btn_potions setScale:0.5+0.5*IS_RETINA];
        btn_potions.position = ccp(btn_potions.boundingBox.size.width/2+10+10*IS_IPAD, winSize.height-btn_potions.boundingBox.size.height);
       
        [self addChild:btn_potions z:1];
        
        if (!IS_IPAD)
            btn_skins = [CCSprite spriteWithFile:@"btn_skins.png"];
        else
            btn_skins = [CCSprite spriteWithFile:@"btn_skins-iPad.png"];
        [btn_skins setScale:0.5+0.5*IS_RETINA];
        btn_skins.position = ccp(winSize.width-btn_skins.boundingBox.size.width/2-10-10*IS_IPAD, winSize.height-btn_skins.boundingBox.size.height);
        [btn_skins setScale:0.5+0.5*IS_RETINA];
        [self addChild:btn_skins z:1];
        
        if (!IS_IPAD)
            btn_eat = [CCSprite spriteWithFile:@"btn_eat.png"];
        else
            btn_eat = [CCSprite spriteWithFile:@"btn_eat-iPad.png"];
         [btn_eat setScale:0.5+0.5*IS_RETINA];
        btn_eat.position = ccp(btn_eat.boundingBox.size.width/2+10+10*IS_IPAD , btn_eat.boundingBox.size.height/2+10+30*IS_IPAD);
        
        [self addChild:btn_eat z:1];
        
        if (!IS_IPAD)
            btn_shop = [CCSprite spriteWithFile:@"btn_bodyShop.png"];
        else
            btn_shop = [CCSprite spriteWithFile:@"btn_bodyShop-iPad.png"];
        [btn_shop setScale:0.5+0.5*IS_RETINA];
        
        btn_shop.position = ccp(winSize.width-btn_shop.boundingBox.size.width/2, btn_shop.boundingBox.size.height/2+10+30*IS_IPAD);
        [self addChild:btn_shop z:1];
        [personaj person].position = ccp(winSize.width/2, 150+50*[stateTamagochi sizeTamagochi]+30*IS_WIDESCREEN+260*IS_IPAD);
        [personaj initRects];
        [personaj person].delegate=self;
        [self addChild:[personaj person] z:1];
        
        CCSprite *shadow=[CCSprite spriteWithFile:[NSString stringWithFormat:@"shadow_mall%@.png", (IS_IPAD)?@"-iPad":@""]];
        [shadow setScale:[stateTamagochi sizeTamagochi]];
        [shadow setPosition:ccp(winSize.width/2, [personaj person].position.y-80*[stateTamagochi sizeTamagochi])];
        [self addChild:shadow z:0];
        
        colorLayer= [[CCLayerColor layerWithColor:ccc4(0, 0, 0, 160)] retain];
        [stateTamagochi setTouchObject:tagDefault];
        [self scheduleUpdate];
    }
    return self;
}

- (void)update:(float)dt
{

}

-(void)createBackground:(NSNotification *)notif
{
    switch ([notif.object intValue]) {
        case 0:
        {
          
            [[personaj person] setZOrder:1];
            
            if (![self getChildByTag:300])
                [self addChild:colorLayer z:2 tag:300];
            else
                [[personaj person] setZOrder:3];
        }
            break;
        case 1:
        {
             if ([self getChildByTag:300])
            [colorLayer removeFromParentAndCleanup:NO];
        }
            break;
        case 2:
        {
            [[personaj person] setZOrder:3];
            [personaj initRects];
        }
            break;
            
        default:
            break;
    }
}

-(CGPoint)returnPosition
{
    return CGPointMake(xx, yy);
}

-(float)returnTouchValue
{
    switch ([stateTamagochi mouthState]) {
        case mouthStateFun:
        {
            return tamTouch*sin(totalTime);
        }
            break;
        case mouthStateReadyEat:
        {
            return 0.3;
        }
            break;
        case mouthStateEat:
        {
            return 0.25*sin(totalTime);
        }
            break;
        case mouthStateDefault:
        {
            return 0.25*sin(totalTime);
        }
            break;
            
        default:return tamTouch;
            break;
    }
}


-(void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    nr_sounds=0;
    UITouch* touch = [touches anyObject];
    CGPoint location = [touch locationInView: [touch view]];
    startTouchPoint=location;
    date_start=[[NSDate date] retain];
    location = [[CCDirector sharedDirector] convertToGL:location];
    xx=location.x;
    yy=location.y;
    touchFun=0;
    if (CGRectContainsPoint( [leftBottom   boundingBox], location) ) {
         [stateTamagochi setTouchObject:tagDefault];
        [leftBottom runAction:[CCMoveTo actionWithDuration:0.1 position:location]];
    }
    if (CGRectContainsPoint([personaj rectPerson],location))
    {
        [stateTamagochi setTouchObject:tagTamagochi];
    }
    
    if (CGRectContainsPoint([btn_potions boundingBox], location))
    {
        shopTapped=shopPotions;
    }
    if (CGRectContainsPoint([btn_shop boundingBox], location))
    {
         shopTapped=shopBody;
    }
    if (CGRectContainsPoint([btn_eat boundingBox], location))
    {
         shopTapped=shopEat;
    }
    if (CGRectContainsPoint([btn_skins boundingBox], location))
    {
         shopTapped=shopSkins;
    }
}

-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch* touch = [touches anyObject];
    CGPoint location = [touch locationInView: [touch view]];
    location = [[CCDirector sharedDirector] convertToGL:location];

    if ([stateTamagochi touchObject]==tagTamagochi && CGRectContainsPoint([personaj rectPerson],location))
    {
        [stateTamagochi setMouthState:mouthStateFun];
        touchFun+=0.006;
        if (touchFun>=0.05)
        {
            if (!repeatTouches)
            [self fun];
        }
        nr_sounds++;
    }
  
    xx=location.x;
    yy=location.y;
}
-(void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [stateTamagochi setMouthState:mouth];
    UITouch* touch = [touches anyObject];
    CGPoint location = [touch locationInView: [touch view]];
    endTouchPoint=location;
    float moveDifference=startTouchPoint.x-endTouchPoint.x;
    float differenceY=startTouchPoint.y-endTouchPoint.y;
    if (differenceY<0) differenceY*=-1;
    NSTimeInterval secondsBetween = [[NSDate date] timeIntervalSinceDate:date_start];
    [date_start release];
    date_start=nil;
    if (moveDifference>150.0 && differenceY<40.0 && endTouchPoint.y<150.0 && secondsBetween<0.3)
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"nextRoom" object:@"1"];
        return;
    }
    else if (moveDifference<-150.0 && differenceY<40.0 && endTouchPoint.y<150.0 && secondsBetween<0.3)
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"nextRoom" object:@"2"];
        return;
    }
    
    location = [[CCDirector sharedDirector] convertToGL:location];
    if (CGRectContainsPoint([btn_potions boundingBox], location) && shopTapped==shopPotions && ![stateTamagochi nameRoomTap])
    {
        [self createBackground:nil];
        [stateTamagochi setShopCategory:sh_potions];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"shopView" object:nil];
    }
    if (CGRectContainsPoint([btn_shop boundingBox], location) && shopTapped==shopBody  && ![stateTamagochi nameRoomTap])
    {
        [self createBackground:nil];
        [stateTamagochi setShopCategory:sh_body];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"shopView" object:nil];
    }
    if (CGRectContainsPoint([btn_eat boundingBox], location) && shopTapped==shopEat  && ![stateTamagochi nameRoomTap])
    {
        [self createBackground:nil];
        [stateTamagochi setShopCategory:sh_eat];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"shopView" object:nil];
    }
    if (CGRectContainsPoint([btn_skins boundingBox], location) && shopTapped==shopSkins  && ![stateTamagochi nameRoomTap])
    {
        [self createBackground:nil];
        [stateTamagochi setShopCategory:sh_skins];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"shopView" object:nil];
    }
    
    nr_sounds=0;
    touchFun=0;
    xx=0;
    yy=0;
    touchObject=tagDefault;
}

-(void)changePosition:(CGPoint)pos
{
    xx=pos.x;
    yy=pos.y;
}

#pragma Tamagochi effects

-(void)resetVariables
{
    repeatTouches=NO;
    touchFun=0;
    if ([[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying])
        [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
}

-(void)fun
{
    repeatTouches=YES;
    totalTime=0;
    tamTouch=0.3;
    if (![[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying] &&  [stateTamagochi soundOn])
        [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"giggle.wav" loop:NO];
    [self performSelector:@selector(resetVariables) withObject:nil afterDelay:1.57];
}
-(void)dealloc
{
    [[SimpleAudioEngine sharedEngine] stopEffect:sound1];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"showBackgroundAlpha" object:nil];
    [super dealloc];
}

@end
