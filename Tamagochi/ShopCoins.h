//
//  ShopCoins.h
//  Tamagochi
//
//  Created by Sergiu Moțipan on 11/28/13.
//
//

#import <UIKit/UIKit.h>
#import "MKStoreManager.h"

@interface ShopCoins : UIView <UITableViewDataSource, UITableViewDelegate,MKStoreKitDelegate>
{
    IBOutlet UIButton *btnBack;
    IBOutlet UITableView *tableCoins;
    int loadingIndex;
    BOOL loadingA;
    BOOL loadingB;
    BOOL loadingC;
    BOOL loadingD;
}
@property (retain, nonatomic) IBOutlet UIButton *btnBack;
@property (retain, nonatomic) IBOutlet UIView *viewBackground;
@property (assign) int loadingIndex;

- (IBAction)shopCoins:(id)sender forEvent:(UIEvent *)event;
-(void)refresh;
@end
