#ifndef HUESHIFT_HSVTRANSFORM_H
#define HUESHIFT_HSVTRANSFORM_H

#ifdef __cplusplus
extern "C" {
#endif
    
    typedef struct
    {
        float r;
        float g;
        float b;
    } RGBFloatColor;
    
    typedef struct
    {
        RGBFloatColor r;
        RGBFloatColor g;
        RGBFloatColor b;
    } HSVTransform;
    
    HSVTransform HSVTransformCreate(double hueShift, double saturationMult, double valueMult);
    
    RGBFloatColor HSVTransformColor(HSVTransform* transform, RGBFloatColor* color);
    
    unsigned int HSVTransformRGBA8(HSVTransform* transform, unsigned int rgba8);
    
#ifdef __cplusplus
}
#endif

#endif /* HUESHIFT_HSVTRANSFORM_H */