//
//  CCTexture2D+HSVTransform.h
//  Tamagochi
//
//  Created by Sergiu Moțipan on 11/8/13.
//
//

#import "CCTexture2D.h"
#import "HSVTransform.h"

@interface CCTexture2D (HSVTransform)

- (id) initWithImage:(UIImage *)uiImage transform:(HSVTransform*) transform;
@end
