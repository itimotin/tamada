//
//  PouNode.h
//  Tamagochi
//
//  Created by Sergiu Moțipan on 6/13/13.
//
//

#import "CCNode.h"
#import "cocos2d.h"
#import "RotNode.h"
#import "SimpleAudioEngine.h"

@class PouNode;

@protocol PouNodeDelegate
@required
-(CGPoint)returnPosition;
-(int)returnState;
-(float)returnAlpha;
@end
@interface PouNode : CCNode 
{
    id<PouNodeDelegate, NSObject> delegate;
    CGSize winSize;
    int curentPage;
    int touchObject;
    int timp;
    int timp_2;
    int eye_centerX;
    int eye_centerY;
    float eye_posX, eye_posY, longX, longY, glassesPosX,glassesPosY, coafuraPosX,coafuraPosY, mustetePosX,mustetePosY,pleopUpX,pleopUpY,pleopDownX, pleopDownY,pleopUpTime,pleopDownTime, typePleop, rotX, rotY, rotTime, rotTouch, rotType, cashtiPosX,cashtiPosY, termPosX, termPosY, genePosX, genePosY, hainePosX, hainePosY, palariiPosX, palariiPosY, urechiPosX, urechiPosY;
    float pleopDownHeight, pleopDownOriginY, haineHeight, guraPosX, guraPosY;
    float totalTime,yy,xx;
   // float positionEyeX, positionEyeeeY;
    CCSpriteBatchNode *spritesAll;
    float slowEffect;
    CCSprite *dirt;
    CCSprite *umed;
    
    int assetLoadCount;
	BOOL loadingAsset;
    float x, y;
    BOOL jos;
    CGPoint foundPos;
    BOOL found;
    CGMutablePathRef path;
    CGPoint tatooPos[12];
    CCParticleExplosion *emmiter;
}
@property (nonatomic, assign) id<PouNodeDelegate, NSObject> delegate;
@property (nonatomic, assign) CGSize tamSize;

@property (nonatomic, assign) CCSprite *tamagochi;
@property (nonatomic, assign) CCSprite *leftBottom;
@property (nonatomic, assign) CCSprite *centerBottom;
@property (nonatomic, assign) CCSprite *rightBottom;
@property (nonatomic, assign) CCSprite *eye_center;
@property (nonatomic, assign) CCSprite *eye;
@property (nonatomic, assign) CCSprite *glasses;
@property (nonatomic, assign) CCSprite *coafura;
@property (nonatomic, assign) CCSprite *mustete;
@property (nonatomic, assign) CCSprite *pleopUp;
@property (nonatomic, assign) CCSprite *pleopDown;
@property (nonatomic, assign) CCSprite *rot;
@property (nonatomic, retain) CCSprite *termometer;
@property (nonatomic, assign) CCSprite *cashti;
@property (nonatomic, assign) CCSprite *gene;
@property (nonatomic, assign) CCSprite *haine;
@property (nonatomic, assign) CCSprite *palarii;
@property (nonatomic, assign) CCSprite *tatoo;
@property (nonatomic, assign) CCSprite *urechi;
@property (nonatomic, assign) RotNode *gura;


-(void)createChildByTag:(NSInteger)tag nameTexture:(NSString *)name;
-(void)createTatooChild:(NSInteger)tag nameTexture:(NSString *)name position:(CGPoint)point;
-(void)setPositionByIndex:(NSInteger)index;
+(id)sharedInstance;
-(void)addMoney:(NSInteger)summ;
-(void)sufferingEye;
-(PouNode*)obj;
-(void)setPositionTatoo:(int)index position:(CGPoint)point;
-(CGPoint)getPositionTatoo:(int)index;
-(void)addExplosionR:(GLfloat)r g:(GLfloat)g b:(GLfloat)b a:(GLfloat)a;
@end
