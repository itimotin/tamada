#ifdef GL_ES
precision mediump float;
#endif

varying vec2 v_texCoord;
varying  vec4 v_colorVarying;
uniform sampler2D u_texture0;

uniform float u_time;
uniform float u_type;
uniform vec2 u_viewport;

varying float v_xpos;
varying float v_ypos;
varying float v_zpos;

#define epsilon 0.0001
// 1
const float speed = 5.0;
const float bendFactor = 0.58;
void main()
{
    
    float ratio=0.5;
    float height;
    if (u_type!=2.0)
        height = -0.35 + v_texCoord.y/(2.0-sin(u_time));
    else
        height = 0.5 - v_texCoord.y/(0.705+sin(u_time));
    // 3
    float offset = pow(height, 2.5);
    
    // 4 multiply by sin since it gives us nice bending
    offset *= (sin(1.9  * speed) * bendFactor);
    
    // Get the height value and calculate the new texture coord.

    
    
    vec4 normalColor = texture2D(u_texture0, fract(vec2(v_texCoord.x, v_texCoord.y+offset))).rgba;
    gl_FragColor = normalColor;
    
     // lowp vec4 textureColor = texture2D(u_texture0, fract(vec2(v_texCoord.x+offset , v_texCoord.y)));
   // lowp float gray = dot(textureColor, vec4(2.5, 1.0, 1.0, 0.0));
  //  gl_FragColor = vec4(gray * ratio + textureColor.r * (1.0 - ratio),  textureColor.g , textureColor.b, textureColor.a);
   // gl_FragColor=vec4(1.0,1.0,0,1);
}