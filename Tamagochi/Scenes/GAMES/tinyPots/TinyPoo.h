//
//  TinyPoo.h
//  Tamagochi
//
//  Created by iVanea! on 9/26/13.
//
//

#import "CCLayer.h"
#import "cocos2d.h"
#import "Constants.h"
#import "Box2D.h"
#import "Main.h"
#import "MainViewController.h"
#import "AppDelegate.h"

enum{
    enemyTag1 = 1000,
    enemyTag2,
    enemyTag3,
    enemyBossTag
};

@interface TinyPoo : CCLayer{
    b2World * _world;
    BOOL isRunning;
    int_fast8_t stage;
    int_fast8_t positionWin;
    MainViewController *myView;
}
@property (nonatomic, readonly)b2World *world;
@property (nonatomic, readwrite)int_least8_t currentStage;

+(CCScene *) sceneWithStage:(int_least8_t)stage;

- (void) showHit;
- (void) showFrenzy;
- (void) showPerfectSlide;
@end
