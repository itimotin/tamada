//
//  FirstScene.m
//  Tamagochi
//
//  Created by Sergiu Moțipan on 6/4/13.
//
//

#import "StudioRoom.h"

#import "fmod.hpp"
#import "fmodiphone.h"
#import "fmod_errors.h"


static const float notefreq[120] =
{
    16.35f,   17.32f,   18.35f,   19.45f,    20.60f,    21.83f,    23.12f,    24.50f,    25.96f,    27.50f,    29.14f,    30.87f,
    32.70f,   34.65f,   36.71f,   38.89f,    41.20f,    43.65f,    46.25f,    49.00f,    51.91f,    55.00f,    58.27f,    61.74f,
    65.41f,   69.30f,   73.42f,   77.78f,    82.41f,    87.31f,    92.50f,    98.00f,   103.83f,   110.00f,   116.54f,   123.47f,
    130.81f,  138.59f,  146.83f,  155.56f,   164.81f,   174.61f,   185.00f,   196.00f,   207.65f,   220.00f,   233.08f,   246.94f,
    261.63f,  277.18f,  293.66f,  311.13f,   329.63f,   349.23f,   369.99f,   392.00f,   415.30f,   440.00f,   466.16f,   493.88f,
    523.25f,  554.37f,  587.33f,  622.25f,   659.26f,   698.46f,   739.99f,   783.99f,   830.61f,   880.00f,   932.33f,   987.77f,
    1046.50f, 1108.73f, 1174.66f, 1244.51f,  1318.51f,  1396.91f,  1479.98f,  1567.98f,  1661.22f,  1760.00f,  1864.66f,  1975.53f,
    2093.00f, 2217.46f, 2349.32f, 2489.02f,  2637.02f,  2793.83f,  2959.96f,  3135.96f,  3322.44f,  3520.00f,  3729.31f,  3951.07f,
    4186.01f, 4434.92f, 4698.64f, 4978.03f,  5274.04f,  5587.65f,  5919.91f,  6271.92f,  6644.87f,  7040.00f,  7458.62f,  7902.13f,
    8372.01f, 8869.84f, 9397.27f, 9956.06f, 10548.08f, 11175.30f, 11839.82f, 12543.85f, 13289.75f, 14080.00f, 14917.24f, 15804.26f
};

#define OUTPUTRATE      44100
#define SPECTRUMSIZE    2048
#define SPECTRUMRANGE   ((float)OUTPUTRATE / 8.0f)
#define BINSIZE         (SPECTRUMRANGE / (float)SPECTRUMSIZE)
#define __PACKED __attribute__((packed))   


@implementation StudioRoom
{
    int touchObject;
    int timp;
    int timp_2;
    int mouse;
    int oche_mouse;
    int n_mouse;
    int eye_pos;
    float totalTime,yy,xx, tamTouch;
    BOOL repeatTouches;
    float limit;
    NSTimer *timer;
    
    
    FMOD::System   *system;
    FMOD::Sound    *sound;
    FMOD::Sound    *sound2;
    FMOD::Channel  *channel;
    FMOD::Channel  *channel2;
    FMOD::DSP      *dsppitchshift;
    FMOD::DSP      *dsplowpass;
    
    bool            loopFlag;
    BOOL start;
    unsigned int    bytesPerSample;
    unsigned int    bytesPerSecond;
    unsigned int    dataLength;
    FILE           *outFile;
    float sec;
    float gura;
}



+(CCScene *) scene
{
	CCScene *scene = [CCScene node];
	StudioRoom *layer = [StudioRoom node];
	[scene addChild: layer];
	return scene;
}


void ERRCHECK(FMOD_RESULT result)
{
    if (result != FMOD_OK)
    {
        fprintf(stderr, "FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
        exit(-1);
    }
}

 
-(void)record
{
    [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
   
    char        buffer[200] = {0};

    [self stop];
    [[NSString stringWithFormat:@"%@/record.wav", [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]] getCString:buffer maxLength:200 encoding:NSASCIIStringEncoding];
    outFile = fopen(buffer, "wb");
    if (!outFile)
    {
        fprintf(stderr, "Error! Could not open record.wavw output file.\n");
        return;
    }
    writeWavHeader(outFile, sound, dataLength);
}

-(void)play
{
    [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
    char          buffer[200]   = {0};
    xx=0;
    yy=0;
    [self stop];
    [self toggleLowpass:NO];
     FMOD_RESULT result = FMOD_OK;
    
    NSString *documentsFolder = [self getPath];

   [[documentsFolder stringByAppendingPathComponent:@"/record.wav"] getCString:buffer maxLength:200 encoding:NSUTF8StringEncoding];

  //  result = system->createStream(buffer, FMOD_SOFTWARE, NULL, &sound2);
    
    result = system->createSound(buffer, FMOD_SOFTWARE, NULL, &sound2);

    result = system->playSound(FMOD_CHANNEL_FREE, sound2, false, &channel);

    ERRCHECK(result);
 
}
- (NSString*)getPath
{
    NSArray *arrayPaths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docDirectory = [arrayPaths objectAtIndex:0];
    return docDirectory;
}
-(void)stop
{
    if (outFile != NULL)
    {
    
        writeWavHeader(outFile, sound, dataLength);
        fclose(outFile);
        outFile = NULL;
        dataLength = 0;
        
        if (channel)
        {
            channel->stop();
            channel = NULL;
        }
        [self toggleLowpass:YES];
    }
}


- (void)toggleLowpass:(BOOL)stop
{
    FMOD_RESULT result  = FMOD_OK;
    
    if (!stop)
    {
        result = system->addDSP(dsplowpass, NULL);
        ERRCHECK(result);
        result = system->addDSP(dsppitchshift, NULL);
        ERRCHECK(result);
    }
    else
    {
        bool        active  = false;
        result = dsplowpass->getActive(&active);
        ERRCHECK(result);
        if (active)
        {
            result = dsplowpass->remove();
            ERRCHECK(result);
        }
        
        result = dsppitchshift->getActive(&active);
        ERRCHECK(result);
        if (active)
        {
            result = dsppitchshift->remove();
            ERRCHECK(result);
        }

    }  
}

void writeWavHeader(FILE *file, FMOD::Sound *sound, int length)
{
    int               channels;
    int               bits;
    float             rate;
    FMOD_SOUND_FORMAT format;
    
    if (!sound)
    {
        return;
    }
    
    fseek(file, 0, SEEK_SET);
    
    sound->getFormat(NULL, &format, &channels, &bits);
    sound->getDefaults(&rate, NULL, NULL, NULL);
    
    {
 
        typedef struct
        {
	        signed char id[4];
	        int 		size;
        } RiffChunk;
        
        struct
        {
            RiffChunk       chunk           __PACKED;
            unsigned short	wFormatTag      __PACKED;
            unsigned short	nChannels       __PACKED;
            unsigned int	nSamplesPerSec  __PACKED;
            unsigned int	nAvgBytesPerSec __PACKED;
            unsigned short	nBlockAlign     __PACKED;
            unsigned short	wBitsPerSample  __PACKED;
        } __PACKED FmtChunk  = { {{'f','m','t',' '}, sizeof(FmtChunk) - sizeof(RiffChunk) }, 1, channels, (int)rate, (int)rate * channels * bits / 8, 1 * channels * bits / 8, bits };
        
        if (format == FMOD_SOUND_FORMAT_PCMFLOAT)
        {
            FmtChunk.wFormatTag = 3;
        }
        
        struct
        {
            RiffChunk   chunk;
        } DataChunk = { {{'d','a','t','a'}, length } };
        
        struct
        {
            RiffChunk   chunk;
	        signed char rifftype[4];
        } WavHeader = { {{'R','I','F','F'}, sizeof(FmtChunk) + sizeof(RiffChunk) + length }, {'W','A','V','E'} };
        
 
        fwrite(&WavHeader, sizeof(WavHeader), 1, file);
        fwrite(&FmtChunk, sizeof(FmtChunk), 1, file);
        fwrite(&DataChunk, sizeof(DataChunk), 1, file);
    }
}




-(void)initMusic
{
    UInt32 audioRouteOverride = kAudioSessionOverrideAudioRoute_Speaker;
    
    
    AudioSessionSetProperty (
                             kAudioSessionProperty_OverrideAudioRoute,
                             sizeof (audioRouteOverride),
                             &audioRouteOverride
                             );

    system      = NULL;
    sound       = NULL;
    sound2       = NULL;
    channel     = NULL;
    channel2    = NULL;
    loopFlag    = false;
    start=NO;
    
    FMOD_RESULT             result      = FMOD_OK;
    unsigned int            version     = 0;
    FMOD_CREATESOUNDEXINFO  exinfo      = {0};
    
    result = FMOD::System_Create(&system);
    ERRCHECK(result);
    
    result = system->getVersion(&version);
    ERRCHECK(result);
    
    if (version < FMOD_VERSION)
    {
        fprintf(stderr, "You are using an old version of FMOD %08x.  This program requires %08x\n", version, FMOD_VERSION);
        exit(-1);
    }
    
    result = system->init(32, FMOD_INIT_NORMAL | FMOD_INIT_ENABLE_PROFILE,NULL);
    ERRCHECK(result);
    
    
  //  memset(&exinfo, 0, sizeof(FMOD_CREATESOUNDEXINFO));
    
    exinfo.cbsize           = sizeof(FMOD_CREATESOUNDEXINFO);
    exinfo.numchannels      = 2;
    exinfo.format           = FMOD_SOUND_FORMAT_PCM16;
    exinfo.defaultfrequency = OUTPUTRATE;
    exinfo.length           = exinfo.defaultfrequency * sizeof(short) * exinfo.numchannels * 1;
    
    bytesPerSample = sizeof(short) * exinfo.numchannels;
    bytesPerSecond = exinfo.defaultfrequency * bytesPerSample;
    
    result = system->createSound(NULL, FMOD_3D | FMOD_SOFTWARE | FMOD_LOOP_NORMAL | FMOD_OPENUSER, &exinfo, &sound);
    ERRCHECK(result);
    
  
    
    
    
    ERRCHECK(result);
    result = system->createDSPByType(FMOD_DSP_TYPE_LOWPASS, &dsplowpass);
    ERRCHECK(result);
    result = system->createDSPByType(FMOD_DSP_TYPE_PITCHSHIFT, &dsppitchshift);
    ERRCHECK(result);
    
    result = dsppitchshift->setParameter(FMOD_DSP_PITCHSHIFT_PITCH, 1.9f);
    ERRCHECK(result);
    result = dsppitchshift->setParameter(FMOD_DSP_PITCHSHIFT_FFTSIZE, 256.0f);
    ERRCHECK(result);
    result = dsplowpass->setParameter(FMOD_DSP_LOWPASS_RESONANCE, 4);
    ERRCHECK(result);
    result = dsplowpass->setParameter(FMOD_DSP_LOWPASS_CUTOFF, 22000);
    ERRCHECK(result);
    
    result = system->recordStart(0, sound, true);
     ERRCHECK(result);
    usleep(200 * 1000);
    
    result = system->playSound(FMOD_CHANNEL_FREE, sound, false, &channel2);
    ERRCHECK(result);
   
    result = channel2->setVolume(0  );
    ERRCHECK(result);
}

-(void)alertShow:(NSString *)title message:(NSString *)message
{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    [alert release];
}
 
-(void)microfonOnOFF
{
    if (start)
    {
        [stateTamagochi setMouthState:mouth];
        start=NO;
        [stateTamagochi setStartRecord:NO];
        [ofOn setString:@"OFF"];
         [microfon stopAllActions];
        [microfon setScale:1.0];
        [microfon runAction:[self initAnimations]];
        [microfon setOpacity:150];
    }
    else
    {
        if ([stateTamagochi energy]>20.0 && [stateTamagochi health]>20.0)
        {
            start=YES;
            [stateTamagochi setStartRecord:YES];
            [ofOn setString:@"MICRO"];
        [microfon stopAllActions];
        [microfon setOpacity:255];
        }
        else
        {
               [self alertShow:NSLocalizedString(@"attention", nil) message:NSLocalizedString(@"pou_sick_energy", nil)];
        }
    }
}

-(ccColor3B)tranformColor:(NSInteger)tag object:(NSInteger)object
{
    NSMutableArray *arr=[[[NSMutableArray alloc] initWithArray:[stateTamagochi colorSkin:tag object:object]] autorelease];
    ccColor3B color={[[arr objectAtIndex:0] floatValue], [[arr objectAtIndex:1] floatValue], [[arr objectAtIndex:2] floatValue]};
    return color;
    
}


-(CCAction*)initAnimations
{
    id action1;
    id action2;
    if (!IS_RETINA)
    {
        action1 = [CCScaleTo actionWithDuration:0.4 scale:0.55];
        action2 = [CCScaleTo actionWithDuration:0.4 scale:0.5];
    }
    else
    {
        action1 = [CCScaleBy actionWithDuration:0.4 scale:1.1];
        action2 = [CCScaleTo actionWithDuration:0.4 scale:1.0];
    }
    id  animationObjects = [CCSequence actions:action1, action2, nil];
    return [CCRepeatForever actionWithAction:animationObjects];
}
-(id)init
{
    self=[super init];
    if (self)
    {
        [[SimpleAudioEngine sharedEngine] stopAllEffects];
        [[SimpleAudioEngine sharedEngine] setEffectsVolume:[stateTamagochi soundOn]];
        [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume:[stateTamagochi soundOn]];
        [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
        
        disp=0.0;
        ener=0.0;
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(changeColor:) name:@"skinColor" object:nil];
        [[CDAudioManager sharedManager] init:kAMM_PlayAndRecord];
      //  [fmodObj timerKill:NO];
        [self initMusic];
        [stateTamagochi setRoom:studioRoom];
        mouth=[stateTamagochi mouthState];
        [[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"giggle.wav"];
        [[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"mtem.wav"];
        [[SimpleAudioEngine sharedEngine] setEffectsVolume:[stateTamagochi soundOn]];
        [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume:[stateTamagochi soundOn]];

        winSize = [[CCDirector sharedDirector] winSize];
        self.isTouchEnabled=YES;
        
        microfon = [CCSprite spriteWithFile:[NSString stringWithFormat:@"microfon_3%@.png",(IS_IPAD)?@"-iPad":@""]];
        microfon.tag=8;
        [microfon setScale:0.5+0.5*IS_RETINA];
		microfon.position = CGPointMake(winSize.width/2, 50+50*IS_IPAD);
		[self addChild:microfon z:2];
        
        if ([stateTamagochi shopCategory]!=sh_skins)
        {
            [microfon runAction:[self initAnimations]];
            [microfon setOpacity:150];
        }
        
        ofOn = [CCLabelTTF labelWithString:@"OFF" fontName:@"Arial" fontSize:20];
        ofOn.position = CGPointMake(winSize.width/2,10);
        [self addChild:ofOn z:3];
        start = NO;
        
        CCSprite *foot = [CCSprite spriteWithFile:[NSString stringWithFormat:@"foot_3%@.png",(IS_IPAD)?@"-iPad":@""]];
        foot.tag=2;
        [foot setColor:[self tranformColor:[stateTamagochi selectColorSkin:2  room:3] object:0]];
        foot.position = ccp(winSize.width/2, foot.boundingBox.size.height/2);
        [self addChild:foot z:-13];
        
        CCSprite *background = [CCSprite spriteWithFile:[NSString stringWithFormat:@"draperie_3%@.png",(IS_IPAD)?@"-iPad":@""]];
        background.tag=3;
        [background setColor:[self tranformColor:[stateTamagochi selectColorSkin:3  room:3] object:0]];
        background.position = ccp(winSize.width/2, winSize.height/2+foot.boundingBox.size.height/1.5);
        [self addChild:background z:-12];
        
       
        
        CCSprite *equalazier = [CCSprite spriteWithFile:[NSString stringWithFormat:@"sound_panel%@.png",(IS_IPAD)?@"-iPad":@""]];
        equalazier.position = ccp(equalazier.boundingBox.size.width/2, equalazier.boundingBox.size.height/2);
        [self addChild:equalazier z:-11];
        
        CCSprite *dinamik = [CCSprite spriteWithFile:[NSString stringWithFormat:@"dinamic%@.png",(IS_IPAD)?@"-iPad":@""]];
        dinamik.position = ccp(WIDTH_DEVICE-dinamik.boundingBox.size.width/2, dinamik.boundingBox.size.height/2);
        [self addChild:dinamik z:-11];
 
    
        [personaj person].position = ccp(winSize.width/2, 120+60*[stateTamagochi sizeTamagochi]+170*IS_IPAD);
        [personaj initRects];
        [personaj person].delegate=self;
        [[[personaj person] getChildByTag:10] setDelegate:self];
        [self addChild:[personaj person] z:1];
        [[personaj person] createChildByTag:18 nameTexture:nil];
        
        [stateTamagochi setTouchObject:tagDefault];
   
        sec=0;
        [self scheduleUpdate];
    }
    
    return self;
}

-(void)changeColor:(NSNotification *)notif
{
    [(CCSprite *)[self getChildByTag:[[[notif.object componentsSeparatedByString:@","] objectAtIndex:1] intValue]] setColor:[self tranformColor:[[[notif.object componentsSeparatedByString:@","] objectAtIndex:0] intValue] object:[[[notif.object componentsSeparatedByString:@","] objectAtIndex:1] intValue]]];
}

- (void)update:(float)dt
{
    
    FMOD_RESULT     result                  = FMOD_OK;
    static float    spectrum[SPECTRUMSIZE]  = {0};
    float           dominanthz              = 0.0f;
    float           max                     = 0.0f;
    int             dominantnote            = 0;
    int             bin                     = 0;
    
    static float    spectrum_p[SPECTRUMSIZE]  = {0};
    float           dominanthz_p              = 0.0f;
    float           max_p                     = 0.0f;
    int             dominantnote_p            = 0;
    int             bin_p                     = 0;
    
    
    bool            recording       = false;
    bool            playing         = false;
    unsigned int    currentrecpos   = 0;
    static unsigned int lastRecordPos   = 0;
    unsigned int        soundLength     = 0;
    
    
    
    result = system->isRecording(0, &recording);
    ERRCHECK(result);
    
    result = system->getRecordPosition(0, &currentrecpos);
    ERRCHECK(result);
    
    if (outFile != NULL)
    {
        result = sound->getLength(&soundLength, FMOD_TIMEUNIT_PCM);
        ERRCHECK(result);
        
        result = system->getRecordPosition(0, &currentrecpos);
        ERRCHECK(result);
        
        if (currentrecpos != lastRecordPos)
        {
            void           *ptr1        = NULL;
            void           *ptr2        = NULL;
            int             blockLength = 0;
            unsigned int    len1        = 0;
            unsigned int    len2        = 0;
            
            blockLength = (int)currentrecpos - (int)lastRecordPos;
            if (blockLength < 0)
            {
                blockLength += soundLength;
            }
            
     
            result = sound->lock(lastRecordPos * bytesPerSample, blockLength * bytesPerSample, &ptr1, &ptr2, &len1, &len2);
            ERRCHECK(result);
            
     
            if (ptr1 && len1)
            {
                dataLength += fwrite(ptr1, 1, len1, outFile);
            }
            if (ptr2 && len2)
            {
                dataLength += fwrite(ptr2, 1, len2, outFile);
            }
            
     
            result = sound->unlock(ptr1, ptr2, len1, len2);
            ERRCHECK(result);
        }
        
        lastRecordPos = currentrecpos;
    }
    
    
    result = channel2->getSpectrum(spectrum, SPECTRUMSIZE, 0, FMOD_DSP_FFT_WINDOW_TRIANGLE);
    ERRCHECK(result);
    
    for (int i = 0; i < SPECTRUMSIZE; i++)
    {
        if (spectrum[i] > 0.01f && spectrum[i] > max)
        {
            max = spectrum[i];
            bin = i;
        }
    }
    
    dominanthz = (float)bin * BINSIZE;
    
    for (int i = 0; i < 120; i++)
    {
        if (dominanthz >= notefreq[i] && dominanthz < notefreq[i + 1])
        {
     
            if (fabs(dominanthz - notefreq[i]) < fabs(dominanthz - notefreq[i + 1]))
            {
                dominantnote = i;
            }
            else
            {
                dominantnote = i + 1;
            }
            
            break;
        }
    }
    
    result = channel->isPlaying(&playing);
    
    if (playing)
    {
        result = channel->getSpectrum(spectrum_p, SPECTRUMSIZE, 0, FMOD_DSP_FFT_WINDOW_TRIANGLE);
        ERRCHECK(result);
        
        for (int i = 0; i < SPECTRUMSIZE; i++)
        {
            if (spectrum_p[i] > 0.01f && spectrum_p[i] > max)
            {
                max_p = spectrum_p[i];
                bin_p = i;
            }
        }
        
        dominanthz_p = (float)bin_p * BINSIZE;
        
        for (int i = 0; i < 120; i++)
        {
            if (dominanthz_p >= notefreq[i] && dominanthz_p < notefreq[i + 1])
            {
               
                if (fabs(dominanthz_p - notefreq[i]) < fabs(dominanthz_p - notefreq[i + 1]))
                {
                    dominantnote_p = i;
                }
                else
                {
                    dominantnote_p = i + 1;
                }
                
                break;
            }
        }

    }
        result = system->update();
        ERRCHECK(result);
    if (playing)
    {
        
        [stateTamagochi setMouthState:mouthSpeak];
        gura=dominanthz_p/40;
        disp+=dominanthz_p/40000;
        ener+=dominanthz_p/100000;
        gura=(int)gura;
        gura=gura/10;
       
    }
    else if ([stateTamagochi mouthState]==mouthSpeak)
    {
        if (disp>0.0)
        {
            if ([stateTamagochi funny]<100.0)
            {
                [stateTamagochi setFunny:[stateTamagochi funny]+disp];
                [stateTamagochi setNextLevelPoints:[stateTamagochi nextLevelPoints] +disp*2.5*(1.3-[stateTamagochi sizeTamagochi])];
                if ([stateTamagochi nextLevelPoints]>180.0)
                {
                    [stateTamagochi setNextLevelPoints:180.0];
                }
                [stateTamagochi addToNextLevel:4];
            }
            
            [stateTamagochi setEnergy:[stateTamagochi energy]-ener];
            
            if ([stateTamagochi energy]<0.0)
                  [stateTamagochi setEnergy:0.0];
            if ([stateTamagochi funny]>100.0)
                [stateTamagochi setFunny:100.0];
            
            [stateTamagochi updateFun];
            [stateTamagochi updateEnergy];

            disp=0.0;
            ener=0.0;
        }
        [stateTamagochi setMouthState:mouth];
    }

    if (start)
    {
     
    
        if (dominanthz>100.0  && !playing)
        {
            id action1;
            id action2;
            if (!IS_RETINA)
            {
                action1 = [CCScaleTo actionWithDuration:0.3 scale:0.5+dominanthz/1200];
                action2  = [CCScaleTo actionWithDuration:0.3 scale:0.5];
            }
            else
            {
                action1 = [CCScaleBy actionWithDuration:0.3 scale:1.0+dominanthz/1000];
                action2  = [CCScaleTo actionWithDuration:0.3 scale:1.0];
            }

            [microfon runAction:[CCSequence actions:action2, action1, action2, nil]];
            
            xx=microfon.position.x;
            yy=microfon.position.y;
            if (outFile == NULL )
            {
                [self record];
            }
            sec=0.0;
        }
        else
        {
            
            sec+=0.1;
            if (!playing && sec>6.0 && sec<7.0 && outFile != NULL)
            {
                [self play];
                sec=8.0;
                xx=0;
                yy=0;
            }
        }
    }
}


-(CGPoint)returnPosition
{
    return CGPointMake(xx, yy);
}

-(float)returnTouchValue
{
    return gura;
}


-(void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    nr_sounds=0;
    UITouch* touch = [touches anyObject];
    CGPoint location = [touch locationInView: [touch view]];
    startTouchPoint=location;
    date_start=[[NSDate date] retain];
    location = [[CCDirector sharedDirector] convertToGL:location];
    xx=location.x;
    yy=location.y;
    touchFun=0;
    if (CGRectContainsPoint([personaj rectPerson],location))
    {
        [stateTamagochi setTouchObject:tagTamagochi];
    }
    
    if (CGRectContainsPoint([microfon boundingBox],location))
    {
         [stateTamagochi setTouchObject:tagMicrofon];
    }
}

-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch* touch = [touches anyObject];
    CGPoint location = [touch locationInView: [touch view]];
    location = [[CCDirector sharedDirector] convertToGL:location];

    if ([stateTamagochi touchObject]==tagMicrofon)
    {
        [stateTamagochi setTouchObject:tagMicrofon];
    }
    else
    if ([stateTamagochi touchObject]==tagTamagochi && CGRectContainsPoint([personaj rectPerson],location))
    {
        [stateTamagochi setMouthState:mouthStateFun];
        touchFun+=0.006;
        if (touchFun>=0.05)
        {
            if (!repeatTouches)
            [self fun];
        }
        nr_sounds++;
    }
    else
    {
        [stateTamagochi setTouchObject:tagDefault];
    }
  
    xx=location.x;
    yy=location.y;
        
    
}
-(void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    
    UITouch* touch = [touches anyObject];
    CGPoint location = [touch locationInView: [touch view]];
    
    endTouchPoint=location;
    float moveDifference=startTouchPoint.x-endTouchPoint.x;
    float differenceY=startTouchPoint.y-endTouchPoint.y;
    if (differenceY<0) differenceY*=-1;
    NSTimeInterval secondsBetween = [[NSDate date] timeIntervalSinceDate:date_start];
    [date_start release];
    date_start=nil;
    if (moveDifference>150.0 && differenceY<40.0 && endTouchPoint.y<150.0 && secondsBetween<0.3)
        [[NSNotificationCenter defaultCenter]postNotificationName:@"nextRoom" object:@"1"];
    else if (moveDifference<-150.0 && differenceY<40.0 && endTouchPoint.y<150.0 && secondsBetween<0.3)
        [[NSNotificationCenter defaultCenter]postNotificationName:@"nextRoom" object:@"2"];
    
    location = [[CCDirector sharedDirector] convertToGL:location];
    nr_sounds=0;
    touchFun=0;
    xx=0;
    yy=0;

    touchObject=tagDefault;
    
    if ([stateTamagochi touchObject]==tagMicrofon)
    {
        [self microfonOnOFF];
        [stateTamagochi setTouchObject:tagDefault];
    }
    if ([stateTamagochi mouthState]!=mouthSpeak)
        [stateTamagochi setMouthState:mouth];
}

-(void)changePosition:(CGPoint)pos
{
    xx=pos.x;
    yy=pos.y;
}

#pragma Tamagochi effects

-(void)resetVariables
{
    tamTouch=0;
    repeatTouches=NO;
    touchFun=0;
    if ([[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying])
        [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
}

-(void)fun
{
    repeatTouches=YES;
    totalTime=0;
    tamTouch=0.3;
    if (![[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying] &&  [stateTamagochi soundOn])
        [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"giggle.wav" loop:NO];
    [self performSelector:@selector(resetVariables) withObject:nil afterDelay:1.57];
}

-(void)dealloc
{
    [self stop];
    [stateTamagochi setMouthState:mouth];
    NSString *documentsFolder = [self getPath];
    NSError *error = nil;
    NSString *pathToRemove = [documentsFolder stringByAppendingPathComponent:@"/record.wav"];
    if ([[NSFileManager defaultManager] removeItemAtPath:pathToRemove error:&error] != YES)
    {
        NSLog(@"Unable to delete file: %@", [error localizedDescription]);
    }
    
    if (sound)
    {
        sound->release();
        sound = NULL;
    }
    
    if (sound2)
    {
        sound2->release();
        sound2 = NULL;
    }
    if (system)
    {
        system->release();
        system = NULL;
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"skinColor" object:nil];
    [super dealloc];
}

@end
