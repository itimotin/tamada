//
//  TamagochiStates.m
//  Tamagochi
//
//  Created by Sergiu Moțipan on 6/28/13.
//
//

#import "TamagochiStates.h"

static TamagochiStates *stateManager=nil;



@implementation TamagochiStates
+(id)sharedInstance
{
    if (!stateManager)
        stateManager = [[super allocWithZone:NULL] init];
    return stateManager;
}

-(void)firstStart
{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    if (!_kitchenStart || !_gameStart || !_sleepStart || !_bathStart || !_healthStart)
    {
        [self createNotificationsFirstPlayGame];
    }
}
-(id)init
{
    self=[super init];
    if (self)
    {
         database=[[Functions alloc] init];
        [database openDataBase];
        _mouthState=mouthStateDefault;
        _eyeState=eyeStateDefault;
        _touchObject=tagTamagochi;
        _state=0;
        _shopCategory=1;
        _bodyCategory=0;
        _eatCategory=1;
        _skinCategory=0;
        _bodyCategory=sh_body;
        _nextLevelPoints=[database nextLevel];
        _adult=[database adult];
        _fat=1.0;
        _mouthSad=NO;
        _dirtFood=0.0;
        _healthFood=0.0;
        _sweetsFood=0.0;
        
        _years=[[[NSArray alloc] initWithObjects:NSLocalizedString(@"Baby", nil),NSLocalizedString(@"Child", nil), NSLocalizedString(@"Adult", nil), nil] retain];
        
         if ([[NSUserDefaults standardUserDefaults] objectForKey:@"soundOn"])
         {
             _soundOn=[[[NSUserDefaults standardUserDefaults] objectForKey:@"soundOn"] intValue];
         }
         else
            _soundOn=1;
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"musicOn"])
        {
            _musicOn=[[[NSUserDefaults standardUserDefaults] objectForKey:@"musicOn"] intValue];
        }
        else
            _musicOn=1;
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"notificationsOn"])
        {
             _notificationsOn=[[[NSUserDefaults standardUserDefaults] objectForKey:@"notificationsOn"] intValue];
        }
        else
            _notificationsOn=1;
        
        [[SimpleAudioEngine sharedEngine] setEffectsVolume:_soundOn];
        [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume:_soundOn];
        
         [NSTimer scheduledTimerWithTimeInterval:60.0*time_food target:self selector:@selector(foodTimer:) userInfo:nil repeats:YES];
         [NSTimer scheduledTimerWithTimeInterval:60.0*time_health target:self selector:@selector(healthTimer:) userInfo:nil repeats:YES];
         [NSTimer scheduledTimerWithTimeInterval:60.0*time_energy target:self selector:@selector(energyTimer:) userInfo:nil repeats:YES];
         [NSTimer scheduledTimerWithTimeInterval:60.0*time_fun target:self selector:@selector(funTimer:) userInfo:nil repeats:YES];
    }
    
    return self;
}

-(void)foodTimer:(NSTimer *)pTmpTimer
{
    _food=_food-1;
    [self updateFood];
}

-(void)healthTimer:(NSTimer *)pTmpTimer
{
    _health=_health-1;
    [self updateHealth];
}

-(void)energyTimer:(NSTimer *)pTmpTimer
{
    _energy=_energy-1;
    [self updateEnergy];
}
-(void)funTimer:(NSTimer *)pTmpTimer
{
    _funny=_funny-1;
    [self updateFun];
}
-(void)selectStatesTamagochi
{
    NSMutableArray *arr=[database selectStates];
    [self setFood:[[arr objectAtIndex:0] floatValue]];
    [self setEnergy:[[arr objectAtIndex:1] floatValue]];
    [self setHealth:[[arr objectAtIndex:2] floatValue]];
    [self setFunny:[[arr objectAtIndex:3] floatValue]];
    [self setMoneyGame:[[arr objectAtIndex:4] intValue]];
    [self setLevel:[[arr objectAtIndex:5] intValue]];
    [self setSizeLevel:[[arr objectAtIndex:6] intValue]];
    [self setFat:[[arr objectAtIndex:7] floatValue]];
    [self setKitchenStart:[[[database firstPlayGame] objectAtIndex:0] intValue]];
    [self setGameStart:[[[database firstPlayGame] objectAtIndex:1] intValue]];
    [self setSleepStart:[[[database firstPlayGame] objectAtIndex:2] intValue]];
    [self setBathStart:[[[database firstPlayGame] objectAtIndex:3] intValue]];
    [self setHealthStart:[[[database firstPlayGame] objectAtIndex:4] intValue]];
    [self setClosetStart:[[[database firstPlayGame] objectAtIndex:5] intValue]];
   
    _dirtFood=[database selectDirt];
    [self setSizeCustom];
    [self setKCalFoods:(_food*20)];
}

-(void)updateFun
{
    if (_funny>100.0) _funny=100.0;
    if (_funny>20 && _mouthSad==YES)
    {
        _mouthSad=NO;
        [[personaj person] addMoney:10];
    }
    if (_funny<20 && _mouthSad==NO) _mouthSad=YES;
    if (_funny<0) _funny=0;
    [database updateFunny:_funny];
}
-(void)insertNameB:(NSString *)str
{
    [database insertNameBody:str];
}
-(void)insertNameBodyIopta:(NSString *)str price:(NSInteger)prc group:(NSInteger)group nr:(NSInteger)nr level:(NSInteger)lev
{
    [database insertNameBodyIopta:str price:prc group:group nr:nr level:lev];
}

-(void)updateFat
{
    [[[personaj person] tamagochi] runAction:[CCScaleTo actionWithDuration:0.2 scaleX:_fat scaleY:[[personaj person] tamagochi].scaleY]];
    if ([[personaj person] getChildByTag:5])
        [[[personaj person] haine] runAction:[CCScaleTo actionWithDuration:0.2 scaleX:_fat scaleY:[[personaj person] haine].scaleY]];
    [database updateFat];
}
-(void)updateFood
{
    if (_food>100 && _fat<1.2)
    {
        _health=_health-(_food-100)/5.0;
        [self updateHealth];
        
        _fat=_food/100.0;
        if (_fat>1.2)
            _fat=1.2;
        [self updateFat];
    }

    if (_sweetsFood>=20)
    {
        self.energy=self.energy+1.0;
        _sweetsFood=0.0;
    }
    
    if (_healthFood>=20)
    {
        self.health=self.health+1.0;
        _healthFood=0.0;
    }
    if (_dirtFood>=50.0 && [stateTamagochi state]!=stateDirt)
    {
        [self updateDirt];
        [stateTamagochi setState:stateDirt];
        [[[[personaj person] getChildByTag:6]  getChildByTag:39]  runAction:[CCFadeIn actionWithDuration:1.0]];
        _dirtFood=0.0;
    }
    
    if (_food>20 && [[personaj person] eye_center].scale!=0.5+0.5*IS_RETINA)
    {
        [[[personaj person] eye_center] setScale:0.5+0.5*IS_RETINA];
        [[[personaj person] eye_center] stopAllActions];
        [[personaj person] addMoney:10];
    }
    else if (_food<=20 && [[personaj person] eye_center].scale>=0.5+0.5*IS_RETINA)
    {
         [[[personaj person] eye_center] setScale:0.9+0.5*IS_RETINA];
         [[personaj person] sufferingEye];
    }
    if (_food<0) _food=0;
    [database updateFood:_food];
}
-(void)updateEnergy
{
    if (_energy>100.0) _energy=100.0;
    if (_energy<0) _energy=0;
    if (!_sleep)
    {
    if (_energy<=20 && [stateTamagochi eyeState]!=eyeStateShower)
    {
        [stateTamagochi setEyeState:eyeStateShower];
    }
    else if (_energy>20 && [stateTamagochi eyeState]==eyeStateShower)
    {
        [stateTamagochi setEyeState:eyeStateDefault];
        [[personaj person] addMoney:10];
    }
    }
    [database updateEnergy:_energy];
}
-(void)updateHealth
{
    if (_health>100.0) _health=100.0;
    if (_health<0) _health=0;
    [database updateHealth:_health];
    if (_health>20 && [[personaj person] getChildByTag:15] )
    {
        [[personaj person] addMoney:10];
        [[[personaj person] getChildByTag:15] removeFromParentAndCleanup:YES];
    }
    else if (_health<=20 && ![[personaj person] getChildByTag:15])
    {
        [[personaj person] createChildByTag:15 nameTexture:@"termometr.png"];
    }
}
-(void)updateMoney
{
    [database updateMoney:_moneyGame];
}

-(void)updateDirt
{
    [database updateDirt:_dirtFood];
}
-(void)setSizeCustom
{
    if (_sizeLevel<=7)
        [self setSizeTamagochi:0.3+_sizeLevel/35.0];
    else if (_sizeLevel<=30)
        [self setSizeTamagochi:0.5+_sizeLevel/60.0];
    else
        [self setSizeTamagochi:1.0];
}
-(void)setYears
{
    if (_sizeLevel<=7)
    {
        _procentYears=(int)_sizeLevel*100.0/7;
    }
    else if (_sizeLevel<=17)
    {
        _procentYears=(int)(_sizeLevel-7)*100.0/10;
    }
    else
    {
        _procentYears=0;
    }
}
-(void)updateLevel
{
    [database updateLevel:_level];
}
-(void)updateSizeLevel
{
    [self setSizeCustom];
    [self setYears];
    if ((_room==kitchenRoom || _room==closetRoom) && _sizeTamagochi>0.8)
        [[personaj person] runAction:[CCScaleTo actionWithDuration:0.1 scale:0.8]];
    else
        [[personaj person] runAction:[CCScaleTo actionWithDuration:0.1 scale:_sizeTamagochi]];
    [database updateSizeLevel:_sizeLevel];
}

-(void)eatTamagochi:(int)nr category:(int)category
{
    [database tamagochiEat:nr category:category];
}

-(NSMutableArray*)selectFoods:(int)category
{
    return [database selectFood:category];
}

-(void)deleteFood:(int)category food:(int)nr
{
    
}
-(NSMutableArray *)selectBody_Shop:(NSInteger)index
{
   return [database selectBody_Shop:index];
}
-(NSMutableArray *)selectBodyGroup:(NSArray *)arr
{
    return [database selectBodyGroup:arr];
}
-(NSMutableArray *)selectBodyShop:(NSInteger)category
{
    return [database selectBodyShop:category];
}
-(void)shopProduct:(NSInteger)index index2:(NSInteger)index2
{
    [database shopProduct:index index2:index2];
}

-(NSMutableArray *)selectShoppedColors:(NSInteger)index
{
    return [database selectShoppedColors:index];
}
-(int)returnPrice:(NSString *)name group:(NSString *)group nr:(NSInteger)nr
{
    return [database returnPrice:name group:group nr:nr];
}
-(int)returnLevel:(NSString *)name group:(NSString *)group nr:(NSInteger)nr
{
    return [database returnLevel:name group:group nr:nr];
}
-(int)returnIDName:(NSString *)name
{
    return [database returnIDName:name];
}

-(NSMutableArray *)selectSkins:(NSInteger)index
{
    return [database selectSkins:index];
}

-(int)returnPriceSkin:(NSInteger)index
{
    return [database returnPriceSkin:index];
}
-(NSMutableArray *)colorSkin:(NSInteger)index object:(NSInteger)object
{
    return [database colorSkin:index object:object];
}
-(NSInteger)selectColorSkin:(NSInteger)tag room:(NSInteger)room
{
    return  [database selectColorSkin:tag room:room];
}
-(void)updateColorSkin:(NSInteger)tag color:(NSInteger)color
{
    [database updateColorSkin:tag color:color];
}

-(NSInteger)selectNameSkin:(NSInteger)tag
{
    return [database selectNameSkin:tag];
}
-(NSInteger)getPriceProduct:(NSInteger)index
{
    return [database getPriceProduct:index];
}

-(void)unlockColor:(NSInteger)index color:(NSInteger)color group:(NSString*)group
{
    [database unlockColor:index color:color group:group];
}

-(NSMutableArray *)selectCoinsShop:(NSInteger)index
{
    return [database selectCoinsShop:index];
}
-(NSString *)getPriceBodyShop:(NSString *)index group:(NSString *)group
{
    return [database getPriceBodyShop:index group:group];
}
-(void)updateSleep
{
    [database updateSleep];
}
-(void)updateStateOut
{
    [database updateStateOut];
}
-(NSMutableArray *)returnStateOut
{
    return [database returnStateOut];
}


-(void)cancelLocalNotification:(NSString*)notificationID
{
    for (int j=0;j<[[[UIApplication sharedApplication]scheduledLocalNotifications]count]; j++)
    {
        UILocalNotification *someNotification = [[[UIApplication sharedApplication]scheduledLocalNotifications]objectAtIndex:j];
        if([[someNotification.userInfo objectForKey:@"category"] isEqualToString:notificationID])
        {
            [[UIApplication sharedApplication] cancelLocalNotification:someNotification];
        }
    }
}

-(NSInteger) convertToGMT:(NSDate*)sourceDate
{
    NSTimeZone* currentTimeZone = [NSTimeZone localTimeZone];
    NSTimeInterval gmtInterval = [currentTimeZone secondsFromGMTForDate:sourceDate];
    return gmtInterval;
}
-(void)createLocalNotification:(NSString *)notificationID message:(NSString *)text interval:(NSInteger)min
{
    [self cancelLocalNotification:notificationID];
    UILocalNotification* localNotification = [[[UILocalNotification alloc] init] autorelease];
    localNotification.timeZone = [NSTimeZone localTimeZone];
    localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:min];
    localNotification.repeatInterval=kCFCalendarUnitWeek;
    localNotification.soundName=[NSString stringWithFormat:@"no_%@.mp3", notificationID];
    localNotification.alertBody = text;
    localNotification.applicationIconBadgeNumber = 1;
    NSDictionary *userDict = [NSDictionary dictionaryWithObject:notificationID forKey:@"category"];
    localNotification.userInfo = userDict;
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

-(void)resetStates:(NSInteger)interval gmt:(NSInteger)gmt
{
    
    if (interval>=gmt)
    {
        float sleepON=0.0;
        _food=_food-(interval/60.0/time_food);
        _health=_health-(interval/60.0/time_health);
        if ([[[stateTamagochi returnStateOut] objectAtIndex:0]intValue]==1)
        {
            
            sleepON=_energy;
            _energy=_energy+(interval/60.0);
        }
        else
            _energy=_energy-(interval/60.0/time_energy);
        _funny=_funny-(interval/60.0/time_fun);
        (_funny<0)?_funny=0:_funny;
        (_health<0)?_health=0:_health;
        (_food<0)?_food=0:_food;
        (_energy<0)?_energy=0:_energy;
        (_funny>100.0)?_funny=100.0:_funny;
        (_health>100.0)?_health=100.0:_health;
        (_food>100.0)?_food=100.0:_food;
        (_energy>100.0)?_energy=100.0:_energy;
        
        if (_funny==0 && _health==0 && _food==0 && _energy==0 && _dirtFood!=0.0 && [(CCSprite *)[[[personaj person] getChildByTag:6]  getChildByTag:39] opacity]!=255.0 )
        {
            _dirtFood=50.0;
            [self updateDirt];
            [stateTamagochi setState:stateDirt];
            [[[[personaj person] getChildByTag:6]  getChildByTag:39]  runAction:[CCFadeIn actionWithDuration:1.0]];
        }
        if (sleepON!=0.0 && sleepON<_energy)
        {
            [stateTamagochi setNextLevelPoints:[stateTamagochi nextLevelPoints] +(_energy-sleepON)];
            [self setSleepONBed:[stateTamagochi energy]];
            [stateTamagochi addToNextLevel:4];
            [stateTamagochi setHealth:[stateTamagochi health]+(_energy-sleepON)/2.0 ];
            if ([stateTamagochi health]>100.0)
                [stateTamagochi setHealth:100.0];
            [stateTamagochi updateHealth];
        }
        [self updateFood];
        [self updateEnergy];
        [self updateFun];
        [self updateHealth];
    }
}

-(void)createNotificationsOrder
{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    if (_food<20)
    {
        if (self.sleep==0)
            [self createLocalNotification:@"food" message:NSLocalizedString(@"food_notif", nil) interval:5];
        else
            [self createLocalNotification:@"food" message:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"sleep_no", nil),NSLocalizedString(@"food_notif", nil) ] interval:5];
    }
    else if ([stateTamagochi state]==stateDirt)
    {
        if (self.sleep==0)
            [self createLocalNotification:@"dirt" message:NSLocalizedString(@"dirt_notif", nil) interval:5];
        else
            [self createLocalNotification:@"dirt" message:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"sleep_no", nil),NSLocalizedString(@"dirt_notif", nil) ] interval:5];
    }
    else if (_health<20)
    {
        if (self.sleep==0)
            [self createLocalNotification:@"health" message:NSLocalizedString(@"health_notif", nil) interval:5];
        else
            [self createLocalNotification:@"health" message:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"sleep_no", nil),NSLocalizedString(@"health_notif", nil) ] interval:5];
    }
    else if (_energy<20 && [stateTamagochi sleep]==0)
        [self createLocalNotification:@"energy" message:NSLocalizedString(@"energy_notif", nil) interval:5];
    else if (_funny<20 && [stateTamagochi sleep]==0)
    {
        if (_energy>20)
            [self createLocalNotification:@"fun" message:NSLocalizedString(@"funny_notif", nil) interval:5];
        else
            [self createLocalNotification:@"fun" message:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"sleep_no", nil),NSLocalizedString(@"funny_notif", nil) ] interval:5];
    }
}

-(void)createNotificationsFirstPlayGame
{
    
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    if ([stateTamagochi notificationsOn])
    {
    if (_food<20 && [[[self firstPlayGame] objectAtIndex:0] intValue]==0)
    {
        [self createLocalNotification:@"food" message:[NSString stringWithFormat:@"%@ \n \n %@ \n\n %@", NSLocalizedString(@"firstPlay_title", nil), NSLocalizedString(@"firstPlay_food", nil), NSLocalizedString(@"firstPlay_food_inst", nil) ]  interval:5];
        
    }
    else if ([stateTamagochi state]==stateDirt && [[[self firstPlayGame] objectAtIndex:3] intValue]==0)
        [self createLocalNotification:@"dirt" message:[NSString stringWithFormat:@"%@ \n\n %@ \n\n %@", NSLocalizedString(@"firstPlay_title", nil), NSLocalizedString(@"firstPlay_dirt", nil), NSLocalizedString(@"firstPlay_dirt_inst", nil) ] interval:5];
    else if (_health<20 && [[[self firstPlayGame] objectAtIndex:4] intValue]==0)
        [self createLocalNotification:@"health" message:[NSString stringWithFormat:@"%@ \n\n %@", NSLocalizedString(@"firstPlay_title", nil), NSLocalizedString(@"firstPlay_health", nil) ] interval:5];
    else if (_energy<20 && [[[self firstPlayGame] objectAtIndex:2] intValue]==0)
        [self createLocalNotification:@"energy" message:[NSString stringWithFormat:@"%@ \n\n %@", NSLocalizedString(@"firstPlay_title", nil), NSLocalizedString(@"firstPlay_energy", nil) ] interval:5];
    else if (_funny<20 && [[[self firstPlayGame] objectAtIndex:1] intValue]==0)
        [self createLocalNotification:@"fun" message:[NSString stringWithFormat:@"%@ \n\n %@ \n\n %@", NSLocalizedString(@"firstPlay_title", nil), NSLocalizedString(@"firstPlay_food", nil), NSLocalizedString(@"firstPlay_funny", nil) ] interval:5];
    }
}

-(void)updateFirstStart:(int)category
{
    [database updateFirstStart:category];
}

-(void)updateBodyShopped:(NSMutableArray*)array
{
     [database updateBodyShopped:array];
}

-(void)updateTatooShopped:(NSMutableArray *)array
{
    NSString *str=@"";
    NSMutableArray *aux=[[NSMutableArray alloc] init];
    for (int i=0;i<[array count];i++)
    {
        str=[NSString stringWithFormat:@"%@, %@, %@", [[array objectAtIndex:i] objectAtIndex:0], [[array objectAtIndex:i] objectAtIndex:1], [[array objectAtIndex:i] objectAtIndex:2]];
        [aux addObject:str];
    }
    
    [database updateTatooShopped:aux];
    [aux release];
}
-(void)createNotificationsOnExit
{
    [self updateBodyShopped:[personaj bodyShopped]];
    [self updateTatooShopped:[personaj tatooShopped]];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    if ([stateTamagochi notificationsOn])
    {
    [self createNotificationsOrder];
    if ([[[UIApplication sharedApplication] scheduledLocalNotifications] count]==0)
    {
        int max=(_food-20)*time_food*60;
        if (max<(_energy-20)*time_energy*60) max=(_energy-20)*time_energy*60;
        if (max<(_health-20)*time_health*60) max=(_health-20)*time_health*60;
        if (max<(_funny-20)*time_fun*60) max=(_funny-20)*time_fun*60;
        if ([stateTamagochi sleep]==1)
        {
             [self createLocalNotification:@"sleep" message:NSLocalizedString(@"sleep_notif", nil) interval:(100.0-_energy)*60];
        }
        else if (_food>20)
            [self createLocalNotification:@"food" message:NSLocalizedString(@"food_notif", nil) interval:(_food-20)*time_food*60];
        else if ([stateTamagochi state]==stateDefault)
            [self createLocalNotification:@"dirt" message:NSLocalizedString(@"dirt_notif", nil) interval:max];
        else if (_energy>20 && [stateTamagochi sleep]==0)
            [self createLocalNotification:@"energy" message:NSLocalizedString(@"energy_notif", nil) interval:(_energy-20)*time_energy*60];
        else if (_health>20)
            [self createLocalNotification:@"health" message:NSLocalizedString(@"health_notif", nil) interval:(_health-20)*time_health*60];
        else if (_funny>20)
            [self createLocalNotification:@"fun" message:NSLocalizedString(@"funny_notif", nil) interval:(_funny-20)*time_fun*60];
    }
    }
}

-(void)removeNotificationsOnEnter
{
    if ([[database selectTatooShopped] count]>0)
    {
        [[personaj tatooShopped] removeAllObjects];
        [[personaj tatooShopped] addObjectsFromArray:[database selectTatooShopped]];
        [personaj replaceDefaultAllTatoo];
        
    }
    
    if ([[database selectBodyShopped] count]==14)
    {
        [[personaj bodyShopped] removeAllObjects];
        [[personaj bodyShopped] addObjectsFromArray:[database selectBodyShopped]];
        [personaj replaceDefaultAllBody];
    }
    _dirtFood=[database selectDirt];
    
    NSDate *myDate;
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss ZZZ"];
    [df setTimeZone:[NSTimeZone localTimeZone]];
   
    if (![[[self returnStateOut] objectAtIndex:1] isEqualToString:@"0"])
    {
        myDate=[df dateFromString:[[self returnStateOut] objectAtIndex:1]];
    }
    else
    {
        myDate=[NSDate date];
    }
    [df release];
    
    NSDate *now=[NSDate date];
    NSComparisonResult duration=[now timeIntervalSinceDate:myDate];
    int diff=0;
    if ([[[self returnStateOut] objectAtIndex:2] intValue]!=[self convertToGMT:[NSDate date]])
        diff=[self convertToGMT:[NSDate date]]-[[[self returnStateOut] objectAtIndex:2] intValue];
        
    if (duration!=0)
           [self resetStates:duration gmt:diff];
    
}

- (float)randomFloatBetween:(float)smallNumber and:(float)bigNumber {
    float diff = bigNumber - smallNumber;
    return (((float) (arc4random() % ((unsigned)RAND_MAX + 1)) / RAND_MAX) * diff) + smallNumber;
}

-(void)addToNextLevel:(int)category
{
    switch (category) {
        case 1:
        {
            _nextLevelPoints+=[self randomFloatBetween:(30.0/_level/2.0) and:(30.0/_level)];
        }
            break;
        case 2:
        {
            _nextLevelPoints+=[self randomFloatBetween:(60.0/_level/2.0) and:(60.0/_level)];
        }
            break;
            
        default:
            break;
    }
    [database updateNextLevel:_nextLevelPoints];
    if (_nextLevelPoints>=100.0)
    {
        _nextLevelPoints=_nextLevelPoints-100.0;
        [database updateNextLevel:_nextLevelPoints];
        
        _level++;
        _sizeLevel++;
        [[NSNotificationCenter defaultCenter]postNotificationName:@"nextLevel" object:nil];
        _moneyGame+=_level*10;
        [self updateMoney];
        [self updateLevel];
        [self updateSizeLevel];
    }
}

-(NSMutableArray *)selectOpenedNextLevel
{
    return [database selectOpenedNextLevel];
}

-(NSMutableArray *)firstPlayGame
{
     return [database firstPlayGame];
}
-(void)dealloc
{
    [_years release], _years=nil;
    [super dealloc];
}
// TODO: add THIS
-(BOOL)updateGameWithStageId:(NSNumber*)idStage withTime:(NSString*)bestTime withScore:(NSNumber*)score withCoins:(NSNumber*)coins withPosition:(NSNumber *)bestPosition{
    return [database updateGameWithStageId:idStage withTime:bestTime withScore:score withCoins:coins withPosition:bestPosition];
}

- (NSInteger)getScoreForStageId:(NSNumber*)idStage{
    return [database getScoreForStageId:idStage];
}

- (NSInteger)getPostionForStageID:(NSNumber *)idStage{
    return [database getPostionForStageID:idStage];
}
//- (void)addCoins:(NSInteger)coins {
//    [database addCoins:coins];
//}
- (NSMutableArray*)getAllGameStatisticsForStageID:(NSNumber *)idStage{
    return [database getAllGameStatisticsForStageID:idStage];
}

- (NSDictionary*)getAllGameStatisticsForStageID:(NSNumber *)idStage forPlayer:(NSString*)player{
    return [database getAllGameStatisticsForStageID:idStage forPlayer:player];
}
//END

- (BOOL)addPlayer:(NSString*)player{
    return [database addPlayer:player];
}

-(NSInteger)getStageForPlayer:(NSString*)player{
    return [database getStageForPlayer:player];
}
- (BOOL)setStageForPlayer:(NSString*)player forStage:(NSInteger)stageId{
    return [database setStageForPlayer:player forStage:stageId];
}


@end
