//
//  ShopCoins.m
//  Tamagochi
//
//  Created by Sergiu Moțipan on 11/28/13.
//
//

#import "ShopCoins.h"
#import "ShopCoinsCell.h"

@implementation ShopCoins

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        tableCoins.delegate=self;
        tableCoins.dataSource=self;
       
    }
    return self;
}

-(void)refresh
{
    loadingIndex=-1;
    [tableCoins reloadData];
}
- (IBAction)shopCoins:(id)sender forEvent:(UIEvent *)event
{
    if ([stateTamagochi soundOn])
        [[SimpleAudioEngine sharedEngine] playEffect:@"next_btn.wav" loop:NO];
    UIButton *btn =(UIButton*)sender;
    UITouch *touch = [[event touchesForView:btn] anyObject];
    NSIndexPath *index=[tableCoins indexPathForRowAtPoint:[touch locationInView: tableCoins]];
    [MKStoreManager sharedManager].delegate = self;
    switch ([index row]) {
        case 0:
        {
            loadingA=YES;
            [[MKStoreManager sharedManager] buyFeatureA];
        }
            break;
        case 1:
        {
            loadingB=YES;
            [[MKStoreManager sharedManager] buyFeatureB];
        }
            break;
        case 2:
        {
            loadingC=YES;
            [[MKStoreManager sharedManager] buyFeatureC];
        }
            break;
        case 3:
        {
            loadingD=YES;
            [[MKStoreManager sharedManager] buyFeatureD];
        }
            break;
            
        default:
            break;
    }
    [tableCoins reloadData];
}

-(void)productAPurchased
{
    if ([stateTamagochi soundOn])
        [[SimpleAudioEngine sharedEngine] playEffect:@"buy.wav" loop:NO];
    [self reloadCell:0];
    [stateTamagochi setMoneyGame:[stateTamagochi moneyGame]+1999];
    [stateTamagochi updateMoney];
}

-(void)productBPurchased
{
    if ([stateTamagochi soundOn])
        [[SimpleAudioEngine sharedEngine] playEffect:@"buy.wav" loop:NO];
    [self reloadCell:1];
    [stateTamagochi setMoneyGame:[stateTamagochi moneyGame]+4999];
    [stateTamagochi updateMoney];
}

-(void)productCPurchased
{
    if ([stateTamagochi soundOn])
        [[SimpleAudioEngine sharedEngine] playEffect:@"buy.wav" loop:NO];
    [self reloadCell:2];
    [stateTamagochi setMoneyGame:[stateTamagochi moneyGame]+15999];
    [stateTamagochi updateMoney];
}
-(void)productDPurchased
{
    if ([stateTamagochi soundOn])
        [[SimpleAudioEngine sharedEngine] playEffect:@"buy.wav" loop:NO];
    [self reloadCell:3];
    [stateTamagochi setMoneyGame:[stateTamagochi moneyGame]+49999];
    [stateTamagochi updateMoney];
}
-(void)failed:(NSString *)str
{
    loadingA=NO;
    loadingB=NO;
    loadingC=NO;
    loadingD=NO;
    [tableCoins reloadData];
    
}

-(void)reloadCell:(NSInteger)row
{
    loadingA=NO;
    loadingB=NO;
    loadingC=NO;
    loadingD=NO;
    [tableCoins reloadData];
}



#pragma mark - UItableViewDelegates


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView  numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier;
    if (!IS_IPAD)
        CellIdentifier = @"ShopCoinsCell";
    else
        CellIdentifier = @"ShopCoinsCell_iPad";
    ShopCoinsCell *cell = (ShopCoinsCell *) [tableCoins dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:[NSString stringWithFormat:@"ShopCoinsCell%@", (IS_IPAD)?@"_iPad":@""] owner:self options:nil];
            
        for (id currentObject in topLevelObjects) {
                if ([currentObject isKindOfClass:[ShopCoinsCell class]]) {
                    cell = (ShopCoinsCell *) currentObject;
                    break;
            }
        }
    }
    for (id obj in cell.subviews) {
        if ([obj respondsToSelector:@selector(setDelaysContentTouches:)]) {
            [obj setDelaysContentTouches:NO];
        }
    }
    cell.loading.hidden=YES;
    cell.name_btn.hidden=NO;
    cell.btnShopCoins.enabled=YES;
    if ((indexPath.row==0 && loadingA) || (indexPath.row==1 && loadingB) || (indexPath.row==2 && loadingC) || (indexPath.row==3 && loadingD))
    {
        cell.loading.hidden=NO;
        cell.name_btn.hidden=YES;
        cell.btnShopCoins.enabled=NO;
    }

    if (loadingA || loadingB || loadingC || loadingD)
        cell.btnShopCoins.enabled=NO;
    [cell.loading setTransform: CGAffineTransformMakeScale(0.75, 0.75)];
    cell.name_btn.text=NSLocalizedString(@"buy", nil);
    cell.coins.text=[NSString stringWithFormat:@"%@ %@",[[stateTamagochi selectCoinsShop:[indexPath row]] objectAtIndex:0], NSLocalizedString(@"coins", nil)];
    cell.sale.text=([[[stateTamagochi selectCoinsShop:[indexPath row]] objectAtIndex:1] intValue]==0)?@"":[NSString stringWithFormat:@"%@ %% %@!",[[stateTamagochi selectCoinsShop:[indexPath row]] objectAtIndex:1], NSLocalizedString(@"sale", nil)];
    cell.price.text=[NSString stringWithFormat:@"$%@",[[stateTamagochi selectCoinsShop:[indexPath row]] objectAtIndex:2]];
    cell.backgroundColor=[UIColor clearColor];
    
    return cell;
}

- (void)dealloc {
    [tableCoins release];
    [btnBack release];
    [_btnBack release];
    [_viewBackground release];
    [super dealloc];
}
@end
