//
//  AppDelegate.mm
//  Tamagochi
//
//  Created by Sergiu Moțipan on 6/4/13.
//  Copyright __MyCompanyName__ 2013. All rights reserved.
//

#import "cocos2d.h"
#import "DefaultSHKConfigurator.h"
#import "SHKConfiguration.h"
#import "MySHKConfigurator.h"
#import "AppDelegate.h"
#import "fmod.hpp"
#import "fmodiphone.h"
#import "fmod_errors.h"

@implementation AppController

@synthesize window=window_, navController=navController_;

-(void)createFMOD
{
    [[CDAudioManager sharedManager] init:kAMM_PlayAndRecord];
    UInt32 audioRouteOverride = kAudioSessionOverrideAudioRoute_Speaker;
    
    
    AudioSessionSetProperty (
                             kAudioSessionProperty_OverrideAudioRoute,
                             sizeof (audioRouteOverride),
                             &audioRouteOverride
                             );
    unsigned int    bytesPerSample;
    unsigned int    bytesPerSecond;
    
    FMOD::System   *system      = NULL;
    FMOD::Sound *sound       = NULL;
    FMOD::Sound *sound2       = NULL;
    
    
    
    FMOD_RESULT             result      = FMOD_OK;
    unsigned int            version     = 0;
    FMOD_CREATESOUNDEXINFO  exinfo      = {0};
    
    result = FMOD::System_Create(&system);
    //ERRCHECK(result);
    
    result = system->getVersion(&version);
    // ERRCHECK(result);
    
    if (version < FMOD_VERSION)
    {
        fprintf(stderr, "You are using an old version of FMOD %08x.  This program requires %08x\n", version, FMOD_VERSION);
        exit(-1);
    }
    
    result = system->init(32, FMOD_INIT_NORMAL | FMOD_INIT_ENABLE_PROFILE,NULL);
    //  ERRCHECK(result);
    
    
    //  memset(&exinfo, 0, sizeof(FMOD_CREATESOUNDEXINFO));
    
    exinfo.cbsize           = sizeof(FMOD_CREATESOUNDEXINFO);
    exinfo.numchannels      = 2;
    exinfo.format           = FMOD_SOUND_FORMAT_PCM16;
    exinfo.defaultfrequency = 22000;
    exinfo.length           = exinfo.defaultfrequency * sizeof(short) * exinfo.numchannels * 1;
    
    bytesPerSample = sizeof(short) * exinfo.numchannels;
    bytesPerSecond = exinfo.defaultfrequency * bytesPerSample;
    
    result = system->createSound(NULL, FMOD_3D | FMOD_SOFTWARE | FMOD_LOOP_NORMAL | FMOD_OPENUSER, &exinfo, &sound);
    
    if (sound)
    {
        sound->release();
        sound = NULL;
    }
    
    if (sound2)
    {
        sound2->release();
        sound2 = NULL;
    }
    if (system)
    {
        system->release();
        system = NULL;
    }
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  
    DefaultSHKConfigurator *configurator = [[MySHKConfigurator alloc] init];
    [SHKConfiguration sharedInstanceWithConfigurator:configurator];
    [configurator release];
    [self createFMOD];
    application.applicationIconBadgeNumber = 0;
    [window_ makeKeyAndVisible];
	return YES;
}


// Supported orientations: Landscape. Customize it for your own needs
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}


// getting a call, pause the game
-(void) applicationWillResignActive:(UIApplication *)application
{
     [[CCDirector sharedDirector] pause];
}

// call got rejected
-(void) applicationDidBecomeActive:(UIApplication *)application
{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"enableButtons" object:nil];
     [[CCDirector sharedDirector] resume];
}

-(void) applicationDidEnterBackground:(UIApplication*)application
{
    if ([stateTamagochi loaded])
    {
        [stateTamagochi updateStateOut];
        [stateTamagochi createNotificationsOnExit];
    }
    [[CCDirector sharedDirector] stopAnimation];
}

-(void) applicationWillEnterForeground:(UIApplication*)application
{
     application.applicationIconBadgeNumber = 0;
     [[UIApplication sharedApplication] cancelAllLocalNotifications];
     [stateTamagochi removeNotificationsOnEnter];
     [[CCDirector sharedDirector] startAnimation];
}

// application will be killed
- (void)applicationWillTerminate:(UIApplication *)application
{
    [stateTamagochi updateStateOut];
    CC_DIRECTOR_END();
}


- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateActive) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:notification.alertBody
                                                       delegate:self cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
    // Set icon badge number to zero
    application.applicationIconBadgeNumber = 0;
}
 
// purge memory
- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
    [[CCTextureCache sharedTextureCache] dumpCachedTextureInfo];
	[[CCDirector sharedDirector] purgeCachedData];
}

// next delta time will be zero
-(void) applicationSignificantTimeChange:(UIApplication *)application
{
	[[CCDirector sharedDirector] setNextDeltaTimeZero:YES];
}

- (void) dealloc
{
	[window_ release];
	[navController_ release];
	[super dealloc];
}
@end

