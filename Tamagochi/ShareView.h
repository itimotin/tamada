//
//  ShareView.h
//  Tamagochi
//
//  Created by Sergiu Moțipan on 1/29/14.
//
//

#import <UIKit/UIKit.h>
#import "Social/Social.h"
#import "Accounts/Accounts.h"
#import "Reachability.h"
#import "SimpleAudioEngine.h"
#import "SHKSharer.h"



@interface ShareView : UIView <UIDocumentInteractionControllerDelegate, UIAlertViewDelegate,SHKSharerDelegate>
{
   
}

@property (retain, nonatomic) IBOutlet UILabel *lab_Share_Title;
@property (retain, nonatomic) NSString *scoreShare;
@property (retain, nonatomic) NSString *scoreShareTwitter;
@property (retain, nonatomic) IBOutlet UIButton *btn_CloseShare;
@property (retain, nonatomic) UIImage *imageBar;
- (IBAction)flikrShare:(id)sender;
- (IBAction)mailShare:(id)sender;
- (IBAction)instagramShare:(id)sender;
- (IBAction)vkShare:(id)sender;
- (IBAction)twitterShare:(id)sender;
- (IBAction)facebookShare:(id)sender;
@end
