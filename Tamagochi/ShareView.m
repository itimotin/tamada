//
//  ShareView.m
//  Tamagochi
//
//  Created by Sergiu Moțipan on 1/29/14.
//
//

#import "ShareView.h"
#import "SHK.h"
#import "SHKItem.h"

#import "SHKMail.h"
#import "SHKTwitter.h"
#import "SHKVkontakte.h"
#import "SHKFlickr.h"
#import "Social/Social.h"
#import "Accounts/Accounts.h"
#import "AppDelegate.h"
#import <FacebookSDK/FacebookSDK.h>
#import "CCNode+Screenshot.h"



@implementation ShareView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
         [[SimpleAudioEngine sharedEngine] preloadEffect:@"share.wav"];
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(int)checkConnection
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus;
}

-(UIImage *)scaleImage:(UIImage *)image toSize:(CGSize)newSize {
    
    float width = newSize.width;
    float height = newSize.height;
    
    UIGraphicsBeginImageContext(newSize);
    CGRect rect = CGRectMake(0, 0, width, height);
    
    float widthRatio = image.size.width / width;
    float heightRatio = image.size.height / height;
    float divisor = widthRatio > heightRatio ? widthRatio : heightRatio;
    
    width = image.size.width / divisor;
    height = image.size.height / divisor;
    
    rect.size.width  = width;
    rect.size.height = height;
    
    if(height < width)
        rect.origin.y = height / 3;
    [image drawInRect: rect];
    
    UIImage *smallImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return smallImage;
    
}

-(UIImage *)getTamagochiCapture:(int)type
{
     if ([stateTamagochi soundOn])
         [[SimpleAudioEngine sharedEngine] playEffect:@"share.wav" loop:NO];
    //name the file we want to save in documents
    NSString* file = @"//imageforphotolib.png";
    
    //get the path to the Documents directory
    NSArray* paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths objectAtIndex:0];
    NSString* screenshotPath = [documentsDirectory
                                stringByAppendingPathComponent:file];
    
    [CCDirector sharedDirector].nextDeltaTimeZero = YES;
    
    CCRenderTexture* rtx =   [CCRenderTexture renderTextureWithWidth:WIDTH_DEVICE
                                                              height:HEIGHT_DEVICE];
    
    [rtx begin];// open the texture
    [[[CCDirector sharedDirector] runningScene] visit];
   // [[personaj person] visit];//add a white page to the background
    [rtx end];//close the texture
    
    // save as file as PNG
    [rtx  saveToFile:@"imageforphotolib.png" format:kCCImageFormatPNG];
    
    //get the screenshot as raw data
    NSData *data = [NSData dataWithContentsOfFile:screenshotPath];
    //create an image from the raw data
    UIImage *img = [UIImage imageWithData:data];
    
    CGImageRef imageRef;
    if (type==0)
    imageRef= CGImageCreateWithImageInRect([img CGImage], CGRectMake(([personaj person].boundingBox.origin.x-[[personaj person] getChildByTag:6].boundingBox.size.width/2.0)*2.0, (HEIGHT_DEVICE - ([personaj person].boundingBox.origin.y+[[personaj person] getChildByTag:6].boundingBox.size.height/2.0)-20)*2.0, [[personaj person] getChildByTag:6].boundingBox.size.width*2.0, [[personaj person] getChildByTag:6].boundingBox.size.height*2.5));
    else
    {
        
        imageRef = CGImageCreateWithImageInRect([img CGImage], CGRectMake(0, 0, WIDTH_DEVICE*2.0, HEIGHT_DEVICE*2.0));
    }
    
    
    UIImage *result = [UIImage imageWithCGImage:imageRef ];
     CGImageRelease(imageRef);
    if (type!=0)
    {
        UIView *view1=[[[UIView alloc] initWithFrame:self.bounds] autorelease];
        UIImageView *img1=[[[UIImageView alloc] initWithImage:_imageBar] autorelease];
        UIImageView *img2=[[[UIImageView alloc] initWithImage:[self scaleImage:result toSize:CGSizeMake(result.size.width/2.0, result.size.height/2.0)]] autorelease];
        [view1 addSubview:img2];
        [view1 addSubview:img1];
        
        UIGraphicsBeginImageContext(CGSizeMake(self.bounds.size.width,self.bounds.size.height));
        [view1 drawViewHierarchyInRect:self.bounds afterScreenUpdates:YES];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return image;
    }
   
    
    return [self scaleImage:result toSize:CGSizeMake(result.size.width*2.0, result.size.height*2.0)];
}
- (IBAction)flikrShare:(id)sender {
    [SHKFlickr shareImage:[self getTamagochiCapture:1] title:@"Paramon!"];
}


- (IBAction)mailShare:(id)sender {
    
    NSURL *url = [NSURL URLWithString:@"http://cyberia.net.in/cameras/pou/tamagochi/"];
	SHKItem *item=[SHKItem image:[self getTamagochiCapture:1] title:@"Paramon game!"];
    item.text=[NSString stringWithFormat:@"%@ \n %@", _scoreShare, url];
    [SHKMail shareItem:item];
}

- (IBAction)instagramShare:(id)sender {
  
    if ([self checkConnection]!=0)
    {
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://camera"];
    if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
        NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:@"jumpto.igo"];
        
        UIImage *image = [self getTamagochiCapture:0];
        
        NSData *imageData = UIImagePNGRepresentation(image);
        [imageData writeToFile:savedImagePath atomically:YES];
        NSURL *imageUrl = [NSURL fileURLWithPath:savedImagePath];
        UIDocumentInteractionController *docController = [[UIDocumentInteractionController alloc] init];
        docController.delegate = self;
        docController.UTI = @"com.instagram.photo";
        docController.URL = imageUrl;
        [docController presentOpenInMenuFromRect:CGRectZero inView:self animated:YES];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"not_have_instagram", nil)
                                                        message:NSLocalizedString(@"install_now", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"later", nil)
                                              otherButtonTitles:@"OK", nil];
        [alert show];
        [alert release];
    }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"attention", nil)
                                                        message:NSLocalizedString(@"no_internet_connection", nil)
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
        [alert release];
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        // Add the action here
    } else {
       [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/instagram/id389801252?mt=8"]];
    }
}

- (IBAction)vkShare:(id)sender {
    
    if ([self checkConnection]!=0)
    {
        NSURL *url = [NSURL URLWithString:@"http://cyberia.net.in/cameras/pou/tamagochi/"];
     //   SHKItem *item=[SHKItem image:[self getTamagochiCapture:1] title:[NSString stringWithFormat:@"Paramon! \n\n %@ \n %@", _scoreShare, url]];
        SHKItem *item1=[[SHKItem alloc] init];
        item1.shareType=SHKShareTypeURL;
        item1.URL=url;
        item1.text=[NSString stringWithFormat:@"Paramon! \n\n %@ \n", _scoreShare];
        item1.image=[self getTamagochiCapture:1];
        
        [SHKVkontakte shareItem:item1];
        [item1 release];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"attention", nil)
                                                        message:NSLocalizedString(@"no_internet_connection", nil)
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
        [alert release];
    }

}

- (IBAction)twitterShare:(id)sender {
    if ([self checkConnection]!=0)
    {
        NSURL *url = [NSURL URLWithString:@"http://cyberia.net.in/cameras/pou/tamagochi/"];
        SHKItem *item=[SHKItem image:[self getTamagochiCapture:0] title:[NSString stringWithFormat:@"%@ \n %@ \n", _scoreShareTwitter, url]];
      //  item.text=[NSString stringWithFormat:@"Paramon! \n\n %@ \n %@", _scoreShare, url];
        [SHKTwitter shareItem:item];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"attention", nil)
                                                        message:NSLocalizedString(@"no_internet_connection", nil)
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
        [alert release];
    }
  }


- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[[NSMutableDictionary alloc] init]
                                   autorelease];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val = [[kv objectAtIndex:1]
                         stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [params setObject:val forKey:[kv objectAtIndex:0]];
    }
    return params;
}
-(void)facebookViewCreate
{
    // Check if the Facebook app is installed and we can present the share dialog
    FBShareDialogParams *params = [[FBShareDialogParams alloc] init];
    params.link = [NSURL URLWithString:@"http://cyberia.net.in/cameras/pou/tamagochi/"];
    params.name = @"Paramon!";
    
    params.caption = @"Great Game";
    params.picture = [NSURL URLWithString:@"http://s27.postimg.org/n403qdddf/logo_top.png"];
    params.description = _scoreShare;
    
    
    // If the Facebook app is installed and we can present the share dialog
    if ([FBDialogs canPresentShareDialogWithParams:params]) {
        
        // Present share dialog
        [FBDialogs presentShareDialogWithLink:params.link
                                         name:params.name
                                      caption:params.caption
                                  description:params.description
                                      picture:params.picture
                                  clientState:nil
                                      handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
                                          if(error) {
                                              // An error occurred, we need to handle the error
                                              // See: https://developers.facebook.com/docs/ios/errors
                                              //  NSLog([NSString stringWithFormat:@"Error publishing story: %@", error.description]);
                                          } else {
                                              // Success
                                              NSLog(@"result %@", results);
                                          }
                                      }];
        
        // If the Facebook app is NOT installed and we can't present the share dialog
    } else {
        // FALLBACK: publish just a link using the Feed dialog
        
        // Put together the dialog parameters
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       @"Paramon!", @"name",
                                       @"Great Game", @"caption",
                                       _scoreShare, @"description",
                                       @"http://cyberia.net.in/cameras/pou/tamagochi/", @"link",
                                       @"http://s27.postimg.org/n403qdddf/logo_top.png", @"picture",
                                       nil];
        
        // Show the feed dialog
        [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                               parameters:params
                                                  handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                      if (error) {
                                                          // An error occurred, we need to handle the error
                                                          // See: https://developers.facebook.com/docs/ios/errors
                                                          // NSLog([NSString stringWithFormat:@"Error publishing story: %@", error.description]);
                                                      } else {
                                                          if (result == FBWebDialogResultDialogNotCompleted) {
                                                              // User canceled.
                                                              NSLog(@"User cancelled.");
                                                          } else {
                                                              // Handle the publish feed callback
                                                              NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                                                              
                                                              if (![urlParams valueForKey:@"post_id"]) {
                                                                  // User canceled.
                                                                  NSLog(@"User cancelled.");
                                                                  
                                                              } else {
                                                                  // User clicked the Share button
                                                                  NSString *result = [NSString stringWithFormat: @"Posted story, id: %@", [urlParams valueForKey:@"post_id"]];
                                                                  NSLog(@"result %@", result);
                                                              }
                                                          }
                                                      }
                                                  }];
    }

}
- (IBAction)facebookShare:(id)sender {
     if ([stateTamagochi soundOn])
         [[SimpleAudioEngine sharedEngine] playEffect:@"share.wav" loop:NO];
    if ([self checkConnection]!=0)
        [self facebookViewCreate];
    else
    {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"attention", nil)
                                                    message:NSLocalizedString(@"no_internet_connection", nil)
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles: nil];
        [alert show];
        [alert release];
    }
}
- (void)dealloc {
    [[SimpleAudioEngine sharedEngine] unloadEffect:@"share.wav"];
    [_lab_Share_Title release];
    [_btn_CloseShare release];
    [super dealloc];
}
@end
