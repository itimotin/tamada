//
//  PouNode.m
//  Tamagochi
//
//  Created by Sergiu Moțipan on 6/13/13.
//
//

#import "PouNode.h"

static PouNode *stateManager=nil;
@implementation PouNode
@synthesize delegate;


+(id)sharedInstance
{
    
    if (!stateManager)
        stateManager = [[super allocWithZone:NULL] init];
    return stateManager;
}
-(PouNode*)obj
{
    return self;
}

- (void)performBlock:(void (^)(void))block afterDelay:(NSTimeInterval)delay
{
    int64_t delta = (int64_t)(1.0e9 * delay);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delta), dispatch_get_main_queue(), block);
}

- (UIImage*)imageByCombiningImage:(UIImage*)firstImage withImage:(UIImage*)secondImage alpha:(float)alpha{
    UIImage *image = nil;
    
    CGSize newImageSize = CGSizeMake(MAX(firstImage.size.width, secondImage.size.width), MAX(firstImage.size.height, secondImage.size.height));
    if (UIGraphicsBeginImageContextWithOptions != NULL) {
        UIGraphicsBeginImageContextWithOptions(newImageSize, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(newImageSize);
    }
    
    [firstImage drawAtPoint:CGPointMake(roundf((newImageSize.width-firstImage.size.width)/2),
                                        roundf((newImageSize.height-firstImage.size.height)/2)) blendMode:kCGBlendModeOverlay alpha:1.0];
    [secondImage drawAtPoint:CGPointMake(roundf((newImageSize.width-secondImage.size.width)/2),
                                         roundf((newImageSize.height-secondImage.size.height)/2)) blendMode:kCGBlendModeMultiply alpha:alpha];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

-(void)addMoney:(NSInteger)summ
{
    [[SimpleAudioEngine sharedEngine] playEffect:@"coin.wav" loop:NO];
    CCSprite *money=[CCSprite spriteWithFile:[NSString stringWithFormat:@"money_4%@.png", (IS_IPAD)?@"-iPad":@""]];
    money.scale=0.5+0.5*IS_RETINA;
    money.position=_tamagochi.position;
    [self addChild:money z:20];
    [money runAction:[CCMoveTo actionWithDuration:0.5 position:CGPointMake(-WIDTH_DEVICE*(1.8-self.scale), HEIGHT_DEVICE*(2.3-self.scale))]];
    [self performBlock:^{ [money removeFromParentAndCleanup:YES];} afterDelay:0.6];
    [stateTamagochi setMoneyGame:[stateTamagochi moneyGame]+summ];
    [stateTamagochi updateMoney];
}

-(void)addExplosionR:(GLfloat)r g:(GLfloat)g b:(GLfloat)b a:(GLfloat)a
{
    emmiter=[[[CCParticleExplosion alloc] init] autorelease];
    emmiter.texture=[[CCTextureCache sharedTextureCache] addImage:@"fireworks.png"];
    emmiter.startColor=ccc4f(r/255.0, g/255.0, b/255.0, a/255.0);
    emmiter.position=ccp(_tamagochi.position.x, _tamagochi.position.y);
    emmiter.life=1.0;
    [self addChild:emmiter z:20];
}
-(void)setPositionByIndex:(NSInteger)index
{
    switch (index) {
        case 2:
        {
            _gene.position = CGPointMake(-[_eye boundingBox].size.width/2+[_gene boundingBox].size.width/2.2, [_eye boundingBox].size.height-[_gene boundingBox].size.height/2+50*IS_IPAD);
            genePosX=_gene.position.x;
            genePosY=_gene.position.y;
        }
            break;
        case 4:
        {
            
            _palarii.position = CGPointMake(-4, _tamSize.height/1.35 -[_palarii boundingBox].size.height/1.6+50*IS_IPAD);
            palariiPosX=_palarii.position.x;
            palariiPosY=_palarii.position.y;
        }
            break;
        case 5:
        {
           
            _haine.position=CGPointMake(0, [_tamagochi boundingBox].origin.y+[_haine boundingBox].size.height/2-1+50*IS_IPAD);
            haineHeight=_haine.boundingBox.size.height;
        }
            break;
        case 9:
        {
           _coafura.position = CGPointMake(-4-10*IS_IPAD, _tamSize.height/1.85-[_coafura boundingBox].size.height/3+80*IS_IPAD);
            coafuraPosX=_coafura.position.x;
            coafuraPosY=_coafura.position.y;
        }
            break;
        default:
            break;
    }
}

-(void)randomPositionTatoo
{
    
    int lowerBound1 = (int)(_tamagochi.boundingBox.origin.x);
    int upperBound1 = (int)_tamagochi.boundingBox.size.width;
    int x1 = lowerBound1 + arc4random() % (upperBound1 - lowerBound1);
    int lowerBound2 = (int)(_tamagochi.boundingBox.origin.y);
    int upperBound2 = (int)_tamagochi.boundingBox.size.height;
    int y1 = lowerBound2 + arc4random() % (upperBound2 - lowerBound2);
    if (CGPathContainsPoint(path, NULL,CGPointMake(x1, y1), NO) && !CGRectContainsPoint([_eye boundingBox], CGPointMake(x1, y1)))
    {
        found=YES;
        foundPos=CGPointMake(x1, y1);
    }
    else
        found=NO;
    
}

-(void)createTatooChild:(NSInteger)tag nameTexture:(NSString *)name position:(CGPoint)point
{
    found=NO;
    _tatoo = [CCSprite spriteWithFile:name];
    _tatoo.position = point;
    _tatoo.tag=tag;
    [self addChild:_tatoo z:1];
    tatooPos[tag-21]=point;
  
    if (CGPointEqualToPoint(point, CGPointZero))
    {
        int i=0;
        while (found==NO && i<100) {
            i++;
            [self randomPositionTatoo];
        }
        if (i==100)
            tatooPos[tag-21]=CGPointMake(-10.0, -30.0);
        tatooPos[tag-21]=foundPos;
    }
}

-(void)setPositionTatoo:(int)index position:(CGPoint)point
{
    tatooPos[index-21]=point;
}

-(CGPoint)getPositionTatoo:(int)index
{
    return tatooPos[index-21];
}
-(void)createChildByTag:(NSInteger)tag nameTexture:(NSString *)name
{
   
    switch (tag) {
        case 2:
        {
            _gene = [CCSprite spriteWithFile:name];
            _gene.scale=0.45+0.45*IS_RETINA;
            _gene.position = CGPointMake(-[_eye boundingBox].size.width/2+[_gene boundingBox].size.width/2.2, [_eye boundingBox].size.height-[_gene boundingBox].size.height/2+50*IS_IPAD);
            _gene.tag=tag;
            [self addChild:_gene z:1];
            genePosX=_gene.position.x;
            genePosY=_gene.position.y;
        }
            break;
        case 5:
        {
            _haine = [CCSprite spriteWithFile:name];
            _haine.position = CGPointMake(0, [_tamagochi boundingBox].origin.y-[_haine boundingBox].origin.y-1+50*IS_IPAD);
            _haine.scale=0.5+0.5*IS_RETINA;
            _haine.scaleX=[stateTamagochi fat];
            _haine.tag=tag;
            [self addChild:_haine z:6];
            haineHeight=_haine.boundingBox.size.height;

        }
            break;
        case 4:
        {
            _palarii = [CCSprite spriteWithFile:name];
            
            _palarii.position = CGPointMake(-4, _tamSize.height/1.35 -[_palarii boundingBox].size.height/1.6+50*IS_IPAD);
            _palarii.scale=0.5+0.5*IS_RETINA;
            _palarii.tag=tag;
            [self addChild:_palarii z:8];
            palariiPosX=_palarii.position.x;
            palariiPosY=_palarii.position.y;
        }
            break;
        case 7:
        {
            _mustete = [CCSprite spriteWithFile:name];
            _mustete.position = CGPointMake(-3, -25);
            _mustete.tag=7;
            [self addChild:_mustete z:3];
            mustetePosX=_mustete.position.x;
            mustetePosY=_mustete.position.y;
        }
            break;
        case 8:
        {
            _glasses = [CCSprite spriteWithFile:name];
            _glasses.position = CGPointMake(0, 28.5);
            _glasses.tag=tag;
            [self addChild:_glasses z:7];
            glassesPosX=_glasses.position.x;
            glassesPosY=_glasses.position.y;
        }
            break;
        case 9:
        {
            _coafura = [CCSprite spriteWithFile:name];
            _coafura.scale=0.5+0.5*IS_RETINA;
            _coafura.position = CGPointMake(-4-10*IS_IPAD,  _tamSize.height/1.85-[_coafura boundingBox].size.height/3+80*IS_IPAD);
            _coafura.tag=tag;
            [self addChild:_coafura z:6];
            coafuraPosX=_coafura.position.x;
            coafuraPosY=_coafura.position.y;
        }
            break;
        case 10:
        {
            _gura=[RotNode node];
            [_gura setTexture:[[CCTextureCache sharedTextureCache] addImage:name]];
            _gura.tag=tag;
            _gura.position = CGPointMake(-4, -50);
            guraPosX=_gura.position.x;
            guraPosY=_gura.position.y;
            [self addChild:_gura z:2];
        }
            break;
        case 11:
        {
            _urechi = [CCSprite spriteWithFile:name];
            _urechi.position = CGPointMake(0, 45);
            _urechi.scale=0.4+0.4*IS_RETINA;
            _urechi.tag=tag;
            [self addChild:_urechi z:-1];
            urechiPosX=_urechi.position.x;
            urechiPosY=_urechi.position.y;
        }
            break;

        case 15:
        {
            _termometer= [CCSprite spriteWithFile:name];
            _termometer.position = CGPointMake(-67, 0);
            _termometer.scale=0.35;
            _termometer.tag=15;
            [self addChild:_termometer z:10];
            termPosX=_termometer.position.x;
            termPosY=_termometer.position.y;
            [[CCTextureCache sharedTextureCache] removeTextureForKey:@"termometr.png"];
        }
            break;
            
        case 18:
        {
            _cashti = [CCSprite spriteWithFile:[NSString stringWithFormat:@"cashti%@.png", (IS_IPAD)?@"-iPad":@""]];
            [_cashti setScale:0.5+0.5*IS_RETINA];
            _cashti.position = CGPointMake(0, 50+120*IS_IPAD);
            _cashti.tag=18;
            [self addChild:_cashti z:9];
            cashtiPosX=_cashti.position.x;
            cashtiPosY=_cashti.position.y;
        }
            break;
            
        default:
            break;
    }
}

-(void) loadAssetsThenGotoMainMenu
{
    dirt=[CCSprite spriteWithFile:@"dirt.png"];
    dirt.scale=0.5+0.5*IS_RETINA;
    dirt.position = CGPointMake(_tamagochi.boundingBox.size.width/2, _tamagochi.boundingBox.size.height/2);
    dirt.tag=39;
    [dirt runAction:[CCFadeOut actionWithDuration:0]];
    [_tamagochi addChild:dirt z:2];
    
    umed=[CCSprite spriteWithFile:@"ud.png"];
    umed.scale=0.5+0.5*IS_RETINA;
    umed.position = CGPointMake(_tamagochi.boundingBox.size.width/2, _tamagochi.boundingBox.size.height/2);
    umed.tag=40;
    [umed runAction:[CCFadeOut actionWithDuration:0]];
    [_tamagochi addChild:umed z:1];
    
    _eye_center = [CCSprite spriteWithFile:[NSString stringWithFormat:@"ochi_centru_7%@.png", (IS_IPAD)?@"-iPad":@""]];
    _eye_center.scale=0.5+0.5*IS_RETINA;
    _eye_center.position = CGPointMake(-2, 24+34*IS_IPAD);
    _eye_center.tag=1;
    [self addChild:_eye_center z:3];
    eye_centerX=_eye_center.position.x;
    eye_centerY=_eye_center.position.y;
  
    
    _eye = [CCSprite spriteWithFile:[NSString stringWithFormat:@"contur_ochi_7%@.png", (IS_IPAD)?@"-iPad":@""]];
    _eye.position = CGPointMake(-2, 30+50*IS_IPAD);
    _eye.scale=0.45+0.45*IS_RETINA;
    _eye.tag=3;
    [self addChild:_eye z:2];
    eye_posX=_eye.position.x;
    eye_posY=_eye.position.y;
    
  
}

- (id) init {
    self = [super init];
    if (self) {
     
        [[SimpleAudioEngine sharedEngine] preloadEffect:@"coin.wav"];
        
        curentPage=1;
        winSize = [[CCDirector sharedDirector] winSize];
        
        _tamagochi = [CCSprite spriteWithFile:[NSString stringWithFormat:@"corpul_10%@.png", (IS_IPAD)?@"-iPad":@""]];
        _tamagochi.scale=0.5+0.5*IS_RETINA;
        _tamSize=[_tamagochi boundingBox].size;
        _tamagochi.position = CGPointMake(0, 0);
        _tamagochi.tag=6;
        [self addChild:_tamagochi z:1];
        
        _pleopUp = [CCSprite spriteWithFile:[NSString stringWithFormat:@"yeylid_up_10%@.png", (IS_IPAD)?@"-iPad":@""]];
        _pleopUp.position = CGPointMake(-1.5, 70);
        _pleopUp.tag=16;
        [self addChild:_pleopUp z:5];
        pleopUpX=_pleopUp.position.x;
        pleopUpY=_pleopUp.position.y;
        
        _pleopDown = [CCSprite spriteWithFile:[NSString stringWithFormat:@"yeylid_down_10%@.png", (IS_IPAD)?@"-iPad":@""]];
        _pleopDown.position = CGPointMake(-1.5, -12.0);
        _pleopDown.tag=17;
        [self addChild:_pleopDown z:4];
        
        jos=YES;
        pleopDownX=_pleopDown.position.x;
        pleopDownY=_pleopDown.position.y;
        pleopDownHeight=_pleopDown.textureRect.size.height;
        pleopDownOriginY=_pleopDown.textureRect.origin.y;
      
        [self pleopDownMove:YES];
        touchObject=1;
        
       
        
        path=CGPathCreateMutable();
        CGPathMoveToPoint(path, NULL, _tamagochi.boundingBox.origin.x+50, _tamagochi.boundingBox.origin.y+25);
        CGPathAddLineToPoint(path, NULL, _tamagochi.boundingBox.origin.x+120, _tamagochi.boundingBox.origin.y+160);
        CGPathAddLineToPoint(path, NULL, _tamagochi.boundingBox.origin.x+190, _tamagochi.boundingBox.origin.y+25);
        CGPathCloseSubpath(path);
        
        [self loadAssetsThenGotoMainMenu];
        [self createChildByTag:10 nameTexture:@"buze_03_4.png"];
        [self scheduleUpdate];
        
    }
    return self;
}

-(void)sufferingEye
{
   
    id sa=[CCScaleTo actionWithDuration:0.1 scale:_eye_center.scale+0.05];
    id sa1=[CCScaleTo actionWithDuration:0.1 scale:_eye_center.scale-0.05];
   // id Runleft = [CCMoveTo actionWithDuration:0.05 position:ccp(_eye_center.position.x+1.0, _eye_center.position.y-0.5)];
   // id RunRight = [CCMoveTo actionWithDuration:0.05 position:ccp(_eye_center.position.x-1.0, _eye_center.position.y+0.5)];
    [_eye_center runAction:[CCRepeatForever actionWithAction:[CCSequence actions:sa, sa1,nil]]];
    //[_eye_center runAction:scale1];
}

-(void)pleopDownMove:(BOOL)sus
{
    switch (sus) {
        case 0:
        {
            id action1;
            id action2;
            if ([stateTamagochi eyeState]==eyeStateShower)
            {
                action1=[CCScaleBy actionWithDuration:0.1 scaleX:0.9 scaleY:0.6];
                action2=[CCScaleBy actionWithDuration:0.1 scaleX:1.0 scaleY:0.7];
            }
            else
            {
                action1=[CCScaleBy actionWithDuration:0.1 scaleX:0.6 scaleY:0];
                action2=[CCScaleBy actionWithDuration:0.1 scaleX:0.6 scaleY:0];
            }
            [_pleopDown runAction:action1];
            [_pleopUp runAction:action2];
            
        }
            break;
        case 1:
        {
            
            id action1=[CCScaleTo actionWithDuration:0.1 scaleX:1.0 scaleY:1.0];
            id action2=[CCScaleTo actionWithDuration:0.1 scaleX:1.0 scaleY:1.0];
            [_pleopDown runAction:action1];
            [_pleopUp runAction:action2];
        }
            break;
            
        default:
            break;
    }
    jos=!sus;
}

-(void)rotMoveDownUp
{
    switch ([stateTamagochi mouthState]) {
        case mouthStateDefault:
        {
            id actionDown=[CCScaleBy actionWithDuration:0.3 scaleX:1.0 scaleY:0.1];
            id actionUp=[CCScaleTo actionWithDuration:0.5 scaleX:1.0 scaleY:1.0];
            id actionKombi=[CCSequence actions:actionUp,actionDown, nil];
            [_rot runAction:actionKombi];
        }
            break;
        case mouthStateEat:
        {
            id actionDown=[CCScaleBy actionWithDuration:0.3 scaleX:1.0 scaleY:0.1];
            id actionUp=[CCScaleTo actionWithDuration:0.5 scaleX:1.0 scaleY:1.0];
            id actionKombi=[CCSequence actions:actionUp,actionDown, nil];
            [_rot runAction:actionKombi];

        }
            break;
        case mouthStateReadyEat:
        {
            id actionUp=[CCScaleTo actionWithDuration:0.1 scaleX:1.0 scaleY:1.0];
            [_rot runAction:actionUp];
            
        }
            break;
        default:
            break;
    }
}
- (void)update:(float)dt
{
   [[NSNotificationCenter defaultCenter] postNotificationName:@"updateStates" object:nil];
    
    totalTime += dt;
    slowEffect+=0.25;
    x=[delegate returnPosition].x;
    y=[delegate returnPosition].y;
    
    if (y>485)
    {
        y=485;
    }

    if (x!=0)
    {
        xx=(x-self.position.x)/8*slowEffect;
    }
    else
    {
        slowEffect-=0.25;
        xx=xx*slowEffect;
    }
    if (y!=0)
    {
        yy=(y-self.position.y)/14*slowEffect;
    }
    else
    {
        slowEffect-=0.25;
        yy=yy*slowEffect;
    }
    
    _tamagochi.skewX=xx/7;
    _tamagochi.skewY=yy/10;
    _tamagochi.position=ccp((xx)/3, _tamagochi.position.y);
    _eye_center.position=ccp(eye_centerX+xx*1.3, eye_centerY+yy);
    _eye.position=ccp(eye_posX+xx/1.5, eye_posY+yy/2.5);
    
    if ([self getChildByTag:2])
    {
         _gene.position=ccp(genePosX+xx/1.5, genePosY+yy/2.5);
    }
    
    if ([self getChildByTag:5])
    {
        _haine.skewX=xx/7;
        _haine.skewY=yy/10;
        _haine.position=ccp((xx)/(5.5-haineHeight/83.0), _haine.position.y);
    }
    
    if ([self getChildByTag:4])
    {
          _palarii.position=ccp(palariiPosX+xx/2, palariiPosY+yy/20);
    }
 
    if ([self getChildByTag:7])
    {
        _mustete.position=ccp(mustetePosX+xx/1.5, mustetePosY+yy/2.5);
    }
    
    if ([self getChildByTag:8])
    {
       _glasses.position=ccp(glassesPosX+xx/1.5, glassesPosY+yy/2.5);
    }
    
    if ([self getChildByTag:9])
    {
        _coafura.position=ccp(coafuraPosX+xx/2, coafuraPosY+yy/20);
    }
    if ([self getChildByTag:15])
    {
        _termometer.position=ccp(termPosX+xx/1.5, termPosY+yy/2.5);
    }

    for (int i=22;i<=32;i++)
    {
         if ([self getChildByTag:i] )
              [self getChildByTag:i].position=ccp(tatooPos[i-21].x+xx/1.5, tatooPos[i-21].y+yy/2.5);
    }
  
    if ([self getChildByTag:11])
    {
        _urechi.position=ccp(urechiPosX+xx/2.5, urechiPosY+yy/2.5);
    }
    
    
    if ([self getChildByTag:18])
    {
          _cashti.position=ccp(cashtiPosX+xx/2.5, cashtiPosY+yy/3.5);
    }
    
     [_gura setSkewX:xx/5];
     [_gura setSkewY:yy/20];
    _rot.position=ccp(rotX+xx/1.5, rotY+yy/2.5);
    _gura.position=ccp(guraPosX+xx/1.5, guraPosY+yy/2.5);
    
    if (slowEffect<=0.0)
    {
        slowEffect=0.0;
    }
    
    if (slowEffect>=1.0)
    {
        slowEffect=1.0;
    }
    
    if ([stateTamagochi eyeState]==eyeStateErase && jos)
    {
         [self pleopDownMove:YES];
    }
    
    if (jos && [stateTamagochi eyeState]!=eyeStateErase)
    {
        if (totalTime>=3.0)
        {
            totalTime=0.0;
            [self pleopDownMove:jos];
            [self rotMoveDownUp];
        }
    }
    else if ([stateTamagochi eyeState]!=eyeStateErase)
    {
        if (totalTime>=0.2)
        {
            totalTime=0.0;
            [self pleopDownMove:jos];
        }
    }
 
    _pleopUp.position=ccp(pleopUpX+xx/1.5, pleopUpY+yy/2.5-_pleopUp.boundingBox.size.height/2);
    _pleopDown.position=ccp(pleopDownX+xx/1.5, pleopDownY+yy/2.5+_pleopDown.boundingBox.size.height/2);

}

-(void)dealloc
{
        [super dealloc];
}
@end
