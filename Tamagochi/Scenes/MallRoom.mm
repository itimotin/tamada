//
//  FirstScene.m
//  Tamagochi
//
//  Created by Sergiu Moțipan on 6/4/13.
//
//

#import "MallRoom.h"
#import "InMallRoom.h"

@implementation MallRoom
{
    int touchObject;
    int timp;
    int timp_2;
    int mouse;
    int oche_mouse;
    int n_mouse;
    int eye_pos;
    float totalTime,yy,xx, tamTouch;
    BOOL repeatTouches;
    float limit;
    CCSprite *mall;

   
}



+(CCScene *) scene
{
	CCScene *scene = [CCScene node];
	MallRoom *layer = [MallRoom node];
	[scene addChild: layer];
	return scene;
}


-(id)init
{
    self=[super init];
    if (self)
    {

         [[SimpleAudioEngine sharedEngine] stopAllEffects];
        [[SimpleAudioEngine sharedEngine] setEffectsVolume:[stateTamagochi soundOn]];
        [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume:[stateTamagochi soundOn]];
        [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
        [stateTamagochi setRoom:mallRoom];
        mouth=[stateTamagochi mouthState];
      
        sound=[[SimpleAudioEngine sharedEngine] playEffect:@"mall.wav" loop:YES];
        [[SimpleAudioEngine sharedEngine] setEffectsVolume:[stateTamagochi soundOn]];
        [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume:[stateTamagochi soundOn]];
        
        winSize = [[CCDirector sharedDirector] winSize];
        self.isTouchEnabled=YES;
        
        CCSprite *background = [CCSprite spriteWithFile:[NSString stringWithFormat:@"bg_mall%@.png",IMG_PREFIX]];
        background.position = ccp(winSize.width/2, winSize.height/2+20);
        [background setScale:0.5+0.5*IS_RETINA];
        [self addChild:background z:-12];
        
        CCSprite *background1 ;
        if (!IS_IPAD)
        {
            background1  = [CCSprite spriteWithFile:@"bg_mall1.png"];
            background1.position = ccp(winSize.width/2, background1.boundingBox.size.height/2);
        }
        else
        {
            background1  = [CCSprite spriteWithFile:@"bg_mall1-iPad.png"];
            [background1 setScale:0.5+0.5*IS_RETINA];
            background1.position = ccp(winSize.width/2, background1.boundingBox.size.height/2-5);
        }
        
        
        [self addChild:background1 z:-10];
        
        if (!IS_IPAD)
        {
            mall = [CCSprite spriteWithFile:@"mall.png"];
            mall.position = ccp(winSize.width/2, winSize.height/2+38);
        }
        else
        {
            mall = [CCSprite spriteWithFile:@"mall-iPad.png"];
            [mall setScale:0.5+0.5*IS_RETINA];
            mall.position = ccp(winSize.width/2,winSize.height/2+93);
        }
        
       
    
        [self addChild:mall z:-11];
    
        id action1;
        id action2;
        if (!IS_RETINA)
        {
            action1 = [CCScaleTo actionWithDuration:0.4 scale:0.51];
            action2 = [CCScaleTo actionWithDuration:0.4 scale:0.5];
        }
        else
        {
            action1 = [CCScaleBy actionWithDuration:0.4 scale:1.05];
            action2  = [CCScaleTo actionWithDuration:0.4 scale:1.0];
        }
        id  animationObjects = [CCSequence actions:action1, action2, nil];
        [mall runAction:[CCRepeatForever actionWithAction:animationObjects]];
        
        [personaj person].position = ccp(winSize.width/2-10*[stateTamagochi sizeTamagochi], 100+80*[stateTamagochi sizeTamagochi]+200*IS_IPAD);
        [personaj initRects];
        [personaj person].delegate=self;
        [self addChild:[personaj person] z:1];
        
        [stateTamagochi setTouchObject:tagDefault];
        [self scheduleUpdate];
    }
    
    return self;
}


- (void)update:(float)dt
{
 
}

-(CGPoint)returnPosition
{
    return CGPointMake(xx, yy);
}

-(float)returnTouchValue
{
    switch ([stateTamagochi mouthState]) {
        case mouthStateFun:
        {
            return tamTouch*sin(totalTime);
        }
            break;
        case mouthStateReadyEat:
        {
            return 0.3;
        }
            break;
        case mouthStateEat:
        {
            return 0.25*sin(totalTime);
        }
            break;
        case mouthStateDefault:
        {
            return 0.25*sin(totalTime);
        }
            break;
            
        default:return tamTouch;
            break;
    }
}


-(void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    nr_sounds=0;
    UITouch* touch = [touches anyObject];
    CGPoint location = [touch locationInView: [touch view]];
    startTouchPoint=location;
    date_start=[[NSDate date] retain];
    location = [[CCDirector sharedDirector] convertToGL:location];
    xx=location.x;
    yy=location.y;
    touchFun=0;
    if (CGRectContainsPoint( [ mall   boundingBox], location) )
    {
         [stateTamagochi setTouchObject:tagMall];
    }
    if (CGRectContainsPoint([personaj rectPerson],location))
    {
        [stateTamagochi setTouchObject:tagTamagochi];
    }
}

-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch* touch = [touches anyObject];
    CGPoint location = [touch locationInView: [touch view]];
    location = [[CCDirector sharedDirector] convertToGL:location];

    if ([stateTamagochi touchObject]==tagTamagochi && CGRectContainsPoint([personaj rectPerson],location))
    {
        [stateTamagochi setMouthState:mouthStateFun];
        touchFun+=0.006;
        if (touchFun>=0.05)
        {
            if (!repeatTouches)
            [self fun];
        }
        nr_sounds++;
    }
  
    xx=location.x;
    yy=location.y;
}
-(void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [stateTamagochi setMouthState:mouth];
    UITouch* touch = [touches anyObject];
    CGPoint location = [touch locationInView: [touch view]];
    
    endTouchPoint=location;
    float moveDifference=startTouchPoint.x-endTouchPoint.x;
    float differenceY=startTouchPoint.y-endTouchPoint.y;
    if (differenceY<0) differenceY*=-1;
    NSTimeInterval secondsBetween = [[NSDate date] timeIntervalSinceDate:date_start];
    [date_start release];
    date_start=nil;
    if (moveDifference>150.0 && differenceY<40.0 && endTouchPoint.y<150.0 && secondsBetween<0.3)
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"nextRoom" object:@"1"];
        return;
    }
    else if (moveDifference<-150.0 && differenceY<40.0 && endTouchPoint.y<150.0 && secondsBetween<0.3)
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"nextRoom" object:@"2"];
        return;
    }
    
    location = [[CCDirector sharedDirector] convertToGL:location];
    if (CGRectContainsPoint( [ mall   boundingBox], location) &&  [stateTamagochi touchObject]==tagMall)
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"renameRoom" object:@"InMall_room"];
        [[personaj person] removeFromParentAndCleanup:NO];
        [[CCDirectorIOS sharedDirector] replaceScene:[InMallRoom scene]];
    }
    nr_sounds=0;
    touchFun=0;
    xx=0;
    yy=0;
    touchObject=tagDefault;
}

-(void)changePosition:(CGPoint)pos
{
    xx=pos.x;
    yy=pos.y;
}

#pragma Tamagochi effects

-(void)resetVariables
{
    repeatTouches=NO;
    touchFun=0;
    if ([[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying])
        [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
}

-(void)fun
{
    repeatTouches=YES;
    totalTime=0;
    tamTouch=0.3;
    if (![[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying] &&  [stateTamagochi soundOn])
        [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"giggle.wav" loop:NO];
    [self performSelector:@selector(resetVariables) withObject:nil afterDelay:1.57];
}
-(void)dealloc
{
    if ([[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying])
        [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
    [[SimpleAudioEngine sharedEngine] stopEffect:sound];
    [super dealloc];
}

@end
