//
//  Personaj.h
//  Tamagochi
//
//  Created by Sergiu Moțipan on 9/30/13.
//
//

#import <Foundation/Foundation.h>
#import "PouNode.h"

@interface Personaj : NSObject
{
   
}

@property (nonatomic, retain)  PouNode *person;
@property (nonatomic)  CGRect rectPerson;
@property (nonatomic, retain) NSMutableArray  *bodyShopped;
@property (nonatomic, retain) NSMutableArray  *tatooShopped;
-(void)replaceTexturePerson:(NSInteger)index obj:(NSString *)str;
-(void)replaceTextureDefault:(NSInteger)index;
-(void)replaceTatooPerson:(NSString*)obj;
-(void)replaceTatooDefault:(NSString*)str;
-(void)deleteTatoo:(NSString *)str;
-(void)replaceDefaultAllBody;
+(id)sharedInstance;
-(void)initRects;
-(void)setScale:(float)scale;
-(void)repositioningTatoo;
-(void)replaceDefaultAllTatoo;
@end
