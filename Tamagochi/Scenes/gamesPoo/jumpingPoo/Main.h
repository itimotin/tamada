#import "cocos2d.h"

//#define RESET_DEFAULTS

#define kFPS 60

#define kNumClouds			12

#define kMinPlatformStep	50
#define kMaxPlatformStep	(WIDTH_DEVICE/2+50)
#define kNumPlatforms		10
#define kOrdCoin            20
#define kPlatformTopPadding 10

#define kMinBonusStep		20
#define kMaxBonusStep		30
#define kMinCoin            40
#define kMaxCoin            55

#define kBackgroundFirst 1111


enum {
	kSpriteManager = 0,
	kPoo,
	kScoreLabel,
	kCloudsStartTag = 100,
	kPlatformsStartTag = 200,
	kBonusStartTag = 300,
    kCoinBonusTag = 400
};

enum {
	kBonus5 = 0,
	kBonus10,
	kBonus50,
	kBonus100,
	kNumBonuses
};

@interface Main : CCLayer
{
	int currentCloudTag;
}


- (void)resetClouds;
- (void)resetCloud;
- (void)step:(ccTime)dt;
@end