//
//  Hero.h
//  gamesPoo
//
//  Created by iVanea! on 6/25/13.
//  Copyright (c) 2013 iVanea!. All rights reserved.
//

#import "CCSprite.h"
#import "Box2D.h"

@interface Hero : CCSprite

- (id)initWithWorld:(b2World *)world;
- (void)update;

@property (readonly) BOOL awake;
- (void)wake;
- (void)dive;
- (void)limitVelocity;

- (void)nodive;
- (void)runForceAnimation;
- (void)runNormalAnimation;
@end