//
//  TinyPoo.m
//  Tamagochi
//
//  Created by iVanea! on 9/26/13.
/************************************************************
 *                                                           *
 *  .=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-.       *
 *   |                     ______                     |      *
 *   |                  .-"      "-.                  |      *
 *   |                 /            \                 |      *
 *   |     _          |              |          _     |      *
 *   |    ( \         |,  .-.  .-.  ,|         / )    |      *
 *   |     > "=._     | )(__/  \__)( |     _.=" <     |      *
 *   |    (_/"=._"=._ |/     /\     \| _.="_.="\_)    |      *
 *   |           "=._"(_     ^^     _)"_.="           |      *
 *   |               "=\__|IIIIII|__/="               |      *
 *   |              _.="| \IIIIII/ |"=._              |      *
 *   |    _     _.="_.="\          /"=._"=._     _    |      *
 *   |   ( \_.="_.="     `--------`     "=._"=._/ )   |      *
 *   |    > _.="                            "=._ <    |      *
 *   |   (_/                                    \_)   |      *
 *   |                                                |      *
 *   '-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-='      *
 *                                                           *
 *        WARNING!!! Very perversive code ahead!             *
 *************************************************************/
//


#import "TinyPoo.h"
#import "Terrain.h"
#import "Hero.h"
#import "SimpleAudioEngine.h"
//#import "Highscores.h"
#import "Enemy.h"

@interface TinyPoo() {
    Terrain * _terrain;
    NSMutableArray* highscores;
    CCSprite * _background,* _background1;
    Hero * _hero;
    Enemy *_enemyAngry;
    Enemy *_energyBird;
    Enemy *_eficientBot;
    Enemy *bossHuiSos;
    
    BOOL _tapDown;
    CCSprite *obj;
    float offsetBackground;
    //    CCParticleMeteor *emiter;
    int _nPerfectSlides;
    BOOL _flying;
    BOOL _flyingEnemyAngry;
    BOOL _wasChangedBackgr;
    
    bool _wasStoped ;
    BOOL a_wasStoped ;
    BOOL b_wasStoped ;
    BOOL c_wasStoped ;
    BOOL boss_wasStoped;
    
    CCSprite *sprE1 ;
    CCSprite *sprE2 ;
    CCSprite *sprE3 ;
    CCSprite *sprLocationHuiSos;
    
    CCSprite *sprShadowE1 ;
    CCSprite *sprShadowE2 ;
    CCSprite *sprShadowE3 ;
    CCSprite *sprShadowBossHUIsos;
    
    CCSprite *sprNavaH ;
    CCSprite *_pauseButton;
    SimpleAudioEngine *audioengine;
    unsigned char positionStart;
    CGPoint finishPosition;
    uint_fast16_t scoreContor, coinContor;
    BOOL pause;
    BOOL bossInStage;
    int goToPositionFinish;
    NSTimer *timer;
    double time;
    BOOL timerIsRunning;
    int stopMent;
    BOOL toFirstStage,winner;
    float nextLevelAdd;
}
- (void) onDisplayEachPersonajLocation;
- (void) functionEnemyEnergy;
- (void) worldSettings:(float)dt;
- (void)showHighscoresWithScore:(uint_fast16_t)totalScore;
@end

@implementation TinyPoo
@synthesize world = _world;

+(CCScene *) sceneWithStage:(int_least8_t)stage {
	CCScene *scene = [CCScene node];
	TinyPoo *layer = [TinyPoo node];
    // TODO: change to default
    layer.currentStage = stage;
//    layer.currentStage = 10;
	[scene addChild: layer];
	return scene;
}

#pragma mark -
#pragma mark COLOR/BACKGROUND/SPRITE
- (ccColor4F)randomBrightColor {
    const int minSum = 450;
	const int minDelta = 150;
	int r, g, b, min, max;
	while (true) {
		r = arc4random_uniform(256);
		g = arc4random_uniform(256);
		b = arc4random_uniform(256);
		min = MIN(MIN(r, g), b);
		max = MAX(MAX(r, g), b);
		if (max-min < minDelta) continue;
		if (r+g+b < minSum) continue;
		break;
	}
	return ccc4FFromccc3B(ccc3(r, g, b));
}
- (void)generateBackgroundSound{
    if (bossInStage) {
        [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"game_default_3.mp3"];
    }else{
        [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"1.mp3"];
    }
    audioengine.backgroundMusicVolume = 0.7;
}
- (void)genBackground {

    [_background removeFromParentAndCleanup:YES];
    [_background1 removeFromParentAndCleanup:YES];

    //    ccColor4F bgColor = [self randomBrightColor];
    //    _background = [self spriteWithColor:bgColor textureWidth:IS_IPHONE_5 ? 1024:512 textureHeight:IS_IPHONE_5 ? 1024:512];
    //
    //    _background.position = ccp(HEIGHT_DEVICE/2, WIDTH_DEVICE/2);
    //    ccTexParams tp = {GL_LINEAR, GL_LINEAR, GL_REPEAT, GL_REPEAT};
    uint_fast16_t i;
    if (self.currentStage < 3) { i=1;}
    else if(self.currentStage < 5){i=2;}
    else if(self.currentStage < 7){i=3;}
    else if (self.currentStage <9){i=4;}
    else{i=5;}
    
    NSString *tex = [NSString stringWithFormat:@"tiny1_%i.png",i];//[self getThemeBG];
    [[CCSpriteFrameCache sharedSpriteFrameCache] removeUnusedSpriteFrames];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    _background = [CCSprite spriteWithFile:tex];
    _background.position = ccp(_background.contentSize.width*0.5f,_background.contentSize.height/2);
    [self addChild:_background z:-1];
    
    _background1 = [CCSprite spriteWithFile:tex];
    _background1.position = ccp(_background.contentSize.width+_background.contentSize.width*0.5f,_background.contentSize.height/2);

    _background1.flipX = true;
    if (bossInStage) {
        _background.flipY = _background1.flipY = true;
    }else{
        _background.flipY = _background1.flipY = false;
    }
    [self addChild:_background1 z:-1];
    
    if(IS_FOUR_INCH){
        _background.scale = 1;
    }
    
    ccColor4F color3 = [self randomBrightColor];
    ccColor4F color4 = [self randomBrightColor];
    
    const int minStripes = 3;
	const int maxStripes = 6;
	// random even number of stripes
	int nStripes = arc4random_uniform(maxStripes-minStripes)+minStripes;
	if (nStripes%2) {
		nStripes++;
	}
    
    CCSprite *stripes = [self stripedSpriteWithColor1:color3 color2:color4
                                         textureWidth:IS_IPHONE_5 ? 1024:512    textureHeight:IS_IPHONE_5 ? 1024:512 stripes:nStripes];
    ccTexParams tp2 = {GL_LINES, GL_LINES, GL_REPEAT, GL_CLAMP_TO_EDGE};
    [stripes.texture setTexParameters:&tp2];
    _terrain.stripes = stripes;
}

-(void)scrollBackground:(float)dt
{
    CGPoint pos1 = _background.position;
    CGPoint pos2 = _background1.position;
    float moveBackground = COEF_MOVE_BACKGROUND *dt;
    pos1.x -= moveBackground;
    pos2.x -= moveBackground;
    
    
    if(pos1.x <=-(_background.contentSize.width*0.5f) )
    {
        pos1.x = pos2.x + _background.contentSize.width;
    }
    
    if(pos2.x <=-(_background.contentSize.width*0.5f) )
    {
        pos2.x = pos1.x + _background.contentSize.width;
    }
    
    //    [_background setTextureRect:CGRectMake(dt * COEF_MOVE_BACKGROUND_TINY, 0, _background.contentSize.width, _background.contentSize.height)];
    //    [_background1 setTextureRect:CGRectMake(dt * COEF_MOVE_BACKGROUND_TINY, 0, _background1.contentSize.width, _background1.contentSize.height)];
    _background.position = pos1;
    _background1.position = pos2;
    
}

-(CCSprite *)spriteWithColor:(ccColor4F)bgColor textureWidth:(float)textureWidth textureHeight:(float)textureHeight {
    
    // 1: Create new CCRenderTexture
    CCRenderTexture *rt = [CCRenderTexture renderTextureWithWidth:textureWidth height:textureHeight];
    
    // 2: Call CCRenderTexture:begin
    [rt beginWithClear:bgColor.r g:bgColor.g b:bgColor.b a:bgColor.a];
    
    // 3: Draw into the texture
    
    self.shaderProgram = [[CCShaderCache sharedShaderCache] programForKey:kCCShader_PositionColor];
    CC_NODE_DRAW_SETUP();
    
    // 3: Draw into the texture
    float gradientAlpha = 0.9f;
    CGPoint vertices[4];
    ccColor4F colors[4];
    int nVertices = 0;
    
    vertices[nVertices] = CGPointMake(0, 0);
    colors[nVertices++] = (ccColor4F){0, 0, 0, 0 };
    vertices[nVertices] = CGPointMake(textureWidth, 0);
    colors[nVertices++] = (ccColor4F){0, 0, 0, 0};
    vertices[nVertices] = CGPointMake(0, textureHeight);
    colors[nVertices++] = (ccColor4F){0, 0, 0, gradientAlpha};
    vertices[nVertices] = CGPointMake(textureWidth, textureHeight);
    colors[nVertices++] = (ccColor4F){0, 0, 0, gradientAlpha};
    
    ccGLEnableVertexAttribs(kCCVertexAttribFlag_Position  | kCCVertexAttribFlag_Color);
    
    glVertexAttribPointer(kCCVertexAttrib_Position, 2, GL_FLOAT, GL_FALSE, 0, vertices);
    glVertexAttribPointer(kCCVertexAttrib_Color, 4, GL_FLOAT, GL_FALSE, 0, colors);
    glBlendFunc(CC_BLEND_SRC, CC_BLEND_DST);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, (GLsizei)nVertices);
    
    //    CCSprite *noise = [CCSprite spriteWithFile:@"SQCLDX.png"];
    //    [noise setBlendFunc:(ccBlendFunc){GL_DST_COLOR, GL_ZERO}];
    //    noise.position = ccp(textureWidth/2, textureHeight/2);
    //    [noise visit];
    
    // 4: Call CCRenderTexture:end
    [rt end];
    
    // 5: Create a new Sprite from the texture
    return [CCSprite spriteWithTexture:rt.sprite.texture];
    
}

-(CCSprite *)stripedSpriteWithColor1:(ccColor4F)c1 color2:(ccColor4F)c2 textureWidth:(float)textureWidth
                       textureHeight:(float)textureHeight stripes:(int)nStripes {
    
    // 1: Create new CCRenderTexture
    CCRenderTexture *rt = [CCRenderTexture renderTextureWithWidth:textureWidth height:textureHeight];
    ccColor4F *colors = (ccColor4F*)malloc(sizeof(ccColor4F)*nStripes*6);
    // 2: Call CCRenderTexture:begin
    [rt beginWithClear:c1.r g:c1.g b:c1.b a:c1.a];
    
    ccColor4F c;
    
    // 3: Draw into the texture
    
    // Layer 1: Stripes
    CGPoint vertices[nStripes*6];
    int nVertices = 0;
    
    float x1, x2, y1, y2, dy;//dx,
    /*
     if (arc4random_uniform(2)) {
     
     // diagonal stripes
     
     dx = (float)textureHeight*2 / (float)nStripes;
     dy = 0;
     
     x1 = -textureHeight;
     y1 = 0;
     
     x2 = 0;
     y2 = textureHeight;
     
     for (int i=0; i<nStripes/2; i++) {
     c = [self randomBrightColor];
     for (int j=0; j<2; j++) {
     for (int k=0; k<6; k++) {
     colors[nVertices+k] = c;
     }
     vertices[nVertices++] = CGPointMake(x1+j*textureHeight, y1);
     vertices[nVertices++] = CGPointMake(x1+j*textureHeight+dx, y1);
     vertices[nVertices++] = CGPointMake(x2+j*textureHeight, y2);
     vertices[nVertices++] = vertices[nVertices-3];
     vertices[nVertices++] = vertices[nVertices-3];
     vertices[nVertices++] = CGPointMake(x2+j*textureHeight+dx, y2);
     
     }
     x1 += dx;
     x2 += dx;
     }
     
     } else {
     */
    // horizontal stripes
    
    //dx = 0;
    dy = (float)textureHeight / (float)nStripes;
    
    x1 = 0;
    y1 = 0;
    
    x2 = textureHeight;
    y2 = 0;
    
    for (int i=0; i<nStripes; i++) {
        c = [self randomBrightColor];
        for (int k=0; k<6; k++) {
            colors[nVertices+k] = c;
        }
        vertices[nVertices++] =  CGPointMake(x1, y1);
        vertices[nVertices++] =  CGPointMake(x2, y2);
        vertices[nVertices++] =  CGPointMake(x1,y1+dy);
        vertices[nVertices++] = vertices[nVertices-3];
        vertices[nVertices++] = vertices[nVertices-3];
        
        vertices[nVertices++] =  CGPointMake(x2, y2+dy);
        y1 += dy;
        y2 += dy;
    }
	
    self.shaderProgram =
    [[CCShaderCache sharedShaderCache] programForKey:kCCShader_PositionColor];
    
    //    layer 2: Noise
    //    CCSprite *noise = [CCSprite spriteWithFile:@"q1.png"];
    //    [noise setBlendFunc:(ccBlendFunc){GL_DST_COLOR, GL_ZERO}];
    //    noise.position = ccp(textureWidth/2, textureHeight/2);
    //    [noise visit];
    
    // Layer 3: Stripes
    CC_NODE_DRAW_SETUP();
    glVertexAttribPointer(kCCVertexAttrib_Position, 2, GL_FLOAT, GL_FALSE, 0, vertices);
    glVertexAttribPointer(kCCVertexAttrib_Color, 4, GL_FLOAT, GL_TRUE, 0, colors);
    glDrawArrays(GL_TRIANGLES, 0, (GLsizei)nVertices);
    
    float gradientAlpha = 0.7;
    
    nVertices = 0;
    
    vertices[nVertices] = CGPointMake(0, 0);
    colors[nVertices++] = (ccColor4F){0, 0, 0, 0};
    
    vertices[nVertices] = CGPointMake(textureWidth, 0);
    colors[nVertices++] = (ccColor4F){0, 0, 0, 0};
    
    vertices[nVertices] = CGPointMake(0, textureHeight);
    colors[nVertices++] = (ccColor4F){0, 0, 0, gradientAlpha};
    
    vertices[nVertices] = CGPointMake(textureWidth, textureHeight);
    colors[nVertices++] = (ccColor4F){0, 0, 0, gradientAlpha};
    
    glVertexAttribPointer(kCCVertexAttrib_Position, 2, GL_FLOAT, GL_FALSE, 0, vertices);
    glVertexAttribPointer(kCCVertexAttrib_Color, 4, GL_FLOAT, GL_TRUE, 0, colors);
    glBlendFunc(CC_BLEND_SRC, CC_BLEND_DST);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, (GLsizei)nVertices);
    
    // layer 3: top highlight
    float borderHeight = textureHeight/3;
    float borderAlpha = 0.3f;
    nVertices = 0;
    
    vertices[nVertices] = CGPointMake(0, 0);
    colors[nVertices++] = (ccColor4F){1, 1, 1, borderAlpha};
    
    vertices[nVertices] = CGPointMake(textureWidth, 0);
    colors[nVertices++] = (ccColor4F){1, 1, 1, borderAlpha};
    
    vertices[nVertices] = CGPointMake(0, borderHeight);
    colors[nVertices++] = (ccColor4F){0, 0, 0, 0};
    
    vertices[nVertices] = CGPointMake(textureWidth, borderHeight);
    colors[nVertices++] = (ccColor4F){0, 0, 0, 0};
    
    glVertexAttribPointer(kCCVertexAttrib_Position, 2, GL_FLOAT, GL_FALSE, 0, vertices);
    glVertexAttribPointer(kCCVertexAttrib_Color, 4, GL_FLOAT, GL_TRUE, 0, colors);
    glBlendFunc(CC_BLEND_SRC, CC_BLEND_DST);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, (GLsizei)nVertices);
    
    // 4: Call CCRenderTexture:end
    [rt end];
    
    // 5: Create a new Sprite from the texture
    return [CCSprite spriteWithTexture:rt.sprite.texture];
}

#pragma mark -
#pragma mark INIT
-(id) init {
	if((self=[super init])) {
        nextLevelAdd=0.0;
        toFirstStage = NO;
        positionStart = RADIUS_POU*10;
        _wasStoped = NO;
        _flyingEnemyAngry = YES;

        sprE1 = [CCSprite spriteWithFile:@"e1.png"];
        sprE2 = [CCSprite spriteWithFile:@"e2.png"];
        sprE3 = [CCSprite spriteWithFile:@"e3.png"];
        sprLocationHuiSos = [CCSprite spriteWithFile:@"space_cowboy.png"];
        
        sprNavaH = [CCSprite spriteWithFile:@"nava.png"];
        
        sprE1.scale = sprE2.scale = sprE3.scale = 0.3f;
        sprLocationHuiSos.scale=0.4;
        sprShadowE1.scale = sprShadowE2.scale = sprShadowE3.scale = 0.7f;
        sprNavaH.scale = 0.5f;
        
        [self addChild:sprE1 z:3];
        [self addChild:sprE2 z:3];
        [self addChild:sprE3 z:3];
        [self addChild:sprLocationHuiSos z:3];
        [self addChild:sprNavaH z:3];
        _wasChangedBackgr = YES;
        
        _pauseButton = [CCSprite spriteWithFile:@"pause@2x.png"];
		[self addChild:_pauseButton z:3];
		CGSize size = _pauseButton.contentSize;
		float padding = 8;
		_pauseButton.position = ccp(HEIGHT_DEVICE-size.height/2-padding, WIDTH_DEVICE-size.width/2-padding);
        
        a_wasStoped = b_wasStoped = c_wasStoped = NO;
        

        positionWin = 0;
        CCLabelTTF *scoreLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%@: %d",NSLocalizedString(@"score", nil),scoreContor] fontName:@"Marker Felt" fontSize:(IS_IPAD)?25.0:20.0];
        [self addChild:scoreLabel z:5 tag:kScoreLabel];
        scoreLabel.position = ccp(HEIGHT_DEVICE/8, WIDTH_DEVICE - WIDTH_DEVICE/17);
        CCLabelTTF *timeLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%@: %.2f",NSLocalizedString(@"time", nil),time] fontName:@"Marker Felt" fontSize:(IS_IPAD)?25.0:20.0];
        [self addChild:timeLabel z:5 tag:kTimerLabel];
        timeLabel.position = ccp(HEIGHT_DEVICE - HEIGHT_DEVICE/6, WIDTH_DEVICE - WIDTH_DEVICE/17);
        
        CCSprite *sprBank = [CCSprite spriteWithFile:@"bank.png"];
        sprBank.position = ccp(HEIGHT_DEVICE/16, WIDTH_DEVICE - WIDTH_DEVICE/7);
        [self addChild:sprBank z:5];
        CCLabelTTF *bankLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%d",scoreContor] fontName:@"Marker Felt" fontSize:(IS_IPAD)?25.0:20.0];
        [self addChild:bankLabel z:5 tag:kBankLabel];
        bankLabel.position = ccp(HEIGHT_DEVICE/8, WIDTH_DEVICE - WIDTH_DEVICE/7);

    }
	return self;
}


- (void) onEnter {
    [super onEnter];
    winner = NO;
    [self setupWorld];
    if (self.currentStage%2==0) {
        bossInStage = YES;
    }else{
        bossInStage = NO;
    }
    
    if (bossInStage) {
        bossHuiSos.visible = NO;
    }else{
        sprShadowE1.visible = sprShadowE2.visible = sprShadowE3.visible =YES;
    }
    
    pause = YES;
    positionWin=0;
    offsetBackground = 0;
    _terrain = [[[Terrain alloc] initWithWorld:_world withStageID:self.currentStage withBoss:bossInStage] autorelease];
    [self addChild:_terrain z:1];
    
    [self generateBackgroundSound];
    [self genBackground];
    self.isTouchEnabled = YES;
    [self scheduleUpdate];
    self.scale = SCALE_GAME;
    
    
    _hero = [Hero heroWithGame:self];
    [_terrain addChild:_hero z:10];
    
    _enemyAngry = [Enemy heroWithGame:self withName:@"e1" withPosition:CGPointMake(-5*RADIUS_POU, ((IS_FOUR_INCH)?HEIGHT_DEVICE:568)/2)];
    [_terrain addChild: _enemyAngry z:9 tag:enemyTag1];
    
    _energyBird = [Enemy heroWithGame:self withName:@"e2" withPosition:CGPointMake(-3*RADIUS_POU, ((IS_FOUR_INCH)?HEIGHT_DEVICE:568)/2)];
    [_terrain addChild:_energyBird z:8 tag:enemyTag2];
    
    _eficientBot = [Enemy heroWithGame:self withName:@"e3" withPosition:CGPointMake(-1*RADIUS_POU, ((IS_FOUR_INCH)?HEIGHT_DEVICE:568)/2)];
    [_terrain addChild:_eficientBot z:7 tag:enemyTag3];
    _enemyAngry.scale = _energyBird.scale = _eficientBot.scale = 0.7;
    
    bossHuiSos = [Enemy heroWithGame:self withName:@"space_cowboy" withPosition:CGPointMake(-5*RADIUS_POU, ((IS_FOUR_INCH)?HEIGHT_DEVICE:568)/2)];
    [_terrain addChild:bossHuiSos z:6];
    bossHuiSos.scale = 0.6;
    
    if (bossInStage) {
        bossHuiSos.visible = YES;
        _enemyAngry.visible = _energyBird.visible = _eficientBot.visible = NO;
    }else{
        _enemyAngry.visible = _energyBird.visible = _eficientBot.visible = YES;
        bossHuiSos.visible = NO;
        [_terrain removeChild:bossHuiSos cleanup:YES];
    }
    finishPosition = _terrain.theDistanceOfWay;
    NSLog(@"initial value finis %f", finishPosition.x);
    goToPositionFinish = 0;
    
    sprShadowE1 = [CCSprite spriteWithFile:@"e1_.png"];
    sprShadowE2 = [CCSprite spriteWithFile:@"e2_.png"];
    sprShadowE3 = [CCSprite spriteWithFile:@"e3_.png"];
    
    sprShadowBossHUIsos = [CCSprite spriteWithFile:@"e6_.png"];
    sprShadowBossHUIsos.scaleX =sprShadowE1.scaleX = sprShadowE2.scaleX = sprShadowE3.scaleX = 0.4;
    sprShadowBossHUIsos.scaleY =sprShadowE1.scaleY = sprShadowE2.scaleY = sprShadowE3.scaleY = 10.0;
    sprShadowE3.position = sprShadowE2.position = sprShadowE1.position = sprShadowBossHUIsos.position = ccp(0,200);
    
    [_terrain addChild:sprShadowE1 z:5];
    [_terrain addChild:sprShadowE2 z:5];
    [_terrain addChild:sprShadowE3 z:5];
    [_terrain addChild:sprShadowBossHUIsos z:5];
    
    sprShadowE1.visible = sprShadowE2.visible = sprShadowE3.visible = sprShadowBossHUIsos.visible =NO;
    
    if(self.currentStage>1){
        NSDictionary *dict = [stateTamagochi getAllGameStatisticsForStageID:[NSNumber numberWithInt:(self.currentStage-1)] forPlayer:[[NSUserDefaults standardUserDefaults] objectForKey:@"player"]];
        time = [[dict objectForKey:@"TIME"] floatValue];
        scoreContor = [[dict objectForKey:@"SCORE"] integerValue];
        coinContor = [[dict objectForKey:@"COINS"] integerValue];
        
    }else{
        time = 0;
        scoreContor = 0;
        coinContor = 0;
    }
    [self setTimeLabel:time];
    [self updateOnScreenScore:scoreContor];
    
    [self showOrHidePositionForEnemies];
}

#pragma mark - UPDATE TIMER
- (void)updateTimerMain{
    if (timerIsRunning) {
        time = time+0.01;
        NSString *strTime = [self formattedStringForDuration:time];
        CCLabelTTF *timeLabel = (CCLabelTTF*)[self getChildByTag:kTimerLabel];
        [timeLabel setString:strTime];
    }

}

- (void)setTimeLabel:(double)ti{
    NSString *strTime = [self formattedStringForDuration:time];
    CCLabelTTF *timeLabel = (CCLabelTTF*)[self getChildByTag:kTimerLabel];
    [timeLabel setString:strTime];
}

- (void)setupWorld {
    b2Vec2 gravity ;
    gravity.Set(0.0f, -9.8f);
    bool doSleep = true;
    _world = new b2World(gravity);
    _world->SetAllowSleeping(doSleep);
    
    
}

#pragma mark -
- (void)showHighscoresWithScore:(uint_fast16_t)totalScore {
   
    [UIView animateWithDuration: .05
                     animations:^{
                         [[CCDirector sharedDirector] view].alpha = 0;
                     }
                     completion:^(BOOL finished){
                         [UIView animateWithDuration:.05
                                          animations:^{
                                              [_hero removeHero];
                                              [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
                                              [[[CCDirector sharedDirector] view] setTransform:CGAffineTransformMakeRotation(2*M_PI)];
                                              [[[CCDirector sharedDirector] view] setFrame:CGRectMake(0, 0, WIDTH_DEVICE, HEIGHT_DEVICE)];
                                              [_hero pouRealSize];
                                             
                                          }
                                          completion:^(BOOL finished){
                                              [[NSNotificationCenter defaultCenter]postNotificationName:@"closeGame" object:nil];
                                              [UIView animateWithDuration:.05
                                                               animations:^{
                                                                   [[CCDirector sharedDirector] view].alpha = 1;
                                                                 
                                                               }
                                                               completion:^(BOOL finished){
                                                                   
                                                               }];
                                              
                                          }];
                     }];
    
}

//- (void)loadMyViewController {
//    myView = [[MainViewController alloc] init];
//    AppController *app = (AppController *)[[UIApplication sharedApplication] delegate];
//    [app.navController pushViewController:myView animated:YES];
//    [CCDirector sharedDirector].pause;
//}

#pragma mark UPDATE
- (void)update:(ccTime)dt {
    
    if (bossInStage) {
        if (_hero.position.x < bossHuiSos.position.x && !_wasStoped) {
            _wasStoped = YES;
            [self addButtonsOnView];
            [self addStateWithText:NSLocalizedString(@"Under arest!", nil) AndInstruction:NSLocalizedString(@"Need to be fast to pass stage!", nil)];
            [[SimpleAudioEngine sharedEngine] stopEffect:stopMent];
            [_hero sleep];
            [_hero resetWithPosition:CGPointMake(_hero.position.x, _hero.position.y)];

            
        }
        if (_hero.position.y < 0 && !_wasStoped) {
               [_hero sleep];
               _wasStoped = YES;
            [self addButtonsOnView];
            [self addStateWithText:NSLocalizedString(@"Failed", nil) AndInstruction:NSLocalizedString(@"Need to be careful to pass stage!",nil)];
        }
        
        if ([_terrain isTimeToStopAtPosition:_hero.position] && !_wasStoped) {
            if (finishPosition.x + DISTANCE_STOP < _hero.position.x) {
                _wasStoped = YES;
                [[SimpleAudioEngine sharedEngine] setEffectsVolume:0.0];
                [[SimpleAudioEngine sharedEngine] stopEffect:stopMent];
                [_hero sleep];
                [_hero resetWithPosition:CGPointMake(finishPosition.x + DISTANCE_STOP, finishPosition.y)];
                NSLog(@"\n\n FINISH %f===================================================\n\n", finishPosition.x);
                finishPosition = CGPointMake(finishPosition.x - RADIUS_FOREACH_HERO_WHEN_STOP, finishPosition.y);
                positionWin++;
                if (positionWin == 1) {
                    winner = YES;
                }
                [_hero setWinnerPosition:positionWin];
                [self addButtonsOnView];
                if (positionWin==1) {
                    [[SimpleAudioEngine sharedEngine] playEffect:@"Weeee.mp3" loop:NO];
                    if (_currentStage==10) {
                        [self addStateWithText:NSLocalizedString(@"winner", nil) AndInstruction:NSLocalizedString(@"You pass the game!", nil)];
                    }else{
                        [self addStateWithText:NSLocalizedString(@"winner", nil) AndInstruction:NSLocalizedString(@"He lost YOU!",nil)];
                    }
                    if ([stateTamagochi funny]<100.0)
                    {
                        [stateTamagochi setFunny:[stateTamagochi funny]+20];
                        nextLevelAdd=20.0;
                      
                    }

                    
                     [stateTamagochi setStageForPlayer:[[NSUserDefaults standardUserDefaults] objectForKey:@"player"] forStage:self.currentStage];
                }else{
                    [self addStateWithText:NSLocalizedString(@"Failed", nil) AndInstruction:NSLocalizedString(@"Need to be careful to pass stage!",nil)];
                    [stateTamagochi setFunny:[stateTamagochi funny]-5];
                }
                NSString *t = [NSString stringWithFormat:@"%2f",time];
                if ([stateTamagochi updateGameWithStageId:@(self.currentStage) withTime:t withScore:@(scoreContor) withCoins:@(coinContor+_terrain.coinBank) withPosition:@(positionWin)])
                    [self addMessageHighScore:NSLocalizedString(@"new_highscore", nil)];
                timerIsRunning = NO;
            }else{
                if (_hero.position.y > 100) {
                    [_hero diveForce];
                }else{
                    [_hero limitVelocity:0];
                }
            }
        }
        [_terrain coinCollisionWithBodyX:_hero.position.x Y: _hero.position.y];
        
        if (_tapDown) {
            if (!_hero.awake) {
                if (_wasStoped) {

                }else{
                    pause = NO;
                    [_hero wake];
                    [bossHuiSos wake];
                    
                    timer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(updateTimerMain) userInfo:nil repeats:YES];
                    timerIsRunning = YES;
                }
                isRunning = YES;
                _tapDown = NO;
            } else {
                [_hero dive];
                if ([_terrain increaseVelocity]) {
                    [_hero diveForce];
                }
            }
            
        }else {
            if ([_terrain increaseVelocity ]) {
                [_hero diveForce];
            }
        }
        [_hero limitVelocity:1];
        //BOSS MOTION
        [self functionMotionBoss];
        
        [self worldSettings:dt];
        [_hero update];
        [bossHuiSos updateNode];
        float offset = _hero.position.x;
        float scale = (WIDTH_DEVICE*4/5)/_hero.position.y;
        if (scale > 1) scale = 1;
        if (scale < 0) scale = 1;
        
        _terrain.scale = scale;
        
        if (!_wasStoped && !pause) {
            [self scrollBackground:(12-(scale*10))];
        }
        if (!_wasStoped) {
            [_terrain setOffsetX:((!positionStart)?offset:offset-positionStart) Y:_hero.position.y];
            if (positionStart && _hero.awake) {
                positionStart--;
            }
        }else{
            if (goToPositionFinish < (450 - 100*_hero.winPosition)) {
                goToPositionFinish = goToPositionFinish+3;
            }
            [_terrain setOffsetX:_hero.position.x-goToPositionFinish Y:_hero.position.y];
        }
        
        [self onDisplayBossHuiSOS];
        [self showBossHUIsos];
    }else{
        if (_hero.position.y < 0 && !_wasStoped) {
            NSLog(@"e pozitia mai mika ca 0");
              _wasStoped = YES;
            [self addButtonsOnView];
            [self addStateWithText:NSLocalizedString(@"Failed",nil) AndInstruction:NSLocalizedString(@"Need to be careful to pass stage!",nil)];
            [_hero sleep];

          
        }
        
        if ([_terrain isTimeToStopAtPosition:_hero.position] && !_wasStoped) {
            if (finishPosition.x + DISTANCE_STOP < _hero.position.x) {
                [_hero sleep];
                [_hero resetWithPosition:CGPointMake(finishPosition.x + DISTANCE_STOP, finishPosition.y)];
                NSLog(@"\n\n FINISH %f===================================================\n\n", finishPosition.x);
                finishPosition = CGPointMake(finishPosition.x - RADIUS_FOREACH_HERO_WHEN_STOP, finishPosition.y);
                positionWin++;
                [_hero setWinnerPosition:positionWin];
                if (positionWin==1) {
                    [[SimpleAudioEngine sharedEngine] playEffect:@"Weeee.mp3" loop:NO];
                    [self addStateWithText:NSLocalizedString(@"winner", nil) AndInstruction:NSLocalizedString(@"You can race with BOSS!",nil)];
                    if ([stateTamagochi funny]<100.0)
                    {
                        [stateTamagochi setFunny:[stateTamagochi funny]+10];
                        nextLevelAdd=10;
                    }
                    [stateTamagochi setStageForPlayer:[[NSUserDefaults standardUserDefaults] objectForKey:@"player"] forStage:self.currentStage];
                }else{
                    [self addStateWithText:NSLocalizedString(@"Loose!", nil)  AndInstruction:NSLocalizedString(@"Need to be fast for BOSS", nil)];
                    [stateTamagochi setFunny:[stateTamagochi funny]-5];
                }
                
                timerIsRunning = NO;
                _wasStoped = YES;
                
                [self addButtonsOnView];
                NSString *t = [NSString stringWithFormat:@"%2f",time];
                if ([stateTamagochi updateGameWithStageId:@(self.currentStage) withTime:t withScore:@(scoreContor) withCoins:@(coinContor+_terrain.coinBank) withPosition:@(positionWin)])
                        [self addMessageHighScore:NSLocalizedString(@"new_highscore", nil)];
                [self addToFinishOther];

            }else{
                if (_hero.position.y > 100) {
                    [_hero diveForce];
                }else{
                    [_hero limitVelocity:0];
                }
            }
        }
        [_terrain coinCollisionWithBodyX:_hero.position.x Y: _hero.position.y];
        
        if (_tapDown) {
            if (!_hero.awake) {
                if (_wasStoped) {

                }else{
                    pause = NO;
                    [_hero wake];
                    [_enemyAngry wake];
                    [_energyBird wake];
                    [_eficientBot wake];
                    timer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(updateTimerMain) userInfo:nil repeats:YES];
                    timerIsRunning = YES;
                }
                isRunning = YES;
                _tapDown = NO;
            } else {
                [_hero dive];
                if ([_terrain increaseVelocity]) {
                    [_hero diveForce];
                }
            }
        }else {
            if ([_terrain increaseVelocity ]) {
                [_hero diveForce];
            }
        }
        [_hero limitVelocity:1];
        
        [self functionEnemyEnergy];
        [self worldSettings:dt];
        [_hero update];
        [_enemyAngry updateNode];
        [_energyBird updateNode];
        [_eficientBot updateNode];
        float offset = _hero.position.x;
        
        float scale = (WIDTH_DEVICE*4/5)/_hero.position.y;
        if (scale > 1 || scale < 0) scale = 1;
        _terrain.scale = scale;
        
        if (!_wasStoped && !pause) {
            [self scrollBackground:(12-(scale*10))];
        }
        if (!_wasStoped) {
            [_terrain setOffsetX:((!positionStart)?offset:offset-positionStart) Y:_hero.position.y];
            if (positionStart && _hero.awake) {
                positionStart--;
            }
        }else{
            if (goToPositionFinish < (450 - 100*_hero.winPosition)) {
                goToPositionFinish = goToPositionFinish+3;
            }
            [_terrain setOffsetX:_hero.position.x-goToPositionFinish Y:_hero.position.y];
        }
        
        [self onDisplayEachPersonajLocation];
        [self showShadowIfCanBeFast];
    }
    CCLabelTTF *bankLabel = (CCLabelTTF*)[self getChildByTag:kBankLabel];
    [bankLabel setString:[NSString stringWithFormat:@"%d",(coinContor+_terrain.coinBank)]];
}



#pragma mark - World Changes
- (void) worldSettings:(float)dt {
    static double timeAccumulator = 3;
    timeAccumulator += dt;
    
    if (timeAccumulator > (MAX_CYCLES_PER_FRAME * UPDATE_INTERVAL)) {
        timeAccumulator = UPDATE_INTERVAL;
    }
    
    int32 velocityIterations = 8;
    int32 positionIterations = 3;
    
    while (timeAccumulator >= UPDATE_INTERVAL) {
        timeAccumulator -= UPDATE_INTERVAL;
        _world->Step(UPDATE_INTERVAL,
                     velocityIterations, positionIterations);
        _world->ClearForces();
    }
}


#pragma mark - Effects OnScreen

- (void) showPerfectSlide {
	NSString *str = [NSString stringWithFormat:@"x%d",_hero.nPerfectSlides];
    //	CCLabelBMFont *label = [CCLabelBMFont labelWithString:str fntFile:@"good_dog_plain_32.fnt"];
    CCLabelTTF *label = [CCLabelTTF labelWithString:str fontName:@"Marker Felt" fontSize:(IS_IPAD)?28.:22.0];
	label.position = ccp(HEIGHT_DEVICE/2, WIDTH_DEVICE/10);
	[label runAction:[CCScaleTo actionWithDuration:1.0f scale:2.2f]];
	[label runAction:[CCSequence actions:
					  [CCFadeOut actionWithDuration:1.0f],
					  [CCCallFuncND actionWithTarget:label selector:@selector(removeFromParentAndCleanup:) data:(void*)YES],
					  nil]];
	[self addChild:label];
    [self addPointToScore];
}

- (void) showFrenzy {
	NSString *str = [NSString stringWithFormat:@"x%d + %d UP",_hero.nPerfectSlides,(10 * _hero.nPerfectSlides)];
    //	CCLabelBMFont *label = [CCLabelBMFont labelWithString:str fntFile:@"good_dog_plain_32.fnt"];
    CCLabelTTF *label = [CCLabelTTF labelWithString:str fontName:@"Marker Felt" fontSize:(IS_IPAD)?28.:22.0];
	label.position = ccp(HEIGHT_DEVICE/2, WIDTH_DEVICE/10);
	[label runAction:[CCScaleTo actionWithDuration:2.0f scale:2.4f]];
	[label runAction:[CCSequence actions:
					  [CCFadeOut actionWithDuration:2.0f],
					  [CCCallFuncND actionWithTarget:label selector:@selector(removeFromParentAndCleanup:) data:(void*)YES],
					  nil]];
	[self addChild:label];
    [self addPointToScore];
}

- (void) showHit {
    _nPerfectSlides = 0;
	NSString *str = @"X1";
    //	CCLabelBMFont *label = [CCLabelBMFont labelWithString:str fntFile:@"good_dog_plain_32.fnt"];
    CCLabelTTF *label = [CCLabelTTF labelWithString:str fontName:@"Marker Felt" fontSize:(IS_IPAD)?28.:22.0];
	label.position = ccp(HEIGHT_DEVICE/2, WIDTH_DEVICE/10);
	[label runAction:[CCScaleTo actionWithDuration:1.0f scale:2.2f]];
	[label runAction:[CCSequence actions:
					  [CCFadeOut actionWithDuration:1.0f],
					  [CCCallFuncND actionWithTarget:label selector:@selector(removeFromParentAndCleanup:) data:(void*)YES],
					  nil]];
	[self addChild:label];
}

#pragma mark TOUCHES
- (void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    //    [self createTestBodyAtPostition: [_terrain convertTouchToNodeSpace:[touches anyObject]]];
    CGPoint location = [self convertTouchToNodeSpace:[touches anyObject]];
    CGPoint posPause = _pauseButton.position;
	CGSize size = _pauseButton.contentSize;
	float padding = 8;
    
	CGRect rect = CGRectMake(posPause.x-size.height, posPause.y-size.width,size.height+ 2*padding, size.width+2*padding);
    
	if (CGRectContainsPoint(rect, location)) {
        
		[self addButtonsOnView];
	} else {
        _tapDown = YES;
	}
    //    emiter.totalParticles = 10;
}

- (void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    //    emiter.position = [_terrain convertTouchToNodeSpace:[touches anyObject]];
    //    emiter.totalParticles = 10;
}

-(void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    _tapDown = NO;
    [_hero resetVelocityY];
}

- (void)ccTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    _tapDown = NO;
}


#pragma mark - PAUSE MENU

- (void)addButtonsOnView {
    
    if ([stateTamagochi soundOn])
        [[SimpleAudioEngine sharedEngine] playEffect:@"btn_press.mp3" loop:NO];
    [self pauseGame];
    [[SimpleAudioEngine sharedEngine] pauseBackgroundMusic];
    
    [self removeButtons];
    CCSprite *sprBackgroundButtons = [CCSprite spriteWithFile:@"menu_game.png"];
    sprBackgroundButtons.position = ccp(HEIGHT_DEVICE+HEIGHT_DEVICE/8 ,WIDTH_DEVICE/2);
    [self addChild:sprBackgroundButtons z:5 tag:kBackgroundMenu];
    
    CCMenuItem *button1ContinueGame,*button2ResetGame,*button3NextStage, *button4ExitGame;
    if (self.currentStage<10) {
        button1ContinueGame = [CCMenuItemImage itemWithNormalImage:[NSString stringWithFormat:((_wasStoped)?@"btn_play_.png":@"btn_play.png")]
                                                     selectedImage:[NSString stringWithFormat:((_wasStoped)?@"btn_play_.png":@"btn_play.png")]
                                                            target:self selector:@selector(continueGame:)];
        button2ResetGame = [CCMenuItemImage itemWithNormalImage:[NSString stringWithFormat:@"btn_replay.png"]
                                                  selectedImage:[NSString stringWithFormat:@"btn_replay.png"]
                                                         target:self  selector:@selector(resetGame:)];
        
        button3NextStage = [CCMenuItemImage itemWithNormalImage:[NSString stringWithFormat:((_hero.winPosition!=1 || self.currentStage>9)?@"btn_next_lev_.png":@"btn_next_lev.png")]
                                                  selectedImage:[NSString stringWithFormat:((_hero.winPosition!=1 || self.currentStage>9)?@"btn_next_lev_.png":@"btn_next_lev.png")]
                                                         target:self  selector:@selector(nextStage:)];
        
        button4ExitGame = [CCMenuItemImage itemWithNormalImage:[NSString stringWithFormat:@"btn_close_highscore.png"]
                                                 selectedImage:[NSString stringWithFormat:@"btn_close_highscore.png"]
                                                        target:self selector:@selector(exitGame:)];

    }else{
        if (winner) {
            button1ContinueGame = [CCMenuItemImage itemWithNormalImage:[NSString stringWithFormat:@"btn_play.png"]
                                                         selectedImage:[NSString stringWithFormat:@"btn_play.png"] target:self selector:@selector(firstStage:)];
        }else{
            button1ContinueGame = [CCMenuItemImage itemWithNormalImage:[NSString stringWithFormat:((_wasStoped)?@"btn_play_.png":@"btn_play.png")]
                                                                                    selectedImage:[NSString stringWithFormat:((_hero.winPosition!=1)?@"btn_play_.png":@"btn_play.png")] target:self selector:@selector(firstStage:)];
        }
       
                                                            
        button2ResetGame = [CCMenuItemImage itemWithNormalImage:[NSString stringWithFormat:@"btn_replay.png"]
                                                  selectedImage:[NSString stringWithFormat:@"btn_replay.png"]
                                                         target:self  selector:@selector(resetGame:)];
        
        button3NextStage = [CCMenuItemImage itemWithNormalImage:[NSString stringWithFormat:((_hero.winPosition!=1 || self.currentStage>9)?@"btn_next_lev_.png":@"btn_next_lev.png")]
                                                  selectedImage:[NSString stringWithFormat:((_hero.winPosition!=1 || self.currentStage>9)?@"btn_next_lev_.png":@"btn_next_lev.png")]
                                                         target:self  selector:@selector(nextStage:)];
        
        button4ExitGame = [CCMenuItemImage itemWithNormalImage:[NSString stringWithFormat:@"btn_close_highscore.png"]
                                                 selectedImage:[NSString stringWithFormat:@"btn_close_highscore.png"]
                                                        target:self selector:@selector(exitGame:)];

    }
	   
    CCMenu *menu = [CCMenu menuWithItems: button1ContinueGame, button2ResetGame, button3NextStage, button4ExitGame, nil];
    
	[menu alignItemsInRows:[NSNumber  numberWithInt:4], nil];
    
	menu.position = ccp(HEIGHT_DEVICE+HEIGHT_DEVICE/8 ,WIDTH_DEVICE/2.4);
    [self addChild:menu z:6 tag:menuTag];
    
    [menu runAction:[CCMoveTo actionWithDuration:0.5 position:CGPointMake(HEIGHT_DEVICE - HEIGHT_DEVICE/8, WIDTH_DEVICE/2.4)]];
    [sprBackgroundButtons runAction:[CCMoveTo actionWithDuration:0.5 position:CGPointMake(HEIGHT_DEVICE - HEIGHT_DEVICE/8, WIDTH_DEVICE/2.1)]];

}

- (void) close {
    timerIsRunning = NO;
    [timer invalidate];
    if (_terrain.coinBank>0) {
        [[personaj person] addMoney:_terrain.coinBank];
    }

    int_least8_t winPosition = _hero.winPosition;
    if (!_hero.winPosition) {
        winPosition = 4;
    }
    time =0;

    [self removeButtons];
    [_hero sleep];
    [self showHighscoresWithScore:1];
    
    NSString *scoreStr = [NSString stringWithFormat:@"%@: %d",NSLocalizedString(@"score", nil),scoreContor];
    CCLabelTTF *scoreLabel = (CCLabelTTF*)[self getChildByTag:kScoreLabel];
    [scoreLabel setString:scoreStr];
}

- (void) resetGame:(id)sender{
   
      [[self getChildByTag:10002] removeFromParentAndCleanup:YES];
    if (_hero.winPosition == 1) {
        [self addAlertWithTitle:NSLocalizedString(@"You are first!", nil) withContainInformation:NSLocalizedString(@"Are you sure? Do you want to stage?", nil)];
        
    }else {
        [[SimpleAudioEngine sharedEngine] resumeBackgroundMusic];
        
        [self resetStage];
    }
}

- (void) nextStage:(id)sender{
    [[self getChildByTag:10002] removeFromParentAndCleanup:YES];
    if ([stateTamagochi fat]>1.0)
        [stateTamagochi setFat:[stateTamagochi fat]-time/100.0];
    
    if ([stateTamagochi fat]<1.0)
        [stateTamagochi setFat:1.0];
    if ([stateTamagochi food]>0)
    {
        [stateTamagochi setFood:[stateTamagochi food]-time/10.0];
        
    }
    if ([stateTamagochi  food]<0) [stateTamagochi setFood:0];
    if ([stateTamagochi energy]>0)
    {
        [stateTamagochi setEnergy:[stateTamagochi energy]-time/6.0];
    }
    if ([stateTamagochi  energy]<0) [stateTamagochi setEnergy:0];
    [stateTamagochi updateEnergy];
    [stateTamagochi updateFood];
    [stateTamagochi updateFat];

    
    if ([stateTamagochi soundOn])
        [[SimpleAudioEngine sharedEngine] playEffect:@"btn_press.mp3" loop:NO];
    // TODO: _hero.winPosition == 1 for next Stage
    if (_hero.winPosition ==  1 && self.currentStage<10) {
        
        if (_currentStage == 10) {
            [self addAlertWithTitle:NSLocalizedString(@"Do You want to try again?", nil)  withContainInformation:NSLocalizedString(@"It was last stage. Congradulations!", nil) ];
            return;
        }
        int_least8_t winPosition = _hero.winPosition;
        if (!_hero.winPosition) {
            winPosition = 4;
        }
    
        NSLog(@"TIME ====      %f", time);
        time = 0;
        [self setTimeLabel:time];
        bossInStage = !bossInStage;
        [self showOrHidePositionForEnemies];
        if (!bossInStage) {
            bossHuiSos.visible = NO;
            _enemyAngry.visible = _energyBird.visible = _eficientBot.visible = YES;
            [_terrain removeChild:_enemyAngry cleanup:YES];
            [_terrain removeChild:_energyBird cleanup:YES];
            [_terrain removeChild:_eficientBot cleanup:YES];
            [_terrain addChild:_enemyAngry z:2];
            [_terrain addChild:_energyBird z:3];
            [_terrain addChild:_eficientBot z:4];
            [_terrain removeChild:bossHuiSos cleanup:YES];
        }else{
           stopMent = [[SimpleAudioEngine sharedEngine] playEffect:@"siren.mp3" loop:YES];
            [[SimpleAudioEngine sharedEngine] setEffectsVolume:0.7];
            bossHuiSos.visible = YES;
            _enemyAngry.visible = _energyBird.visible = _eficientBot.visible =NO;
            [_terrain addChild:bossHuiSos z:5];
            [_terrain removeChild:_enemyAngry cleanup:YES];
            [_terrain removeChild:_energyBird cleanup:YES];
            [_terrain removeChild:_eficientBot cleanup:YES];
        }
        
        if (_currentStage < 10 ) {
            _currentStage = _currentStage+1;
        }
        NSLog(@"Next stage %d", _currentStage);
        [self genBackground];
        [self generateBackgroundSound];
        [self resetStage];

        [self updateOnScreenScore:scoreContor];
    }else{
        if (self.currentStage>9 && _hero.winPosition ==  1) {
            [self addAlertWithTitle:NSLocalizedString(@"winner", nil) withContainInformation:NSLocalizedString(@"You can play again this stage!", nil) ];
        }else{
            [self addAlertWithTitle:NSLocalizedString(@"Do You want to try again?", nil)  withContainInformation:NSLocalizedString(@"Need to be First, for the BOSS!", nil) ];
        }
    }
}

- (void)continueGame:(id)sender {
    if (_wasStoped) {
        return;
    }
    if ([stateTamagochi soundOn])
        [[SimpleAudioEngine sharedEngine] playEffect:@"btn_press.mp3" loop:NO];
    [[SimpleAudioEngine sharedEngine] resumeBackgroundMusic];
    [self removeButtons];
    self.isTouchEnabled = YES;
    if (_hero.position.x != 0.0) {
        timerIsRunning = YES;
        [_hero wakeContinue];

        if (bossInStage) {
            [bossHuiSos wakeContinue];
        }else{
            [_eficientBot wakeContinue];
            [_energyBird wakeContinue];
            [_enemyAngry wakeContinue];
        }
        pause = NO;
    }

}

- (void)firstStage:(id)sender {
    if ([stateTamagochi soundOn])
        [[SimpleAudioEngine sharedEngine] playEffect:@"btn_press.mp3" loop:NO];
    if (_hero.winPosition!=1) {
        [self performSelector:@selector(continueGame:) withObject:self afterDelay:0.0];
    }else{
        [self addAlertWithTitle:NSLocalizedString(@"winner", nil) withContainInformation:NSLocalizedString(@"Do you want to pass first stage?", nil) ];
        toFirstStage = YES;
    }
}

- (void)goToFirstStage{
    [self removeButtons];
    self.isTouchEnabled = YES;
    time = 0;
    [self setTimeLabel:time];
    bossInStage = NO;
    [self showOrHidePositionForEnemies];
    if (!bossInStage) {
        bossHuiSos.visible = NO;
        _enemyAngry.visible = _energyBird.visible = _eficientBot.visible = YES;
        [_terrain removeChild:_enemyAngry cleanup:YES];
        [_terrain removeChild:_energyBird cleanup:YES];
        [_terrain removeChild:_eficientBot cleanup:YES];
        [_terrain addChild:_enemyAngry z:2];
        [_terrain addChild:_energyBird z:3];
        [_terrain addChild:_eficientBot z:4];
        [_terrain removeChild:bossHuiSos cleanup:YES];
    }else{
        stopMent = [[SimpleAudioEngine sharedEngine] playEffect:@"siren.mp3" loop:YES];
        [[SimpleAudioEngine sharedEngine] setEffectsVolume:0.7];
        bossHuiSos.visible = YES;
        _enemyAngry.visible = _energyBird.visible = _eficientBot.visible =NO;
        [_terrain addChild:bossHuiSos z:5];
        [_terrain removeChild:_enemyAngry cleanup:YES];
        [_terrain removeChild:_energyBird cleanup:YES];
        [_terrain removeChild:_eficientBot cleanup:YES];
    }
    
    _currentStage = 1;
    NSLog(@"Next stage %d", _currentStage);
    [self genBackground];
    [self generateBackgroundSound];
    [self resetStage];
    
    [self updateOnScreenScore:scoreContor];}

- (void)exitGame:(id)sender {
    
    [stateTamagochi setNextLevelPoints:[stateTamagochi nextLevelPoints] +nextLevelAdd];
    [stateTamagochi addToNextLevel:4];
    
    if ([stateTamagochi energy]>0.0)
        [stateTamagochi setEnergy:[stateTamagochi energy]-time/60.0];
    if ([stateTamagochi fat]>1.0)
        [stateTamagochi setFat:[stateTamagochi fat]-time/100.0];
    
    if ([stateTamagochi fat]<1.0)
        [stateTamagochi setFat:1.0];
    if ([stateTamagochi food]>0)
    {
        [stateTamagochi setFood:[stateTamagochi food]-time/10.0];
    }
    if ([stateTamagochi  food]<0) [stateTamagochi setFood:0];
    if ([stateTamagochi energy]>0)
    {
        [stateTamagochi setEnergy:[stateTamagochi energy]-time/6.0];
    }
    if ([stateTamagochi  energy]<0) [stateTamagochi setEnergy:0];
    [stateTamagochi updateEnergy];
    [stateTamagochi updateFood];
    [stateTamagochi updateFat];
    timer = nil;
    if ([stateTamagochi soundOn])
        [[SimpleAudioEngine sharedEngine] playEffect:@"btn_press.mp3" loop:NO];
    [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
    [self removeButtons];
    [self close];
}

- (void)pauseGame {
    timerIsRunning = NO;
    pause = YES;
    self.isTouchEnabled = NO;
    [_eficientBot sleep];
    [_energyBird sleep];
    [_enemyAngry sleep];
    [bossHuiSos sleep];
    [_hero sleep];
}

- (void)removeButtons {
    [self removeState];
    [self removeAlert];
    CCMenu *menu = (CCMenu*)[self getChildByTag:menuTag];
    [menu removeFromParentAndCleanup:YES];
    
    CCSprite *backGr = (CCSprite*)[self getChildByTag:kBackgroundMenu];
    backGr.visible = NO;
    [self removeChild:backGr cleanup:YES];
}

- (void)resetStage {
    
    if ([stateTamagochi fat]>1.0)
        [stateTamagochi setFat:[stateTamagochi fat]-time/100.0];
    
    if ([stateTamagochi fat]<1.0)
        [stateTamagochi setFat:1.0];
    if ([stateTamagochi food]>0)
    {
        [stateTamagochi setFood:[stateTamagochi food]-time/10.0];
        
    }
    if ([stateTamagochi  food]<0) [stateTamagochi setFood:0];
    if ([stateTamagochi energy]>0)
    {
        [stateTamagochi setEnergy:[stateTamagochi energy]-time/6.0];
    }
    if ([stateTamagochi  energy]<0) [stateTamagochi setEnergy:0];
    [stateTamagochi updateEnergy];
    [stateTamagochi updateFood];
    [stateTamagochi updateFat];
    [[SimpleAudioEngine sharedEngine] resumeBackgroundMusic];
    if ([stateTamagochi soundOn])
        [[SimpleAudioEngine sharedEngine] playEffect:@"btn_press.mp3" loop:NO];
    [self removeState];
    timerIsRunning = NO;
    winner = NO;
    [timer invalidate];
    timer = nil;
   
    if(self.currentStage>1){
        NSDictionary *dict = [stateTamagochi getAllGameStatisticsForStageID:[NSNumber numberWithInt:(self.currentStage-1)] forPlayer:[[NSUserDefaults standardUserDefaults] objectForKey:@"player"]];
        time = [[dict objectForKey:@"TIME"] floatValue];
        scoreContor = [[dict objectForKey:@"SCORE"] integerValue];
        coinContor = [[dict objectForKey:@"COINS"] integerValue];
    }else{
        time = 0;
        scoreContor = 0;
        coinContor = 0;
    }
   
    [self setTimeLabel:time];
    [self updateOnScreenScore:scoreContor];
    
    [_terrain selectWayFromStageID:self.currentStage withBoss:bossInStage];
    if(_terrain.coinBank>0){
        [[personaj person] addMoney:_terrain.coinBank];
    }
    if (bossInStage) {
        [bossHuiSos resetEnemyWithPosition:CGPointMake(-5*RADIUS_POU, ((IS_FOUR_INCH)?HEIGHT_DEVICE:568)/2)];
    }else{
        [_enemyAngry resetEnemyWithPosition:CGPointMake(-5*RADIUS_POU, ((IS_FOUR_INCH)?HEIGHT_DEVICE:568)/2)];
        [_energyBird resetEnemyWithPosition:CGPointMake(-3*RADIUS_POU, ((IS_FOUR_INCH)?HEIGHT_DEVICE:568)/2)];
        [_eficientBot resetEnemyWithPosition:CGPointMake(-1*RADIUS_POU, ((IS_FOUR_INCH)?HEIGHT_DEVICE:568)/2)];
    }
    [_hero resetHero];
    [_terrain resetTerain];
    
    [self removeButtons];
    
    a_wasStoped = b_wasStoped  = c_wasStoped = _wasStoped = NO;
    positionWin = 0;
    positionStart = RADIUS_POU*10;
    
    self.isTouchEnabled = YES;
    [_hero setWinnerPosition:4];
    finishPosition = CGPointMake(_terrain.theDistanceOfWay.x, _terrain.theDistanceOfWay.y);
    NSLog(@"REstart value finis %@", NSStringFromCGPoint(finishPosition));
    goToPositionFinish = 0;
    pause = YES;
}

#pragma mark ENEMY

- (void)addToFinishOther{
    if (_enemyAngry.position.x>_energyBird.position.x) {
        if (_enemyAngry.position.x > _eficientBot.position.x) {
            
            [self sleepEnemy:_enemyAngry];
            if (_energyBird.position.x>_eficientBot.position.x) {
                [self sleepEnemy:_energyBird];
                [self sleepEnemy:_eficientBot];
            }else{
                [self sleepEnemy:_eficientBot];
                [self sleepEnemy:_energyBird];
            }
        }else{
            [self sleepEnemy:_eficientBot];
            [self sleepEnemy:_enemyAngry];
            [self sleepEnemy:_energyBird];
        }
    }else
    {
        if (_energyBird.position.x > _eficientBot.position.x) {
            [self sleepEnemy:_energyBird];
            if (_enemyAngry.position.x>_eficientBot.position.x) {
                [self sleepEnemy:_enemyAngry];
                [self sleepEnemy:_eficientBot];
            }else{
                [self sleepEnemy:_eficientBot];
                [self sleepEnemy:_enemyAngry];
            }
        }else{
            [self sleepEnemy:_eficientBot];
            [self sleepEnemy:_enemyAngry];
            [self sleepEnemy:_energyBird];
        }
    }
    a_wasStoped= b_wasStoped=c_wasStoped = YES;
}

- (void)sleepEnemy:(Enemy*)en{
    if (en.awake) {
        [en sleep];
        [en resetEnemyWithPosition:CGPointMake(finishPosition.x+DISTANCE_STOP, finishPosition.y)];
        finishPosition = CGPointMake(finishPosition.x - RADIUS_FOREACH_HERO_WHEN_STOP, finishPosition.y);
        positionWin++;
    }
}

- (void)functionMotionBoss{
    if ([_terrain isTimeToStopAtPosition:bossHuiSos.position] && !boss_wasStoped) {
        if (finishPosition.x+DISTANCE_STOP < bossHuiSos.position.x) {
            [bossHuiSos sleep];
            [bossHuiSos resetEnemyWithPosition:CGPointMake(finishPosition.x+DISTANCE_STOP, finishPosition.y)];
            finishPosition = CGPointMake(finishPosition.x - (RADIUS_FOREACH_HERO_WHEN_STOP*2), finishPosition.y);
            positionWin++;
            
            boss_wasStoped = YES;
        }else{
            if ([_terrain isGoodPositionToDive:bossHuiSos.position indexHill:4])
                for (int i=0; i < 3; i++) {
                    [bossHuiSos timeToDive:4];
                }
        }
    }else{
        if (bossHuiSos.position.y < 800) {
            if ([_terrain isGoodPositionToDive:bossHuiSos.position indexHill:3]) {
                int i ;
                if (_currentStage>2 && _currentStage<5) {
                    i=2;
                }else {
                    i=2;
                }
                [ bossHuiSos timeToDive:i];
            }
        }
        [bossHuiSos simpleVelocity];
    }
}

- (void)functionEnemyEnergy{
        
    if ([_terrain isTimeToStopAtPosition:_enemyAngry.position] && !a_wasStoped) {
        if (finishPosition.x+DISTANCE_STOP < _enemyAngry.position.x) {
            [_enemyAngry sleep];
            [_enemyAngry resetEnemyWithPosition:CGPointMake(finishPosition.x+DISTANCE_STOP, finishPosition.y)];
            finishPosition = CGPointMake(finishPosition.x - RADIUS_FOREACH_HERO_WHEN_STOP, finishPosition.y);
            positionWin++;
            
            a_wasStoped = YES;
        }else{
            if ([_terrain isGoodPositionToDive:_enemyAngry.position indexHill:4])
                for (int i=0; i < 3; i++) {
                    [_enemyAngry timeToDive:4];}
        }
    }else{
        if (_enemyAngry.position.y < 800) {
            if ([_terrain isGoodPositionToDive:_enemyAngry.position indexHill:4]) {
                    [ _enemyAngry timeToDive:2];
                
            }
        }

        [_enemyAngry simpleVelocity];
 
    }
    
    if ([_terrain isTimeToStopAtPosition:_energyBird.position] && !b_wasStoped) {
        if (finishPosition.x+DISTANCE_STOP < _energyBird.position.x) {
            [_energyBird sleep];
            [_energyBird resetEnemyWithPosition:CGPointMake(finishPosition.x+DISTANCE_STOP, finishPosition.y)];
            finishPosition = CGPointMake(finishPosition.x - RADIUS_FOREACH_HERO_WHEN_STOP, finishPosition.y);
            positionWin++;
            b_wasStoped = YES;
        }else{
            if ([_terrain isGoodPositionToDive:_energyBird.position indexHill:3])
                for (int i=0; i < 3; i++) {
                    [_energyBird timeToDive:4];}
        }
    }else{
        if (_energyBird.position.y < 800) {
            if ([_terrain isGoodPositionToDive:_energyBird.position indexHill:3]) {
                [ _energyBird timeToDive:2];
            }
        }
       
        [_energyBird simpleVelocity];
        
    }
    
    if ([_terrain isTimeToStopAtPosition:_eficientBot.position] && !c_wasStoped) {
        if (finishPosition.x+DISTANCE_STOP < _eficientBot.position.x) {
            [_eficientBot sleep];
            [_eficientBot resetEnemyWithPosition:CGPointMake(finishPosition.x+DISTANCE_STOP, finishPosition.y)];
            finishPosition = CGPointMake(finishPosition.x - RADIUS_FOREACH_HERO_WHEN_STOP, finishPosition.y);
            positionWin++;
            c_wasStoped = YES;
        }else{
            if ([_terrain isGoodPositionToDive:_eficientBot.position indexHill:4])
                for (int i=0; i < 3; i++) {
                    [_eficientBot timeToDive:4];}
        }
    }else{
        if (_eficientBot.position.y < 900) {
            if ([_terrain isGoodPositionToDive:_eficientBot.position indexHill:4]) {
                
                [ _eficientBot timeToDive:2];
            }
        }
            [_eficientBot simpleVelocity];
        
    }
}

- (void)onDisplayBossHuiSOS{
        sprLocationHuiSos.position = ccp(bossHuiSos.position.x /_terrain.theDistanceOfWay.x*(HEIGHT_DEVICE-10), 15);
       sprNavaH.position = ccp(_hero.position.x/_terrain.theDistanceOfWay.x*(HEIGHT_DEVICE-10), 15);
}

- (void)showOrHidePositionForEnemies{
    if (bossInStage) {
        sprLocationHuiSos.visible = YES;
        sprShadowBossHUIsos.visible = YES;
        sprE1.visible = NO;
        sprE2.visible = NO;
        sprE3.visible = NO;
        sprShadowE1.visible = NO;
        sprShadowE2.visible = NO;
        sprShadowE3.visible = NO;
        
    }else{
        sprLocationHuiSos.visible = NO;
        sprShadowBossHUIsos.visible = NO;
        
        sprE1.visible = YES;
        sprE2.visible = YES;
        sprE3.visible = YES;
        sprShadowE1.visible = YES;
        sprShadowE2.visible = YES;
        sprShadowE3.visible = YES;
    }
}

- (void)showBossHUIsos{
    if (bossHuiSos.position.y > _hero.position.y && bossHuiSos.position.y > WIDTH_DEVICE){
        sprShadowBossHUIsos.position = ccp(bossHuiSos.position.x, _hero.position.y+50);
        sprShadowBossHUIsos.visible = YES;
    }else{
        sprShadowBossHUIsos.visible = NO;
    }
}

- (void)onDisplayEachPersonajLocation{

        sprE1.position = ccp(_enemyAngry.position.x /_terrain.theDistanceOfWay.x*(HEIGHT_DEVICE-10), 15);
        sprE2.position = ccp(_energyBird.position.x /_terrain.theDistanceOfWay.x*(HEIGHT_DEVICE-10), 15);
        sprE3.position = ccp(_eficientBot.position.x/_terrain.theDistanceOfWay.x*(HEIGHT_DEVICE-10), 15);

    sprNavaH.position = ccp(_hero.position.x/_terrain.theDistanceOfWay.x*(HEIGHT_DEVICE-10), 15);
}

- (void)showShadowIfCanBeFast{
    if (_enemyAngry.position.y > _hero.position.y && _enemyAngry.position.y > WIDTH_DEVICE){
        sprShadowE1.position = ccp(_enemyAngry.position.x, _hero.position.y+50);
        sprShadowE1.visible = YES;
    }else{
        sprShadowE1.visible = NO;
    }
    
    if (_energyBird.position.y > _hero.position.y && _energyBird.position.y > WIDTH_DEVICE){
        sprShadowE2.position = ccp(_energyBird.position.x, _hero.position.y+50);
        sprShadowE2.visible = YES;
    }else{
        sprShadowE2.visible = NO;
    }
    
    if (_eficientBot.position.y > _hero.position.y && _eficientBot.position.y > WIDTH_DEVICE){
        sprShadowE3.position = ccp(_eficientBot.position.x,_hero.position.y+50);
        sprShadowE3.visible = YES;
    }else{sprShadowE3.visible = NO;}
}

#pragma mark - SCORE

- (void)addPointToScore{
    scoreContor += 10 * _hero.nPerfectSlides;
    [self updateOnScreenScore:scoreContor];
}

-(void) updateOnScreenScore:(uint_fast16_t)scor{
   
    NSString *scoreStr = [NSString stringWithFormat:@"%@: %d",NSLocalizedString(@"score", nil), scor];
    CCLabelTTF *scoreLabel = (CCLabelTTF*)[self getChildByTag:kScoreLabel];
    [scoreLabel setString:scoreStr];
    id a1 = [CCScaleTo actionWithDuration:0.2f scaleX:1.5f scaleY:0.8f];
    id a2 = [CCScaleTo actionWithDuration:0.2f scaleX:1.0f scaleY:1.0f];
    id a3 = [CCSequence actions:a1,a2,a1,a2,a1,a2,nil];
    [scoreLabel runAction:a3];
}


#pragma mark - State WIN/LOSE
- (NSString*)formattedStringForDuration:(NSTimeInterval)duration
{
    NSInteger minutes = floor(duration/60);
    NSInteger seconds = round(duration - minutes * 60);
    return [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
}

-(void)addMessageHighScore:(NSString *)message
{
    CCLabelTTF *alert = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%@",message] dimensions:CGSizeMake(HEIGHT_DEVICE-20, 30) hAlignment:kCCTextAlignmentCenter fontName:gameFont fontSize:(IS_IPAD)?32:28];
    alert.color=ccc3(255.0, 0, 0);
    alert.position = alert.position = ccp(HEIGHT_DEVICE/3, 20);
    [self addChild:alert z:8 tag:10002];
}
- (void)addStateWithText:(NSString*)textInfo AndInstruction:(NSString*)strInstruction{
    [self removeAlert];
    [self removeState];
    CCSprite *sprAlertBG = [CCSprite spriteWithFile:@"top_highscore.png"];
    sprAlertBG.scaleY = 0.9;
    sprAlertBG.scaleX = 1.3;
    sprAlertBG.position = ccp(HEIGHT_DEVICE/3, WIDTH_DEVICE/2);
    [self addChild:sprAlertBG z:6 tag:kAlertBG];
    CCSprite *spr = [CCSprite spriteWithFile:@"cell.png"];
    
    CCLabelTTF *titleAlert = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%@",textInfo] dimensions:CGSizeMake(sprAlertBG.contentSize.width, 30) hAlignment:kCCTextAlignmentCenter fontName:gameFont fontSize:(IS_IPAD)?32:28];
    spr.position = titleAlert.position = ccp(HEIGHT_DEVICE/3, WIDTH_DEVICE/2+90);
    [self addChild:titleAlert z:8 tag:kInfoAlert];
    [self addChild:spr z:7 tag:kUmbra];
    
    CCSprite *spr1 = [CCSprite spriteWithFile:@"cell.png"];
    spr1.scaleX = 1.35;
    CCLabelTTF *titleInstruction = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%@",strInstruction] dimensions:CGSizeMake(sprAlertBG.contentSize.width, 30) hAlignment:kCCTextAlignmentCenter fontName:gameFont fontSize:(IS_IPAD)?27:22];
    spr1.position =ccp(HEIGHT_DEVICE/3, WIDTH_DEVICE/2+51);
    titleInstruction.position = ccp(HEIGHT_DEVICE/3, WIDTH_DEVICE/2+50);
    [self addChild:titleInstruction z:8 tag:kInstruction];
    [self addChild:spr1 z:7 tag:kUmbra1];
    CCSprite *spr2 = [CCSprite spriteWithFile:@"cell.png"];
    CCLabelTTF *bankLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"coins", nil),@(coinContor+_terrain.coinBank)] dimensions:CGSizeMake(sprAlertBG.contentSize.width, 30) hAlignment:kCCTextAlignmentCenter fontName:gameFont fontSize:(IS_IPAD)?25:20];
    spr2.position = ccp(HEIGHT_DEVICE/3, WIDTH_DEVICE/2+3);
    bankLabel.position = ccp(HEIGHT_DEVICE/3, WIDTH_DEVICE/2);
    [self addChild:bankLabel z:8 tag:kBankMoney];
    [self addChild:spr2 z:7 tag:kUmbra2];
    
    CCSprite *spr3 = [CCSprite spriteWithFile:@"cell.png"];
    CCLabelTTF *scoreLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%@: %d",NSLocalizedString(@"score", nil),scoreContor] dimensions:CGSizeMake(sprAlertBG.contentSize.width, 30) hAlignment:kCCTextAlignmentCenter fontName:gameFont fontSize:(IS_IPAD)?25:20];
    spr3.position = ccp(HEIGHT_DEVICE/3, WIDTH_DEVICE/2-37);
    scoreLabel.position = ccp(HEIGHT_DEVICE/3, WIDTH_DEVICE/2-40);
    [self addChild:scoreLabel z:8 tag:kScore];
    [self addChild:spr3 z:7 tag:kUmbra3];
    
    CCSprite *spr4 = [CCSprite spriteWithFile:@"cell.png"];
    NSString *strTime = [self formattedStringForDuration:time];
    CCLabelTTF *timeLabel = [CCLabelTTF labelWithString:strTime dimensions:CGSizeMake(sprAlertBG.contentSize.width, 30) hAlignment:kCCTextAlignmentCenter fontName:gameFont fontSize:(IS_IPAD)?25:20];
    spr4.position = ccp(HEIGHT_DEVICE/3,  WIDTH_DEVICE/2-77);
    timeLabel.position = ccp(HEIGHT_DEVICE/3,  WIDTH_DEVICE/2-80);
    [self addChild:timeLabel z:8 tag:kTime];
    [self addChild:spr4 z:7 tag:kUmbra4];

    
//    [self performSelector:@selector(removeState) withObject:Nil afterDelay:20.0];

    
}

- (void)removeState{

    [self removeChildByTag:kAlertBG cleanup:YES];
    [self removeChildByTag:kInfoAlert cleanup:YES];
    [self removeChildByTag:kInstruction cleanup:YES];
    [self removeChildByTag:kTime cleanup:YES];
    [self removeChildByTag:kBankMoney cleanup:YES];
    [self removeChildByTag:kScore cleanup:YES];
    [self removeChildByTag:kUmbra cleanup:YES];
     [self removeChildByTag:kUmbra1 cleanup:YES];
     [self removeChildByTag:kUmbra2 cleanup:YES];
     [self removeChildByTag:kUmbra3 cleanup:YES];
     [self removeChildByTag:kUmbra4 cleanup:YES];
}

#pragma mark - ALERT

- (void)addAlertWithTitle:(NSString*)title withContainInformation:(NSString*)info{
    [self removeAlert];
    [self removeState];
    CCSprite *sprAlertBG = [CCSprite spriteWithFile:@"top_highscore.png"];
    sprAlertBG.scaleX = 1.3;
    sprAlertBG.position = ccp(HEIGHT_DEVICE/3, WIDTH_DEVICE/2);
    [self addChild:sprAlertBG z:6 tag:kInformationBG];
    
    CCLabelTTF *titleAlert = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%@",title] dimensions:CGSizeMake(sprAlertBG.contentSize.width, 30) hAlignment:kCCTextAlignmentCenter fontName:gameFont fontSize:(IS_IPAD)?28:20];
    titleAlert.position = ccp(sprAlertBG.position.x, (sprAlertBG.position.y+sprAlertBG.contentSize.height/2)-titleAlert.contentSize.height);
    [self addChild:titleAlert z:7 tag:kInformation];
    
    CCLabelTTF *containAlert = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%@\n",info] dimensions:CGSizeMake(sprAlertBG.contentSize.width+20, 30) hAlignment:kCCTextAlignmentCenter fontName:gameFont fontSize:(IS_IPAD)?25:16];
    containAlert.position = ccp(sprAlertBG.position.x, (sprAlertBG.position.y+sprAlertBG.contentSize.height/3)-containAlert.contentSize.height);
    
    [self addChild:containAlert z:7 tag:kSolution];
    
    CCMenuItem *button1ContinueGame = [CCMenuItemImage itemWithNormalImage:[NSString stringWithFormat:@"btn_play.png"]
                                                             selectedImage:[NSString stringWithFormat:@"btn_play.png"]
                                                                    target:self selector:@selector(yesAlert:)];
    
    CCMenuItem *button4ExitGame = [CCMenuItemImage itemWithNormalImage:[NSString stringWithFormat:@"btn_close_highscore.png"]
                                                         selectedImage:[NSString stringWithFormat:@"btn_close_highscore.png"]
                                                                target:self selector:@selector(closeAlert:)];
    
    CCMenu *menu = [CCMenu menuWithItems: button1ContinueGame, button4ExitGame, nil];
    
    menu.contentSize = CGSizeMake(100, 100);
    
    [menu alignItemsInColumns:[NSNumber  numberWithInt:2], nil];
    
    menu.position = ccp(HEIGHT_DEVICE/3,WIDTH_DEVICE/4);
    [self addChild:menu z:6 tag:kMenuButtons];
}

- (void)removeAlert{
    [self removeChildByTag:kInformationBG cleanup:YES];
    [self removeChildByTag:kInformation cleanup:YES];
    [self removeChildByTag:kSolution cleanup:YES];
    [self removeChildByTag:kMenuButtons cleanup:YES];
}

- (void)yesAlert:(id)sender {
    if (toFirstStage) {
        toFirstStage = NO;
        [self goToFirstStage];
    }else{
    [self removeAlert];
    int_least8_t winPosition = _hero.winPosition;
    if (!_hero.winPosition) {
        winPosition = 4;
    }

    [self resetStage];
    [self removeButtons];
    self.isTouchEnabled = YES;
    }
}

- (void)closeAlert:(id)sender {
    if (toFirstStage) {
        toFirstStage = NO;
    }
    [self removeAlert];
    self.isTouchEnabled = YES;
    
}

@end
