//
//  Hero.m
//  Tamagochi
//
//  Created by iVanea! on 9/26/13.
/************************************************************
 *                                                           *
 *  .=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-.       *
 *   |                     ______                     |      *
 *   |                  .-"      "-.                  |      *
 *   |                 /            \                 |      *
 *   |     _          |              |          _     |      *
 *   |    ( \         |,  .-.  .-.  ,|         / )    |      *
 *   |     > "=._     | )(__/  \__)( |     _.=" <     |      *
 *   |    (_/"=._"=._ |/     /\     \| _.="_.="\_)    |      *
 *   |           "=._"(_     ^^     _)"_.="           |      *
 *   |               "=\__|IIIIII|__/="               |      *
 *   |              _.="| \IIIIII/ |"=._              |      *
 *   |    _     _.="_.="\          /"=._"=._     _    |      *
 *   |   ( \_.="_.="     `--------`     "=._"=._/ )   |      *
 *   |    > _.="                            "=._ <    |      *
 *   |   (_/                                    \_)   |      *
 *   |                                                |      *
 *   '-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-='      *
 *                                                           *
 *        WARNING!!! Very perversive code ahead!             *
 *************************************************************/
//
//

#import "Hero.h"
#import "HeroContactListener.h"
#import "TinyPoo.h"

#define NUM_PREV_VELS 5


@interface Hero() {
    b2World *_world;
    b2Body *_body;
    BOOL _awake;
    
    b2Vec2 _prevVels[NUM_PREV_VELS];
    int _nextVel;
    int8_t yForce;
    BOOL _flying, _wasDive, _isSuperDive;
    int_fast8_t minVelocityY;
}
- (void)limitVelocityForDive;
@end

@implementation Hero
@synthesize game = _game;
@synthesize nPerfectSlides = _nPerfectSlides;
@synthesize winPosition = _winPosition;

- (void)createBody {
    
    CGPoint startPosition = ccp(0*RADIUS_POU, ((IS_FOUR_INCH)?HEIGHT_DEVICE:568)/2+RADIUS_POU/2);
    
    b2BodyDef bd;
    bd.type = b2_dynamicBody;
    bd.linearDamping = 0.03f;
    bd.fixedRotation = true;
    bd.position.Set(startPosition.x/PTM_RATIO, startPosition.y/PTM_RATIO);
    
    _body = _world->CreateBody(&bd);
    
    b2CircleShape shape;
    shape.m_radius = RADIUS_POU/PTM_RATIO;
    
    b2FixtureDef fd;
    fd.shape = &shape;
    fd.filter.groupIndex = -1;
    fd.density = .9f ;
    fd.restitution = 0.0f;
    fd.friction = 0.07f;
     //TODO: friction 0.1
    _body->CreateFixture(&fd);
    
}


- (void)createBodyWithPosition:(CGPoint)pos {
    b2BodyDef bd;
    bd.type = b2_dynamicBody;
    bd.linearDamping = 0.03f;
    bd.fixedRotation = true;
    bd.position.Set((pos.x-RADIUS_POU)/PTM_RATIO, (pos.y+RADIUS_POU)/PTM_RATIO);
    
    _body = _world->CreateBody(&bd);
    
    b2CircleShape shape;
    shape.m_radius = RADIUS_POU/PTM_RATIO;
    
    b2FixtureDef fd;
    fd.shape = &shape;
    fd.filter.groupIndex = -1;
    fd.density = .9f ;
    fd.restitution = 0.0f;
    fd.friction = 0.07f;
    //TODO: friction 0.1
    
    _body->CreateFixture(&fd);
    
}

+ (id)heroWithGame:(TinyPoo *)game{
    return [[[self alloc] initWithGame:game] autorelease];
}

- (id)initWithGame:(TinyPoo *)game{
    [self cleanup];
    if ((self = [super init])) {
        self.game = game;
        [[personaj person] removeFromParentAndCleanup:NO];
        _world = game.world;
        [self createBody];
        [personaj person].tag = tagTamagochi;
        [personaj person].delegate = self;
        [personaj person].position = ccp(0, 0);
        [personaj person].scale = 0.18;
        [self addChild:[personaj person] z:1];
        CCSprite *spr = [CCSprite spriteWithFile:@"nava.png"];
        [spr setScale:1.1];
        [self addChild:spr z:2];
        _contactListener = new HeroContactListener(self);
		_game.world->SetContactListener(_contactListener);
        yForce = 30;
        _isSuperDive = NO;
        minVelocityY = -35;
    }
    return self;

}

- (void)pouRealSize {
    [personaj person].position = ccp(-WIDTH_DEVICE, -HEIGHT_DEVICE);
    [personaj person].scale = 1.0f;
}

-(CGPoint)returnPosition
{
    return CGPointMake([personaj person].position.x, [personaj person].position.y);
}

-(float)returnTouchValue
{
    return 0.1;
}




- (void) wake {
    _awake = YES;
    _body->SetActive(true);
    _body->ApplyLinearImpulse(b2Vec2(1.4f,2), _body->GetPosition());
}

- (void) wakeContinue{
	_awake = YES;
	_body->SetActive(true);
}

- (void)sleep{
    _awake = NO;
    _body->SetActive(false);
    
}


#pragma mark - Simple Dive

- (void) dive {
    _wasDive = YES;
    if (yForce<50) {
        // sa cada ushor si sa formeze o curba
        yForce++;
    }else{
        yForce =50;
    }
//    _body->ApplyForce(b2Vec2((1+((_nPerfectSlides>5)?_nPerfectSlides:5)) ,-yForce-(_nPerfectSlides/2)),_body->GetPosition());
_body->ApplyForce(b2Vec2((1+((_nPerfectSlides>5)?_nPerfectSlides:5)) ,-yForce),_body->GetPosition());
    [self limitVelocityForDive];
    if (_isSuperDive) {
        _isSuperDive = NO;
    }
}

- (void)resetVelocityY {
    minVelocityY = -35;
}

- (void) limitVelocity:(int)velocity {
    if (!_awake) return;
    const float minVelocityX = 3;
    int8_t bonusPerfect = _nPerfectSlides*2;
    
    b2Vec2 vel = _body->GetLinearVelocity();
    if (vel.x < (minVelocityX + bonusPerfect)) {
       vel.x = (minVelocityX + bonusPerfect)*velocity;
    }
    if (vel.y < minVelocityY-bonusPerfect/2) {
        vel.y = minVelocityY-bonusPerfect/2;
    }
    _body->SetLinearVelocity(vel);
    
    yForce = 30+_nPerfectSlides;
    if (yForce>50) {
        yForce = 50;
    }
}


#pragma mark - Force Dive

- (void)diveForce{
    if (!_isSuperDive) {
        _isSuperDive = YES;
    }
    _body->ApplyForce(b2Vec2(15 ,-90),_body->GetPosition());
}

- (void)limitVelocityForDive{
    if (!_awake) return;
    
    const float minVelocityX = 10;
    
    b2Vec2 vel = _body->GetLinearVelocity();
    if (vel.x < minVelocityX) {
        vel.x = minVelocityX;
    }
    if (vel.y < minVelocityY) {
        vel.y = minVelocityY;
    }
    _body->SetLinearVelocity(vel);
}


#pragma mark - 
#pragma mark UPDATE

- (void)update {
    if (_body->GetPosition().y*PTM_RATIO > 800) {
        if (minVelocityY > -60) {
            minVelocityY--;
        }
    }

    self.position = ccp(_body->GetPosition().x*PTM_RATIO, _body->GetPosition().y*PTM_RATIO);

    b2Vec2 vel = _body->GetLinearVelocity();
    b2Vec2 weightedVel = vel;
    
    b2ContactEdge *c = _body->GetContactList();
	if (c) {
		if (_flying) {
			[self landed];
		}
	} else {
		if (!_flying && _wasDive) {
            _wasDive = NO;
			[self tookOff];
		}
	}
    
    for(int i = 0; i < NUM_PREV_VELS; ++i) {
        weightedVel += _prevVels[i];
    }
    weightedVel = b2Vec2(weightedVel.x/NUM_PREV_VELS, weightedVel.y/NUM_PREV_VELS);
    _prevVels[_nextVel++] = vel;
    if (_nextVel >= NUM_PREV_VELS) _nextVel = 0;
    
    float angle = ccpToAngle(ccp(weightedVel.x, weightedVel.y));
    if (_awake) {
        self.rotation = -1 * CC_RADIANS_TO_DEGREES(angle);
    }

}


#pragma mark -
#pragma  mark Functions Information On Screen

- (void) landed {
	_flying = NO;
}

- (void) tookOff {
	_flying = YES;
	b2Vec2 vel = _body->GetLinearVelocity();
	if (vel.y > kPerfectTakeOffVelocityY) {
		_nPerfectSlides++;
		if (_nPerfectSlides > 1) {
            if (_nPerfectSlides >= 3) {
				[_game showFrenzy];
			} else {
				[_game showPerfectSlide];
			}
		}
	}
}

- (void) hit {
    if (!_isSuperDive) {
        _wasDive = NO;
        _nPerfectSlides = 0;
        [_game showHit];
    }
}


#pragma mark - REMOVE HERO
- (void)removeHero{
    [[personaj person] removeFromParentAndCleanup:NO];
}

#pragma mark - RESET HERO

- (void)resetHero {
    _flying = NO;
    yForce = 30;
    _isSuperDive = NO;
    minVelocityY = -35;
	_nPerfectSlides = 0;
    
	if (_body) {
		_game.world->DestroyBody(_body);
	}
	[self createBody];
    [self update];
    self.rotation = -1 * CC_RADIANS_TO_DEGREES(0);
	[self sleep];
}

- (void) resetWithPosition:(CGPoint)bodyPosition {
	_flying = NO;
	if (_body) {
		_game.world->DestroyBody(_body);
	}
	[self createBodyWithPosition:bodyPosition];
	[self update];
    self.rotation = -1 * CC_RADIANS_TO_DEGREES(0);
	[self sleep];
}

- (void)setWinnerPosition:(int_fast8_t)winnerPosition{
    _winPosition = winnerPosition;
}

@end

