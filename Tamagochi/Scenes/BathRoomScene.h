//
//  FirstScene.h
//  Tamagochi
//
//  Created by Sergiu Moțipan on 6/4/13.
//
//

#import "CCLayer.h"
#import "cocos2d.h"
#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES2/gl.h>
#import "Box2D.h"
#import "GLES-Render.h"
#import "PouNode.h"
#import "SimpleAudioEngine.h"
#import "CCSprite+HSVTransform.h"
#import "CCTexture2D+HSVTransform.h"


#define PTM_RATIO 32

@interface BathRoomScene : CCLayer<PouNodeDelegate>
{
   
    CGSize winSize;
    CCSprite *stergar;
    CCSprite *centerBottom;
    CCSprite *rightBottom;
    CCSprite *ochelari;
    CCSprite *eye;
    int nr_sounds;
    float touchFun;
    CCParticleSystem* rainEmitter;
    CCSprite *stishka;
    CCSprite *cranik;
    CCSprite *soap;
    double hue;
    double sat;
    double val;
    HSVTransform transform;
    int stateEye;
    int mouth;
    ALuint sound_dush;
    ALuint sound_soap;
    ALuint sound_stergar;
    CGPoint startTouchPoint;
    CGPoint endTouchPoint;
    
    NSDate *date_start;
    
}
+(CCScene *) scene;
@end
