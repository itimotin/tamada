//
//  LockedCell.h
//  Tamagochi
//
//  Created by Sergiu Moțipan on 11/26/13.
//
//

#import <UIKit/UIKit.h>

@interface LockedCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UITextView *lockedText;
@property (retain, nonatomic) IBOutlet UIButton *waitButton;
@property (retain, nonatomic) IBOutlet UIButton *unlockButton;
@property (retain, nonatomic) IBOutlet UILabel *titleUnlockBtn;
@property (retain, nonatomic) IBOutlet UILabel *titleWaitBtn;
@property (retain, nonatomic) IBOutlet UIImageView *imgLock;

@end
