//
//  FirstScene.h
//  Tamagochi
//
//  Created by Sergiu Moțipan on 6/4/13.
//
//

#import "CCLayer.h"
#import "cocos2d.h"
#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES2/gl.h>
#import "Box2D.h"
#import "GLES-Render.h"
#import "PouNode.h"
#import "SimpleAudioEngine.h"

#define PTM_RATIO 32

enum shopTap {
    shopPotions =0,
    shopSkins = 1,
    shopEat = 2,
    shopBody = 3
};

@interface InMallRoom : CCLayer< PouNodeDelegate>
{
    CGSize winSize;
    CCSprite *leftBottom;
    GLESDebugDraw *m_debugDraw;
    int nr_sounds;
    float touchFun;
    CCLayerColor * colorLayer;
    int shopTapped;
    int mouth;
    ALuint sound1;
    
    CGPoint startTouchPoint;
    CGPoint endTouchPoint;
    
    NSDate *date_start;

}
+(CCScene *) scene;
@end
