//
//  SettingsView.h
//  Tamagochi
//
//  Created by Sergiu Moțipan on 12/25/13.
//
//

#import <UIKit/UIKit.h>

@interface SettingsView : UIView <UIAlertViewDelegate>
{
    IBOutlet UIView *viewInfo;
    IBOutlet UIView *viewSettings;
    IBOutlet UILabel *lab_next;
    IBOutlet UILabel *lab_level;
    IBOutlet UILabel *lab_size;
    IBOutlet UILabel *lab_adult;
    IBOutlet UIButton *btn_back;
    IBOutlet UILabel *nameView;
    IBOutlet UILabel *num_level;
    IBOutlet UILabel *num_size;
    IBOutlet UILabel *num_adult;
    IBOutlet UILabel *num_next;
    IBOutlet UILabel *lab_settings;
    IBOutlet UILabel *lab_share;
}
- (IBAction)settings_tap:(id)sender;
- (IBAction)close:(id)sender;
- (IBAction)back:(id)sender;
- (IBAction)getAccount:(id)sender;
- (IBAction)sound_onOff:(id)sender;
- (IBAction)music_onOff:(id)sender;
- (IBAction)notifications_onOff:(id)sender;
@property (retain, nonatomic) IBOutlet UISwitch *switchSound;
@property (retain, nonatomic) IBOutlet UISwitch *switchMusic;
@property (retain, nonatomic) IBOutlet UISwitch *switchNotifications;

@end
