//
//  SHKShareMenu.m
//  ShareKit
//
//  Created by Nathan Weiner on 6/18/10.

//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//
//

#import "SHKConfiguration.h"
#import "SHKShareMenu.h"
#import "SHK.h"
#import "SHKSharer.h"
#import "SHKCustomShareMenuCell.h"
#import "SHKShareItemDelegate.h"

@implementation SHKShareMenu

@synthesize item;
@synthesize tableData;
@synthesize exclusions;
@synthesize shareDelegate;
@synthesize tableView;
@synthesize textToSend;

#pragma mark -
#pragma mark Initialization

- (void)dealloc 
{
	[item release];
	[tableData release];
	[exclusions release];
	[shareDelegate release];
    [super dealloc];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
	if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])
	{
		/*self.title = SHKLocalizedString(@"Share");
         
         self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
         target:self
         action:@selector(cancel)];
         
         self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:SHKLocalizedString(@"Edit")
         style:UIBarButtonItemStyleBordered
         target:self
         action:@selector(edit)];
         */
        
        self.tableView.backgroundColor = [UIColor clearColor];
        self.navigationController.navigationBarHidden = YES;
        
		
	}
	return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (SHKCONFIG(formBackgroundColor) != nil)
        self.tableView.backgroundColor = SHKCONFIG(formBackgroundColor);
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
	
	// Remove the SHK view wrapper from the window
	[[SHK currentHelper] viewWasDismissed];
}

- (void)setItem:(SHKItem *)i
{
	[item release];
	item = [i retain];
	
	[self rebuildTableDataAnimated:NO];
}

- (void)rebuildTableDataAnimated:(BOOL)animated
{
	self.tableView.allowsSelectionDuringEditing = YES;
	self.tableData = [NSMutableArray arrayWithCapacity:0];
	[tableData addObject:[self section:@"actions"]];
	[tableData addObject:[self section:@"services"]];
    
	// Handling Excluded items
	// If in editing mode, show them
	// If not editing, hide them
	
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"SHKExcluded"] != nil){
    
        NSObject *excluded = [[NSUserDefaults standardUserDefaults] objectForKey:@"SHKExcluded"];
        
        //due to backwards compatibility - SHKExcluded used to be saved as NSDictionary. It is better as NSArray, as favourites are NSArray too.
        if ([excluded isKindOfClass:[NSDictionary class]]) {
            [self setExclusions:[NSMutableArray arrayWithArray:[(NSDictionary*)excluded allKeys]]];
        } else if ([excluded isKindOfClass:[NSArray class]]) {
            [self setExclusions:[NSMutableArray arrayWithArray:(NSArray*)excluded]];
        }
    }else{
        [self setExclusions:[NSMutableArray arrayWithCapacity:0]];
    }
    
	NSMutableArray *excluded = [NSMutableArray arrayWithCapacity:0];
    
	if (!self.tableView.editing || animated)
	{
		int s = 0;
		int r = 0;
		
		// Use temp objects so we can mutate as we are enumerating
		NSMutableArray *sectionCopy;
		NSMutableDictionary *tableDataCopy = [[tableData mutableCopy] autorelease];
		NSMutableIndexSet *indexes = [[NSMutableIndexSet alloc] init];
        
		for(NSMutableArray *section in tableDataCopy)
		{
			r = 0;
			[indexes removeAllIndexes];
			
			sectionCopy = [[section mutableCopy] autorelease];
			
			for (NSMutableDictionary *row in section)
			{
				if ([exclusions containsObject:[row objectForKey:@"className"]])
				{
					[excluded addObject:[NSIndexPath indexPathForRow:r inSection:s]];
					
					if (!self.tableView.editing)
						[indexes addIndex:r];
				}
				
				r++;
			}
            
			if (!self.tableView.editing)
			{
				[sectionCopy removeObjectsAtIndexes:indexes];
				[tableData replaceObjectAtIndex:s withObject:sectionCopy];
			}
			
			s++;
		}
		
		[indexes release];
		
		if (animated)
		{
			[self.tableView beginUpdates];	
			
			if (!self.tableView.editing)
				[self.tableView deleteRowsAtIndexPaths:excluded withRowAnimation:UITableViewRowAnimationFade];		
			else
				[self.tableView insertRowsAtIndexPaths:excluded withRowAnimation:UITableViewRowAnimationFade];		
			
			[self.tableView endUpdates];
		}
	}
	
}

- (NSMutableArray *)section:(NSString *)section
{
	id class;
	NSMutableArray *sectionData = [NSMutableArray arrayWithCapacity:0];	
	NSArray *source = [[SHK sharersDictionary] objectForKey:section];
	
	for( NSString *sharerClassName in source)
	{
		class = NSClassFromString(sharerClassName);
		if ( [class canShare] && [class canShareType:item.shareType] )
			[sectionData addObject:[NSDictionary dictionaryWithObjectsAndKeys:sharerClassName,@"className",[class sharerTitle],@"name",nil]];
	}
    
	if (sectionData.count && [SHKCONFIG(shareMenuAlphabeticalOrder) boolValue])
		[sectionData sortUsingDescriptors:[NSArray arrayWithObject:[[[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)] autorelease]]];
	
	return sectionData;
}

#pragma mark -
#pragma mark View lifecycle

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation 
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)_tableView 
{
    return tableData.count;
}


- (NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)section 
{
    return [[tableData objectAtIndex:section] count];
}


- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{    
    static NSString *CellIdentifier = @"Cell";
    UIImageView *imgview = nil;
    
    SHKCustomShareMenuCell *cell = (SHKCustomShareMenuCell *)[_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
	{
        cell = [[[SHKCustomShareMenuCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
		//cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	}
    
	NSDictionary *rowData = [self rowDataAtIndexPath:indexPath];

    imgview= [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 245,  49)];
    imgview.tag = 1024;
    UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"btn_%@.png",[rowData objectForKey:@"name"]]];
    NSLog(@"img is %@",img);
    imgview.image = img;
    NSLog(@"image is %@",[NSString stringWithFormat:@"btn_%@.png",[rowData objectForKey:@"name"]]);
    if([cell viewWithTag:1024]){
        [[cell viewWithTag:1024] removeFromSuperview];
    }
    
    
    [cell addSubview:[imgview autorelease]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    
   	
	if (cell.editingAccessoryView == nil)
	{
		UISwitch *toggle = [[UISwitch alloc] initWithFrame:CGRectZero];
		toggle.userInteractionEnabled = NO;
		cell.editingAccessoryView = toggle;
		[toggle release];
	}
	
	[(UISwitch *)cell.editingAccessoryView setOn:![exclusions containsObject:[rowData objectForKey:@"className"]]];
	
    return cell;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)_tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{	
	return UITableViewCellEditingStyleNone;
}

- (BOOL)tableView:(UITableView *)_tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
	return NO;
}
- (UIView *)tableView:(UITableView *)_tableView viewForHeaderInSection:(NSInteger)section{
    return nil;
}
- (UIView *)tableView:(UITableView *)_tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
	NSDictionary *rowData = [self rowDataAtIndexPath:indexPath];
	
	if (tableView.editing)
	{
		UITableViewCell *cell = [_tableView cellForRowAtIndexPath:indexPath];
		
		UISwitch *toggle = (UISwitch *)[cell editingAccessoryView];
		BOOL newOn = !toggle.on;
		[toggle setOn:newOn animated:YES];
		
		if (newOn) {
			[exclusions removeObject:[rowData objectForKey:@"className"]];
            
		} else {
			NSString *sharerId = [rowData objectForKey:@"className"];
			[exclusions addObject:sharerId];
			[SHK logoutOfService:sharerId];
		}
        
		[self.tableView deselectRowAtIndexPath:indexPath animated:NO];
	}
	
	else 
	{
		bool doShare = YES;
		SHKSharer* sharer = [[[NSClassFromString([rowData objectForKey:@"className"]) alloc] init] autorelease];
		[sharer loadItem:item];
		if (shareDelegate != nil && [shareDelegate respondsToSelector:@selector(aboutToShareItem:withSharer:)])
		{
			doShare = [shareDelegate aboutToShareItem:item withSharer:sharer];
		}
		if(doShare)
			[sharer share];
		
		[[SHK currentHelper] hideCurrentViewControllerAnimated:YES];
	}
}

- (NSDictionary *)rowDataAtIndexPath:(NSIndexPath *)indexPath
{
	return [[tableData objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
}



#pragma mark -
#pragma mark Toolbar Buttons

- (void)cancel
{
	[[SHK currentHelper] hideCurrentViewControllerAnimated:YES];
}

- (void)edit
{
	[self.tableView setEditing:YES animated:YES];
	[self rebuildTableDataAnimated:YES];
	
	[self.navigationItem setRightBarButtonItem:[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                              target:self
																							  action:@selector(save)] autorelease] animated:YES];
}

- (void)save
{
	[[NSUserDefaults standardUserDefaults] setObject:exclusions forKey:@"SHKExcluded"];	
	
	[self.tableView setEditing:NO animated:YES];
	[self rebuildTableDataAnimated:YES];
	
	[self.navigationItem setRightBarButtonItem:[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit
																							  target:self
																							  action:@selector(edit)] autorelease] animated:YES];	
}


-(void)viewDidUnload{
    self.textToSend = nil;
    self.tableView = nil;
    [super viewDidUnload];
}

- (IBAction)copyText:(id)sender {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.textToSend;
    
}

- (IBAction)sendSmsWithText:(id)sender {
    MFMessageComposeViewController *controller = [[[MFMessageComposeViewController alloc] init] autorelease];
    if([MFMessageComposeViewController canSendText])
    {
        controller.body = self.textToSend;
        controller.recipients = [NSArray arrayWithObjects:nil, nil];
        controller.messageComposeDelegate = self;
        [self presentModalViewController:controller animated:YES];
    }
    
}



#pragma mark - SMS Delegates
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
	switch (result) {
		case MessageComposeResultCancelled:
			NSLog(@"Cancelled");
			break;
		case MessageComposeResultFailed:{
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information" message:@"Unknown Error"
														   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
			[alert release];
        }
			break;
		case MessageComposeResultSent:
            
			break;
		default:
			break;
	}
    
	[self dismissModalViewControllerAnimated:YES];
}


@end

