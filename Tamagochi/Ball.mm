//
//  Ball.m
//  Tennis
//
//  Created by Sergiu Motipan on 10/22/12.
//
//

#import "Ball.h"



@implementation Ball
@synthesize delegate, ballBody;
class QueryCallback : public b2QueryCallback
{
public:
    QueryCallback(const b2Vec2& point)
    {
        m_point = point;
        m_fixture = NULL;
    }
    bool ReportFixture(b2Fixture* fixture)
    {
        b2Body* body = fixture->GetBody();
        if (body->GetType() == b2_dynamicBody)
        {
            bool inside = fixture->TestPoint(m_point);
            if (inside)
            {
                m_fixture = fixture;
                // We are done, terminate the query.
                return false;
            }
        }
        // Continue the query.
        return true;
    }
    b2Vec2 m_point;
    b2Fixture* m_fixture;
};

+(CCScene *)scene
{
    CCScene *scene=[CCScene node];
    Ball *bal=[Ball node];
    [scene addChild:bal];
    return  scene;
}

- (void)performBlock:(void (^)(void))block afterDelay:(NSTimeInterval)delay
{
    int64_t delta = (int64_t)(1.0e9 * delay);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delta), dispatch_get_main_queue(), block);
}
-(id)init
{
    self=[super init];
    if (self!=nil)
    {
        startRez=0.0;
        startFun=0.0;
        self.isTouchEnabled = YES;
        self.isAccelerometerEnabled = YES;
        [self initPhysic];
        
        // Create contact listener
        _contactListener = new MyContactListener();
        _world->SetContactListener(_contactListener);
        [[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"helicopter.mp3"];
        _start=NO;
       [self schedule:@selector(tick:)];
        
    }
    return self;
}

- (void)accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration
                                                                      *)acceleration {
    
    float32 accelerationFraction = acceleration.y * 6;
    if (accelerationFraction < -1) {
        accelerationFraction = -1;
    } else if (accelerationFraction > 1) 
        accelerationFraction = 1;
    
    b2Vec2 gravity(acceleration.x *5, acceleration.y *5);
        _world->SetGravity( gravity );
    
}
-(void)initPhysic
{
    s = [[CCDirector sharedDirector] winSize];
	k=0;
	b2Vec2 gravity;
	gravity.Set(0.0f, 0.0f);
	_world = new b2World(gravity);
	
	
	// Do we want to let bodies sleep?
	_world->SetAllowSleeping(true);

	_world->SetContinuousPhysics(true);
	
	m_debugDraw = new GLESDebugDraw( PTM_RATIO );
	_world->SetDebugDraw(m_debugDraw);
	
	uint32 flags = 0;
	flags += b2Draw::e_shapeBit;

	m_debugDraw->SetFlags(flags);
	
	
	// Define the ground body.
	b2BodyDef groundBodyDef;
	groundBodyDef.position.Set(0, 0); // bottom-left corner
	
	// Call the body factory which allocates memory for the ground body
	// from a pool and creates the ground box shape (also from a pool).
	// The body is also added to the world.
    _groundBody = _world->CreateBody(&groundBodyDef);
	
	// Define the ground box shape.
	b2EdgeShape groundBox;
	
	// bottom
	
	groundBox.Set(b2Vec2(0,0), b2Vec2(s.width/PTM_RATIO,0));
	//_groundBody->CreateFixture(&groundBox,0);
    _bottomFixture = _groundBody->CreateFixture(&groundBox,0);
	
	// top
	groundBox.Set(b2Vec2(0,s.height/PTM_RATIO), b2Vec2(s.width/PTM_RATIO,s.height/PTM_RATIO));
	_topFixture= _groundBody->CreateFixture(&groundBox,0);
	
	// left
	groundBox.Set(b2Vec2(0,s.height/PTM_RATIO), b2Vec2(0,0));
	_leftFixture= _groundBody->CreateFixture(&groundBox,0);
	
	// right
	groundBox.Set(b2Vec2(s.width/PTM_RATIO,s.height/PTM_RATIO), b2Vec2(s.width/PTM_RATIO,0));
	_rightFixture= _groundBody->CreateFixture(&groundBox,0);
    
    NSString *device=@"";
    if (IS_IPAD) device=@"-iPad";
    // Create sprite and add it to the layer
    ball = [CCSprite spriteWithFile:[NSString stringWithFormat:@"helicopter%@.png",device]];
    [ball setScale:0.5+0.5*IS_RETINA];
   // [ball setScale:0.5];
    ball.position = ccp(100, 100);
    ball.tag = 1;
    [self addChild:ball z:5];
    
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:[NSString stringWithFormat:@"helicopter_morisca%@.plist", device]];
    morisca = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"morisca%@.png",device]];
     if (!IS_IPAD)
         morisca.position = ccp(37, 67);
    else
         morisca.position = ccp(137, -100+100*IS_RETINA);
    CCSpriteBatchNode *batchNode = [CCSpriteBatchNode batchNodeWithFile:[NSString stringWithFormat:@"helicopter_morisca%@.png", device]];
    [batchNode addChild:morisca];
    
    morisca_spate = [CCSprite spriteWithFile:[NSString stringWithFormat:@"morisca_spate%@.png",device]];
    if (!IS_IPAD)
        morisca_spate.position = ccp(93, 43);
    else
        morisca_spate.position = ccp(893-850*IS_RETINA, 343+150*IS_RETINA);
    [ball addChild:morisca_spate];
    [ball addChild:batchNode];
    
    NSMutableArray *animFrames = [NSMutableArray array];
    CCSpriteFrame *frame1 = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"morisca%@.png",device]];
    CCSpriteFrame *frame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"morisca_02%@.png",device]];
    [animFrames addObject:frame1];
    [animFrames addObject:frame];
    animation =[CCAnimation animationWithSpriteFrames:animFrames delay:0.1];
    [morisca runAction:[CCRepeatForever actionWithAction: [CCAnimate actionWithAnimation:animation] ]];
    [morisca_spate runAction:[CCRepeatForever actionWithAction:[CCRotateBy actionWithDuration:0.5 angle:360]] ];
    
  
    
    umbra = [CCSprite spriteWithFile:@"umbra_elicopter.png"];
    // [ball setScale:0.5];
    umbra.position = ccp(250, 20);
    umbra.tag = 1;
    [self addChild:umbra z:3];

    // Create ball body
    b2BodyDef ballBodyDef;
    ballBodyDef.type = b2_dynamicBody;
    ballBodyDef.fixedRotation=YES;
    ballBodyDef.position.Set(100/PTM_RATIO, 100/PTM_RATIO);
    ballBodyDef.userData = ball;
   ballBody = _world->CreateBody(&ballBodyDef);
    
    // Create circle shape
    b2CircleShape circle;
    circle.m_radius = 26.0/PTM_RATIO;
    
    // Create shape definition and add to body
    b2FixtureDef ballShapeDef;
    ballShapeDef.shape = &circle;
    ballShapeDef.density = 1.5f;
    ballShapeDef.friction = 0.8f;
    ballShapeDef.restitution = 0.8f;
    
    _ballFixture = ballBody->CreateFixture(&ballShapeDef);
    
    if (!IS_IPAD)
        ballBody->SetTransform(b2Vec2(8, 1.5), 0);
    else
        ballBody->SetTransform(b2Vec2(20, 2.5), 0);
    
    [self performBlock:^{
            [morisca pauseSchedulerAndActions];
            [morisca_spate pauseSchedulerAndActions];
        } afterDelay:0.5];
    
}

- (void)tick:(ccTime) dt {
    
   
    if (ball.position.x<=s.width/2)
    {
        if (!IS_IPAD)
        {
            morisca_spate.position = ccp(5, 43);
            [morisca setPosition:ccp(65, 67)];
        }
        else
        {
            morisca_spate.position = ccp(35-12*IS_RETINA, 190-95*IS_RETINA);
            [morisca setPosition:ccp(282-142*IS_RETINA, 285-140*IS_RETINA)];
        }
          [ball runAction:[CCFlipX actionWithFlipX:YES]];
       
    }
    else
    {
        if (!IS_IPAD)
        {
            morisca_spate.position = ccp(93, 43);
            [morisca setPosition:ccp(37, 67)];
        }
        else
        {
            morisca_spate.position = ccp(410-210*IS_RETINA, 190-95*IS_RETINA);
            [morisca setPosition:ccp(160-81*IS_RETINA, 285-140*IS_RETINA)];
        }
        [ball runAction:[CCFlipX actionWithFlipX:NO]];
    }
    
     if (ball.position.y<=77.0)
    {
        umbra.position=ccp(ball.position.x, ball.position.y-28);
        [umbra setScale:((ball.position.y*(-1)+77)/100)*3];
    }

    
    b2Vec2 force;
	force.Set(0.0f, 80.0f);
    
    _world->Step(dt, 10, 10);
    for(b2Body *b = _world->GetBodyList(); b; b=b->GetNext()) {
        
        
        if (b->GetUserData() != NULL)
        {
            CCSprite *sprite = (CCSprite *)b->GetUserData();
            sprite.position = ccp(b->GetPosition().x * PTM_RATIO,
                                  b->GetPosition().y * PTM_RATIO);
            
            if (_start)
            {
                [delegate changePosition:sprite.position];
                float x=ballBody->GetLinearVelocity().x;
                float y=ballBody->GetLinearVelocity().y;
             
                
                if (x<0) x*=-1;
                if (y<0) y*=-1;
         
                if ((x>1.0 || y>1.0) && ((ball.position.x>ball.boundingBox.size.width/2 && ball.position.x<WIDTH_DEVICE - ball.boundingBox.size.width/2) && (ball.position.y>ball.boundingBox.size.height/2 && ball.position.y<HEIGHT_DEVICE - ball.boundingBox.size.height/2) ))
                {
                    
                    k=0;
                }
                if ([stateTamagochi funny]<100 && [stateTamagochi energy]>0.0)
                {
                    [stateTamagochi setFunny:[stateTamagochi funny]+x/1000+y/1000];
                    startFun+=x/1000+y/1000;
                }
                
                float s1=x/5000-y/5000;
                if (s1<0) s1=s1*(-1);
                if ([stateTamagochi energy]-s1>=0.0)
                [stateTamagochi setEnergy:[stateTamagochi energy]-s1];
               
                startRez+=s1/50;
                
            }
           
            sprite.rotation = -1 * CC_RADIANS_TO_DEGREES(b->GetAngle());
            if (b==ballBody){        
                b2Vec2 callPoint;
                callPoint.Set (b->GetPosition().x,b->GetPosition().y);
                QueryCallback callback(callPoint);
                _world->QueryAABB(&callback, aabb);
            }
        }
    }
    
    //check contacts
    std::vector<MyContact>::iterator pos;
    for(pos = _contactListener->_contacts.begin();
        pos != _contactListener->_contacts.end(); ++pos) {
        MyContact contact = *pos;
        
        if ((contact.fixtureA == _bottomFixture && contact.fixtureB == _ballFixture) ||
            (contact.fixtureA == _ballFixture && contact.fixtureB == _bottomFixture) ||
            (contact.fixtureA == _ballFixture && contact.fixtureB == _leftFixture) ||
            (contact.fixtureA == _leftFixture && contact.fixtureB == _ballFixture) ||
            (contact.fixtureA == _ballFixture && contact.fixtureB == _rightFixture) ||
            (contact.fixtureA == _rightFixture && contact.fixtureB == _ballFixture) ||
            (contact.fixtureA == _ballFixture && contact.fixtureB == _topFixture) ||
            (contact.fixtureA == _topFixture && contact.fixtureB == _ballFixture)) {
           
            if (k==0)
                [[SimpleAudioEngine sharedEngine] playEffect:@"shot.wav" loop:NO];
            k++;
        }
    }

}


- (void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (_mouseJoint != NULL) return;
    
    UITouch *myTouch = [touches anyObject];
    CGPoint location = [myTouch locationInView:[myTouch view]];
    location = [[CCDirector sharedDirector] convertToGL:location];
    b2Vec2 locationWorld = b2Vec2(location.x/PTM_RATIO, location.y/PTM_RATIO);
    
    if (_ballFixture->TestPoint(locationWorld)) {
        if (![stateTamagochi gameStart])
        {
            [[personaj person] addMoney:50];
            [stateTamagochi setGameStart:YES];
            [stateTamagochi updateFirstStart:1];
        }
      
        
        if (!_start)
        soundBack=[[SimpleAudioEngine sharedEngine] playEffect:@"helicopter.mp3" loop:YES];
          _start=YES;
       
       [morisca resumeSchedulerAndActions];
        [morisca_spate resumeSchedulerAndActions];
        b2Vec2 gravity(1.0,-1.0);
        _world->SetGravity( gravity );
        self.isAccelerometerEnabled=YES;
        b2MouseJointDef md;
        md.bodyA = _groundBody;
        md.bodyB = ballBody;
        md.target = locationWorld;
        md.collideConnected = true;
        md.maxForce = 1000000.0f * ballBody->GetMass();
        _mouseJoint = (b2MouseJoint *)_world->CreateJoint(&md);
       ballBody->SetAwake(false);
    }
}

-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (_mouseJoint == NULL) return;
    
    UITouch *myTouch = [touches anyObject];
    CGPoint location = [myTouch locationInView:[myTouch view]];
    location = [[CCDirector sharedDirector] convertToGL:location];
    b2Vec2 locationWorld = b2Vec2(location.x/PTM_RATIO, location.y/PTM_RATIO);
    _mouseJoint->SetTarget(locationWorld);
    
}

-(void)ccTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (_mouseJoint) {
        _world->DestroyJoint(_mouseJoint);
        _mouseJoint = NULL;
    }
    
}

-(void)stopHelicopter
{
    [stateTamagochi setNextLevelPoints:[stateTamagochi nextLevelPoints] +startFun*5.0*(1.3-[stateTamagochi sizeTamagochi])];
    [stateTamagochi addToNextLevel:4];
    
    [stateTamagochi updateFun];
    [stateTamagochi updateEnergy];
    
    
    if ([stateTamagochi fat]>1.0)
        [stateTamagochi setFat:[stateTamagochi fat]-startRez];
    else if ([stateTamagochi fat]<1.0)
        [stateTamagochi setFat:1.0];
    if ([stateTamagochi food]>0)
    {
        [stateTamagochi setFood:[stateTamagochi food]-startRez*10];
        
    }
    if ([stateTamagochi  food]<0) [stateTamagochi setFood:0];
    [stateTamagochi updateFood];
    [stateTamagochi updateFat];
   
    startRez=0.0;
     startFun=0.0;
    [[SimpleAudioEngine sharedEngine] stopEffect:soundBack];
    _start=NO;
    b2Vec2 gravity(0,0);
    _world->SetGravity(gravity);
    self.isAccelerometerEnabled=NO;
    [morisca pauseSchedulerAndActions];
    [morisca_spate pauseSchedulerAndActions];
    ballBody->SetLinearVelocity(b2Vec2(0, 0));
    if (!IS_IPAD)
        ballBody->SetTransform(b2Vec2(8, 1.5), 0);
    else
        ballBody->SetTransform(b2Vec2(20, 2.5), 0);
    [delegate changePosition:CGPointMake(0, 0)];
}
- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if (_mouseJoint)
    {
        UITouch *myTouch = [touches anyObject];
        CGPoint location = [myTouch locationInView:[myTouch view]];
        location = [[CCDirector sharedDirector] convertToGL:location];
        
        if (CGRectContainsPoint([ball boundingBox],location) && (ballBody->GetLinearVelocity().x==0.0 ||  ballBody->GetLinearVelocity().y==0))
        {
            _start=NO;
         
              [self stopHelicopter];
         
        }
        _world->DestroyJoint(_mouseJoint);
        _mouseJoint = NULL;
    }
   
}

- (void)dealloc {
    
    [stateTamagochi updateFun];
    [stateTamagochi updateEnergy];
    [stateTamagochi updateFood];
    [stateTamagochi updateFat];
    delete _contactListener;
    delete _world;
    _groundBody = NULL;
    [super dealloc];
    
}

@end
