#include "HSVTransform.h"
#include <math.h>

#define SHIFT_R 0
#define SHIFT_G 8
#define SHIFT_B 16
#define SHIFT_A 24

float clamp01( float a)
{
    return a < 0 ? 0 : (a > 1 ? 1 : a);
}

unsigned int clamp255(unsigned int color)
{
    return color > 255 ? 255 : color;
}

HSVTransform HSVTransformCreate(double h, double s, double v)
{
    float vsu = v * s * cosf(h * M_PI / 180);
    float vsw = v * s * sinf(h * M_PI / 180);
    
    HSVTransform transform;
    
    transform.r.r = .299f * v + .701f * vsu + .168f * vsw;
    transform.r.g = .587f * v - .587f * vsu + .330f * vsw;
    transform.r.b = .114f * v - .114f * vsu - .497f * vsw;
    
    transform.g.r = .299f * v - .299f * vsu - .328f * vsw;
    transform.g.g = .587f * v + .413f * vsu + .035f * vsw;
    transform.g.b = .114f * v - .114f * vsu + .292f * vsw;
    
    transform.b.r = .299f * v - .300f * vsu + 1.25f * vsw;
    transform.b.g = .587f * v - .588f * vsu - 1.05f * vsw;
    transform.b.b = .114f * v + .886f * vsu - .203f * vsw;
    
    return transform;
}

RGBFloatColor HSVTransformColor(HSVTransform* transform, RGBFloatColor* color)
{
    RGBFloatColor result;
    
    result.r = clamp01(transform->r.r * color->r + transform->r.g * color->g + transform->r.b * color->b);
    result.g = clamp01(transform->g.r * color->r + transform->g.g * color->g + transform->g.b * color->b);
    result.b = clamp01(transform->b.r * color->r + transform->b.g * color->g + transform->b.b * color->b);
    
    return result;
}

unsigned int HSVTransformRGBA8(HSVTransform* transform, unsigned int rgba8)
{
    unsigned int r = (rgba8>>SHIFT_R) & 0xff;
    unsigned int g = (rgba8>>SHIFT_G) & 0xff;
    unsigned int b = (rgba8>>SHIFT_B) & 0xff;
    unsigned int a = (rgba8>>SHIFT_A) & 0xff;
    
    unsigned int resultR = clamp255(transform->r.r * r + transform->r.g * g + transform->r.b * b);
    unsigned int resultG = clamp255(transform->g.r * r + transform->g.g * g + transform->g.b * b);
    unsigned int resultB = clamp255(transform->b.r * r + transform->b.g * g + transform->b.b * b);
    
    return (resultR << SHIFT_R) | (resultG << SHIFT_G) | (resultB << SHIFT_B) | (a << SHIFT_A);
}