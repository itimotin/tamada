//
//  SettingsView.m
//  Tamagochi
//
//  Created by Sergiu Moțipan on 12/25/13.
//
//

#import "SettingsView.h"

@implementation SettingsView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)dealloc {
    [viewInfo release];
    [lab_next release];
    [lab_level release];
    [lab_size release];
    [lab_adult release];
    [viewSettings release];
    [btn_back release];
    [nameView release];
    [num_level release];
    [num_size release];
    [num_adult release];
    [num_next release];
    [_switchSound release];
    [_switchMusic release];
    [_switchNotifications release];
    [lab_settings release];
    [lab_share release];
    [super dealloc];
}

- (IBAction)settings_tap:(id)sender {
    if ([stateTamagochi soundOn])
        [[SimpleAudioEngine sharedEngine] playEffect:@"btn_press.mp3" loop:NO];
    [nameView setText:NSLocalizedString(@"Settings", nil)];
    [viewSettings setFrame:CGRectMake(0, 180, WIDTH_DEVICE, 300)];
    [viewInfo setFrame:CGRectMake(0, HEIGHT_DEVICE, WIDTH_DEVICE, 300)];
    [btn_back setAlpha:1.0];
}

- (IBAction)close:(id)sender {
    
    if (viewSettings.frame.origin.y!=HEIGHT_DEVICE)
        [self back:nil];
    [self removeFromSuperview];
}

- (IBAction)back:(id)sender {
    if ([stateTamagochi soundOn])
        [[SimpleAudioEngine sharedEngine] playEffect:@"btn_press.mp3" loop:NO];
    [nameView setText:NSLocalizedString(@"info", nil)];
    [lab_level setText:[NSString stringWithFormat:@"%@:", NSLocalizedString(@"level", nil)]];
    [num_level setText:[NSString stringWithFormat:@"%d",[stateTamagochi level]]];
    [lab_next setText:[NSString stringWithFormat:@"%d %@:", [stateTamagochi level]+1, NSLocalizedString(@"next_level", nil)]];
    [num_next setText:[NSString stringWithFormat:@"%d%%", (int)[stateTamagochi nextLevelPoints]]];
    [lab_size setText:[NSString stringWithFormat:@"%@:", NSLocalizedString(@"size", nil)]];
    NSString *str1;
    
    if ([stateTamagochi sizeLevel]<=7)
    {
        str1=[[stateTamagochi years] objectAtIndex:0];
    }
    else if ([stateTamagochi sizeLevel]<=17)
    {
        str1=[[stateTamagochi years] objectAtIndex:1];
    }
    else
    {
        str1=[[stateTamagochi years] objectAtIndex:2];
    }
   
    [num_size setText:str1];
    NSString *str;
    if ([stateTamagochi sizeLevel]<=7)
    {
        str=[[stateTamagochi years] objectAtIndex:1];
    }
    else
    {
        str=[[stateTamagochi years] objectAtIndex:2];
    }
  
    if ([stateTamagochi sizeLevel]<=17)
    {
        lab_adult.alpha=1.0;
        num_adult.alpha=1.0;
        [lab_adult setText:str];
        [num_adult setText:[NSString stringWithFormat:@"%d%%", [stateTamagochi procentYears]]];
    
        [lab_adult sizeToFit];
        [lab_adult setFrame:CGRectMake(300-lab_adult.frame.size.width, lab_adult.frame.origin.y, lab_adult.frame.size.width, lab_adult.frame.size.height)];
        [num_adult sizeToFit];
        [num_adult setFrame:CGRectMake(lab_adult.frame.origin.x-num_adult.frame.size.width-2, num_adult.frame.origin.y, num_adult.frame.size.width, num_adult.frame.size.height)];
    }
    else
    {
        lab_adult.alpha=0.0;
        num_adult.alpha=0.0;
    }

    [lab_settings setText:NSLocalizedString(@"Settings", nil)];
    [lab_share setText:NSLocalizedString(@"Share", nil)];
    [lab_level sizeToFit];
    [num_level sizeToFit];
    [num_level setFrame:CGRectMake(lab_level.frame.origin.x+lab_level.frame.size.width+2, num_level.frame.origin.y, num_level.frame.size.width, num_level.frame.size.height)];
    [lab_next sizeToFit];
    [num_next sizeToFit];
    [num_next setFrame:CGRectMake(lab_next.frame.origin.x+lab_next.frame.size.width+2, num_next.frame.origin.y, num_next.frame.size.width+10, num_next.frame.size.height)];
    
    [lab_size sizeToFit];
   
    [num_size sizeToFit];
    [num_size setFrame:CGRectMake(300-num_size.frame.size.width, num_size.frame.origin.y, num_size.frame.size.width, num_size.frame.size.height)];
    [lab_size setFrame:CGRectMake(num_size.frame.origin.x-lab_size.frame.size.width-2, lab_size.frame.origin.y, lab_size.frame.size.width, lab_size.frame.size.height)];
    
    
    [viewSettings setFrame:CGRectMake(0, HEIGHT_DEVICE, WIDTH_DEVICE, 300)];
    [viewInfo setFrame:CGRectMake(0, 180, WIDTH_DEVICE, 300)];
    [btn_back setAlpha:0.0];
}
- (IBAction)music_onOff:(id)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSString stringWithFormat:@"%d", [sender isOn]] forKey:@"musicOn"];
    [defaults synchronize];
    
    [stateTamagochi setMusicOn:[sender isOn]];
}

- (IBAction)notifications_onOff:(id)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSString stringWithFormat:@"%d", [sender isOn]] forKey:@"notificationsOn"];
    [defaults synchronize];
    [stateTamagochi setNotificationsOn:[sender isOn]];
}

- (IBAction)getAccount:(id)sender {
}

- (IBAction)sound_onOff:(id)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSString stringWithFormat:@"%d", [sender isOn]] forKey:@"soundOn"];
    [defaults synchronize];
    [[SimpleAudioEngine sharedEngine] setEffectsVolume:[sender isOn]];
    [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume:[sender isOn]];
    [stateTamagochi setSoundOn:[sender isOn]];
}

@end


