//
//  FirstScene.m
//  Tamagochi
//
//  Created by Sergiu Moțipan on 6/4/13.
//
//

#import "BedRoom.h"
#import "ClosetRoom.h"

@implementation BedRoom
{
    int touchObject;
    int timp;
    int timp_2;
    int mouse;
    int oche_mouse;
    int n_mouse;
    int eye_pos;
    float totalTime,yy,xx, tamTouch;
    BOOL repeatTouches;
    float limit;
    CCSprite *dark;
    CGRect shkafRect;
    int objTouch;
    NSTimer *timer;
    
}



+(CCScene *) scene
{
	CCScene *scene = [CCScene node];
	BedRoom *layer = [BedRoom node];
	[scene addChild: layer];
	return scene;
}

-(ccColor3B)tranformColor:(NSInteger)tag object:(NSInteger)object
{
    NSMutableArray *arr=[[[NSMutableArray alloc] initWithArray:[stateTamagochi colorSkin:tag object:object]] autorelease];
    ccColor3B color={[[arr objectAtIndex:0] floatValue], [[arr objectAtIndex:1] floatValue], [[arr objectAtIndex:2] floatValue]};
    return color;
}

-(CCAction*)initAnimations
{
    id action1;
    id action2;
    if (!IS_RETINA)
    {
        action1 = [CCScaleTo actionWithDuration:0.4 scale:0.55];
        action2 = [CCScaleTo actionWithDuration:0.4 scale:0.5];
    }
    else
    {
        action1 = [CCScaleBy actionWithDuration:0.4 scale:1.05];
        action2 = [CCScaleTo actionWithDuration:0.4 scale:1.0];
    }
    id  animationObjects = [CCSequence actions:action1, action2, nil];
    return [CCRepeatForever actionWithAction:animationObjects];
}


-(id)init
{
    self=[super init];
    if (self)
    {
        sleepOn=[stateTamagochi energy];
         [[SimpleAudioEngine sharedEngine] stopAllEffects];
        [[SimpleAudioEngine sharedEngine] setEffectsVolume:[stateTamagochi soundOn]];
        [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume:[stateTamagochi soundOn]];
        [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
        objTouch=0;
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(changeColor:) name:@"skinColor" object:nil];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(sleeping) name:@"sleeping" object:nil];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(timerStop) name:@"timerStop" object:nil];
        [stateTamagochi setRoom:bedRoom];
        stateEye=[stateTamagochi eyeState];
        mouth=[stateTamagochi mouthState];
        [[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"giggle.wav"];
        [[SimpleAudioEngine sharedEngine] preloadEffect:@"closet.mp3"];
        [[SimpleAudioEngine sharedEngine] preloadEffect:@"lamp.wav"];
        [[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"lullaby.mp3"];
        [[SimpleAudioEngine sharedEngine] setEffectsVolume:[stateTamagochi soundOn]];
        [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume:[stateTamagochi soundOn]];
        winSize = [[CCDirector sharedDirector] winSize];
        self.isTouchEnabled=YES;
        
        
        lamp = [CCSprite spriteWithFile:[NSString stringWithFormat:@"light_off%@.png",IMG_PREFIX]];
       
        if (!IS_IPAD)
            lamp.position = CGPointMake(70, winSize.height-210-43*IS_WIDESCREEN);
        else
            lamp.position = CGPointMake(115, winSize.height-498);
        lamp.tag=tagLamp;
        [lamp setScale:0.5+0.5*IS_RETINA];
		[self addChild:lamp z:2];
        if ([stateTamagochi energy]<20.0)
            [lamp runAction:[self initAnimations]];
        
        dark = [CCSprite spriteWithFile:[NSString stringWithFormat:@"darck%@.png",IMG_PREFIX]];
		dark.position = CGPointMake(winSize.width/2, winSize.height/2+20);
        [dark setScale:0.5+0.5*IS_RETINA];
		[self addChild:dark z:1];
        [dark setOpacity:0];
        
        CCSprite *background = [CCSprite spriteWithFile:[NSString stringWithFormat:@"bg_5%@.png",(IS_IPAD)?@"-iPad":@""]];
        background.tag=1;
        background.position = ccp(winSize.width/2, winSize.height/2+20);
        [background setColor:[self tranformColor:[stateTamagochi selectColorSkin:1 room:5] object:0]];
        [background setScale:0.5+0.5*IS_RETINA];
        [self addChild:background z:-12];
        
        CCSprite *background_img = [CCSprite spriteWithFile:[NSString stringWithFormat:@"bg_5_img%@.png",(IS_IPAD)?@"-iPad":@""]];
        background_img.position = ccp(winSize.width/2, winSize.height/2+50);
        [background_img setScale:0.5+0.5*IS_RETINA];
        [self addChild:background_img z:-11];
        
        CCSprite *mebeli = [CCSprite spriteWithFile:[NSString stringWithFormat:@"mebeli_5%@.png",(IS_IPAD)?@"-iPad":@""]];
        mebeli.tag=7;
        mebeli.position = ccp(winSize.width/2, winSize.height/2-28);
        [mebeli setColor:[self tranformColor:[stateTamagochi selectColorSkin:7 room:5] object:0]];
        [mebeli setScale:0.5+0.5*IS_RETINA];
        [self addChild:mebeli z:-10];
        
        if (![stateTamagochi closetStart])
        {
            CCSprite *open_start = [CCSprite spriteWithFile:[NSString stringWithFormat:@"btn_start_closet%@.png",(IS_IPAD)?@"-iPad":@""]];
            open_start.tag=17;
            open_start.position = ccp(winSize.width/2+20, winSize.height/2+45);
            [open_start setScale:0.5+0.5*IS_RETINA];
            [self addChild:open_start z:-9];
            [open_start runAction:[self initAnimations]];
        }
    
        
        CCSprite *toys = [CCSprite spriteWithFile:[NSString stringWithFormat:@"toys_5%@.png",(IS_IPAD)?@"-iPad":@""]];
        toys.position = ccp(winSize.width-toys.boundingBox.size.width/2, mebeli.position.y+mebeli.boundingBox.size.height/2-toys.boundingBox.size.height/5);
        [toys setScale:0.5+0.5*IS_RETINA];
        [self addChild:toys z:-10];
        
        CCSprite *frame = [CCSprite spriteWithFile:[NSString stringWithFormat:@"frame_5%@.png",(IS_IPAD)?@"-iPad":@""]];
        frame.tag=6;
        frame.position = ccp(winSize.width/2-frame.boundingBox.size.width, winSize.height/2+frame.boundingBox.size.height);
        [frame setColor:[self tranformColor:[stateTamagochi selectColorSkin:6 room:5] object:0]];
        [frame setScale:0.5+0.5*IS_RETINA];
        [self addChild:frame z:-9];
        
        CCSprite *frame_img = [CCSprite spriteWithFile:[NSString stringWithFormat:@"frame_5_img%@.png",(IS_IPAD)?@"-iPad":@""]];
        frame_img.tag=7;
        frame_img.position = ccp(winSize.width/2-frame.boundingBox.size.width, winSize.height/2+frame.boundingBox.size.height);
        [frame_img setScale:0.5+0.5*IS_RETINA];
        [self addChild:frame_img z:-9];
        
        CCSprite *posteli = [CCSprite spriteWithFile:[NSString stringWithFormat:@"posteli%@.png",(IS_IPAD)?@"-iPad":@""]];
        posteli.position = ccp(winSize.width-posteli.boundingBox.size.width/2, winSize.height/2-150);
        [posteli setScale:0.5+0.5*IS_RETINA];
        [self addChild:posteli z:-9];
    
       
        
        [personaj person].position = ccp(winSize.width/2+30*[stateTamagochi sizeTamagochi], 80+90*[stateTamagochi sizeTamagochi]+50*IS_WIDESCREEN+120*IS_IPAD);
        [personaj initRects];
        [personaj person].delegate=self;
        [self addChild:[personaj person] z:0];
        
        if (!IS_IPAD)
            shkafRect=CGRectMake(180, 170, 140, 230);
        else
            shkafRect=CGRectMake(400, 200, 360, 800);
        [stateTamagochi setTouchObject:tagDefault];
        [self scheduleUpdate];
    }
    
    return self;
}

-(void)timerStop
{
    if (timer)
    {
        [[SimpleAudioEngine sharedEngine] stopEffect:sound1];
        [timer invalidate];
        timer=nil;
    }
    [[SimpleAudioEngine sharedEngine] stopEffect:sound2];
}
-(void)changeColor:(NSNotification *)notif
{
    [(CCSprite *)[self getChildByTag:[[[notif.object componentsSeparatedByString:@","] objectAtIndex:1] intValue]] setColor:[self tranformColor:[[[notif.object componentsSeparatedByString:@","] objectAtIndex:0] intValue] object:[[[notif.object componentsSeparatedByString:@","] objectAtIndex:1] intValue]]];
}

- (void)update:(float)dt
{
    if ([stateTamagochi sleep])
        [self resetEnergy];
}

-(CGPoint)returnPosition
{
    if (![stateTamagochi sleep])
        return CGPointMake(xx, yy);
    else
        return CGPointZero;
}

-(float)returnTouchValue
{
    switch ([stateTamagochi mouthState]) {
        case mouthStateFun:
        {
            return tamTouch*sin(totalTime);
        }
            break;
        case mouthStateReadyEat:
        {
            return 0.3;
        }
            break;
        case mouthStateEat:
        {
            return 0.25*sin(totalTime);
        }
            break;
        case mouthStateDefault:
        {
            return 0.25*sin(totalTime);
        }
            break;
            
        default:return tamTouch;
            break;
    }
}


-(void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    nr_sounds=0;
    UITouch* touch = [touches anyObject];
    CGPoint location = [touch locationInView: [touch view]];
    startTouchPoint=location;
    date_start=[[NSDate date] retain];
    location = [[CCDirector sharedDirector] convertToGL:location];
    xx=location.x;
    yy=location.y;
    touchFun=0;
    if (CGRectContainsPoint( [ lamp   boundingBox], location) )
        objTouch=2;
    
    if (CGRectContainsPoint([personaj rectPerson],location))
    {
        [stateTamagochi setTouchObject:tagTamagochi];
    }
    
    if (CGRectContainsPoint( shkafRect, location) && [stateTamagochi touchObject]!=tagTamagochi)
    {
        objTouch=1;
    }
}

-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch* touch = [touches anyObject];
    CGPoint location = [touch locationInView: [touch view]];
    location = [[CCDirector sharedDirector] convertToGL:location];

    if ([stateTamagochi touchObject]==tagTamagochi && CGRectContainsPoint([personaj rectPerson],location) && ![stateTamagochi sleep])
    {
        [stateTamagochi setMouthState:mouthStateFun];
        touchFun+=0.006;
        if (touchFun>=0.05)
        {
            if (!repeatTouches)
            [self fun];
        }
        nr_sounds++;
    }
    if ((!CGRectContainsPoint(shkafRect, location) && objTouch==1) || (!CGRectContainsPoint([lamp boundingBox], location) && objTouch==2))
    {
        objTouch=0;
    }
    
    xx=location.x;
    yy=location.y;
        
    
}

-(void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [stateTamagochi setMouthState:mouth];
    UITouch* touch = [touches anyObject];
    CGPoint location = [touch locationInView: [touch view]];
    
    endTouchPoint=location;
    float moveDifference=startTouchPoint.x-endTouchPoint.x;
    float differenceY=startTouchPoint.y-endTouchPoint.y;
    if (differenceY<0) differenceY*=-1;
    NSTimeInterval secondsBetween = [[NSDate date] timeIntervalSinceDate:date_start];
    [date_start release];
    date_start=nil;
    if (moveDifference>150.0 && differenceY<40.0 && endTouchPoint.y<150.0 && secondsBetween<0.3)
        [[NSNotificationCenter defaultCenter]postNotificationName:@"nextRoom" object:@"1"];
    else if (moveDifference<-150.0 && differenceY<40.0 && endTouchPoint.y<150.0 && secondsBetween<0.3)
        [[NSNotificationCenter defaultCenter]postNotificationName:@"nextRoom" object:@"2"];
    
    location = [[CCDirector sharedDirector] convertToGL:location];
    
    if (CGRectContainsPoint( shkafRect, location) &&  [stateTamagochi eyeState]!=eyeStateErase && objTouch==1)
    {

            if (![stateTamagochi closetStart]) {
                [[personaj person] addMoney:50];
                [stateTamagochi setClosetStart:YES];
                [stateTamagochi updateFirstStart:5];
                if ([self getChildByTag:17])
                {
                    [[self getChildByTag:17] removeFromParentAndCleanup:YES];
                }
            }
        
            [[SimpleAudioEngine sharedEngine] playEffect:@"closet.mp3" loop:NO];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"renameRoom" object:@"Closed_room"];
            [[personaj person] removeFromParentAndCleanup:NO];
            [[CCDirectorIOS sharedDirector] replaceScene:[ClosetRoom scene]];
            [CCDirectorIOS sharedDirector].runningScene.tag=10;
    }
    
     if (CGRectContainsPoint( [lamp boundingBox], location) && objTouch==2)
     {
            [[SimpleAudioEngine sharedEngine] playEffect:@"lamp.wav" loop:NO];
         
        if (![stateTamagochi sleep])
        {
            [self sleep:YES];
        }
        else
        {
            [self sleep:NO];
        }
     }
    nr_sounds=0;
    touchFun=0;
    xx=0;
    yy=0;
      [stateTamagochi setTouchObject:tagDefault];
    if ([stateTamagochi touchObject]==2)
    touchObject=tagDefault;
}

-(void)sleeping
{
    [self sleep:YES];
}

-(void)sleepSound
{
    sound1=[[SimpleAudioEngine sharedEngine] playEffect:@"sleeping_adult.mp3" loop:NO];
}

-(void)sleep:(BOOL)sleep
{
    switch (sleep) {
        case 1:
        {
            if (![stateTamagochi sleepStart])
            {
                [[personaj person] addMoney:50];
                [stateTamagochi setSleepStart:YES];
                [stateTamagochi updateFirstStart:2];
            }
            
            rainEmitter = [CCParticleSmoke node];
            [rainEmitter setPosition:CGPointMake(200,180)];
            rainEmitter.gravity=CGPointMake(15, 5);
            rainEmitter.life = 3.0;
            rainEmitter.angle=80.0;
            
            rainEmitter.emissionRate=1.0f;
            rainEmitter.texture = [[CCTextureCache sharedTextureCache] addImage: @"zerply_z.png"];
            rainEmitter.startSize = 4.0f+4.0*IS_IPAD;
            rainEmitter.endSize=30.0f+6.0*IS_IPAD;
            rainEmitter.startColor=ccc4f(1.0, 1.0, 1.0, 1.0);
            rainEmitter.endColor=ccc4f(1.0, 1.0, 1.0, 1.0);
            [self addChild: rainEmitter z:21];
            
            
            if ([stateTamagochi sizeLevel]>18)
            timer= [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(sleepSound) userInfo:nil repeats:YES];
            sound2 = [[SimpleAudioEngine sharedEngine] playEffect:@"lullaby.mp3" loop:YES];
            sleepOn=[stateTamagochi energy];
            [stateTamagochi setSleepONBed:[stateTamagochi energy]];
            [stateTamagochi setSleep:YES];
            [stateTamagochi updateSleep];
            [stateTamagochi setEyeState:eyeStateErase];
            [dark runAction:[CCFadeIn actionWithDuration:0.5]];
            [lamp stopAllActions];
            lamp.scale=1.0;
            [lamp setTexture:[[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"light_on%@.png",IMG_PREFIX]]];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"moveCarousel" object:@"on"];

        }
            break;
        case 0:
        {
            if ([stateTamagochi sizeLevel]>18)
            {
                [timer invalidate];
                timer=nil;
            }
          
            if (rainEmitter)
            {
                [rainEmitter removeFromParentAndCleanup:YES];
                rainEmitter=nil;
            }
            
            [[SimpleAudioEngine sharedEngine] stopEffect:sound2];
            [stateTamagochi setSleep:NO];
            [stateTamagochi updateSleep];
            [stateTamagochi setEyeState:stateEye];
            [stateTamagochi updateEnergy];
            [stateTamagochi setNextLevelPoints:[stateTamagochi nextLevelPoints] +([stateTamagochi energy]-[stateTamagochi sleepONBed])];
            [stateTamagochi addToNextLevel:4];
            [stateTamagochi setHealth:[stateTamagochi health]+([stateTamagochi energy]-[stateTamagochi sleepONBed])/2.0 ];
            if ([stateTamagochi health]>100.0)
                [stateTamagochi setHealth:100.0];
            [stateTamagochi updateHealth];
            if ([stateTamagochi energy]<20)
            {
                [lamp runAction:[self initAnimations]];
            }
            stateEye=[stateTamagochi eyeState];
            [dark runAction:[CCFadeOut actionWithDuration:0.5]];
            [lamp setTexture:[[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"light_off%@.png",IMG_PREFIX]]];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"moveCarousel" object:@"off"];
        }
            break;
            
        default:
            break;
    }
}

-(void)resetEnergy
{
    [stateTamagochi setEnergy:[stateTamagochi energy]+0.0003];
}
-(void)changePosition:(CGPoint)pos
{
    xx=pos.x;
    yy=pos.y;
}

#pragma Tamagochi effects

-(void)resetVariables
{
    repeatTouches=NO;
    touchFun=0;
    if ([[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying])
        [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
}

-(void)fun
{
    repeatTouches=YES;
    totalTime=0;
    tamTouch=0.3;
    if (![[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying] &&  [stateTamagochi soundOn])
        [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"giggle.wav" loop:NO];
    [self performSelector:@selector(resetVariables) withObject:nil afterDelay:1.57];
}
-(void)dealloc
{
    [stateTamagochi setEyeState:stateEye];
    if (timer)
    {
        [timer invalidate];
        timer=nil;
    }
    [[SimpleAudioEngine sharedEngine] stopEffect:sound2];
    [[SimpleAudioEngine sharedEngine] stopEffect:sound1];
    [[NSNotificationCenter defaultCenter]    removeObserver:self name:@"sleeping" object:nil];
    [[NSNotificationCenter defaultCenter]    removeObserver:self name:@"skinColor" object:nil];
    [[NSNotificationCenter defaultCenter]    removeObserver:self name:@"timerStop" object:nil];
    [super dealloc];
}

@end
