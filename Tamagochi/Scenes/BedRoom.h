//
//  FirstScene.h
//  Tamagochi
//
//  Created by Sergiu Moțipan on 6/4/13.
//
//

#import "CCLayer.h"
#import "cocos2d.h"
#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES2/gl.h>
#import "Box2D.h"
#import "GLES-Render.h"
#import "PouNode.h"
#import "SimpleAudioEngine.h"

#define PTM_RATIO 32

@interface BedRoom: CCLayer< PouNodeDelegate>
{
    CGSize winSize;
    CCSprite *lamp;

    int nr_sounds;
    float touchFun;
    NSDate *timeStart;
    NSDate *timeEnd;
    int stateEye;
    int mouth;
    CCParticleSystem *rainEmitter;
    ALuint sound1;
    ALuint sound2;
    
    CGPoint startTouchPoint;
    CGPoint endTouchPoint;
    
    NSDate *date_start;
    float sleepOn;
    
}
+(CCScene *) scene;
@end
