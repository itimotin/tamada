

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "Box2D.h"

#define kPerfectTakeOffVelocityY 2.0f
#define kEnemyTag 110

@class TinyPoo;


@interface Enemy : CCNode {
	TinyPoo *_game;
	CCSprite *_sprite;
	b2Body *_body;
	float _radius;
	BOOL _awake;
	BOOL _flying;
	BOOL _diving;
}
@property (nonatomic, retain) TinyPoo *game;
@property (nonatomic, retain) CCSprite *sprite;
@property (readonly) BOOL awake;
@property (nonatomic) BOOL diving;

+ (id) heroWithGame:(TinyPoo*)game withName:(NSString *)bodyName withPosition:(CGPoint)bodyPosition;
- (id) initWithGame:(TinyPoo*)game withName:(NSString *)bodyName withPosition:(CGPoint)bodyPosition;

//- (void) reset;
- (void) sleep;
- (void) wake;
- (void) updateNode;
- (void) timeToDive:(int8)forceDive;
- (void) simpleVelocity;
- (void) wakeContinue;

- (void)resetEnemyWithPosition:(CGPoint)position;

@end
