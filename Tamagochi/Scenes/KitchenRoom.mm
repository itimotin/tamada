//
//  FirstScene.m
//  Tamagochi
//
//  Created by Sergiu Moțipan on 6/4/13.
//
//

#import "KitchenRoom.h"
#import "CCButton.h"

@implementation KitchenRoom
{
    int touchObject;
    int timp;
    int timp_2;
    int mouse;
    int oche_mouse;
    int n_mouse;
    int eye_pos;
    float totalTime,yy,xx, tamTouch;
    BOOL repeatTouches;
    float limit;
    CCSprite *prod1;
    CCSprite *prod4;
    CCLayer *layer4;

    
    CCButton *but_sus;
    CCButton *but_jos;
    CCSprite *movedFood;

    CGPoint startPosition;
    CCButton *but_left;
    int kCalObject;
    
   
}

@synthesize scrolFood_1, scrolFood_2, scrolFood_3, scrolFood_4;

+(CCScene *)scene
{
	CCScene *scene = [CCScene node];
	KitchenRoom *layer = [KitchenRoom node];
	[scene addChild: layer];
	return scene;
}

- (void)performBlock:(void (^)(void))block afterDelay:(NSTimeInterval)delay
{
    int64_t delta = (int64_t)(1.0e9 * delay);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delta), dispatch_get_main_queue(), block);
}

-(void)changePositionScrolls:(BOOL)sus
{
    scrolFood_4.visibleRect = CGRectMake(30, -100+200*sus+100*IS_WIDESCREEN*!sus+90*IS_IPAD, WIDTH_DEVICE-50, 80+60*IS_IPAD);
    scrolFood_1.visibleRect = CGRectMake(30, 40+150*sus+80*IS_WIDESCREEN*!sus+90*IS_IPAD, WIDTH_DEVICE-50, 80+60*IS_IPAD);
    scrolFood_3.visibleRect = CGRectMake(30, 130+80*sus+80*IS_WIDESCREEN+150*IS_IPAD, WIDTH_DEVICE-50, 80+60*IS_IPAD);
    scrolFood_2.visibleRect = CGRectMake(30, -100+110*sus+150*IS_IPAD, WIDTH_DEVICE-50, 80+60*IS_IPAD);
}
- (void)touchUpInSide:(id)sender
{
    tableScroll=YES;
    [self performBlock:^{ tableScroll=NO; } afterDelay:1.0];
    switch ([sender  tag]) {
        case 1:
        {
            [but_jos setTouchEnbled:YES];
            [but_sus setTouchEnbled:NO];
            [scrollTable setContentOffset:CGPointMake(0, 280) animated:YES];
            [but_sus stopAllActions];
            [but_jos stopAllActions];
            [but_jos runAction:[CCFadeIn actionWithDuration:1.0]];
            [but_sus runAction:[CCFadeOut actionWithDuration:0.0]];
            [self changePositionScrolls:NO];
        }
            break;
            
        case 2:
        {
            [but_sus setTouchEnbled:YES];
            [scrollTable setContentOffset:CGPointMake(0, -200) animated:YES];
            [but_jos stopAllActions];
            [but_sus stopAllActions];
            id action1 = [CCScaleBy actionWithDuration:0.3 scale:1.1];  // the action it sounds like you have written above.
            id action2 = [CCScaleTo actionWithDuration:1.0 scale:1.0];
            id  animationObjects = [CCSequence actions:action1, action2, nil];
            [but_sus runAction:[CCRepeatForever actionWithAction:animationObjects]];
            [but_sus runAction:[CCFadeIn actionWithDuration:1.0]];
            [but_jos runAction:[CCFadeOut actionWithDuration:0.0]];
            [self changePositionScrolls:YES];
            [but_jos setTouchEnbled:NO];
        }
            break;
            case 3:
        {
           
        }
            
        default:
            break;
    }
    
}
-(void)touchDown:(id)sender
{
}

-(CCSprite *)createNamePolka:(NSString *)name spr:(CCSprite *)parent
{
    CCSprite *sprite=[CCSprite spriteWithFile:[NSString stringWithFormat:@"bg_mane_food%@.png", (IS_IPAD)?@"-iPad":@""]];
    [sprite setPosition:CGPointMake(0,parent.boundingBox.size.height/2-20*IS_IPAD)];
    CCLabelTTF *label=[CCLabelTTF labelWithString:name fontName:@"Marker Felt" fontSize:15+30*IS_IPAD];
    if (!IS_IPAD)
        label.fontSize=13;
    else
        label.fontSize=45-15*IS_RETINA;
    label.color=ccc3(7, 154, 187);
    [label setPosition:CGPointMake(sprite.boundingBox.size.width/2, sprite.boundingBox.size.height/2)];
    [sprite addChild:label];
    [sprite setScale:0.5+0.5*IS_RETINA];
    return sprite;
}


-(void)createFrigider
{
    CCSprite *but_l;
    CCSprite *but_r;
    CCSprite *sel_but_l;
    CCSprite *sel_but_r;
  
    CCSprite *jos=[CCSprite spriteWithFile:@"but_jos.png"];
    CCSprite *jos1=[CCSprite spriteWithFile:@"but_jos_.png"];
    CCSprite *sus=[CCSprite spriteWithFile:@"but_sus.png"];
    CCSprite *sus1=[CCSprite spriteWithFile:@"but_sus_.png"];
    
    but_sus=[CCButton buttonWithNormalSprite:sus selectedSprite:sus1 target:self action:@selector(touchUpInSide:) forEvent:buttonEvent_TouchUpInside];
    but_sus.tag=1;
    [but_sus setPosition:CGPointMake(winSize.width/2, winSize.height/2-but_sus.boundingBox.size.height/2+40*IS_WIDESCREEN)];
        
    but_jos=[CCButton buttonWithNormalSprite:jos selectedSprite:jos1 target:self action:@selector(touchUpInSide:) forEvent:buttonEvent_TouchUpInside];
    but_jos.tag=2;
    [but_jos setPosition:CGPointMake(winSize.width/2, but_jos.boundingBox.size.height/2)];
    
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"but_jos.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"but_jos_.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"but_sus.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"but_sus_.png"];
    
    [self addChild:but_sus z:50];
    [self addChild:but_jos z:50];
    [but_sus runAction:[CCFadeOut actionWithDuration:0.0]];
    [but_sus setTouchEnbled:NO];
    id action1 = [CCScaleBy actionWithDuration:0.3 scale:1.1];  // the action it sounds like you have written above.
    id action2 = [CCScaleTo actionWithDuration:1.0 scale:1.0];
    id  animationObjects = [CCSequence actions:action1, action2, nil];
    [but_jos runAction:[CCRepeatForever actionWithAction:animationObjects]];
    
    scrollTable = [ScrollLayer node];
    scrollTable.tag = 10;
    [scrollTable setScrollingEnabled:NO];
    scrollTable.scrollDelegate=self;
    scrollTable.position    = CGPointMake(winSize.width/2, 250*IS_IPAD);
    scrollTable.contentSize = CGSizeMake(winSize.width, 480-80*IS_WIDESCREEN+400*IS_IPAD);
    [scrollTable setDirectionLock: kScrollLayerDirectionLockVertical];
    scrollTable.visibleRect = CGRectMake(0,50+50*IS_IPAD, WIDTH_DEVICE, 300+300*IS_IPAD);
    
    
    CCLayer *layer5 = [[[CCLayer alloc] init] autorelease];
    CCSprite *polca4=[CCSprite spriteWithFile:[NSString stringWithFormat:@"polita_00%@.png", (IS_IPAD)?@"-iPad":@""]];
    [layer5 setPosition:CGPointMake(0, 0)];
    but_l=[CCSprite spriteWithFile:[NSString stringWithFormat:@"retagle_left%@.png", (IS_IPAD)?@"-iPad":@""]];
    but_r=[CCSprite spriteWithFile:[NSString stringWithFormat:@"retagle_right%@.png", (IS_IPAD)?@"-iPad":@""]];
    but_l.tag=100;
    but_r.tag=101;
    but_l.scale=0.5+0.5*IS_RETINA;
    but_r.scale=0.5+0.5*IS_RETINA;
    polca4.scale=0.5+0.5*IS_RETINA;
    [but_l setPosition:CGPointMake(-winSize.width/2+but_l.boundingBox.size.width/2-1.5-0.5*IS_IPAD, 6+22*IS_IPAD)];
    [but_r setPosition:CGPointMake(winSize.width/2-but_r.boundingBox.size.width/2, 6+20*IS_IPAD)];
    
    sel_but_l=[CCSprite spriteWithFile:[NSString stringWithFormat:@"retagle_left_%@.png", (IS_IPAD)?@"-iPad":@""]];
    [sel_but_l setScale:0.5+0.5*IS_RETINA];
    if (IS_IPAD)
        [sel_but_l setPosition:CGPointMake(but_l.position.x+35, but_l.position.y)];
    else
        [sel_but_l setPosition:but_l.position];
    sel_but_r=[CCSprite spriteWithFile:[NSString stringWithFormat:@"retagle_right_%@.png", (IS_IPAD)?@"-iPad":@""]];
    [sel_but_r setScale:0.5+0.5*IS_RETINA];
    if (IS_IPAD)
        [sel_but_r setPosition:CGPointMake(but_r.position.x-35, but_r.position.y)];
    else
        [sel_but_r setPosition:but_r.position];
    sel_but_l.tag=102;
    sel_but_r.tag=103;
    
    [layer5 addChild:sel_but_l z:1];
    [layer5 addChild:sel_but_r z:1];
    
    [sel_but_l setOpacity:0];
    [sel_but_r setOpacity:0];
    
    [layer5 addChild:polca4];
    [layer5 addChild:[self createNamePolka:NSLocalizedString(@"Desserts", nil) spr:polca4]];
    layer5.tag=scrolFood_2.tag;
    [layer5 addChild:but_l z:2];
    [layer5 addChild:but_r z:2];
    [layer5 addChild:scrolFood_2];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:[NSString stringWithFormat:@"polita_00%@.png", (IS_IPAD)?@"-iPad":@""]];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:[NSString stringWithFormat:@"retagle_left%@.png", (IS_IPAD)?@"-iPad":@""]];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:[NSString stringWithFormat:@"retagle_right%@.png", (IS_IPAD)?@"-iPad":@""]];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:[NSString stringWithFormat:@"retagle_left_%@.png", (IS_IPAD)?@"-iPad":@""]];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:[NSString stringWithFormat:@"retagle_right_%@.png", (IS_IPAD)?@"-iPad":@""]];
    [scrollTable addChild:layer5];
    
    
    
    CCLayer *layer1 = [[[CCLayer alloc] init] autorelease];
    CCSprite *polca3=[CCSprite spriteWithFile:[NSString stringWithFormat:@"polita_01%@.png", (IS_IPAD)?@"-iPad":@""]];
    [layer1 setPosition:CGPointMake(0, 100+80*IS_IPAD)];
    but_l=[CCSprite spriteWithFile:[NSString stringWithFormat:@"retagle_left%@.png", (IS_IPAD)?@"-iPad":@""]];
    but_r=[CCSprite spriteWithFile:[NSString stringWithFormat:@"retagle_right%@.png", (IS_IPAD)?@"-iPad":@""]];
    but_l.tag=100;
    but_r.tag=101;
    but_l.scale=0.5+0.5*IS_RETINA;
    but_r.scale=0.5+0.5*IS_RETINA;
    polca3.scale=0.5+0.5*IS_RETINA;
    [but_l setPosition:CGPointMake(-winSize.width/2+but_l.boundingBox.size.width/2-1.5-0.5*IS_IPAD, 0)];
    [but_r setPosition:CGPointMake(winSize.width/2-but_r.boundingBox.size.width/2, 0)];
    
    sel_but_l=[CCSprite spriteWithFile:[NSString stringWithFormat:@"retagle_left_%@.png", (IS_IPAD)?@"-iPad":@""]];
    [sel_but_l setScale:0.5+0.5*IS_RETINA];
    if (IS_IPAD)
        [sel_but_l setPosition:CGPointMake(but_l.position.x+35, but_l.position.y)];
    else
        [sel_but_l setPosition:but_l.position];
    sel_but_r=[CCSprite spriteWithFile:[NSString stringWithFormat:@"retagle_right_%@.png", (IS_IPAD)?@"-iPad":@""]];
    [sel_but_r setScale:0.5+0.5*IS_RETINA];
    if (IS_IPAD)
        [sel_but_r setPosition:CGPointMake(but_r.position.x-35, but_r.position.y)];
    else
        [sel_but_r setPosition:but_r.position];
    sel_but_l.tag=102;
    sel_but_r.tag=103;
    
    [layer1 addChild:sel_but_l z:1];
    [layer1 addChild:sel_but_r z:1];
    
   [sel_but_l setOpacity:0];
    [sel_but_r setOpacity:0];
    
    [layer1 addChild:polca3];
    [layer1 addChild:[self createNamePolka:NSLocalizedString(@"Potions", nil) spr:polca3]];
    layer1.tag=scrolFood_4.tag;
    [layer1 addChild:but_l z:2];
    [layer1 addChild:but_r z:2];
    [layer1 addChild:scrolFood_4];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:[NSString stringWithFormat:@"polita_00%@.png", (IS_IPAD)?@"-iPad":@""]];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:[NSString stringWithFormat:@"retagle_left%@.png", (IS_IPAD)?@"-iPad":@""]];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:[NSString stringWithFormat:@"retagle_right%@.png", (IS_IPAD)?@"-iPad":@""]];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:[NSString stringWithFormat:@"retagle_left_%@.png", (IS_IPAD)?@"-iPad":@""]];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:[NSString stringWithFormat:@"retagle_right_%@.png", (IS_IPAD)?@"-iPad":@""]];
    [scrollTable addChild:layer1];
 
    
    CCLayer *layer2 = [[[CCLayer alloc] init] autorelease];
    CCSprite *polca1=[CCSprite spriteWithFile:[NSString stringWithFormat:@"polita_01%@.png", (IS_IPAD)?@"-iPad":@""]];
    but_l=[CCSprite spriteWithFile:[NSString stringWithFormat:@"retagle_left%@.png", (IS_IPAD)?@"-iPad":@""]];
    but_r=[CCSprite spriteWithFile:[NSString stringWithFormat:@"retagle_right%@.png", (IS_IPAD)?@"-iPad":@""]];
    but_l.tag=100;
    but_r.tag=101;
    but_l.scale=0.5+0.5*IS_RETINA;
    but_r.scale=0.5+0.5*IS_RETINA;
    polca1.scale=0.5+0.5*IS_RETINA;
    
    [but_l setPosition:CGPointMake(-winSize.width/2+but_l.boundingBox.size.width/2-1.5, 0)];
    [but_r setPosition:CGPointMake(winSize.width/2-but_r.boundingBox.size.width/2, 0)];
    
    sel_but_l=[CCSprite spriteWithFile:[NSString stringWithFormat:@"retagle_left_%@.png", (IS_IPAD)?@"-iPad":@""]];
    [sel_but_l setScale:0.5+0.5*IS_RETINA];
    if (IS_IPAD)
        [sel_but_l setPosition:CGPointMake(but_l.position.x+35, but_l.position.y)];
    else
        [sel_but_l setPosition:but_l.position];
    sel_but_r=[CCSprite spriteWithFile:[NSString stringWithFormat:@"retagle_right_%@.png", (IS_IPAD)?@"-iPad":@""]];
    [sel_but_r setScale:0.5+0.5*IS_RETINA];
    if (IS_IPAD)
        [sel_but_r setPosition:CGPointMake(but_r.position.x-35, but_r.position.y)];
    else
        [sel_but_r setPosition:but_r.position];
    
    [layer2 addChild:sel_but_l z:1];
    [layer2 addChild:sel_but_r z:1];
    
    sel_but_l.tag=102;
    sel_but_r.tag=103;
    
    [sel_but_l setOpacity:0];
    [sel_but_r setOpacity:0];
    
    [layer2 setPosition:CGPointMake(0, 200+137*IS_IPAD)];
    [layer2 addChild:polca1];
    [layer2 addChild:[self createNamePolka:NSLocalizedString(@"Drinks", nil) spr:polca1]];
    layer2.tag=scrolFood_1.tag;
    [layer2 addChild:scrolFood_1];
    [layer2 addChild:but_l z:2];
    [layer2 addChild:but_r z:2];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:[NSString stringWithFormat:@"polita_00%@.png", (IS_IPAD)?@"-iPad":@""]];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:[NSString stringWithFormat:@"retagle_left%@.png", (IS_IPAD)?@"-iPad":@""]];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:[NSString stringWithFormat:@"retagle_right%@.png", (IS_IPAD)?@"-iPad":@""]];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:[NSString stringWithFormat:@"retagle_left_%@.png", (IS_IPAD)?@"-iPad":@""]];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:[NSString stringWithFormat:@"retagle_right_%@.png", (IS_IPAD)?@"-iPad":@""]];
    [scrollTable addChild:layer2];

    CCLayer *layer3 = [[[CCLayer alloc] init] autorelease];
    CCSprite *polca2=[CCSprite spriteWithFile:[NSString stringWithFormat:@"polita_01%@.png", (IS_IPAD)?@"-iPad":@""]];
    
    but_l=[CCSprite spriteWithFile:[NSString stringWithFormat:@"retagle_left%@.png", (IS_IPAD)?@"-iPad":@""]];
    but_r=[CCSprite spriteWithFile:[NSString stringWithFormat:@"retagle_right%@.png", (IS_IPAD)?@"-iPad":@""]];
    but_l.tag=100;
    but_r.tag=101;
    but_l.scale=0.5+0.5*IS_RETINA;
    but_r.scale=0.5+0.5*IS_RETINA;
    polca2.scale=0.5+0.5*IS_RETINA;
    
   [but_l setPosition:CGPointMake(-winSize.width/2+but_l.boundingBox.size.width/2-1.5, 0)];
   [but_r setPosition:CGPointMake(winSize.width/2-but_r.boundingBox.size.width/2, 0)];
  
    sel_but_l=[CCSprite spriteWithFile:[NSString stringWithFormat:@"retagle_left_%@.png", (IS_IPAD)?@"-iPad":@""]];
    [sel_but_l setScale:0.5+0.5*IS_RETINA];
    if (IS_IPAD)
        [sel_but_l setPosition:CGPointMake(but_l.position.x+35, but_l.position.y)];
    else
        [sel_but_l setPosition:but_l.position];
    sel_but_r=[CCSprite spriteWithFile:[NSString stringWithFormat:@"retagle_right_%@.png", (IS_IPAD)?@"-iPad":@""]];
    [sel_but_r setScale:0.5+0.5*IS_RETINA];
    if (IS_IPAD)
        [sel_but_r setPosition:CGPointMake(but_r.position.x-35, but_r.position.y)];
    else
        [sel_but_r setPosition:but_r.position];

    sel_but_l.tag=102;
    sel_but_r.tag=103;
    
    [layer3 addChild:sel_but_r z:1];
    [layer3 addChild:sel_but_l z:1];
    [layer3 setPosition:CGPointMake(0, 300+193*IS_IPAD)];
    [layer3 addChild:polca2];
    [layer3 addChild:[self createNamePolka:NSLocalizedString(@"Breakfast", nil) spr:polca2]];
    [layer3 addChild:but_l z:2];
    [layer3 addChild:but_r z:2];
    layer3.tag=scrolFood_3.tag;
    [layer3 addChild:scrolFood_3];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:[NSString stringWithFormat:@"polita_00%@.png", (IS_IPAD)?@"-iPad":@""]];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:[NSString stringWithFormat:@"retagle_left%@.png", (IS_IPAD)?@"-iPad":@""]];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:[NSString stringWithFormat:@"retagle_right%@.png", (IS_IPAD)?@"-iPad":@""]];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:[NSString stringWithFormat:@"retagle_left_%@.png", (IS_IPAD)?@"-iPad":@""]];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:[NSString stringWithFormat:@"retagle_right_%@.png", (IS_IPAD)?@"-iPad":@""]];
    [scrollTable addChild:layer3];
    
    [sel_but_l setOpacity:0];
    [sel_but_r setOpacity:0];
    
    [self addChild:scrollTable z:-3];
    [self touchUpInSide:but_sus];
}
-(CCAction*)initAnimations
{
    id action1 = [CCScaleBy actionWithDuration:0.2 scale:1.5];
    id action2 = [CCScaleTo actionWithDuration:0.2 scale:1.0];
    id  animationObjects = [CCSequence actions:action1, action2, nil];
    return animationObjects;
}


-(void)loadFoods
{
    
    NSMutableArray *maa=[[[NSMutableArray alloc] init] autorelease];
    
    for (int i=0;i<[[stateTamagochi selectFoods:3] count];i++)
    {
        CCLayer *layer=[[[CCLayer alloc] init] autorelease];
        layer.scale=0.5+0.5*IS_RETINA;
        layer.tag=i;
        CCSprite *prod2=[CCSprite spriteWithFile:[NSString stringWithFormat:@"%@_%@.png", [[[stateTamagochi selectFoods:3] objectAtIndex:i] objectAtIndex:0],[[[stateTamagochi selectFoods:3] objectAtIndex:i] objectAtIndex:2]]];
        prod2.tag=1001;
        
        CCSprite *bg_numb=[CCSprite spriteWithFile:@"elips_number_food.png"];
        [bg_numb setPosition:CGPointMake(-20, 25)];
        bg_numb.tag=1000;
        CCLabelTTF *nr=[CCLabelTTF labelWithString:[NSString stringWithFormat:@"%@", [[[stateTamagochi selectFoods:3] objectAtIndex:i] objectAtIndex:3]] fontName:@"Marker Felt" fontSize:12];
        [nr setColor:ccBLACK];
        nr.tag=1;
        
        CCLabelTTF *name=[CCLabelTTF labelWithString:@"" fontName:@"Marker Felt" fontSize:10];
        name.tag=1010;
        [name setColor:ccBLACK];
        [name setPosition:CGPointMake(0, -30)];
        [layer addChild:name];

        [nr setPosition:CGPointMake(bg_numb.contentSize.width/2, bg_numb.contentSize.height/2)];
        [layer addChild:bg_numb z:1];
        [bg_numb addChild:nr z:2];
        [layer addChild:prod2];
        [[CCTextureCache sharedTextureCache] removeTextureForKey:[NSString stringWithFormat:@"%@_%@.png", [[[stateTamagochi selectFoods:3] objectAtIndex:i] objectAtIndex:0],[[[stateTamagochi selectFoods:3] objectAtIndex:i] objectAtIndex:2]]];
        [[CCTextureCache sharedTextureCache] removeTextureForKey:@"elips_number_food.png"];
        [maa addObject:layer];
    }

    CCLayer *layer=[[[CCLayer alloc] init] autorelease];
    layer.scale=0.5+0.5*IS_RETINA;
    layer.tag=[[stateTamagochi selectFoods:3] count];
    CCSprite *add=[CCSprite spriteWithFile:[NSString stringWithFormat:@"add%@.png", (IS_IPAD)?@"-iPad":@""]];
    add.tag=layer.tag;
    [layer addChild:add];
    [maa addObject:layer];

    scrolFood_1 = [[CCScrollLayer nodeWithLayers:maa widthOffset:150] retain];
    if ([maa count]==0)
        [scrolFood_1 setScrollEnabled:NO];
    scrolFood_1.delegate=self;
    scrolFood_1.tag = 3;
    [scrolFood_1 setPosition:CGPointMake(0, 0)];
    scrolFood_1.showPagesIndicator=NO;
    scrolFood_1.pageSize=CGSizeMake(50, 70);
    [scrolFood_1 setContentSize:CGSizeMake(WIDTH_DEVICE-90-350*IS_IPAD, 50+150*IS_IPAD)];
    [scrolFood_1 updatePages];

    scrolFood_1.visibleRect = CGRectMake(30, 0, 270, -80);
    
    
    NSMutableArray *foodArray1=[[[NSMutableArray alloc] init] autorelease];
    for (int i=0;i<[[stateTamagochi selectFoods:4] count];i++)
    {
        CCLayer *layer=[[[CCLayer alloc] init] autorelease];
        layer.scale=0.5+0.5*IS_RETINA;
        layer.tag=i;
        CCSprite *prod2=[CCSprite spriteWithFile:[NSString stringWithFormat:@"%@_%@.png", [[[stateTamagochi selectFoods:4] objectAtIndex:i] objectAtIndex:0], [[[stateTamagochi selectFoods:4] objectAtIndex:i] objectAtIndex:2]]];
        //prod2.scale=0.6+0.2*IS_IPAD;
        prod2.tag=1001;
        
        CCSprite *bg_numb=[CCSprite spriteWithFile:@"elips_number_food.png"];
        [bg_numb setPosition:CGPointMake(-20, 25)];
         bg_numb.tag=1000;
        CCLabelTTF *nr=[CCLabelTTF labelWithString:[NSString stringWithFormat:@"%@", [[[stateTamagochi selectFoods:4] objectAtIndex:i] objectAtIndex:3]] fontName:@"Marker Felt" fontSize:12];
        [nr setColor:ccBLACK];
        nr.tag=1;
        [nr setPosition:CGPointMake(bg_numb.contentSize.width/2, bg_numb.contentSize.height/2)];
        CCLabelTTF *name=[CCLabelTTF labelWithString:@"" fontName:@"Marker Felt" fontSize:10];
        name.tag=1010;
        [name setColor:ccBLACK];
        [name setPosition:CGPointMake(0, -30)];
        [layer addChild:name];

        [layer addChild:bg_numb z:1];
        [bg_numb addChild:nr z:2];
        [layer addChild:prod2];
        [[CCTextureCache sharedTextureCache] removeTextureForKey:[NSString stringWithFormat:@"%@_%@.png", [[[stateTamagochi selectFoods:4] objectAtIndex:i] objectAtIndex:0],[[[stateTamagochi selectFoods:4] objectAtIndex:i] objectAtIndex:2]]];
        [[CCTextureCache sharedTextureCache] removeTextureForKey:@"elips_number_food.png"];
        [foodArray1 addObject:layer];
    }

    CCLayer *layer1=[[[CCLayer alloc] init] autorelease];
    layer1.scale=0.5+0.5*IS_RETINA;
    layer1.tag=[[stateTamagochi selectFoods:4] count];
    CCSprite *add1=[CCSprite spriteWithFile:[NSString stringWithFormat:@"add%@.png", (IS_IPAD)?@"-iPad":@""]];
    add1.tag=layer1.tag;
    [layer1 addChild:add1];
    [foodArray1 addObject:layer1];
    
    scrolFood_2=[[CCScrollLayer nodeWithLayers:foodArray1 widthOffset:150] retain];
    if ([foodArray1 count]==0)
    [scrolFood_2 setScrollEnabled:NO];
    scrolFood_2.tag=4;
    scrolFood_2.delegate=self;
    [scrolFood_2 setPosition:CGPointMake(0, 0)];
    scrolFood_2.showPagesIndicator=NO;
    scrolFood_2.pageSize=CGSizeMake(50, 70);
    [scrolFood_2 setContentSize:CGSizeMake(WIDTH_DEVICE-90-350*IS_IPAD, 50+150*IS_IPAD)];
    [scrolFood_2 updatePages];
    scrolFood_2.visibleRect = CGRectMake(30, 50+scrollTable.position.y, WIDTH_DEVICE-50, 80);
    
    
    NSMutableArray *foodArray2=[[[NSMutableArray alloc] init] autorelease];
    for (int i=0;i<[[stateTamagochi selectFoods:1] count];i++)
    {
        CCLayer *layer=[[[CCLayer alloc] init] autorelease];
        layer.scale=0.5+0.5*IS_RETINA;
        layer.tag=i;
        CCSprite *prod2=[CCSprite spriteWithFile:[NSString stringWithFormat:@"%@_%@.png", [[[stateTamagochi selectFoods:1] objectAtIndex:i] objectAtIndex:0], [[[stateTamagochi selectFoods:1] objectAtIndex:i] objectAtIndex:2]]];
        prod2.tag=1001;
        
        CCSprite *bg_numb=[CCSprite spriteWithFile:@"elips_number_food.png"];
        [bg_numb setPosition:CGPointMake(-20, 25)];
        bg_numb.tag=1000;
        CCLabelTTF *nr=[CCLabelTTF labelWithString:[NSString stringWithFormat:@"%@", [[[stateTamagochi selectFoods:1] objectAtIndex:i] objectAtIndex:3]] fontName:@"Marker Felt" fontSize:12];
        [nr setColor:ccBLACK];
        nr.tag=1;
        [nr setPosition:CGPointMake(bg_numb.contentSize.width/2, bg_numb.contentSize.height/2)];
        
        CCLabelTTF *name=[CCLabelTTF labelWithString:@"" fontName:@"Marker Felt" fontSize:10];
        
        name.tag=1010;
        [name setColor:ccBLACK];
        [name setPosition:CGPointMake(0, -30)];
        [layer addChild:name];
        
        [layer addChild:bg_numb z:1];
        [bg_numb addChild:nr z:2];
        [layer addChild:prod2];
        [[CCTextureCache sharedTextureCache] removeTextureForKey:[NSString stringWithFormat:@"%@_%@.png", [[[stateTamagochi selectFoods:1] objectAtIndex:i] objectAtIndex:0],[[[stateTamagochi selectFoods:1] objectAtIndex:i] objectAtIndex:2]]];
        [[CCTextureCache sharedTextureCache] removeTextureForKey:@"elips_number_food.png"];
        [foodArray2 addObject:layer];
    }
    
    CCLayer *layer2=[[[CCLayer alloc] init] autorelease];
    layer2.scale=0.5+0.5*IS_RETINA;
    layer2.tag=[[stateTamagochi selectFoods:1] count];
    CCSprite *add2=[CCSprite spriteWithFile:[NSString stringWithFormat:@"add%@.png", (IS_IPAD)?@"-iPad":@""]];
    add2.tag=layer2.tag;
    [layer2 addChild:add2];
    [foodArray2 addObject:layer2];
    scrolFood_3=[[CCScrollLayer nodeWithLayers:foodArray2 widthOffset:150] retain];
    
    if ([foodArray2 count]==0)
        [scrolFood_3 setScrollEnabled:NO];
    
    scrolFood_3.tag=1;
    scrolFood_3.delegate=self;
    [scrolFood_3 setPosition:CGPointMake(0, 0)];
    scrolFood_3.showPagesIndicator=NO;
    scrolFood_3.pageSize=CGSizeMake(50, 70);
    [scrolFood_3 setContentSize:CGSizeMake(WIDTH_DEVICE-90-350*IS_IPAD, 50+150*IS_IPAD)];
    [scrolFood_3 updatePages];
    scrolFood_3.visibleRect = CGRectMake(30, 130+scrollTable.position.y, 270, 80);
    
    NSMutableArray *foodArray3=[[[NSMutableArray alloc] init] autorelease];
    for (int i=0;i<[[stateTamagochi selectFoods:2] count];i++)
    {
        CCLayer *layer=[[[CCLayer alloc] init] autorelease];
        layer.scale=0.5+0.5*IS_RETINA;
        layer.tag=i;
        CCSprite *prod2=[CCSprite spriteWithFile:[NSString stringWithFormat:@"potion_0%@%@.png", [[[stateTamagochi selectFoods:2] objectAtIndex:i] objectAtIndex:2],(IS_IPAD)?@"-iPad":@""]];
        prod2.tag=1001;
        
        CCSprite *bg_numb=[CCSprite spriteWithFile:@"elips_number_food.png"];
        [bg_numb setPosition:CGPointMake(-20, 25)];
        bg_numb.tag=1000;
        CCLabelTTF *nr=[CCLabelTTF labelWithString:[NSString stringWithFormat:@"%@", [[[stateTamagochi selectFoods:2] objectAtIndex:i] objectAtIndex:3]] fontName:@"Marker Felt" fontSize:12];
        [nr setColor:ccBLACK];
        nr.tag=1;
        [nr setPosition:CGPointMake(bg_numb.contentSize.width/2, bg_numb.contentSize.height/2)];
          NSString *str=[NSString stringWithFormat:@"potion_0%@", [[[stateTamagochi selectFoods:2] objectAtIndex:i] objectAtIndex:2]];
        CCLabelTTF *name=[CCLabelTTF labelWithString:[NSString stringWithFormat:@"%@ %@", NSLocalizedString(str, nil),  ([[[[stateTamagochi selectFoods:2] objectAtIndex:i] objectAtIndex:1] intValue]!=100)?[NSString stringWithFormat:@"%@%%",[[[stateTamagochi selectFoods:2] objectAtIndex:i] objectAtIndex:1]]:@""] fontName:@"Marker Felt" fontSize:10];
        name.tag=1010;
        [name setColor:ccBLACK];
        [name setPosition:CGPointMake(0, -30)];
        [layer addChild:name];
        [layer addChild:bg_numb z:1];
        [bg_numb addChild:nr z:2];
        [layer addChild:prod2];
        [[CCTextureCache sharedTextureCache] removeTextureForKey:[NSString stringWithFormat:@"%@_%@.png", [[[stateTamagochi selectFoods:2] objectAtIndex:i] objectAtIndex:0],[[[stateTamagochi selectFoods:2] objectAtIndex:i] objectAtIndex:2]]];
        [[CCTextureCache sharedTextureCache] removeTextureForKey:@"elips_number_food.png"];
        [foodArray3 addObject:layer];
    }
    
    CCLayer *layer3=[[[CCLayer alloc] init] autorelease];
    layer3.scale=0.5+0.5*IS_RETINA;
    layer3.tag=[[stateTamagochi selectFoods:2] count];
    CCSprite *add3=[CCSprite spriteWithFile:[NSString stringWithFormat:@"add%@.png", (IS_IPAD)?@"-iPad":@""]];
    add3.tag=layer3.tag;
    [layer3 addChild:add3];
    [foodArray3 addObject:layer3];
    
    scrolFood_4=[[CCScrollLayer nodeWithLayers:foodArray3 widthOffset:150] retain];
    
    if ([foodArray3 count]==0)
        [scrolFood_4 setScrollEnabled:NO];
    
    scrolFood_4.tag=2;
    scrolFood_4.delegate=self;
    [scrolFood_4 setPosition:CGPointMake(0, 0)];
    scrolFood_4.showPagesIndicator=NO;
    scrolFood_4.pageSize=CGSizeMake(50, 70);
    [scrolFood_4 setContentSize:CGSizeMake(WIDTH_DEVICE-90-350*IS_IPAD, 50+150*IS_IPAD)];
    [scrolFood_4 updatePages];
    scrolFood_4.visibleRect = CGRectMake(30, -130+scrollTable.position.y, 270, 80);
}

-(ccColor3B)tranformColor:(NSInteger)tag object:(NSInteger)object
{
    NSMutableArray *arr=[[[NSMutableArray alloc] initWithArray:[stateTamagochi colorSkin:tag object:object]] autorelease];
    ccColor3B color={[[arr objectAtIndex:0] floatValue], [[arr objectAtIndex:1] floatValue], [[arr objectAtIndex:2] floatValue]};
    return color;
}

-(id)init
{
    self=[super init];
    if (self)
    {
        [[SimpleAudioEngine sharedEngine] stopAllEffects];
        [[SimpleAudioEngine sharedEngine] setEffectsVolume:[stateTamagochi soundOn]];
        [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume:[stateTamagochi soundOn]];
        [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(changeColor:) name:@"skinColor" object:nil];
        
        foodMoved=0;
        [stateTamagochi setRoom:kitchenRoom];
        mouth=[stateTamagochi mouthState];
        [[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"giggle.wav"];
        [[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"mtem.wav"];
        [[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"no.wav"];
        [[SimpleAudioEngine sharedEngine] setEffectsVolume:[stateTamagochi soundOn]];
        [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume:[stateTamagochi soundOn]];
        winSize = [[CCDirector sharedDirector] winSize];
        self.isTouchEnabled=YES;
        
        CCSprite *background = [CCSprite spriteWithFile:[NSString stringWithFormat:@"bg_4%@.png",(IS_IPAD)?@"-iPad":@""]];
        background.tag=1;
        [background setColor:[self tranformColor:[stateTamagochi selectColorSkin:1 room:4] object:0]];
        background.scale=0.5+0.5*IS_RETINA;
        background.position = ccp(winSize.width/2, winSize.height/2+20);
        [self addChild:background z:-12];
        [[CCTextureCache sharedTextureCache] removeTextureForKey:[NSString stringWithFormat:@"bg_4%@.png",(IS_IPAD)?@"-iPad":@""]];
        
        CCSprite *background_t = [CCSprite spriteWithFile:[NSString stringWithFormat:@"bg_4%@.png",(IS_IPAD)?@"-iPad":@""]];
        background_t.tag=5;
        [background_t setColor:[self tranformColor:[stateTamagochi selectColorSkin:1 room:4] object:0]];
        background_t.scale=0.5+0.5*IS_RETINA;
        background_t.scaleY=0.5;
        background_t.position = ccp(winSize.width/2, winSize.height);
        [self addChild:background_t z:-2];
        [[CCTextureCache sharedTextureCache] removeTextureForKey:[NSString stringWithFormat:@"bg_4%@.png",(IS_IPAD)?@"-iPad":@""]];
        
        CCSprite *background_l = [CCSprite spriteWithFile:@"rect_4.png"];
        background_l.tag=3;
        [background_l setColor:[self tranformColor:[stateTamagochi selectColorSkin:1 room:4] object:0]];
        background_l.scale=0.5+0.5*IS_RETINA;
        [background_l setScaleY:6.0];
        [background_l setScaleX:1.0+1.05*IS_IPAD];
        background_l.position = ccp(background_l.boundingBox.size.width/2, winSize.height/2-35);
        [self addChild:background_l z:1 ];
        [[CCTextureCache sharedTextureCache] removeTextureForKey:@"rect_4.png"];
        
        CCSprite *background_r = [CCSprite spriteWithFile:@"rect_4.png"];
        background_r.tag=4;
        [background_r setColor:[self tranformColor:[stateTamagochi selectColorSkin:1 room:4] object:0]];
        background_r.scale=0.5+0.5*IS_RETINA;
        [background_r setScaleY:6.0];
        [background_r setScaleX:1.0+1.05*IS_IPAD];
        background_r.position = ccp(winSize.width-background_r.boundingBox.size.width/2, winSize.height/2-35);
        [self addChild:background_r z:1];
        [[CCTextureCache sharedTextureCache] removeTextureForKey:@"rect_4.png"];
        
        CCSprite *polita1 = [CCSprite spriteWithFile:[NSString stringWithFormat:@"polita_11%@.png", (IS_IPAD)?@"-iPad":@""]];
        [polita1 setScale:0.5+0.5*IS_RETINA];
        if (!IS_IPAD)
            polita1.position = ccp(winSize.width/2, winSize.height/2+90+35*IS_WIDESCREEN);
        else
             polita1.position = ccp(winSize.width/2, winSize.height/2+200);
        [self addChild:polita1 z:-2];
        [[CCTextureCache sharedTextureCache] removeTextureForKey:[NSString stringWithFormat:@"polita_11%@.png", (IS_IPAD)?@"-iPad":@""]];
        
        [self setPositionTamagochi];
        [personaj initRects];
        
        if ([stateTamagochi sizeTamagochi]>0.8)
            [personaj setScale:0.8];
        [personaj person].delegate=self;
        [self addChild:[personaj person] z:-1];

        [self loadFoods];
        [self createFrigider];
        touchObject=f_default;
        [self scheduleUpdate];
    }
    
    return self;
}

-(void)changeColor:(NSNotification *)notif
{
    [(CCSprite *)[self getChildByTag:[[[notif.object componentsSeparatedByString:@","] objectAtIndex:1] intValue]] setColor:[self tranformColor:[[[notif.object componentsSeparatedByString:@","] objectAtIndex:0] intValue] object:[[[notif.object componentsSeparatedByString:@","] objectAtIndex:1] intValue]]];
    [(CCSprite *)[self getChildByTag:3]   setColor:[self tranformColor:[[[notif.object componentsSeparatedByString:@","] objectAtIndex:0] intValue] object:[[[notif.object componentsSeparatedByString:@","] objectAtIndex:1] intValue]]];
    [(CCSprite *)[self getChildByTag:4]  setColor:[self tranformColor:[[[notif.object componentsSeparatedByString:@","] objectAtIndex:0] intValue] object:[[[notif.object componentsSeparatedByString:@","] objectAtIndex:1] intValue]]];
    [(CCSprite *)[self getChildByTag:5]  setColor:[self tranformColor:[[[notif.object componentsSeparatedByString:@","] objectAtIndex:0] intValue] object:[[[notif.object componentsSeparatedByString:@","] objectAtIndex:1] intValue]]];
}
- (void)update:(float)dt
{
    
}

-(void)setPositionTamagochi
{
    [personaj person].position = ccp(winSize.width/2, winSize.height/2+20+70*[stateTamagochi sizeTamagochi]+35*IS_WIDESCREEN+90*IS_IPAD);
}

-(CGPoint)returnPosition
{
    if (movedFood)
        return movedFood.position;
    else
    {
        return CGPointMake(xx, yy);
    }
}

-(float)returnTouchValue
{
    switch ([stateTamagochi mouthState]) {
        case mouthStateFun:
        {
            return tamTouch*sin(totalTime);
        }
            break;
        case mouthStateReadyEat:
        {
            return 0.3;
        }
            break;
        case mouthStateEat:
        {
            return 0.25*sin(totalTime);
        }
            break;
        case mouthStateDefault:
        {
            return 0.25*sin(totalTime);
        }
            break;
            
        default:return tamTouch;
            break;
    }
}


-(void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    nr_sounds=0;
    UITouch* touch = [touches anyObject];
    CGPoint location = [touch locationInView: [touch view]];
    startTouchPoint=location;
    date_start=[[NSDate date] retain];
    location = [[CCDirector sharedDirector] convertToGL:location];
    
    if (!moveFood)
        startPosition=location;
    xx=location.x;
    if (location.y>=90.0)
        yy=location.y;
    else
        yy=90.0;
    touchFun=0;
   
    if (CGRectContainsPoint([personaj rectPerson],location) && movedFood==nil)
    {
       // [self loadFoods];
        [stateTamagochi setTouchObject:tagTamagochi];
    }
}

-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch* touch = [touches anyObject];
    CGPoint location = [touch locationInView: [touch view]];
    location = [[CCDirector sharedDirector] convertToGL:location];

    if ([stateTamagochi touchObject]==tagTamagochi && CGRectContainsPoint([personaj rectPerson],location))
    {
        [stateTamagochi setMouthState:mouthStateFun];
        touchFun+=0.006;
        if (touchFun>=0.05)
        {
            if (!repeatTouches)
            [self fun];
        }
        nr_sounds++;
    }
  
    if (movedFood && moveFood)
    {
        [movedFood setPosition:CGPointMake(location.x, location.y)];
    if ([stateTamagochi touchObject]==tagFood && CGRectContainsRect([personaj rectPerson],[movedFood boundingBox]) && scrollTag!=2 && ([stateTamagochi fat]<=1.2 || [stateTamagochi food]<=100))
    {
         [stateTamagochi setMouthState:mouthStateReadyEat];
    }
    else if ([stateTamagochi touchObject]==tagFood && CGRectContainsRect([personaj rectPerson],[movedFood boundingBox]) && scrollTag==2)
    {
        switch ([[[[stateTamagochi selectFoods:scrollTag] objectAtIndex:foodMoved] objectAtIndex:2] intValue]) {
            case 1:
            {
                if ([stateTamagochi health]<100.0)
                {
                    [stateTamagochi setMouthState:mouthStateReadyEat];
                }
            }
                break;
            case 8:
            {
                if  ([stateTamagochi food]>20.0)
                {
                    [stateTamagochi setMouthState:mouthStateReadyEat];
                }
            }
                break;
            case 3:
            {
                if  ([stateTamagochi energy]<100.0)
                {
                    [stateTamagochi setMouthState:mouthStateReadyEat];
                }
            }
                break;
            case 4:
            {
                if  ([stateTamagochi energy]<100.0 || [stateTamagochi food]<100.0 || [stateTamagochi funny]<100.0 || [stateTamagochi health]<100.0)
                {
                    [stateTamagochi setMouthState:mouthStateReadyEat];
                }
            }
                break;
            case 5:
            {
                if  ([stateTamagochi funny]<100.0)
                {
                    [stateTamagochi setMouthState:mouthStateReadyEat];
                }
            }
                break;
            case 2:
            {
                if  ([stateTamagochi fat]>1.0)
                {
                    [stateTamagochi setMouthState:mouthStateReadyEat];
                }
            }
                break;
            case 6:
            {
                if  ([stateTamagochi sizeLevel]>7)
                {
                    [stateTamagochi setMouthState:mouthStateReadyEat];
                }
            }
                break;
            case 7:
            {
                if  ([stateTamagochi sizeLevel]<18)
                {
                    [stateTamagochi setMouthState:mouthStateReadyEat];
                }
            }
                break;
            default:
                break;
        }
    }
    else if ([stateTamagochi mouthState]==mouthStateReadyEat)
             [stateTamagochi setMouthState:mouthStateReadyEatCancel];
    }
    
    xx=location.x;
    if (location.y<=90.0)
        yy=90.0;
    else
        yy=location.y;
        
    
}
-(void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch* touch = [touches anyObject];
    CGPoint location = [touch locationInView: [touch view]];
    
    endTouchPoint=location;
    float moveDifference=startTouchPoint.x-endTouchPoint.x;
    float differenceY=startTouchPoint.y-endTouchPoint.y;
    if (differenceY<0) differenceY*=-1;
    NSTimeInterval secondsBetween = [[NSDate date] timeIntervalSinceDate:date_start];
    [date_start release];
    date_start=nil;
    if (moveDifference>150.0 && differenceY<40.0 && endTouchPoint.y<150.0 && secondsBetween<0.3)
        [[NSNotificationCenter defaultCenter]postNotificationName:@"nextRoom" object:@"1"];
    else if (moveDifference<-150.0 && differenceY<40.0 && endTouchPoint.y<150.0 && secondsBetween<0.3)
        [[NSNotificationCenter defaultCenter]postNotificationName:@"nextRoom" object:@"2"];
    
    location = [[CCDirector sharedDirector] convertToGL:location];

    if ([stateTamagochi mouthState]==mouthStateFun)
    {
        [stateTamagochi setMouthState:mouth];
    }
    
    if ([stateTamagochi mouthState]==mouthStateReadyEat)
    {
        [self eat];
    }
    else if (movedFood && [movedFood numberOfRunningActions]<=1 && !stopFood)
    {
        if (CGRectContainsRect([personaj rectPerson],[movedFood boundingBox]) && CGRectContainsPoint([movedFood boundingBox], location))
        [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"no.wav" loop:NO];
        [movedFood runAction:[CCMoveTo actionWithDuration:0.5 position:startPosition ]];
        [movedFood runAction:[CCScaleTo actionWithDuration:0.5 scale:0.6]];
        [movedFood runAction:[CCFadeOut actionWithDuration:0.5]];
        [self performSelector:@selector(deleteFood) withObject:nil afterDelay:0.6];
    }
    moveFood=NO;
    nr_sounds=0;
    touchFun=0;
    xx=0;
    yy=0;
    touchObject=f_default;
}

-(void)changePosition:(CGPoint)pos
{
    xx=pos.x;
    yy=pos.y;
}



#pragma CCScrollLayer delegate

- (void) scrollLayerScrollingStarted:(CCScrollLayer *) sender;
{
}
- (void) scrollLayer: (CCScrollLayer *)sender scrolledToPageNumber: (int) page;
{
}

#pragma ScrollLayer delegate

-(void)ccScrollLayerCurrentItem:(int)current scroll:(CCScrollLayer *)scrollLayer category:(int)category
{
    switch (category) {
        case s_animatedSprite:
        {
           
            
        }
            break;
        case s_createNewSprite:
        {
        
            moveFood=YES;
            stopFood=NO;
            scrollTag=[scrollLayer tag];
            [scrollLayer setScrollEnabled:NO];
            [stateTamagochi setTouchObject:tagFood];
            kCalObject=[[[[stateTamagochi selectFoods:scrollTag] objectAtIndex:current] objectAtIndex:1] intValue];
            foodMoved=current;
        
            movedFood=[CCSprite spriteWithTexture:[[[[[scrollTable getChildByTag:[scrollLayer tag]] getChildByTag:[scrollLayer tag]] getChildByTag:current] getChildByTag:1001 ] texture]];
            [movedFood runAction:[CCScaleTo actionWithDuration:0.2 scale:1.5]];
            [self addChild:movedFood z:10];
        }
            break;
            
        default:
            break;
    }

}

-(void)ccScrollLayerActive:(CCScrollLayer *)scrollLayer active:(BOOL)active
{
    
}

-(void)ccScrollLayerDidEndDragging:(CCScrollLayer *)scrollLayer willDecelerate:(BOOL)willDecelerate
{
    if ([scrollLayer currentScreen]==[[scrollLayer pages] count]-1)
        [(CCSprite *)([[scrollTable getChildByTag:[scrollLayer tag]] getChildByTag:103]) setOpacity:0];
    else
        [(CCSprite *)([[scrollTable getChildByTag:[scrollLayer tag]] getChildByTag:103]) setOpacity:255];
    
     if ([scrollLayer currentScreen]==0)
         [(CCSprite *)([[scrollTable getChildByTag:[scrollLayer tag]] getChildByTag:102]) setOpacity:0];
     else 
        [(CCSprite *)([[scrollTable getChildByTag:[scrollLayer tag]] getChildByTag:102]) setOpacity:255];
}

-(void)ccScrollLayerDidScroll:(CCScrollLayer *)scrollLayer
{
    
}

-(void)ccScrollLayerWillBeginDecelerating:(CCScrollLayer *)scrollLayer
{
    
}

-(void)ccScrollLayerWillBeginDragging:(CCScrollLayer *)scrollLayer
{
    
}

-(void)scrollButtonActive:(ScrollLayer *)scrollLayer button:(int)but rind:(int)rind
{
    switch (rind) {
        case 2:
        {
            if (but==100)
            {
                [scrolFood_4 prevPage];
            }
            else
            {
                [scrolFood_4 nextPage];
            }
        }
            break;
        case 3:
        {
            if (but==100)
            {
                [scrolFood_1 prevPage];
            }
            else
            {
                [scrolFood_1 nextPage];
            }
        }
            break;
        case 1:
        {
          
            if (but==100)
            {
                [scrolFood_3 prevPage];
            }
            else
            {
                [scrolFood_3 nextPage];
            }
        }
            break;
        case 4:
        {
            
            if (but==100)
            {
                [scrolFood_2 prevPage];
            }
            else
            {
                [scrolFood_2 nextPage];
            }
        }
        default:
            break;
    }
}


-(void)ccScrollGoMallShopping:(NSInteger)index
{
    if (!tableScroll)
    {
        if (index!=2)
            [stateTamagochi setShopCategory:sh_eat];
        else
            [stateTamagochi setShopCategory:sh_potions];
    
        [[personaj person] removeFromParentAndCleanup:NO];
        [[CCDirectorIOS sharedDirector] replaceScene:[InMallRoom scene]];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"showBackgroundAlpha" object:nil];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"shopView" object:[NSString stringWithFormat:@"%d", index]];
    }
}

#pragma Tamagochi effects

-(void)deleteFood
{
    [scrolFood_2 setScrollEnabled:YES];
    [scrolFood_1 setScrollEnabled:YES];
    [scrolFood_3 setScrollEnabled:YES];
    [scrolFood_4 setScrollEnabled:YES];
    moveFood=NO;
    stopFood=NO;
    [movedFood removeFromParentAndCleanup:NO];
    movedFood=nil;
}

-(void)resetVariables
{
    repeatTouches=NO;
    touchFun=0;

    if ([[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying])
        [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
}

-(void)fun
{
    repeatTouches=YES;
    totalTime=0;
    tamTouch=0.3;
    if (![[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying] &&  [stateTamagochi soundOn])
        [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"giggle.wav" loop:NO];
    [self performSelector:@selector(resetVariables) withObject:nil afterDelay:1.57];
}

-(void)eatPotion:(int)category percent:(int)percent
{
  
    [stateTamagochi addToNextLevel:2];
    switch (category) {
        case 1:
        {
            if ([stateTamagochi health]!=100.0)
            {
                if (![stateTamagochi healthStart])
                {
                    [[personaj person] addMoney:50];
                    [stateTamagochi setHealthStart:YES];
                    [stateTamagochi updateFirstStart:4];
                }
                [[SimpleAudioEngine sharedEngine] playEffect:@"volshebnoe_prevracshenie.mp3" loop:NO];
                [[personaj person] addExplosionR:236 g:41 b:140 a:255];
                [stateTamagochi setHealth:[stateTamagochi health]+percent];
                [stateTamagochi updateHealth];
            }
          
        }
            break;
        case 8:
        {
            if  ([stateTamagochi food]>20.0)
            {
                [stateTamagochi setFood:percent];
                [stateTamagochi updateFood];
                [[SimpleAudioEngine sharedEngine] playEffect:@"volshebnoe_prevracshenie.mp3" loop:NO];
                [[personaj person] addExplosionR:243 g:173 b:4 a:255];
            }
         
        }
            break;
        case 3:
        {
            if ([stateTamagochi energy]!=100.0)
            {
                [stateTamagochi setEnergy:percent];
                [stateTamagochi updateEnergy];
                [[SimpleAudioEngine sharedEngine] playEffect:@"volshebnoe_prevracshenie.mp3" loop:NO];
                [[personaj person] addExplosionR:98 g:233 b:39 a:255];
            }
         
        }
            break;
        case 4:
        {
            if ([stateTamagochi energy]!=100.0 || [stateTamagochi food]!=100.0 || [stateTamagochi funny]!=100.0 || [stateTamagochi health]!=100.0)
            {
                [stateTamagochi setEnergy:percent];
                [stateTamagochi setFood:percent];
                [stateTamagochi setHealth:percent];
                [stateTamagochi setFunny:percent];
                [stateTamagochi updateFun];
                [stateTamagochi updateHealth];
                [stateTamagochi updateFood];
                [stateTamagochi updateEnergy];
                [[SimpleAudioEngine sharedEngine] playEffect:@"volshebnoe_prevracshenie.mp3" loop:NO];
                [[personaj person] addExplosionR:225 g:225 b:255 a:255];
            }
        }
            break;
        case 5:
        {
            if ([stateTamagochi funny]!=100.0)
            {
                [stateTamagochi setFunny:percent];
                [stateTamagochi updateFun];
                [[SimpleAudioEngine sharedEngine] playEffect:@"volshebnoe_prevracshenie.mp3" loop:NO];
                [[personaj person] addExplosionR:185 g:24 b:207 a:255];
            }
           
        }
            break;
        case 2:
        {
            if ([stateTamagochi fat]>1.0)
            {
                if ([stateTamagochi food]>100)
                {
                    [stateTamagochi setFood:100];
                    [stateTamagochi updateFood];
                    
                }
                [[SimpleAudioEngine sharedEngine] playEffect:@"volshebnoe_prevracshenie.mp3" loop:NO];
                 [[personaj person] addExplosionR:141 g:63 b:1 a:255];
                [stateTamagochi setFat:1.0];
                [stateTamagochi updateFat];
            }
            
        }
            break;
        case 6:
        {
            if ([stateTamagochi sizeLevel]>7)
            {
                [[SimpleAudioEngine sharedEngine] playEffect:@"baby.mp3" loop:NO];
                [stateTamagochi setSizeLevel:1];
                [stateTamagochi updateSizeLevel];
                [[personaj person] addExplosionR:71 g:154 b:255 a:255];
            }
           
        }
            break;
        case 7:
        {
             if ([stateTamagochi sizeLevel]<18)
             {
                 [stateTamagochi setSizeLevel:30];
                 [stateTamagochi updateSizeLevel];
                 [[SimpleAudioEngine sharedEngine] playEffect:@"volshebnoe_prevracshenie.mp3" loop:NO];
                [[personaj person] addExplosionR:238 g:100 b:6 a:255];
             }
            
        }
        default:
            break;
    }
}

-(void)eat
{
    if (![stateTamagochi kitchenStart]) {
        [[personaj person] addMoney:50];
        [stateTamagochi setKitchenStart:YES];
        [stateTamagochi updateFirstStart:0];
    }
    [movedFood runAction:[CCMoveTo actionWithDuration:0.2 position:CGPointMake([personaj person].position .x, [personaj person].position .y-35*[stateTamagochi sizeTamagochi])]];
    [movedFood runAction:[CCScaleTo actionWithDuration:0.7 scale:0.0]];
    totalTime=1.57;
    [stateTamagochi setEatCategory:scrollTag];
    [stateTamagochi setMouthState:mouthStateEat];
    
    [(CCLabelTTF*)([[[[[scrollTable getChildByTag:scrollTag] getChildByTag:scrollTag] getChildByTag:foodMoved] getChildByTag:1000] getChildByTag:1]) setString:[NSString stringWithFormat:@"%d", [[[[stateTamagochi selectFoods:scrollTag] objectAtIndex:foodMoved] objectAtIndex:3] intValue]-1]];

    if (scrollTag!=2)
    {
        [stateTamagochi addToNextLevel:1];
        [stateTamagochi setFood:[stateTamagochi food]+(kCalObject/20.0)];
        [stateTamagochi setDirtFood:[stateTamagochi dirtFood]+(kCalObject/20.0)];
        [stateTamagochi setHealthFood:[stateTamagochi healthFood]+(kCalObject/20.0)];
        if (scrollTag==4)
            [stateTamagochi setSweetsFood:[stateTamagochi sweetsFood]+(kCalObject/20.0)];
        [stateTamagochi updateFood];
        [self setPositionTamagochi];
    }
    else
    {
        [self eatPotion:[[[[stateTamagochi selectFoods:scrollTag] objectAtIndex:foodMoved] objectAtIndex:2] intValue] percent:[[[[stateTamagochi selectFoods:scrollTag] objectAtIndex:foodMoved] objectAtIndex:1] intValue]];
        [self setPositionTamagochi];
    }
    if ([[[[stateTamagochi selectFoods:scrollTag] objectAtIndex:foodMoved] objectAtIndex:3] intValue]==1)
    {
        [(CCScrollLayer*)([[scrollTable getChildByTag:scrollTag] getChildByTag:scrollTag]) removePageWithNumber:foodMoved];
        [(CCScrollLayer*)([[scrollTable getChildByTag:scrollTag] getChildByTag:scrollTag]) updateTags];
    }
    stopFood=YES;
    [stateTamagochi eatTamagochi:[[[[stateTamagochi selectFoods:scrollTag] objectAtIndex:foodMoved] objectAtIndex:4] intValue] category:scrollTag];
    [self performSelector:@selector(resetVariables) withObject:nil afterDelay:0.785];
    [self performSelector:@selector(deleteFood) withObject:nil afterDelay:0.8];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"skinColor" object:nil];
    [super dealloc];
}

@end
