
//
//  Terrain.m
//  Tamagochi
//
//  Created by iVanea! on 9/26/13.
/************************************************************
 *                                                           *
 *  .=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-.       *
 *   |                     ______                     |      *
 *   |                  .-"      "-.                  |      *
 *   |                 /            \                 |      *
 *   |     _          |              |          _     |      *
 *   |    ( \         |,  .-.  .-.  ,|         / )    |      *
 *   |     > "=._     | )(__/  \__)( |     _.=" <     |      *
 *   |    (_/"=._"=._ |/     /\     \| _.="_.="\_)    |      *
 *   |           "=._"(_     ^^     _)"_.="           |      *
 *   |               "=\__|IIIIII|__/="               |      *
 *   |              _.="| \IIIIII/ |"=._              |      *
 *   |    _     _.="_.="\          /"=._"=._     _    |      *
 *   |   ( \_.="_.="     `--------`     "=._"=._/ )   |      *
 *   |    > _.="                            "=._ <    |      *
 *   |   (_/                                    \_)   |      *
 *   |                                                |      *
 *   '-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-='      *
 *                                                           *
 *        WARNING!!! Very perversive code ahead!             *
 *************************************************************/
//http://toucharcade.com/2013/01/09/ta-plays-time-surfer-like-tiny-wings-on-crack/
//


#import "Terrain.h"
#import "TinyPoo.h"
#import "GLES-Render.h"
#import "SimpleAudioEngine.h"
#import "Constants.h"

#define kMaxHillKeyPoints 250
#define kMaxHillVertices kMaxHillKeyPoints*1000
#define kMaxBorderVertices kMaxHillKeyPoints*1000

@interface Terrain() {
    CGPoint _hillKeyPoints[kMaxHillKeyPoints];
    CCSprite *_stripes;
    int _fromKeyPointI;
    int _toKeyPointI;
    long long _nHillVertices;
    CGPoint _hillVertices[kMaxHillVertices];
    CGPoint _hillTexCoords[kMaxHillVertices];
    long long _nBorderVertices;
    CGPoint _borderVertices[kMaxBorderVertices];
    
    int currentCoinTag;
    
    float coinPosX;
    
    b2World *_world;
    b2Body *_body;
    GLESDebugDraw * _debugDraw;
    
    BOOL isCoinReseted;
    BOOL coinOnHill;
    BOOL timeToChangeBackgr;
    
    float bodyPositionY;
    CGPoint point1;
    int xDistanceFromHills;

    uint_fast8_t wayForStage;
    bool bossId;
    uint_fast8_t _kMaxHillKeyPoints;
}
- (void)resetCoin;
- (void)initCoins;
- (void)coinsInWorld;
- (void)coinToPouPosition:(float)Y;
- (void)resetHillVertices;
- (CGPoint)generateHills;

@end

@implementation Terrain

@synthesize offsetXf = _offsetXf;
@synthesize theDistanceOfWay = _theDistanceOfWay;
@synthesize coinBank = _coinBank;


- (void)setupDebugDraw {
    _debugDraw = new GLESDebugDraw(PTM_RATIO);
    _world->SetDebugDraw(_debugDraw);
    _debugDraw->SetFlags(GLESDebugDraw::e_shapeBit | GLESDebugDraw::e_jointBit);
}

- (id)initWithWorld:(b2World *)world withStageID:(uint_fast8_t)idStage withBoss:(BOOL)isBoss {
    if ((self = [super init])) {
        
        
        [self initCoins];
        _coinBank = 0;
        coinPosX = 0.0;
        _world = world;
        [self setupDebugDraw];
        wayForStage = idStage;
        bossId = isBoss;
        _theDistanceOfWay = [self generateHills];
        [self resetHillVertices];
        
        self.offsetXf = 0;
        self.shaderProgram = [[CCShaderCache sharedShaderCache] programForKey:kCCShader_PositionTexture];
        
    }
    return self;
}


- (void)dealloc {
    [_stripes release];
    _stripes = NULL;
    [super dealloc];
}

#pragma mark -
#pragma mark DRAW

- (void) draw {
    CC_NODE_DRAW_SETUP();
    
    ccGLBindTexture2D(_stripes.texture.name);
    ccGLEnableVertexAttribs( kCCVertexAttribFlag_Position | kCCVertexAttribFlag_TexCoords);
    
    glVertexAttribPointer(kCCVertexAttrib_Position, 2, GL_FLOAT, GL_FALSE, 0, _hillVertices);
    glVertexAttribPointer(kCCVertexAttrib_TexCoords, 2, GL_FLOAT, GL_FALSE, 0, _hillTexCoords);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, (GLsizei)_nHillVertices);
    /*
     //    for(int i = MAX(_fromKeyPointI, 1); i <= _toKeyPointI; ++i) {
     //        CGPoint p0 = _hillKeyPoints[i-1];
     //        CGPoint p1 = _hillKeyPoints[i];
     //        int hSegments = floorf((p1.x-p0.x)/kHillSegmentWidth);
     //
     //        float dx = (p1.x - p0.x) / hSegments;
     //        float da = M_PI / hSegments;
     //        float ymid = (p0.y + p1.y) / 2;
     //        float ampl = (p0.y - p1.y) / 2;
     //
     //        CGPoint pt0, pt1;
     //        pt0 = p0;
     //        for (int j = 0; j < hSegments+1; ++j) {
     //            pt1.x = p0.x + j*dx;
     //            pt1.y = ymid + ampl * cosf(da*j);
     //            ccDrawLine(pt0, pt1);
     //            point1 = pt1;
     //            pt0 = pt1;
     //        }
     //    }
     */
    //    _world->DrawDebugData();
    
}

- (void) setOffsetX:(float)newOffsetX Y:(float)y {
 	static BOOL firstTime = YES;
	if (_offsetXf != newOffsetX || firstTime) {
        firstTime = NO;
        _offsetXf = newOffsetX;
        self.position = CGPointMake(WIDTH_DEVICE/8-_offsetXf*self.scale, 0);
        [self resetHillsFromScreenSize];
        [self coinsInWorld];
    }
    //    [self coinToPouPosition:y];
}


#pragma mark - HILLS

- (void) resetBox2DBody {
    
    if(_body) {
        _world->DestroyBody(_body);
    }
    
    b2BodyDef bd;
    bd.position.Set(0, 0);
    
    _body = _world->CreateBody(&bd);
    
    b2EdgeShape shape;
    
    b2Vec2 p1, p2;
    for (int i=0; i<_nBorderVertices-1; i++) {
        p1 = b2Vec2(_borderVertices[i].x/PTM_RATIO,_borderVertices[i].y/PTM_RATIO);
        p2 = b2Vec2(_borderVertices[i+1].x/PTM_RATIO,_borderVertices[i+1].y/PTM_RATIO);
        shape.Set(p1, p2);
        _body->CreateFixture(&shape, 0);
    }
}


- (CGPoint)generateHills {
    NSString *path = [[[NSBundle mainBundle] resourcePath]  stringByAppendingPathComponent:[NSString stringWithFormat:@"way%i.plist",wayForStage]];
//    NSString *path1 = [[[NSBundle mainBundle] resourcePath]  stringByAppendingPathComponent:[NSString stringWithFormat:@"way10.plist"]];
    NSLog(@"\n\n PATH %@",path);
    NSMutableDictionary *savedStock = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
    _kMaxHillKeyPoints = savedStock.count;
    
    for (int i = 0 ; i < _kMaxHillKeyPoints; i++) {
        NSString *str = [savedStock objectForKey:[NSString stringWithFormat:@"%d",i]];
        CGPoint myPoint = CGPointFromString(str);
        _hillKeyPoints[i] = myPoint;
    }
    [savedStock release];
    return _hillKeyPoints [_kMaxHillKeyPoints-2];
}

//- (float) generateHills {
//    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
//    int minDX = random()%(290-120)+120;
//    int minDY = random()%(160-60)+60;;
//    int rangeDX =random()%(60-40)+40;
//    int rangeDY = random()%(60-30)+30;
//    xDistanceFromHills = minDX+rangeDX;
//    float x = -WIDTH_DEVICE;
//    float y = HEIGHT_DEVICE/2 - RADIUS_POU/2;
//    
//    float dy, ny;
//    float sign = 1; // +1 - going up, -1 - going  down
//    
//    float maxHeight = HEIGHT_DEVICE;
//	float minHeight = WIDTH_DEVICE/4;
//    for (int i=0; i<kMaxHillKeyPoints; i++) {
//        if (!((i%100==0 ||i%100==1||i%100==2||i%100==3||i%100==4||i%100==5)&&i>6)) {
//            switch (i%100) {
//                case 80:
//                    _hillKeyPoints[i] = CGPointMake(x, 400);
//                    break;
//                case 81:
//                    _hillKeyPoints[i] = CGPointMake(x, 300);
//                    break;
//                case 82:
//                    _hillKeyPoints[i] = CGPointMake(x, 500);
//                    break;
//                case 83:
//                    _hillKeyPoints[i] = CGPointMake(x, 650);
//                    break;
//                case 84:
//                    _hillKeyPoints[i] = CGPointMake(x, 610);
//                    break;
//                case 85:
//                    _hillKeyPoints[i] = CGPointMake(x, 580);
//                    break;
//                case 86:
//                    _hillKeyPoints[i] = CGPointMake(x, 560);
//                    break;
//                case 87:
//                    _hillKeyPoints[i] = CGPointMake(x, 520);
//                    break;
//                case 88:
//                    _hillKeyPoints[i] = CGPointMake(x, 490);
//                    break;
//                case 89:
//                    _hillKeyPoints[i] = CGPointMake(x, 440);
//                    break;
//                case 90:
//                    _hillKeyPoints[i] = CGPointMake(x, 400);
//                    break;
//                case 91:
//                    _hillKeyPoints[i] = CGPointMake(x, 360);
//                    break;
//                case 92:
//                    _hillKeyPoints[i] = CGPointMake(x, 320);
//                    break;
//                case 93:
//                    _hillKeyPoints[i] = CGPointMake(x, 280);
//                    break;
//                case 94:
//                    _hillKeyPoints[i] = CGPointMake(x, 230);
//                    break;
//                case 95:
//                    _hillKeyPoints[i] = CGPointMake(x, 180);
//                    break;
//                case 96:
//                    _hillKeyPoints[i] = CGPointMake(x, 130);
//                    break;
//                case 97:
//                    _hillKeyPoints[i] = CGPointMake(x+100, 40);
//                    break;
//                case 98:
//                    _hillKeyPoints[i] = CGPointMake(x, 35);
//                    break;
//                case 99:
//                    if (i <100){
//                        _hillKeyPoints[i] = CGPointMake(x-50,65);
//                    }else{
//                        _hillKeyPoints[i] = CGPointMake(x,40);
//                    }
//                    break;
//                    
//                default:
//                    if (i > kMaxHillKeyPoints-10) {
//                        _hillKeyPoints[i] = CGPointMake(x, 80);
//                    }else{
////                        static bool change = false;
////                        (i<100)?change = false:change=true;
////                        if (((i+change)%2==0)&& i%10<=6 ) {
////                            _hillKeyPoints[i] = CGPointMake(x+rangeDX, y);
////                        }else{
//                            _hillKeyPoints[i] = CGPointMake(x, y);
////                        }
//                    }
//                    break;
//            }
//        }else{
//            _hillKeyPoints[i] = CGPointMake(x, -80);
//        }
//        
//        if (i == 0) {
//            x = 40;
//            y = HEIGHT_DEVICE/2 - RADIUS_POU/2;
//        } else {
//            
//        minDX = random()%(290-180)+180;
//        minDY = random()%(140-20)+20;
//            
//        x += rand() % rangeDX + minDX;
//        dy = arc4random_uniform(rangeDY)+minDY;
////            x += rangeDX+minDX;
////            dy = rangeDY+minDY;
//            ny = y + dy*sign;
//            if(ny > maxHeight) ny = maxHeight;
//            if(ny < minHeight) ny = minHeight;
//            
//            y = ny;
//        }
//        sign *= -1;
//        
//    }
//    
//    for (int i=0; i<kMaxHillKeyPoints; i++){
//        CGPoint point  = _hillKeyPoints[i];
//        NSLog(@"point %i %@", i, NSStringFromCGPoint(point));
//        [dict setObject:NSStringFromCGPoint(point) forKey:[NSString stringWithFormat:@"%d",i]];
//    }
//    [self addInPlistDictionary:dict];
//    return _hillKeyPoints [kMaxHillKeyPoints-5].x;
//    
//    
// 
//}

//- (void) addInPlistDictionary:(NSDictionary*)dict {
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"way.plist"];
//    
//    NSFileManager *fileManager = [NSFileManager defaultManager];
//    
//    if (![fileManager fileExistsAtPath: path])
//    {
//        path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat: @"way.plist"] ];
//    }
//
//    //To insert the data into the plist
//    [dict writeToFile: path atomically:YES];
//    [dict release];
//
//    //To reterive the data from the plist
//    NSMutableDictionary *savedStock = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
//    
//    for (int i = 0 ; i < kMaxHillKeyPoints; i++) {
//        NSString *str = [savedStock objectForKey:[NSString stringWithFormat:@"%d",i]];
//        CGPoint myPoint = CGPointFromString(str);
//        NSLog(@"2POINT %f:%f, %@",myPoint.x,myPoint.y,str);
//    }
//    
//    [savedStock release];
//}

//- (void)resetHillVertices {
//    CGSize winSize = [CCDirector sharedDirector].winSize;
//
//    static int prevFromKeyPointI = -1;
//    static int prevToKeyPointI = -1;
//
//    // key points interval for drawing
//    while (_hillKeyPoints[_fromKeyPointI+1].x < _offsetXf-winSize.width/8/self.scale) {
//        _fromKeyPointI++;
//    }
//    while (_hillKeyPoints[_toKeyPointI-1].x < _offsetXf+winSize.width*10/8/self.scale) {
//        if (_toKeyPointI <= kMaxHillKeyPoints) {
//             _toKeyPointI++;
//        }
//    }
//
//    if (prevFromKeyPointI != _fromKeyPointI || prevToKeyPointI != _toKeyPointI) {
//        // vertices for visible area
//        _nHillVertices = 0;
//        _nBorderVertices = 0;
//        CGPoint p0, p1, pt0, pt1;
//        p0 = _hillKeyPoints[_fromKeyPointI-1];
//        
//        for (int i=_fromKeyPointI; i<_toKeyPointI+1; i++) {
//            p1 = _hillKeyPoints[i];
//            // triangle strip between p0 and p1
//            int8_t kHillSegment;
//            kHillSegment = 7;
////            if (i >= _fromKeyPointI-1 && i <= _fromKeyPointI+1) {
////                kHillSegment = 7;
////            }else {//if(i<=_fromKeyPointI || i>= _toKeyPointI+1)
////                kHillSegment = -13;
////            }
//            int hSegments = floorf((p1.x-p0.x)/(kHillSegmentWidth-kHillSegment));
//            float dx = (p1.x - p0.x) / hSegments;
//            float da = M_PI / hSegments;
//            float ymid = (p0.y + p1.y) / 2;
//            float ampl = (p0.y - p1.y) / 2;
//            pt0 = p0;
//            _borderVertices[_nBorderVertices++] = pt0;
//            for (int j=1; j<hSegments+1; j++) {
//                pt1.x = p0.x + j*dx;
//                pt1.y = ymid + ampl * cosf(da*j);
//
//                _borderVertices[_nBorderVertices++] = pt1;
//                //line
//                //draw vertices hils
//                _hillVertices[_nHillVertices] = CGPointMake(pt0.x, pt0.y-30);
//                _hillTexCoords[_nHillVertices++] = CGPointMake(pt0.x/512, 1.0f);
//                _hillVertices[_nHillVertices] = CGPointMake(pt1.x, pt1.y-30);
//                _hillTexCoords[_nHillVertices++] = CGPointMake(pt1.x/512, 1.0f);
//
//                _hillVertices[_nHillVertices] = CGPointMake(pt0.x, pt0.y);
//                _hillTexCoords[_nHillVertices++] = CGPointMake(pt0.x/512, 0);
//                _hillVertices[_nHillVertices] = CGPointMake(pt1.x, pt1.y);
//                _hillTexCoords[_nHillVertices++] = CGPointMake(pt1.x/512, 0);
//                pt0 = pt1;
//            }
//
//            p0 = p1;
//        }
//        prevFromKeyPointI = _fromKeyPointI;
//        prevToKeyPointI = _toKeyPointI;
//    }
//    [self resetBox2DBody];
//}

- (void)resetHillVertices {
    _nHillVertices = 0;
    _nBorderVertices = 0;
    CGPoint p0, p1, pt0, pt1;
    p0 = _hillKeyPoints[0];
    
    for (int i=1; i<_kMaxHillKeyPoints+1; i++) {
        p1 = _hillKeyPoints[i];
        // triangle strip between p0 and p1
        int hSegments = floorf((p1.x-p0.x)/kHillSegmentWidth);
        float dx = (p1.x - p0.x) / hSegments;
        float da = M_PI / hSegments;
        float ymid = (p0.y + p1.y) / 2;
        float ampl = (p0.y - p1.y) / 2;
        pt0 = p0;
        _borderVertices[_nBorderVertices++] = pt0;
        for (int j=1; j<hSegments+1; j++) {
            pt1.x = p0.x + j*dx;
            pt1.y = ymid + ampl * cosf(da*j);
            
            _borderVertices[_nBorderVertices++] = pt1;
            //draw vertices hils
            _hillVertices[_nHillVertices] = CGPointMake(pt0.x, pt0.y-35);
            _hillTexCoords[_nHillVertices++] = CGPointMake(pt0.x/512, 1.0f);
            _hillVertices[_nHillVertices] = CGPointMake(pt1.x, pt1.y-35);
            _hillTexCoords[_nHillVertices++] = CGPointMake(pt1.x/512, 1.0f);
            
            _hillVertices[_nHillVertices] = CGPointMake(pt0.x, pt0.y);
            _hillTexCoords[_nHillVertices++] = CGPointMake(pt0.x/512, 0);
            _hillVertices[_nHillVertices] = CGPointMake(pt1.x, pt1.y);
            _hillTexCoords[_nHillVertices++] = CGPointMake(pt1.x/512, 0);
            pt0 = pt1;
        }
        
        p0 = p1;
    }
    [self resetBox2DBody];
}

- (void)resetHillsFromScreenSize {
    CGSize winSize = [CCDirector sharedDirector].winSize;
    
    static int prevFromKeyPointI = -1;
    static int prevToKeyPointI = -1;
    
    // key points interval for drawing
    while (_hillKeyPoints[_fromKeyPointI+2].x < _offsetXf-winSize.width/8/self.scale) {
        _fromKeyPointI++;
    }
    while (_hillKeyPoints[_toKeyPointI-1].x < _offsetXf+winSize.width*10/8/self.scale) {
        if (_toKeyPointI <= _kMaxHillKeyPoints) {
            _toKeyPointI++;
        }
    }
    
    if (prevFromKeyPointI != _fromKeyPointI || prevToKeyPointI != _toKeyPointI) {
        prevFromKeyPointI = _fromKeyPointI;
        prevToKeyPointI = _toKeyPointI;
    }
    
}


#pragma mark RESET STRIPES
// TODO: reset
- (void) resetTerain {


    _coinBank=0;

    _fromKeyPointI = 0;
	_toKeyPointI = 0;
}

- (void)selectWayFromStageID:(uint_fast8_t)idStage withBoss:(BOOL)isBoss{
    wayForStage = idStage;
    bossId = isBoss;
    _theDistanceOfWay = [self generateHills];
    [self resetHillVertices];
    self.offsetXf = 0;
    [self resetCoin];
}

#pragma mark - COINS

- (void)initCoins {
    
    currentCoinTag = kCoinTag;
	while(currentCoinTag <= kCoinTag+kCoinOrd) {
        CCSprite *coin = [CCSprite spriteWithFile:[NSString stringWithFormat:@"%@19@2x.png",MODEL]];
        coin.scale = coin.scale*(0.5+0.5*IS_RETINA);
        coin.position = ccp(-20, 250);
        [self addChild:coin z:4 tag:currentCoinTag];
		currentCoinTag++;
        [self resetCoin];
	}
}

- (void)resetCoin{
    int k = kCoinTag;
	while(k <= kCoinTag+kCoinOrd) {
        CCSprite *coin = (CCSprite*)[self getChildByTag:k];
        coin.position = ccp(_offsetXf, 250);
        coin.visible = NO;
		k++;
    }
    isCoinReseted = YES;
}

- (void)coinToPouPosition:(float)Y{
    if (!coinOnHill) {
        int k = kCoinTag;
        while(k <= kCoinTag+kCoinOrd) {
            CCSprite *coin = (CCSprite*)[self getChildByTag:k];
            if (coin.position.x - _offsetXf < 200 && coin.position.x - _offsetXf > 50 && (coin.position.y< Y || coin.position.y < Y+200)) {
                [coin runAction:[CCSequence actions:[CCMoveTo actionWithDuration:.2 position:ccp(_offsetXf+10 ,Y)],
                                 [CCCallFuncN actionWithTarget:self selector:@selector(coinFlyingColision:)], nil]];
            }
            k++;
        }
    }
}

- (void)coinFlyingColision:(CCSprite*)c{
    c.visible = NO;
}


- (void)coinCollisionWithBodyX:(float)bodyX Y:(float)bodyY{
    bodyPositionY = bodyY;
    for(int t= kCoinTag; t <= kCoinTag + kCoinOrd; t++) {
        CCSprite *coin = (CCSprite*)[self getChildByTag:t];
        float range = 20.0f;
        if (coin.visible) {
            if(bodyX > coin.position.x - range && bodyX < coin.position.x + range && bodyY > coin.position.y - range && bodyY < coin.position.y + range) {
                coin.visible=NO;
                static unsigned char i = 0;
                i++;
                [[SimpleAudioEngine sharedEngine] playEffect:[NSString stringWithFormat:@"marimba%d.mp3",i%8] loop:NO];
                _coinBank++;
            }
        }
    }
    
}

- (void)coinsInWorld{
    CCSprite *spriteCoin = (CCSprite *)[self getChildByTag: kCoinTag+3];
    if ((spriteCoin.position.x + 100 < _hillKeyPoints[_fromKeyPointI].x) && !isCoinReseted) {
        [self resetCoin];
    }
    
    if (isCoinReseted) {
        //        if (rand()%2) {
        BOOL isOddCoinTag;
        BOOL isDownHill;
        isOddCoinTag = 1;
        CGPoint p0 = _hillKeyPoints[_toKeyPointI+3];
        CGPoint p1 = _hillKeyPoints[_toKeyPointI+4];
        
        (p0.y >p1.y)?isDownHill = 0:isDownHill=1;
        
        for (int i = _toKeyPointI+3+isDownHill; i <= _toKeyPointI+4+isDownHill; i++) {
            p0 = _hillKeyPoints[i];
            p1 = _hillKeyPoints[i+1];
            if (p0.x < self.theDistanceOfWay.x) {
            int hSegments = floorf((p1.x-p0.x)/24);
            float dx = (p1.x - p0.x) / hSegments;
            float da = M_PI / hSegments;
            float ymid = (p0.y + p1.y) / 2;
            float ampl = (p0.y - p1.y) / 2;
            
            CGPoint pt1;
            for (int j = 0; j < hSegments+1; ++j) {
                
                pt1.x = p0.x + j*dx;
                pt1.y = ymid + ampl * cosf(da*j);
                
                currentCoinTag = kCoinTag+(j-1);
                if (currentCoinTag<=kCoinTag+kCoinOrd) {
                    CCSprite *sprite = (CCSprite *)[self getChildByTag: currentCoinTag];
                    if (currentCoinTag%2==isOddCoinTag) {
                        sprite.position = ccp(pt1.x, pt1.y+sprite.contentSize.height/2);
                        sprite.visible = YES;
                    }
                    
                }
            }
            isOddCoinTag = 0;
            coinOnHill = YES;
            }
        }
        /*}else{
         CGPoint p0 = _hillKeyPoints[(int)_offsetXf/xDistanceFromHills+4];
         CGPoint p1 = _hillKeyPoints[(int)_offsetXf/xDistanceFromHills+5];
         
         int k = kCoinTag;
         float yPosCoin = ((p0.y>p1.y)?p0.y+300:p1.y+300);
         while(k <= kCoinTag + kCoinOrd) {
         CCSprite *coin = (CCSprite*)[self getChildByTag:k];
         coin.position = ccp(p1.x+(30*(k%100)), yPosCoin);
         coin.visible = YES;
         k++;
         }
         coinOnHill = NO;
         }*/
        isCoinReseted = NO;
    }
}


#pragma mark -
#pragma mark ENEMY
- (BOOL) isGoodPositionToDive:(CGPoint)enemyPos indexHill: (int8)indexHill {
    int z=0;
    for(int i = 0; i < _kMaxHillKeyPoints;i++){
        CGPoint pt0 = _hillKeyPoints[i];
        CGPoint pt1 = _hillKeyPoints[i+1];
        if (enemyPos.x > pt0.x && enemyPos.x < pt1.x) {
            z = i;
            break;
        }
    }
    switch (wayForStage) {
        case 1:
            if (bossId) {
            
            }else{
                if( z >= 84 &&  z <= 96 ){return YES;}
                if( z >= 96 &&  z <= 105 ){return NO;}
            }
            break;
    }

    
    CGPoint p0, p1;
    
    if ((z+1)%indexHill == 0){
        p0=_hillKeyPoints[z+1];
        p1= _hillKeyPoints[z+2];
        if (p0.y < p1.y) {
            p0=_hillKeyPoints[z];
            p1= _hillKeyPoints[z+1];
        }
        
        if (((enemyPos.x >= p0.x-20) && (enemyPos.x <= p1.x-20)) && p0.y+200 >= enemyPos.y) {
            return YES;
        }else{
            return  NO;
        }
    }
    return NO;
    //    }
}

- (BOOL)increaseVelocity{
    bool b;
    switch (wayForStage) {
        case 1:
            if (bossId) {
                return  NO;
            } else {
                if(_fromKeyPointI %100 >= 84 &&  _fromKeyPointI%100 <= 96){
                    CGPoint p0 = _hillKeyPoints [_fromKeyPointI];
                    (_offsetXf >= p0.x) ? b = YES : b = NO;
                    return b;
                }
                else {
                    return  NO;
                }
            }
        break;
            
        default:
            return NO;
            break;
    }
  
}

- (BOOL) isEndOfWay {
    if (_toKeyPointI >= _kMaxHillKeyPoints) {
        return YES;
    }else{
        return NO;
    }
}

- (BOOL) isTimeToStopAtPosition:(CGPoint)enemyPos{
   
    if(enemyPos.x >= _hillKeyPoints [_kMaxHillKeyPoints-2].x){
//         NSLog(@"time to stop");
        return YES;
    }
    else {
        return  NO;
    }
}

#pragma mark - BACKGROUND
- (BOOL) isTimeToChangeBackgr{
    bool b;
    if((int)_offsetXf/xDistanceFromHills%100 >= 0 &&  (int)_offsetXf/xDistanceFromHills%100 <= 2 ){
        CGPoint p0 = _hillKeyPoints [(int)_offsetXf/xDistanceFromHills];
        (_offsetXf >= p0.x) ? b = YES : b = NO;
        return b;
    }
    else {
        return  NO;
    }
    
}

@end