//
//  FirstScene.m
//  Tamagochi
//
//  Created by Sergiu Moțipan on 6/4/13.
//
//

#import "GamesLabScene.h"
#import "Game.h"


@implementation GameLabScene
{
    float totalTime,yy,xx, tamTouch;
    BOOL repeatTouches;
    float limit;
    Ball *bal;
    CCSprite *tvBig;
    CCSprite *closeTV;
    ScrollLayer *scrollTable;
}



+(CCScene *) scene
{
	CCScene *scene = [CCScene node];
	GameLabScene *layer = [GameLabScene node];
	[scene addChild: layer];
	return scene;
}


-(CCAction*)initAnimations:(float)dim
{
    id action1;
    id action2;
    if (IS_IPAD)
    {
        action1 = [CCScaleTo actionWithDuration:0.5 scale:dim+0.05+0.35*IS_RETINA];
        action2 = [CCScaleTo actionWithDuration:0.5 scale:dim+0.35*IS_RETINA];
    }
    else
    {
        action1 = [CCScaleTo actionWithDuration:0.5 scale:dim+0.05+0.35*IS_RETINA];
        action2 = [CCScaleTo actionWithDuration:0.5 scale:dim+0.35*IS_RETINA];
    }
    id  animationObjects = [CCSequence actions:action1, action2, nil];
    return [CCRepeatForever actionWithAction:animationObjects];
}

-(void)initAssets
{
   
    
}

-(ccColor3B)tranformColor:(NSInteger)tag object:(NSInteger)object
{
    NSMutableArray *arr=[[[NSMutableArray alloc] initWithArray:[stateTamagochi colorSkin:tag object:object]] autorelease];
    ccColor3B color={[[arr objectAtIndex:0] floatValue], [[arr objectAtIndex:1] floatValue], [[arr objectAtIndex:2] floatValue]};
    return color;
}

-(id)init
{
    self=[super init];
    if (self)
    {
        [[SimpleAudioEngine sharedEngine] stopAllEffects];
        [[SimpleAudioEngine sharedEngine] setEffectsVolume:[stateTamagochi soundOn]];
        [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume:[stateTamagochi soundOn]];
        [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(changeColor:) name:@"skinColor" object:nil];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(closeTelevizor) name:@"closeTelevizor" object:nil];
        [stateTamagochi setRoom:gameRoom];
        mouth=[stateTamagochi mouthState];
            
        [[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"giggle.wav"];
        [[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"mtem.wav"];
        
        [[SimpleAudioEngine sharedEngine] setEffectsVolume:[stateTamagochi soundOn]];
        [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume:[stateTamagochi soundOn]];
        
        winSize = CGSizeMake(WIDTH_DEVICE, HEIGHT_DEVICE-40);
        
        CCSprite *foot = [CCSprite spriteWithFile:[NSString stringWithFormat:@"foot_1%@.png",(IS_IPAD)?@"-iPad":@""]];
        foot.tag=2;
        [foot setColor:[self tranformColor:[stateTamagochi selectColorSkin:2 room:1] object:0]];
        foot.position = ccp(winSize.width/2, foot.boundingBox.size.height/2);
        [foot setScale:0.5+0.5*IS_RETINA];
        [self addChild:foot z:-11];

        
        CCSprite *background = [CCSprite spriteWithFile:[NSString stringWithFormat:@"bg_1%@.png",(IS_IPAD)?@"-iPad":@""]];
        background.tag=1;
        background.position = ccp(winSize.width/2, winSize.height/2+foot.boundingBox.size.height/2);
        [background setColor:[self tranformColor:[stateTamagochi selectColorSkin:1 room:1] object:0]];
        [background setScale:0.5+0.5*IS_RETINA];
        [self addChild:background z:-12];
       
        
        CCSprite *window = [CCSprite spriteWithFile:[NSString stringWithFormat:@"window%@.png",(IS_IPAD)?@"-iPad":@""]];
        window.position = ccp(winSize.width/2+window.boundingBox.size.width/3, winSize.height/2+window.boundingBox.size.height/3);
        [window setScale:0.5+0.5*IS_RETINA];
        [self addChild:window z:-10];
        
        CCSprite *draperie = [CCSprite spriteWithFile:[NSString stringWithFormat:@"draperie_1%@.png",(IS_IPAD)?@"-iPad":@""]];
        draperie.tag=3;
        draperie.position = ccp(winSize.width/2+draperie.boundingBox.size.width/3, winSize.height/2+draperie.boundingBox.size.height/4.5);
        [draperie setColor:[self tranformColor:[stateTamagochi selectColorSkin:3 room:1] object:0]];
        [draperie setScale:0.5+0.5*IS_RETINA];
        [self addChild:draperie z:-10];
        
        CCSprite *bows = [CCSprite spriteWithFile:[NSString stringWithFormat:@"bows%@.png",(IS_IPAD)?@"-iPad":@""]];
        bows.position = ccp(winSize.width/2+draperie.boundingBox.size.width/3+5, winSize.height/2-bows.boundingBox.size.height/3);
        [bows setScale:0.5+0.5*IS_RETINA];
        [self addChild:bows z:-10];
        
        CCSprite *frame = [CCSprite spriteWithFile:[NSString stringWithFormat:@"frame_1%@.png",(IS_IPAD)?@"-iPad":@""]];
        frame.tag=6;
        frame.position = ccp(winSize.width/2-frame.boundingBox.size.width*2, winSize.height/2+frame.boundingBox.size.height);
        [frame setColor:[self tranformColor:[stateTamagochi selectColorSkin:6 room:1] object:0]];
        [frame setScale:0.5+0.5*IS_RETINA];
        [self addChild:frame z:-10];
        
        CCSprite *frame_center = [CCSprite spriteWithFile:[NSString stringWithFormat:@"frame_1_img%@.png",(IS_IPAD)?@"-iPad":@""]];
        frame_center.position = ccp(winSize.width/2-frame.boundingBox.size.width*2, winSize.height/2+frame.boundingBox.size.height);
        [frame_center setScale:0.5+0.5*IS_RETINA];
        [self addChild:frame_center z:-10];
        
        
        self.isTouchEnabled=YES;
        
        moreGames = [CCSprite spriteWithFile:[NSString stringWithFormat:@"moreGames%@.png",(IS_IPAD)?@"-iPad":@""]];
        moreGames.position = ccp(60, 60);
        [moreGames setScale:0.3+0.3*IS_RETINA];
        [self addChild:moreGames z:3];
        if ([stateTamagochi shopCategory]!=sh_skins)
            [moreGames runAction:[self initAnimations:0.3]];
        
        [personaj person].position = ccp(winSize.width/2, 50+100*[stateTamagochi sizeTamagochi]-50*IS_IPAD);
        [personaj initRects];
        
        [personaj person].delegate=self;
        [self addChild:[personaj person] z:1];

        bal = [Ball node];
        bal.delegate=self;
        [self addChild:bal z:4];
        [bal stopHelicopter];
        [self scheduleUpdate];
 
    }
    
    return self;
}

-(void)changeColor:(NSNotification *)notif
{
    [(CCSprite *)[self getChildByTag:[[[notif.object componentsSeparatedByString:@","] objectAtIndex:1] intValue]] setColor:[self tranformColor:[[[notif.object componentsSeparatedByString:@","] objectAtIndex:0] intValue] object:[[[notif.object componentsSeparatedByString:@","] objectAtIndex:1] intValue]]];
}

-(void)playGames
{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"openTV" object:nil];
    [moreGames runAction:[CCFadeOut actionWithDuration:0.1]];
}
- (void)update:(float)dt
{
 
}

-(CGPoint)returnPosition
{
    return CGPointMake(xx, yy);
}

-(float)returnTouchValue
{
    
    switch ([stateTamagochi mouthState]) {
        case mouthStateFun:
        {
            return tamTouch*sin(totalTime);
        }
            break;
        case mouthStateReadyEat:
        {
            return 0.3;
        }
            break;
        case mouthStateEat:
        {
            return 0.25*sin(totalTime);
        }
            break;
       
            
        default:return tamTouch;
            break;
    }
}

-(void)closeTelevizor
{
    [bal setIsTouchEnabled:YES];
    [moreGames runAction:[CCFadeIn actionWithDuration:0.1]];
}
-(void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    nr_sounds=0;
    UITouch* touch = [touches anyObject];
    CGPoint location = [touch locationInView: [touch view]];
    startTouchPoint=location;
    date_start=[[NSDate date] retain];

    location = [[CCDirector sharedDirector] convertToGL:location];
    if (!bal.start)
    {
        xx=location.x;
        yy=location.y;
    }
    touchFun=0;
    if (CGRectContainsPoint([moreGames   boundingBox], location) && moreGames.opacity==255 )
    {
         [stateTamagochi setTouchObject:tagTV];
    }
    
    if (CGRectContainsPoint([personaj rectPerson],location))
    {
        [stateTamagochi setTouchObject:tagTamagochi];
    }
   
}

-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch* touch = [touches anyObject];
    CGPoint location = [touch locationInView: [touch view]];
    location = [[CCDirector sharedDirector] convertToGL:location];

    if ([stateTamagochi touchObject]==tagTamagochi && CGRectContainsPoint([personaj rectPerson],location))
    {
        [stateTamagochi setMouthState:mouthStateFun];
        touchFun+=0.006;
        if (touchFun>=0.05)
        {
            if (!repeatTouches)
            [self fun];
        }
        nr_sounds++;
    }
    if (!bal.start)
    {
        xx=location.x;
        yy=location.y;
    }
    
    
}
-(void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [stateTamagochi setMouthState:mouth];
    
    UITouch* touch = [touches anyObject];
    CGPoint location = [touch locationInView: [touch view]];
    
    endTouchPoint=location;
    float moveDifference=startTouchPoint.x-endTouchPoint.x;
    float differenceY=startTouchPoint.y-endTouchPoint.y;
    if (differenceY<0) differenceY*=-1;
    NSTimeInterval secondsBetween = [[NSDate date] timeIntervalSinceDate:date_start];
    [date_start release];
    date_start=nil;
    if (moveDifference>100.0 && differenceY<60.0 && endTouchPoint.y<150.0 && secondsBetween<0.2)
    {
         [[NSNotificationCenter defaultCenter]postNotificationName:@"nextRoom" object:@"1"];
    }
    else if (moveDifference<-100.0 && differenceY<60.0 && endTouchPoint.y<150.0 && secondsBetween<0.2)
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"nextRoom" object:@"2"];
    }
    
    
    location = [[CCDirector sharedDirector] convertToGL:location];
    nr_sounds=0;
    touchFun=0;
    if (!bal.start)
    {
        xx=0;
        yy=0;
    }
    if (CGRectContainsPoint([moreGames   boundingBox], location) && moreGames.opacity==255 && [stateTamagochi touchObject]==tagTV)
    {
        if (bal.start)
        {
            [bal stopHelicopter];
        }

        [bal setIsTouchEnabled:NO];
        [self playGames];
    }

    
    [stateTamagochi setTouchObject:tagDefault];
}

-(void)changePosition:(CGPoint)pos
{
    
    xx=pos.x;
    yy=pos.y;
}

#pragma Tamagochi effects

-(void)resetVariables
{
    tamTouch=0;
    repeatTouches=NO;
    touchFun=0;
    if ([[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying])
        [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
}

-(void)fun
{
    repeatTouches=YES;
    totalTime=0;
    tamTouch=0.3;
    if (![[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying] &&  [stateTamagochi soundOn])
        [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"giggle.wav" loop:NO];
    [self performSelector:@selector(resetVariables) withObject:nil afterDelay:1.57];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]    removeObserver:self name:@"skinColor" object:nil];
    [[NSNotificationCenter defaultCenter]    removeObserver:self name:@"closeTelevizor" object:nil];
    [[CCDirector sharedDirector] purgeCachedData];
    [[CCTextureCache sharedTextureCache] removeAllTextures];
    [CCTextureCache purgeSharedTextureCache];
    [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFrames];
    [CCSpriteFrameCache purgeSharedSpriteFrameCache];

    [super dealloc];
}

@end
