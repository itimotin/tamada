//
//  FirstScene.h
//  Tamagochi
//
//  Created by Sergiu Moțipan on 6/4/13.
//
//

#import "CCLayer.h"
#import "cocos2d.h"
#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES2/gl.h>
#import "Box2D.h"
#import "GLES-Render.h"
#import "PouNode.h"
#import "SimpleAudioEngine.h"
#import "CCScrollLayer.h"
#import "FGScrollLayer.h"
#import "ScrollLayer.h"
#import "InMallRoom.h"




#define PTM_RATIO 32


enum foodTouch {
    f_default =0,
    f_first = 1,
    f_second = 2
};

@interface KitchenRoom : CCLayer< PouNodeDelegate, CCScrollLayerDelegate,CCTargetedTouchDelegate, ScrollLayerDelegate>
{
	ScrollLayer* scrollTable;
    CGSize winSize;
    int nr_sounds;
    float touchFun;
    int foodMoved;
    int scrollTag;
    BOOL tableScroll;
    BOOL moveFood;
    BOOL stopFood;
    int mouth;
    
    CGPoint startTouchPoint;
    CGPoint endTouchPoint;
    NSDate *date_start;
}

@property (nonatomic, strong) CCScrollLayer *scrolFood_1;
@property (nonatomic, strong) CCScrollLayer *scrolFood_2;
@property (nonatomic, strong) CCScrollLayer *scrolFood_3;
@property (nonatomic, strong) CCScrollLayer *scrolFood_4;
+(CCScene *) scene;

@end
