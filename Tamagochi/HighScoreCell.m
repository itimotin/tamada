//
//  HighScoreCell.m
//  Tamagochi
//
//  Created by Timotin Vanea on 2/24/14.
//
//

#import "HighScoreCell.h"

@implementation HighScoreCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_namePlayer release];
    [_scorePlayer release];
    [_scorePl release];
    [_timePlayer release];
    [_numberPlayer release];
    [super dealloc];
}

@end
