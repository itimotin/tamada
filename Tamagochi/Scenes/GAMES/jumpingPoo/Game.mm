#import "Game.h"
#import "Main.h"
//#import "Highscores.h"
#import "SimpleAudioEngine.h"

@interface Game (Private)
- (void)initPlatforms;
- (void)initPlatform;
- (void)startGame;
- (void)resetPlatforms;
- (void)resetPlatform;
- (void)resetpoo;
- (void)resetBonus;
- (void)step:(ccTime)dt;
- (void)jump;
- (void)jumpBonus;
- (void)showHighscores;
- (CCSprite*)changePlatformImage:(CCSprite*)platformSprite  withTag:(NSInteger)tag;
- (void)changePlatformXPosition;
- (void)moveBackgroundWith:(float)delta;
- (void)initCoins;
- (void)addPointToScore;
- (void)ebashimBonus:(CCSprite*)bonus InCollisionWith:(CCSprite*)poo;
- (void)coinInCollisionWithPoo;
- (void)colisonWithPlatfomMAX:(float)max_x MIN:(float)min_x;
- (void)finishAndDealocatePlatforms:(float)delta;
- (void)movePlatformCoinAndBonus:(CCSprite*)bonus WithY:(float)delta;
- (void)addPointToCoinBank:(CCSprite*)coin;
- (void)platformInvisible:(CCSprite*)platform;
@end


@implementation Game

+ (CCScene *)scene
{
    CCScene *game = [CCScene node];
    
    Game *layer = [Game node];
    
    [game addChild:layer];
    
    return game;
}

#pragma mark - INIT
- (id)init {
	if(![super init]) return nil;
    
    isPause = NO;
    _pauseButton = [CCSprite spriteWithFile:@"pause@2x.png"];
    [self addChild:_pauseButton z:3];
    CGSize size = _pauseButton.contentSize;
    float padding = 8;
//    _pauseButton.position = ccp(HEIGHT_DEVICE-size.height/2-padding, WIDTH_DEVICE-size.width/2-padding);
    _pauseButton.position = ccp(WIDTH_DEVICE-size.height/2-padding, HEIGHT_DEVICE-size.width/2-padding);
    // [[personaj person] removeFromParentAndCleanup:NO];
	gameSuspended = YES;
    forNonIpad = NO;
    bonusAccelerateBackground = 1.0;
    [self initCoins];
	[self initPlatforms];
    
    [[personaj person] setScale:0.25];
    [personaj person].tag=kPoo;
    [personaj person].delegate=self;
	[self addChild:[personaj person] z:4];
    /*
    CCSprite *bonus;
	for(int i=0; i<kNumBonuses; i++) {
		bonus = [CCSprite spriteWithFile:[NSString stringWithFormat:@"%@20@2x.png",MODEL]];
		[self addChild:bonus z:4 tag:kBonusStartTag+i];
		bonus.visible = NO;
	}
*/
    CCSprite *coin = [CCSprite spriteWithFile:[NSString stringWithFormat:@"%@19@2x.png",MODEL]];
    coin.scale = coin.scale*(0.5+0.5*IS_RETINA);
    [self addChild:coin z:4 tag:kCoinForMove];
    coin.position = ccp(0,0);
    coin.visible = NO;
    
    CCLabelTTF *scoreLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%@:    %d", NSLocalizedString(@"score", nil),score] fontName:@"Marker Felt" fontSize:(IS_IPAD)?22.0:16.0];

	[self addChild:scoreLabel z:5 tag:kScoreLabel];
	scoreLabel.position = ccp(WIDTH_DEVICE/2-100,HEIGHT_DEVICE-30);

    CCLabelTTF *scoreCoinLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%d",score] fontName:@"Marker Felt" fontSize:(IS_IPAD)?22.0:18.0];
	[self addChild:scoreCoinLabel z:5 tag:kScoreCoinLabel];
	scoreCoinLabel.position = ccp(WIDTH_DEVICE/2 +100,HEIGHT_DEVICE-30);
    CCSprite *sprBank = [CCSprite spriteWithFile:@"bank.png"];
    sprBank.position = ccp(WIDTH_DEVICE/2 +70,HEIGHT_DEVICE-30);
    [self addChild:sprBank z:5];
	[self schedule:@selector(step:)];
	
	self.isTouchEnabled = YES;
	self.isAccelerometerEnabled = YES;
    
	[[UIAccelerometer sharedAccelerometer] setUpdateInterval:(1.0 / kFPS)];
    
	[self startGame];
	
	return self;
    
    
}


- (void)addButtonsOnView {
//    [self pauseGame];
    
   
   
    [[SimpleAudioEngine sharedEngine] pauseBackgroundMusic];
    
    if ([stateTamagochi soundOn])
        [[SimpleAudioEngine sharedEngine] playEffect:@"btn_press.mp3" loop:NO];
    NSLog(@"ADDDED");
    [self removeButtons];
    isPause = YES;
    timerIsRunning = NO;
    CCSprite *sprBackgroundButtons = [CCSprite spriteWithFile:@"menu_game.png"];
    sprBackgroundButtons.position = ccp(WIDTH_DEVICE+WIDTH_DEVICE/8 ,HEIGHT_DEVICE/2);
    [self addChild:sprBackgroundButtons z:5 tag:kBgrMenu];
    
	CCMenuItem *button1ContinueGame = [CCMenuItemImage itemWithNormalImage:[NSString stringWithFormat:((gameSuspended)?@"btn_play_.png":@"btn_play.png")]
                                                             selectedImage:[NSString stringWithFormat:((gameSuspended)?@"btn_play_.png":@"btn_play.png")]
                                                                    target:self selector:@selector(continueGame:)];
	CCMenuItem *button2ResetGame = [CCMenuItemImage itemWithNormalImage:[NSString stringWithFormat:@"btn_replay.png"]
                                                          selectedImage:[NSString stringWithFormat:@"btn_replay.png"]
                                                                 target:self  selector:@selector(resetGame:)];
    
    CCMenuItem *button4ExitGame = [CCMenuItemImage itemWithNormalImage:[NSString stringWithFormat:@"btn_close_highscore.png"]
                                                         selectedImage:[NSString stringWithFormat:@"btn_close_highscore.png"]
                                                                target:self selector:@selector(exitGame:)];
    
    CCMenu *menu = [CCMenu menuWithItems: button1ContinueGame, button2ResetGame, button4ExitGame, nil];
    
	[menu alignItemsInRows:[NSNumber  numberWithInt:3], nil];
    
	menu.position = ccp(WIDTH_DEVICE+WIDTH_DEVICE/8 ,HEIGHT_DEVICE/2.4);
    [self addChild:menu z:11 tag:kMeTag];
    
    [menu runAction:[CCMoveTo actionWithDuration:0.5 position:CGPointMake(WIDTH_DEVICE - WIDTH_DEVICE/8, HEIGHT_DEVICE/2.4)]];
    [sprBackgroundButtons runAction:[CCMoveTo actionWithDuration:0.5 position:CGPointMake(WIDTH_DEVICE - WIDTH_DEVICE/8, HEIGHT_DEVICE/2.1)]];
    
}


- (void)removeButtons {
    isPause = NO;
    timerIsRunning = YES;
    CCMenu *menu = (CCMenu*)[self getChildByTag:kMeTag];
    [menu removeFromParentAndCleanup:YES];
    
    CCSprite *backGr = (CCSprite*)[self getChildByTag:kBgrMenu];
    backGr.visible = NO;
    [self removeChild:backGr cleanup:YES];
}

- (void)continueGame:(id)sender{
   if (gameOver) {
//        [self removeState];
//       alertInfo = [UIAlertView new];
//       alertInfo.title = @"Reset!";
//       alertInfo.message = @"You can't continue, beacause you're broken!";
//       alertInfo.delegate = self;
//       [alertInfo addButtonWithTitle:@"Yes"];
//       [alertInfo addButtonWithTitle:@"Cancel"];
//       //        alertInfo.transform = CGAffineTransformRotate(alertInfo.transform, ( 270 * M_PI ) / 180 );
//       [alertInfo show];
   }else{
       
       [[SimpleAudioEngine sharedEngine] resumeBackgroundMusic];
       if ([stateTamagochi soundOn])
           [[SimpleAudioEngine sharedEngine] playEffect:@"btn_press.mp3" loop:NO];
       if ([stateTamagochi soundOn])
           [[SimpleAudioEngine sharedEngine] playEffect:@"btn_press.mp3" loop:NO];
       [self removeButtons];
       self.isTouchEnabled = YES;
           [self removeState];
   }}
- (void)resetGame:(id)sender{
  
    if ([stateTamagochi funny]<100.0)
    {
        [stateTamagochi setFunny:[stateTamagochi funny]+time/6.0];
        [stateTamagochi setNextLevelPoints:[stateTamagochi nextLevelPoints] +time/60.0*5.0*(1.3-[stateTamagochi sizeTamagochi])];
        [stateTamagochi addToNextLevel:4];
    }
    
    if ([stateTamagochi funny]>100.0)
        [stateTamagochi setFunny:100.0];
    if ([stateTamagochi fat]>1.0)
        [stateTamagochi setFat:[stateTamagochi fat]-time/100.0];
    
    if ([stateTamagochi fat]<1.0)
        [stateTamagochi setFat:1.0];
    if ([stateTamagochi food]>0)
    {
        [stateTamagochi setFood:[stateTamagochi food]-time/10.0];
        
    }
    if ([stateTamagochi  food]<0) [stateTamagochi setFood:0];
    
    if ([stateTamagochi energy]>0)
    {
        [stateTamagochi setEnergy:[stateTamagochi energy]-time/6.0];
    }
    if ([stateTamagochi  energy]<0) [stateTamagochi setEnergy:0];
    [stateTamagochi updateEnergy];
    [stateTamagochi updateFood];
    [stateTamagochi updateFat];
    [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
   
    if ([stateTamagochi soundOn])
        [[SimpleAudioEngine sharedEngine] playEffect:@"btn_press.mp3" loop:NO];
    [self removeButtons];
    self.isTouchEnabled = YES;
    [self startGame];
    [self removeState];
    
}
- (void)exitGame:(id)sender{
    if ([stateTamagochi funny]<100.0)
    {
        [stateTamagochi setFunny:[stateTamagochi funny]+time/6.0];
        [stateTamagochi setNextLevelPoints:[stateTamagochi nextLevelPoints] +time/60.0*5.0*(1.3-[stateTamagochi sizeTamagochi])];
        [stateTamagochi addToNextLevel:4];
    }
    if ([stateTamagochi funny]>100.0)
        [stateTamagochi setFunny:100.0];
    
    if ([stateTamagochi fat]>1.0)
        [stateTamagochi setFat:[stateTamagochi fat]-time/100.0];
    
    if ([stateTamagochi fat]<1.0)
        [stateTamagochi setFat:1.0];
    if ([stateTamagochi food]>0)
    {
        [stateTamagochi setFood:[stateTamagochi food]-time/10.0];
        
    }
    if ([stateTamagochi  food]<0) [stateTamagochi setFood:0];
    if ([stateTamagochi energy]>0)
    {
        [stateTamagochi setEnergy:[stateTamagochi energy]-time/6.0];
    }
    if ([stateTamagochi  energy]<0) [stateTamagochi setEnergy:0];
    [stateTamagochi updateFood];
    [stateTamagochi updateEnergy];
    [stateTamagochi updateFat];
    [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
    if ([stateTamagochi soundOn])
        [[SimpleAudioEngine sharedEngine] playEffect:@"btn_press.mp3" loop:NO];
    [self removeButtons];
    self.isTouchEnabled = YES;
    [self showHighscores];
    [self removeState];
}

- (void)addStateWithText:(NSString*)textInfo AndInstruction:(NSString*)strInstruction{
    CCSprite *sprAlertBG = [CCSprite spriteWithFile:@"top_highscore.png"];
    sprAlertBG.scaleY = 0.9;
    sprAlertBG.scaleX = 0.9;
    sprAlertBG.position = ccp(WIDTH_DEVICE/3, HEIGHT_DEVICE/2);
    [self addChild:sprAlertBG z:6 tag:kInformationBG];
    CCSprite *spr = [CCSprite spriteWithFile:@"cell.png"];
    
    CCLabelTTF *titleAlert = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%@",textInfo] dimensions:CGSizeMake(sprAlertBG.contentSize.width, 30) hAlignment:kCCTextAlignmentCenter fontName:gameFont fontSize:(IS_IPAD)?35:25];
    spr.position = titleAlert.position = ccp(WIDTH_DEVICE/3, HEIGHT_DEVICE/2+80);
    [self addChild:titleAlert z:8 tag:kInformation];
    [self addChild:spr z:7 tag:kUmbra];
    
    CCSprite *spr1 = [CCSprite spriteWithFile:@"cell.png"];
    //    spr1.scaleX = 1.25;
    CCLabelTTF *titleInstruction = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%@",strInstruction] dimensions:CGSizeMake(sprAlertBG.contentSize.width, 30) hAlignment:kCCTextAlignmentCenter fontName:gameFont fontSize:(IS_IPAD)?30:20];
    spr1.position  = titleInstruction.position = ccp(WIDTH_DEVICE/3, HEIGHT_DEVICE/2+40);
    [self addChild:titleInstruction z:8 tag:kInstruction];
    [self addChild:spr1 z:7 tag:kUmbra1];
    
    CCSprite *spr2 = [CCSprite spriteWithFile:@"cell.png"];
    CCLabelTTF *bankLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%@: %d", NSLocalizedString(@"coins", nil),scoreCoin] dimensions:CGSizeMake(sprAlertBG.contentSize.width, 30) hAlignment:kCCTextAlignmentCenter fontName:gameFont fontSize:(IS_IPAD)?25:20];
    spr2.position  = bankLabel.position = ccp(WIDTH_DEVICE/3, HEIGHT_DEVICE/2);
    [self addChild:bankLabel z:8 tag:kBankMoney];
    [self addChild:spr2 z:7 tag:kUmbra2];
    
    CCSprite *spr3 = [CCSprite spriteWithFile:@"cell.png"];
    CCLabelTTF *scoreLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%@: %d",NSLocalizedString(@"score", nil), score] dimensions:CGSizeMake(sprAlertBG.contentSize.width, 30) hAlignment:kCCTextAlignmentCenter fontName:gameFont fontSize:(IS_IPAD)?25:20];
    spr3.position  = scoreLabel.position = ccp(WIDTH_DEVICE/3, HEIGHT_DEVICE/2-40);
    [self addChild:scoreLabel z:8 tag:kScore];
    [self addChild:spr3 z:7 tag:kUmbra3];
    
    CCSprite *spr4 = [CCSprite spriteWithFile:@"cell.png"];
    int minutes = floor(time/60);
    int seconds = round(time - minutes * 60);
    
    CCLabelTTF *timeLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%@: %02d:%02d", NSLocalizedString(@"time", nil),minutes,seconds] dimensions:CGSizeMake(sprAlertBG.contentSize.width, 30) hAlignment:kCCTextAlignmentCenter fontName:gameFont fontSize:(IS_IPAD)?25:20];
    spr4.position = timeLabel.position = ccp(WIDTH_DEVICE/3,  HEIGHT_DEVICE/2-80);
    [self addChild:timeLabel z:8 tag:kTime];
    [self addChild:spr4 z:7 tag:kUmbra4];
    
    
//    [self performSelector:@selector(removeState) withObject:Nil afterDelay:20.0];
    
    
}

- (void)removeState{
    
    [self removeChildByTag:kInformationBG cleanup:YES];
    [self removeChildByTag:kInformation cleanup:YES];
    [self removeChildByTag:kInstruction cleanup:YES];
    [self removeChildByTag:kTime cleanup:YES];
    [self removeChildByTag:kBankMoney cleanup:YES];
    [self removeChildByTag:kScore cleanup:YES];
    [self removeChildByTag:kUmbra cleanup:YES];
    [self removeChildByTag:kUmbra1 cleanup:YES];
    [self removeChildByTag:kUmbra2 cleanup:YES];
    [self removeChildByTag:kUmbra3 cleanup:YES];
    [self removeChildByTag:kUmbra4 cleanup:YES];
}


- (void)dealloc {
    
    
	[super dealloc];
}

- (void)initPlatforms {
	currentPlatformTag = kPlatformsStartTag;
	while(currentPlatformTag < kPlatformsStartTag + kNumPlatforms) {
		[self initPlatform];
		currentPlatformTag++;
	}
	[self resetPlatforms];
}

- (void)initCoins {
    currentCoinTag = kCoinBonusTag;
	while(currentCoinTag < kCoinBonusTag + kOrdCoin) {
        CCSprite *coin = [CCSprite spriteWithFile:[NSString stringWithFormat:@"%@19@2x.png",MODEL]];
        coin.scale = coin.scale*(0.5+0.5*IS_RETINA);
        [self addChild:coin z:4 tag:currentCoinTag];
		currentCoinTag++;
	}
}

- (void)initPlatform {
    CCSprite *platform;
    int k = random()%3;
	switch(k) {
		case 0: {platform = [CCSprite spriteWithFile:[NSString stringWithFormat:@"%@1@2x.png",MODEL]];} break;
		case 1: {platform = [CCSprite spriteWithFile:[NSString stringWithFormat:@"%@2@2x.png",MODEL]];} break;
        case 2: {platform = [CCSprite spriteWithFile:[NSString stringWithFormat:@"%@3@2x.png",MODEL]];} break;
	}
    platform.scale = platform.scale*(0.5+0.5*IS_RETINA);
    platform.atlasIndex = zPlatform;
	[self addChild:platform z:3 tag:currentPlatformTag];
}

#pragma mark - START

- (void)startGame {
    
    xGlance=yGlance = WIDTH_DEVICE/2;
    isFinish = NO;
    forNonIpad = NO;
    gameOver = NO;
    [[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"music_Background_jump.mp3"];
    [self removeChildByTag:kBackgroundFirst cleanup:YES];
    [self removeChildByTag:kBackgroundFirst+1 cleanup:YES];
    CCSprite *background, *background1 = nil;
    background1 = [CCSprite spriteWithFile:[NSString stringWithFormat:@"_%@2@2x.png", MODEL]];
    background1.position = CGPointMake(WIDTH_DEVICE/2.0, 1024+((1024-2)/2));
    
    background = [CCSprite spriteWithFile:[NSString stringWithFormat:@"_%@1@2x.png", MODEL]];
    background.position = CGPointMake(WIDTH_DEVICE/2.0,1024/2);
    
    background.scale = background.scale*(0.5+0.5*IS_RETINA);
    background1.scale = background1.scale*(0.5+0.5*IS_RETINA);
    
	[self addChild:background z:-2 tag:kBackgroundFirst];
    [self addChild:background1 z:-2 tag:kBackgroundFirst+1];

    id action1 =  [CCEaseIn actionWithAction:[CCScaleTo actionWithDuration:0.0 scaleX:[stateTamagochi fat] scaleY:[[personaj person] tamagochi].scaleY] rate:1.0];
    id action2=[CCScaleTo actionWithDuration:0.0 scale:0.25];
    [[personaj person] runAction:[CCSequence actions:action1, action2, nil]];
    

    currentBackgroundID = 2;
	score = 0;
    scoreCoin = 0;
	currentCloudTag = kCloudsStartTag;
	[self resetClouds];
    [self resetpoo];
	[self resetPlatforms];
	
	/*[self resetBonus];*/
	
	[[UIApplication sharedApplication] setIdleTimerDisabled:YES];
	gameSuspended = NO;
    if (![[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying]){
        [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"music_Background_jump.mp3" loop:YES];
    }
    
    if(timer == nil)
        timer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(updateTimerMain) userInfo:nil repeats:YES];
    time = 0;
   timerIsRunning = YES;
}


#pragma mark - RESET

- (void)resetPlatforms {
	currentPlatformY = -1;
	currentPlatformTag = kPlatformsStartTag;
	currentMaxPlatformStep = HEIGHT_DEVICE/4;
	currentBonusPlatformIndex = 0;
	currentBonusType = 0;
	platformCount = 0;
	while(currentPlatformTag < kPlatformsStartTag + kNumPlatforms) {
		[self resetPlatform];
		currentPlatformTag++;
	}
    score = 0;
    NSString *scoreStr = [NSString stringWithFormat:@"%@:    %d", NSLocalizedString(@"score", nil),score];
    CCLabelTTF *scoreLabel = (CCLabelTTF*)[self getChildByTag:kScoreLabel];
    [scoreLabel setString:scoreStr];
}

- (void)resetPlatform {
	if(currentPlatformY < 0) {
		currentPlatformY = 30.0f;
	} else {
        currentMaxPlatformStep = HEIGHT_DEVICE/(4-(score*0.003));
		currentPlatformY += arc4random() % (int)(currentMaxPlatformStep - kMinPlatformStep) + kMinPlatformStep;
	}
    
    [self addPointToScore];
    
	CCSprite *platform = (CCSprite*)[self getChildByTag:currentPlatformTag];
    platform = [self changePlatformImage:platform withTag:currentPlatformTag];
	if(random()%2==0) {
        platform.scaleX = -(0.5+0.5*IS_RETINA);;
    }else{
        platform.scaleX = (0.5+0.5*IS_RETINA);;
    }
	float x;
	CGSize size = platform.contentSize;
	if(currentPlatformY == 30.0f) {
		x = WIDTH_DEVICE/2;
	} else {
		x = random() % ((int)WIDTH_DEVICE -(int)size.width) + size.width/2;
	}
    //	x = WIDTH_DEVICE/2;
	platform.position = ccp(x,currentPlatformY);
    platform.visible = YES;
	platformCount++;
    
	if(platformCount == currentBonusPlatformIndex) {
        //            NSLog(@"created");
        //		CCSprite *bonus = (CCSprite*)[self getChildByTag:kBonusStartTag+currentBonusType];
        //        indexPlatformForBonus = platform.tag;
        //		bonus.position = ccp(x,currentPlatformY+(bonus.contentSize.height*2));
        //		bonus.visible = YES;
	}else{
        if (currentPlatformTag % SUBMULTIPLE_COIN == 0) {
            CCSprite *coin = (CCSprite*)[self getChildByTag:currentPlatformTag+300];
            coin.position = ccp(x, platform.contentSize.width/2 + coin.position.y+20);
            coin.visible = YES;
        }
    }
    
}


- (void)resetpoo {
    currentBackgroundID = 2;
	CCSprite *poo = (CCSprite*)[self getChildByTag:kPoo];
	
	poo_pos.x = WIDTH_DEVICE/2;
	poo_pos.y = WIDTH_DEVICE/2;
	poo.position = poo_pos;
	
	poo_vel.x = 0;
	poo_vel.y = 0;
	
	poo_acc.x = 0;
	poo_acc.y = -1000.0f;
	
	pooLookingRight = YES;
}
/*
- (void)resetBonus {
    
	CCSprite *bonus = (CCSprite*)[self getChildByTag:kBonusStartTag+currentBonusType];
	bonus.visible = NO;
	currentBonusPlatformIndex += random() % (kMaxBonusStep - kMinBonusStep) + kMinBonusStep;

    currentBonusType = random() % 4;
    
}
*/

#pragma mark - SCORE



- (void)addPointToScore{
    score += 1;
    NSString *scoreStr = [NSString stringWithFormat:@"%@:    %d", NSLocalizedString(@"score", nil),score];
    CCLabelTTF *scoreLabel = (CCLabelTTF*)[self getChildByTag:kScoreLabel];
    [scoreLabel setString:scoreStr];
}

- (void)addPointToCoinBank:(CCSprite*)coin{
    [coin runAction:[CCMoveTo actionWithDuration:.3 position:ccp(5.0,HEIGHT_DEVICE-5)]];
    scoreCoin +=1;
    NSString *scoreStr = [NSString stringWithFormat:@"%d",scoreCoin];
    CCLabelTTF *scoreCoinLabel = (CCLabelTTF*)[self getChildByTag:kScoreCoinLabel];
    [scoreCoinLabel setString:scoreStr];
    id a1 = [CCScaleTo actionWithDuration:0.2f scale:1.5];
    id a2 = [CCScaleTo actionWithDuration:0.2f scale:0.8];
    id a3 = [CCSequence actions:a1,a2,a1,a2,a1,a2,nil];
    [scoreCoinLabel runAction:a3];
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW,
                                            0.3f * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void)
                   {
                       coin.visible = NO;
                   });
}


#pragma mark - UPDATE STEP

- (void)step:(ccTime)dt {
    if (!isPause) {
	[super step:dt];
	if(gameSuspended) return;
    
	CCSprite *poo = (CCSprite*)[self getChildByTag:kPoo];
    //    CCSprite *bonus = (CCSprite*)[self getChildByTag:kBonusStartTag+currentBonusType];
    
	poo_pos.x += poo_vel.x * dt;
    
    //nu +/-  doarece poo e de marimea, scale*0.25
	CGSize poo_size = poo.contentSize;
	float max_x = WIDTH_DEVICE;//-poo_size.width/2;
	float min_x = 0;//+poo_size.width/2;
	
	if(poo_pos.x>max_x) poo_pos.x = max_x;
	if(poo_pos.x<min_x) poo_pos.x = min_x;
	
	poo_vel.y += poo_acc.y * dt;
	poo_pos.y += poo_vel.y * dt;
    
    //    [self ebashimBonus:bonus InCollisionWith:poo];
    
    [self coinInCollisionWithPoo];
	
	if(poo_vel.y < 0) {
		[self colisonWithPlatfomMAX:max_x MIN:min_x];
		if(poo_pos.y < -poo_size.height/2) {
            if (scoreCoin >99) {
                scoreCoin = scoreCoin - 100;
                NSString *scoreStr = [NSString stringWithFormat:@"%d",scoreCoin];
                CCLabelTTF *scoreCoinLabel = (CCLabelTTF*)[self getChildByTag:kScoreCoinLabel];
                [scoreCoinLabel setString:scoreStr];
                [self jump];
            }else if(score == 0){
                [self jump];
            }else{
                [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
                gameOver = YES;
                gameSuspended = YES;
               
                [self addButtonsOnView];
                [self addStateWithText:NSLocalizedString(@"game_over", nil) AndInstruction:NSLocalizedString(@"be_carefully", nil)];
                [[SimpleAudioEngine sharedEngine] playEffect:@"broken_poo.mp3" loop:NO];
            }
        }
	} else if(poo_pos.y > HEIGHT_DEVICE/2) {
		float delta = ((IS_IPAD)?(poo_pos.y - HEIGHT_DEVICE/2+HEIGHT_DEVICE/4):(poo_pos.y - HEIGHT_DEVICE/2));
        [self finishAndDealocatePlatforms:delta];
        currentPlatformY -= delta;
        //Aici trebuie de pus Bonus  arata asa [self movePlatformCoinAndBonus:bonus WithY:delta];
        [self movePlatformCoinAndBonusWithY:delta];
    }
    // mishcarea platformelor pe axa x
    [self changePlatformXPosition];
    //trecerea lui Poo prin perete
    if (currentBackgroundID > 1) {
        if (poo_pos.x < 1) {
            poo_pos.x = WIDTH_DEVICE-1;
        }else if (poo_pos.x > WIDTH_DEVICE-1){
            poo_pos.x = 1;
        }
    }
	poo.position = poo_pos;
    }
}


#pragma mark - PLATFORM

- (CCSprite*)changePlatformImage :(CCSprite*)platformSprite  withTag:(NSInteger)tag{
    int k =1;
    int atlasId=zPlatform;
    if (currentBackgroundID >=4) {
        if (currentBackgroundID <5 ) {
            if (random()%2==1) {
                k=5;
            }
            else{
                k=4;
            }
            atlasId = zTerain;
        }else if (currentBackgroundID >= 5 && currentBackgroundID <6){
            int result = rand() % 3 ;
            if (!result) {
                k=6;
                atlasId=zTerain;
            } else {
                
                atlasId= zCloud;
                switch (random()%3) {
                    case 0:
                        k=6;
                        break;
                    case 1:
                        k=7;
                        break;
                    case 2:
                        k=8;
                        break;
                }
            }
            
        }else if (currentBackgroundID >= 6 && currentBackgroundID <7){
            int result = rand() % 12 ;
            if (!result) {
                k=9;
                atlasId=zParashute;
            } else {
                switch (random()%6) {
                        atlasId=zCloud;
                    case 0:
                        k=6;
                        break;
                    case 1:
                        k=7;
                        break;
                    case 2:
                        k=8;
                        break;
                    case 3:
                        k=7;
                        break;
                    case 4:
                        k=8;
                        break;
                    default:
                    {
                        atlasId=zAirplane;
                        switch (random()%3) {
                            case 0:
                                k=10;
                                break;
                            case 1:
                                k=11;
                                break;
                            case 2:
                                k=12;
                                break;
                        }
                    }
                        break;
                }
                
            }
            
        }else if (currentBackgroundID >= 7 && currentBackgroundID <8){
            int result = rand() % 8;
            if (!result) {
                atlasId=zAirplane;
                switch (random()%3) {
                    case 0:
                        k=10;
                        break;
                    case 1:
                        k=11;
                        break;
                    case 2:
                        k=12;
                        break;
                }
            } else {
                atlasId=zCloud;
                switch (random()%3) {
                    case 0:
                        k=6;
                        break;
                    case 1:
                        k=7;
                        break;
                    case 2:
                        k=8;
                        break;
                }
            }
        }else if (currentBackgroundID >= 8 && currentBackgroundID <9){
            int result = rand() % 10;
            if (!result) {
                atlasId = zRacket;
                k=13;
            }else{
                atlasId = zAsteroid;
                switch (random()%3) {
                    case 0:
                        k=16;
                        break;
                    case 1:
                        k=14;
                        break;
                    case 2:
                        k=15;
                        break;
                }
            }
        }else{
            int result = rand() % 6;
            if (!result) {
                atlasId = zSputnic;
                switch (random()%2) {
                    case 0:
                        k=17;
                        break;
                    case 1:
                        k=18;
                        break;
                }
            }else{
                atlasId = zAsteroid;
                switch (random()%3) {
                    case 0:
                        k=16;
                        break;
                    case 1:
                        k=14;
                        break;
                    case 2:
                        k=15;
                        break;
                }
            }
        }
        
      
    }
    CCSprite *spritePlatform = [CCSprite spriteWithFile:[NSString stringWithFormat:@"%@%d@2x.png",MODEL,k]];
    spritePlatform.position = platformSprite.position;
    spritePlatform.scale = spritePlatform.scale*(0.5+0.5*IS_RETINA);
    spritePlatform.atlasIndex = atlasId;
    [self removeChild:platformSprite cleanup:YES];
    [self addChild:spritePlatform z:3 tag:tag];
    
    return spritePlatform;
   // return platformSprite;
}

- (void)platformInvisible:(CCSprite*)platform{
    platform.visible = NO;
}

- (void)changePlatformXPosition{
    for(int k= kPlatformsStartTag; k < kPlatformsStartTag + kNumPlatforms; k++) {
        
        CCSprite *platform = (CCSprite*)[self getChildByTag:k];
        CCSprite *coin = (CCSprite*)[self getChildByTag:k+300];
        //        CCSprite *bonus = (CCSprite*)[self getChildByTag:kBonusStartTag+currentBonusType];
        CGPoint posPlatform = platform.position;
        if (platform.atlasIndex ==zTerain) {
            posPlatform.y +=0.0005*k;
        }else if (platform.atlasIndex == zCloud){
            posPlatform.x += 0.001f * k;
        }else if (platform.atlasIndex == zParashute){
            if(posPlatform.x > (WIDTH_DEVICE-platform.contentSize.width/2 - 60)) {
                platform.scaleX = (0.5+0.5*IS_RETINA);
            }
            if (posPlatform.x < platform.contentSize.width/2+60) {
                platform.scaleX = -(0.5+0.5*IS_RETINA);;
            }
            if (platform.scaleX < 0) {
                posPlatform.x += 0.002f * k;
            }else {
                posPlatform.x -= 0.002f * k;
            }
            posPlatform.y -=0.0009*k;
            
        }else if(platform.atlasIndex ==zAirplane){
            if(posPlatform.x > (WIDTH_DEVICE-platform.contentSize.width/2)) {
                platform.scaleX = (0.5+0.5*IS_RETINA);;
            }
            if (posPlatform.x < platform.contentSize.width/2) {
                platform.scaleX = -(0.5+0.5*IS_RETINA);;
            }
            if (platform.scaleX < 0) {
                posPlatform.x += 0.004f * k;
            }else {
                posPlatform.x -= 0.004f * k;
            }
            
        }else if (platform.atlasIndex == zAsteroid){
            if (platform.scaleX < 0) {
                posPlatform.x += 0.005f * k;
            }else {
                posPlatform.x -= 0.005f * k;
            }
            
        }else if(platform.atlasIndex ==zRacket){
            
            if(posPlatform.x > (WIDTH_DEVICE-platform.contentSize.width/2)) {
                platform.scaleX = (0.5+0.5*IS_RETINA);;
            }
            if (posPlatform.x < platform.contentSize.width/2) {
                platform.scaleX = -(0.5+0.5*IS_RETINA);;
            }
            if (platform.scaleX < 0) {
                posPlatform.x += 0.0005f * k;
            }else {
                posPlatform.x -= 0.0005f * k;
            }
        }else if (platform.atlasIndex == zSputnic){
            if (platform.scaleX < 0) {
                posPlatform.x += 0.003f * k;
            }else {
                posPlatform.x -= 0.003f * k;
            }
        }
        
        
        if(posPlatform.x > WIDTH_DEVICE) {
            posPlatform.x = 0;
        }
        if (posPlatform.x < 0) {
            posPlatform.x = WIDTH_DEVICE;
        }
        
        //        if (platform.tag ==indexPlatformForBonus) {
        //            bonus.position = ccp(posPlatform.x, posPlatform.y+bonus.contentSize.height*2);
        //        }
        platform.position = posPlatform;
        
        if (coin.tag%SUBMULTIPLE_COIN==0) {//
            coin.position = ccp(posPlatform.x, posPlatform.y+coin.contentSize.height*1.6);
        }
        
    }
}


- (void)movePlatformCoinAndBonusWithY:(float)delta{
    for(int t= kPlatformsStartTag; t < kPlatformsStartTag + kNumPlatforms; t++) {
        CCSprite *platform = (CCSprite*)[self getChildByTag:t];
        CGPoint pos = platform.position;
        pos = ccp(pos.x,pos.y - delta);
        
        if(pos.y < - platform.contentSize.height/2) {
            currentPlatformTag = t;
            [self resetPlatform];
        } else {
            platform.position = pos;
        }
    }
    
    for(int t= kCoinBonusTag; t < kCoinBonusTag + kOrdCoin; t++) {
        CCSprite *coin = (CCSprite*)[self getChildByTag:t];
        CGPoint pos = coin.position;
        pos = ccp(pos.x,pos.y - delta);
        
        if(pos.y < - coin.contentSize.height/2) {
            currentCoinTag = t;
        } else {
            coin.position = pos;
        }
    }
    
    /* if(bonus.visible) {
     CGPoint pos = bonus.position;
     pos.y -= delta;
     if(pos.y < -bonus.contentSize.height/2) {
     [self resetBonus];
     } else {
     bonus.position = pos;
     }
     }*/
}


#pragma mark - BACKGROUND

- (void)moveBackgroundWith:(float)delta{
    CCSprite *background = (CCSprite *)[self getChildByTag:kBackgroundFirst];
    CCSprite *background1 = (CCSprite *)[self getChildByTag:kBackgroundFirst+1];
    
    CGPoint positionBackgr = background.position;
    CGPoint positionBackgr1 = background1.position;
    
    positionBackgr.y -= delta * COEF_MOVE_BACKGROUND * bonusAccelerateBackground;
    positionBackgr1.y -= delta * COEF_MOVE_BACKGROUND * bonusAccelerateBackground;
    if (positionBackgr.y < -512) {
        if (currentBackgroundID < 10) {
            currentBackgroundID++;
            CCTexture2D *texture = [[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"_%@%d@2x.png",MODEL,currentBackgroundID]];
            [background setTexture:texture];
            background.position = CGPointMake(WIDTH_DEVICE/2,1024+((1024-2)/2));
            background1.position = CGPointMake(WIDTH_DEVICE/2,1024/2);
        }else{
            if (IS_IPAD) {
                isFinish = YES;
            }else{
                forNonIpad = YES;
                CCTexture2D *texture = [[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"_%@%d@2x.png",MODEL,currentBackgroundID]];
                [background setTexture:texture];
                background.position = CGPointMake(WIDTH_DEVICE/2,1024+((1024-2)/2));
                background1.position = CGPointMake(WIDTH_DEVICE/2,1024/2);
            }
        }
    }else if (positionBackgr1.y < -512) {
        if (currentBackgroundID < 10) {
            currentBackgroundID++;
            CCTexture2D *texture = [[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"_%@%d@2x.png",MODEL,currentBackgroundID]];
            [background1 setTexture:texture];
            background1.position = CGPointMake(WIDTH_DEVICE/2,1024+((1024-2)/2));
            background.position = CGPointMake(WIDTH_DEVICE/2,1024/2);
        }else{
            if (IS_IPAD) {
                isFinish = YES;
            }else{
                forNonIpad = YES;
                CCTexture2D *texture = [[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"_%@%d@2x.png",MODEL,currentBackgroundID]];
                [background setTexture:texture];
                background.position = CGPointMake(WIDTH_DEVICE/2,1024+((1024-2)/2));
                background1.position = CGPointMake(WIDTH_DEVICE/2,1024/2);
            }
        }
    }else{
        if (positionBackgr.y > positionBackgr1.y) {
            background.position = positionBackgr;
            background1.position = positionBackgr1;
        }else{
            background1.position = positionBackgr1;
            background.position = positionBackgr;
        }
        if ( forNonIpad && (positionBackgr1.y < (HEIGHT_DEVICE-512))){
            isFinish = YES;
        }
    }
}



#pragma mark -  JUMP

- (void)jump {
    [[SimpleAudioEngine sharedEngine] playEffect:@"jump.mp3" loop:NO];
    [[SimpleAudioEngine sharedEngine] setEffectsVolume:0.5];
    bonusAccelerateBackground = 1.0;
	poo_vel.y = (IS_IPAD?HEIGHT_DEVICE+WIDTH_DEVICE/2:HEIGHT_DEVICE+WIDTH_DEVICE/2) ;//+ fabsf(poo_vel.x);
}

- (void)jumpBonus {
    [[SimpleAudioEngine sharedEngine] playEffect:@"long_jump.mp3" loop:NO];
    switch(currentBonusType) {
        case kBonus5: {
            poo_vel.y = 2*(HEIGHT_DEVICE-WIDTH_DEVICE/2)+ fabsf(poo_vel.x+1);
            bonusAccelerateBackground = 2;
        }
            break;
        case kBonus10: {
            poo_vel.y = 2.5*(HEIGHT_DEVICE-WIDTH_DEVICE/2)+ fabsf(poo_vel.x+1.5);
            bonusAccelerateBackground = 2.5;
        }
            break;
        case kBonus50:{
            poo_vel.y = 3*(HEIGHT_DEVICE-WIDTH_DEVICE/2)+ fabsf(poo_vel.x+1.75);
            bonusAccelerateBackground = 3;
        }
            break;
        case kBonus100: {
            poo_vel.y = 3.5*(HEIGHT_DEVICE-WIDTH_DEVICE/2)+ fabsf(poo_vel.x+2);
            bonusAccelerateBackground = 3.5;
        }
            break;
    }
}

- (void)accelerometer:(UIAccelerometer*)accelerometer didAccelerate:(UIAcceleration*)acceleration {
	if(gameSuspended) return;
	float accel_filter = 0.2f;
	poo_vel.x = poo_vel.x * accel_filter + acceleration.x * (1.0f - accel_filter) * (WIDTH_DEVICE * 3);
}


#pragma mark - COLLISIONS

- (void)coinInCollisionWithPoo{
    static int_fast16_t distancePooToCoin;
    distancePooToCoin = WIDTH_DEVICE;
    for(int t= kCoinBonusTag; t < kCoinBonusTag + kOrdCoin; t++) {
        CCSprite *coin = (CCSprite*)[self getChildByTag:t];
        float range = 20.0f;
        if (coin.visible) {
            if(poo_pos.x > coin.position.x - range && poo_pos.x < coin.position.x + range && poo_pos.y > coin.position.y - range && poo_pos.y < coin.position.y + range) {
                coin.visible=NO;
                CCSprite *coinMove = (CCSprite*)[self getChildByTag:kCoinForMove];
                coinMove.visible =YES;
                coinMove.position = coin.position;
                [self addPointToCoinBank:coinMove];
                static unsigned char i = 0;
                i++;
                [[SimpleAudioEngine sharedEngine] playEffect:[NSString stringWithFormat:@"marimba%d.mp3",i%8] loop:NO];
            }
            if (coin.position.x > poo_pos.x && ((coin.position.x - poo_pos.x) < distancePooToCoin)) {
                distancePooToCoin = coin.position.x - poo_pos.x;
                xGlance = coin.position.x;
                yGlance = coin.position.y;
            }
        }
    }
}

- (void)colisonWithPlatfomMAX:(float)max_x MIN:(float)min_x{
    for(int t= kPlatformsStartTag; t < kPlatformsStartTag + kNumPlatforms; t++) {
        CCSprite *platform = (CCSprite*)[self getChildByTag:t];
        
        CGSize platform_size = platform.contentSize;
        CGPoint platform_pos = platform.position;

        int_fast16_t min_y = 0;
        int_fast16_t max_y = 0;
        if (platform_pos.y < HEIGHT_DEVICE/4) {
            max_x = platform_pos.x - platform_size.width/2-20;
            min_x = platform_pos.x + platform_size.width/2+20;
            min_y = platform_pos.y + platform_size.height/2 - 16;
            max_y = platform_pos.y + platform_size.height/2 + 16;
        }else{
            max_x = platform_pos.x - platform_size.width/2-5;
            min_x = platform_pos.x + platform_size.width/2+5;
            min_y = platform_pos.y + platform_size.height/2 + 6;
            max_y = platform_pos.y + platform_size.height/2 + 16;
        }

        if(poo_pos.x > max_x &&
           poo_pos.x < min_x &&
           poo_pos.y < max_y&&
           poo_pos.y > min_y && platform.visible) {
            [self jump];
            if ((platform.atlasIndex == zCloud || platform.atlasIndex ==zAsteroid) && platform.tag%3==0) {
                
                id action1 = [CCFadeOut actionWithDuration:1.0f];
                id action2 = [CCCallFuncN actionWithTarget:self selector:@selector(platformInvisible:)];
                [platform runAction:[CCSequence actions:action1,action2,nil]];
            }
        }
    }
}

/*- (void)ebashimBonus:(CCSprite*)bonus InCollisionWith:(CCSprite*)poo{
 if(bonus.visible) {
 CGPoint bonus_pos = bonus.position;
 float range = 20.0f;
 if(poo_pos.x > bonus_pos.x - range && poo_pos.x < bonus_pos.x + range && poo_pos.y > bonus_pos.y - range && poo_pos.y < bonus_pos.y + range) {
 switch(currentBonusType) {
 case kBonus5:{  score += 50;}  break;
 case kBonus10:{ score += 70;} break;
 case kBonus50:{ score += 100; } break;
 case kBonus100:{ score += 200; } break;
 }
 
 [poo runAction:[CCRepeat actionWithAction:[CCRotateBy actionWithDuration:1.0 angle:(poo.scaleX<0)?360:-360] times:1]];
 
 [self jumpBonus];
 
 NSString *scoreStr = [NSString stringWithFormat:@"%d",score];
 CCLabelBMFont *scoreLabel = (CCLabelBMFont*)[self getChildByTag:kScoreLabel];
 [scoreLabel setString:scoreStr];
 id a1 = [CCScaleTo actionWithDuration:0.2f scaleX:1.5f scaleY:0.8f];
 id a2 = [CCScaleTo actionWithDuration:0.2f scaleX:1.0f scaleY:1.0f];
 id a3 = [CCSequence actions:a1,a2,a1,a2,a1,a2,nil];
 [scoreLabel runAction:a3];
 [self resetBonus];
 }
 }
 }*/


#pragma mark - Pou
-(CGPoint)returnPosition
{
    return CGPointMake(xGlance, yGlance);
}

-(float)returnTouchValue
{
    return 0.3;
}



#pragma mark - FINISH
- (void)winner{
    //TODO: AICI HIGHSCORE
    /*
    CCMenuItemLabel *winnerItem = [CCMenuItemFont itemWithString:NSLocalizedString(@"winner", nil) block:^(id sender){
        
  [[NSNotificationCenter defaultCenter]postNotificationName:@"closeGame" object:nil];
//        [[CCDirector sharedDirector] replaceScene:
//         [CCTransitionFade transitionWithDuration:1 scene:[Highscores sceneWithScore:score withIdGame:0] withColor:ccWHITE]];
    }];
     */
    [self addStateWithText:NSLocalizedString(@"winner", nil) AndInstruction:NSLocalizedString(@"play_new_record", nil)];
    /*
    CCMenu *menu = [CCMenu menuWithItems:winnerItem, nil];
    [menu alignItemsVertically];
    [menu setPosition:ccp(WIDTH_DEVICE/2, HEIGHT_DEVICE/2)];
    
    [self addChild: menu z:5];
     */
    [self addButtonsOnView];
}

- (void)finishAndDealocatePlatforms:(float)delta{
    if (isFinish) {
        gameSuspended = YES;
        while(currentPlatformTag < kPlatformsStartTag + kNumPlatforms) {
            CCSprite *spritePlatform = (CCSprite *)[self getChildByTag:currentPlatformTag];
            CCSprite *spriteCoin = (CCSprite *)[self getChildByTag:currentPlatformTag+300];
            [spritePlatform setVisible:NO];
            spriteCoin.visible = NO;
            [self removeChild:spritePlatform cleanup:YES];
            currentPlatformTag++;
        }

        id action = [CCMoveTo actionWithDuration:3.0 position:CGPointMake(WIDTH_DEVICE/2, HEIGHT_DEVICE-WIDTH_DEVICE/4)];
        id action0 =  [CCEaseInOut actionWithAction:[CCScaleTo actionWithDuration:1 scaleX:0.0 scaleY:0.0] rate:1.0];
        id action1 = [CCCallFuncN actionWithTarget:self selector:@selector(platformInvisible:)];
        id action2 = [CCCallFuncN actionWithTarget:self selector:@selector(winner)];
        [[personaj person] runAction:[CCSequence actions:action,action0,action1,action2,nil]];
        gameSuspended = YES;
        //    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW,
        //                                            0.6f * NSEC_PER_SEC);
        //    dispatch_after(popTime, dispatch_get_main_queue(), ^(void)
        //                   {
        //                      // [[personaj person] removeFromParentAndCleanup:NO];
        //                     //  [self removeChildByTag:kPoo cleanup:NO];
        //                       [self showHighscores];
        //                   });
    }else{
        poo_pos.y = HEIGHT_DEVICE/2;
        [self moveBackgroundWith:delta];
    }
}

-(void)alertShow:(NSString *)title message:(NSString *)message
{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    [alert release];
}
- (void)showHighscores {
    [[personaj person] addMoney:scoreCoin];
    NSString *strTime = [NSString stringWithFormat:@"%.02f",time];
    if ([stateTamagochi updateGameWithStageId:@(0) withTime:strTime withScore:@(score) withCoins:@(scoreCoin) withPosition:@(1)])
        [self alertShow:NSLocalizedString(@"new_highscore", nil) message:@""];
    [[personaj person] removeFromParentAndCleanup:NO];
	gameSuspended = YES;
	[[UIApplication sharedApplication] setIdleTimerDisabled:NO];
	// TODO: SI AICI HIGHSCORE !!!!
//	[[CCDirector sharedDirector] replaceScene:
//     [CCTransitionFade transitionWithDuration: 1.f scene:[Highscores sceneWithScore:score withIdGame:0] withColor:ccWHITE]];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"closeGame" object:nil];
//    [self loadMyViewController];
}


//- (void)loadMyViewController {
//    myView = [[MainViewController alloc] init];
//    AppController *app = (AppController *)[[UIApplication sharedApplication] delegate];
//    [app.navController pushViewController:myView animated:YES];
//    [CCDirector sharedDirector].pause;
//}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
//	NSLog(@"alertView:clickedButtonAtIndex: %i",buttonIndex);

	if(buttonIndex == 0) {
        [self removeButtons];
		[self startGame];
	}
}

#pragma mark - Touches
- (void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    NSLog(@"este touch");
    //    [self createTestBodyAtPostition: [_terrain convertTouchToNodeSpace:[touches anyObject]]];
    CGPoint location = [self convertTouchToNodeSpace:[touches anyObject]];
    CGPoint posPause = _pauseButton.position;
	CGSize size = _pauseButton.contentSize;
	float padding = 8;
    
	CGRect rect = CGRectMake(posPause.x-size.height, posPause.y-size.width,size.height+ 2*padding, size.width+2*padding);
    
	if (CGRectContainsPoint(rect, location)) {
        
		[self addButtonsOnView];
	} else {
        
	}
    //    emiter.totalParticles = 10;
}

- (void)updateTimerMain{
    if (timerIsRunning) {
        time = time+0.01;
    }

}

@end
