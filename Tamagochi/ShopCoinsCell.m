//
//  ShopCoinsCell.m
//  Tamagochi
//
//  Created by Sergiu Moțipan on 11/28/13.
//
//

#import "ShopCoinsCell.h"

@implementation ShopCoinsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_coins release];
    [_sale release];
    [_name_btn release];
    [_price release];
    [_btnShopCoins release];
    [_loading release];
    [super dealloc];
}
@end
