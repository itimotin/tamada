#ifdef GL_ES
precision mediump float;
#endif

varying vec2 v_texCoord;
varying  vec4 v_colorVarying;
uniform sampler2D u_texture0;

void main()
{
    float ratio=0.5;

    vec4 normalColor = texture2D(u_texture0, fract(vec2(v_texCoord.x , v_texCoord.y))).rgba;
    gl_FragColor = normalColor;
    
}