//
//  CustomCell.m
//  Tamagochi
//
//  Created by Sergiu Moțipan on 9/2/13.
//
//

#import "CustomCell.h"

@implementation CustomCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_gameImage release];
    [_nameGame release];
    [_btnGame release];
    [super dealloc];
}
@end
