//
//  RageIAPHelper.h
//  Tamagochi
//
//  Created by Sergiu Moțipan on 3/7/14.
//
//

#import "IAPHelper.h"

@interface RageIAPHelper : IAPHelper

+ (RageIAPHelper *)sharedInstance;

@end
