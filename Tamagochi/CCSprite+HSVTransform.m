#import "CCSprite+HSVTransform.h"
#import "CCTexture2D+HSVTransform.h"

@implementation CCSprite (CCSprite_HSVTransform)

+(id) spriteWithFile:(NSString*)filename transform:(HSVTransform*) transform
{
    return [[[self alloc] initWithFile:filename transform:transform] autorelease];
}

-(id) initWithFile:(NSString*)filename transform:(HSVTransform*) transform
{
    NSAssert(filename!=nil, @"Invalid filename for sprite");
    
    NSString *fullpath = [CCFileUtils fullPathFromRelativePath:filename ];
    
    UIImage *image = [ [UIImage alloc] initWithContentsOfFile: fullpath ];
    CCTexture2D* texture = [[CCTexture2D alloc] initWithImage:image transform:transform];
    [image release];
    
    if( texture ) {
        CGRect rect = CGRectZero;
        rect.size = texture.contentSize;
        return [self initWithTexture:texture rect:rect];
    }
    
    [self release];
    return nil;
}

@end