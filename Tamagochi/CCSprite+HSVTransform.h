//
//  CCSprite+CCSprite_HSVTransform.h
//  Tamagochi
//
//  Created by Sergiu Moțipan on 11/8/13.
//
//

#import "CCSprite.h"
#import "HSVTransform.h"

@interface CCSprite (HSVTransform)

+(id) spriteWithFile:(NSString*)filename transform:(HSVTransform*) transform;
-(id) initWithFile:(NSString*)filename transform:(HSVTransform*) transform;
@end
