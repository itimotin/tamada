//
//  SelectColors.h
//  Tamagochi
//
//  Created by Sergiu Moțipan on 10/24/13.
//
//

#import <cocos2d.h>
#import "CCLayer.h"

@class SelectColorsView;

@protocol SelectColorViewDelegate <NSObject>
@required
-(NSMutableArray *)returnCountData;
-(NSMutableArray *)returnPolki;
-(int)returnColor;
-(void)setSelectedColor:(NSInteger)color;
-(void)getShopping:(NSInteger)index;
@end
@interface SelectColorsView : CCLayer
{
   
    CCLayerColor *colorLayer;
    CCSprite *backColors;
    CCSprite *buttons[10];
    int nr_colors;
    int indexTatoo;
    CGPoint startPos;
    int selectedObject;
    CGMutablePathRef path;
    CGPoint newPoint;
}

@property (nonatomic, assign) id<SelectColorViewDelegate> delegate;

-(void)createColors;
@end
