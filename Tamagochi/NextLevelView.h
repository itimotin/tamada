//
//  NextLevelView.h
//  Tamagochi
//
//  Created by Sergiu Moțipan on 12/26/13.
//
//

#import <UIKit/UIKit.h>

@interface NextLevelView : UIView <UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UILabel *title;
    
    IBOutlet UILabel *summ;
    IBOutlet UILabel *level;
    IBOutlet UITableView *tableOpened;
    
    IBOutlet UILabel *labOpen;
    IBOutlet UILabel *lab_share;
    
}
@property (retain, nonatomic) NSMutableArray *dataOpened;
- (IBAction)close:(id)sender;
-(void)reloadLabels;
- (IBAction)share:(id)sender;

@end
