//
//  FirstScene.m
//  Tamagochi
//
//  Created by Sergiu Moțipan on 6/4/13.
//
//

#import "ClosetRoom.h"
#import "BedRoom.h"
#import <InMallRoom.h>

@implementation ClosetRoom
{
    int touchObject;
    int timp;
    int timp_2;
    int mouse;
    int oche_mouse;
    int n_mouse;
    int eye_pos;
    float totalTime,yy,xx, tamTouch;
    BOOL repeatTouches;
    float limit;
    CCSprite *btn_back;
    
    CGRect rect_polk[12];
    CGPoint origin_sprite[12];
    
 
}

+(CCScene *) scene
{
	CCScene *scene = [CCScene node];
    scene.tag=10;
	ClosetRoom *layer = [ClosetRoom node];
	[scene addChild: layer];
	return scene;
}


-(CCAction*)initAnimations
{
    id action1 = [CCScaleBy actionWithDuration:0.2 scale:1.5];  // the action it sounds like you have written above.
    id action2 = [CCScaleTo actionWithDuration:0.2 scale:1.0];
    id animationObjects = [CCSequence actions:action1, action2, nil];
    return animationObjects;
}

-(void)initRects
{
    int k=0;
    if (!IS_IPAD)
    {
        
        int g=4;
        if (IS_WIDESCREEN) g=5;
        for (int i=g;i>=g-3;i--)
        {
            for (int j=0;j<=2;j++)
            {
                rect_polk[k]=CGRectMake(106*j, 82*i, 106, 80);
                k++;
            }
        }
    }
    else
    {
        for (int i=5;i>=2;i--)
        {
            for (int j=0;j<=2;j++)
            {
                rect_polk[k]=CGRectMake(256*j, 150*i, 256, 105);
                k++;
            }
        }

    }
}



-(CCSprite*)createSprite:(int)category name:(NSString *)name
{
    CCSprite *sprite;
     if ([name isEqualToString:[NSString stringWithFormat:@"add%@.png", (IS_IPAD)?@"-iPad":@""]])
     {
         sprite= [CCSprite spriteWithFile:name];
         sprite.scale=0.5+0.5*IS_RETINA;
         return sprite;
     }
    switch (category) {
        case 1:
        {
            sprite=[CCSprite spriteWithFile:[NSString stringWithFormat:@"maneken_1%@.png", (IS_IPAD)?@"-iPad":@""]];
            CCSprite *sprite1=[CCSprite spriteWithFile:@"contur_ochi_7.png"];
            if (!IS_IPAD)
            {
                sprite1.scale=0.6;
                [sprite1 setPosition:ccp(sprite.boundingBox.size.width/2, 30)];
            }
            else
            {
                sprite1.scale=0.9+0.1*IS_RETINA;
                [sprite1 setPosition:ccp(sprite.boundingBox.size.width/2-12+6*IS_RETINA, 85-35*IS_RETINA)];
            }
            
            CCSprite *sprite2=[CCSprite spriteWithFile:name];
            if (!IS_IPAD)
            {
                [sprite2 setPosition:ccp(sprite.boundingBox.size.width/2 , 45)];
                [sprite2 setScale:0.5+0.5*IS_RETINA];
            }
            else
                [sprite2 setPosition:ccp(sprite.boundingBox.size.width/2-52+25*IS_RETINA , 90-50*IS_RETINA)];
           
            [sprite setScale:0.5+0.5*IS_RETINA];
          
            [sprite1 addChild:sprite2];
            [sprite addChild:sprite1 z:-1];
            return sprite;
        }
            break;
            
        case 2:
        {
            sprite=[CCSprite spriteWithFile:[NSString stringWithFormat:@"maneken_1%@.png", (IS_IPAD)?@"-iPad":@""]];
            
            CCSprite *sprite1=[CCSprite spriteWithFile:name];
            //sprite1.scale=0.6;
           
            
            CCSprite *sprite3=[CCSprite spriteWithFile:@"contur_ochi_7.png"];
            CCSprite *sprite2=[CCSprite spriteWithFile:@"ochi_centru_7.png"];
            if (!IS_IPAD)
            {
                sprite3.scale=0.6;
                [sprite3 setPosition:ccp(sprite.boundingBox.size.width/2, 30)];
                [sprite2 setPosition:ccp(sprite.boundingBox.size.width/2 , 45)];
                [sprite2 setScale:0.5+0.5*IS_RETINA];
                [sprite1 setPosition:ccp(sprite.boundingBox.size.width/2, 75)];
            }
            else
            {
                sprite3.scale=0.9+0.1*IS_RETINA;
                [sprite3 setPosition:ccp(sprite.boundingBox.size.width/2-12+6*IS_RETINA, 85-35*IS_RETINA)];
                [sprite2 setPosition:ccp(sprite.boundingBox.size.width/2-52+25*IS_RETINA , 90-50*IS_RETINA)];
                [sprite1 setPosition:ccp(sprite.boundingBox.size.width/2-75+37*IS_RETINA, 140-70*IS_RETINA)];
            }
            
            [sprite setScale:0.5+0.5*IS_RETINA];
            [sprite3 addChild:sprite2];
             if (![name isEqualToString:@""])
            [sprite3 addChild:sprite1 z:-1];
            [sprite addChild:sprite3 z:-1];
            
            return sprite;

        }
            break;
        case 3:
        {
            sprite=[CCSprite spriteWithFile:[NSString stringWithFormat:@"maneken_1%@.png", (IS_IPAD)?@"-iPad":@""]];
            CCSprite *sprite1=[CCSprite spriteWithFile:name];
            if (!IS_IPAD)
            {
                sprite1.scale=0.6;
                [sprite1 setPosition:ccp(sprite.boundingBox.size.width/2, 30)];
            }
            else
            {
               sprite1.scale=0.9+0.1*IS_RETINA;
                [sprite1 setPosition:ccp(sprite.boundingBox.size.width/2-12+6*IS_RETINA, 85-35*IS_RETINA)];
            }
            
            CCSprite *sprite2=[CCSprite spriteWithFile:@"ochi_centru_7.png"];
            if (!IS_IPAD)
            {
                [sprite2 setPosition:ccp(sprite.boundingBox.size.width/2 , 45)];
                [sprite2 setScale:0.5+0.5*IS_RETINA];
            }
            else
            {
                [sprite2 setPosition:ccp(sprite.boundingBox.size.width/2-52+25*IS_RETINA , 90-50*IS_RETINA)];
            }
            
            [sprite setScale:0.5+0.5*IS_RETINA];
            
            [sprite1 addChild:sprite2];
            [sprite addChild:sprite1 z:-1];
            return sprite;
        }
            break;
        case 4:
        {
            sprite=[CCSprite spriteWithFile:[NSString stringWithFormat:@"maneken_2%@.png", (IS_IPAD)?@"-iPad":@""]];
             [sprite setScale:0.5+0.5*IS_RETINA];
            CCSprite *sprite1=[CCSprite spriteWithFile:name];
            if (!IS_IPAD)
            {
                sprite1.scale=0.45;
                [sprite1 setPosition:ccp(sprite.boundingBox.size.width/2, [sprite boundingBox].size.height/1.2-[sprite1 boundingBox].size.height/1.5)];
            }
            else
            {
                sprite1.scale=0.85+0.15*IS_RETINA;
                [sprite1 setPosition:ccp(sprite.boundingBox.size.width/2-5+70*!IS_RETINA, 75+50*!IS_RETINA)];
            }
             if (![name isEqualToString:@""])
            [sprite addChild:sprite1];
            return sprite;
        }
            break;
        case 5:
        {
            sprite=[CCSprite spriteWithFile:[NSString stringWithFormat:@"maneken_3%@.png", (IS_IPAD)?@"-iPad":@""]];
             
            CCSprite *sprite1=[CCSprite spriteWithFile:name];
            if (!IS_IPAD)
            {
                sprite1.scale=0.365;
               sprite1.position= CGPointMake(sprite.boundingBox.size.width/2+1, [sprite boundingBox].origin.y-[sprite1 boundingBox].origin.y+28);
            }
            else
            {
                sprite1.scale=0.67-0.01*IS_RETINA;
                [sprite1 setPosition:ccp(sprite.boundingBox.size.width/2-3-3*!IS_RETINA, [sprite boundingBox].origin.y-[sprite1 boundingBox].origin.y+100-49*IS_RETINA)];
            }
            
            [sprite setScale:0.5+0.5*IS_RETINA];
            if (![name isEqualToString:@""])
            [sprite addChild:sprite1];
            return sprite;
        }
            break;
        case 6:
        {
            sprite=[CCSprite spriteWithFile:[NSString stringWithFormat:@"%@%@.png", [[name componentsSeparatedByString:@"."] objectAtIndex:0], (IS_IPAD)?@"-iPad":@""]];
            [sprite setScale:0.22+0.21*IS_RETINA-0.05*!IS_IPAD];
            return sprite;
        }
            break;
        case 7:
        {
            sprite=[CCSprite spriteWithFile:[NSString stringWithFormat:@"maneken_5%@.png", (IS_IPAD)?@"-iPad":@""]];
             
            CCSprite *sprite1=[CCSprite spriteWithFile:name];
            sprite1.scale=0.8;
            if (!IS_IPAD) {
                 [sprite1 setPosition:ccp(sprite.boundingBox.size.width/2, 15)];
            }
            else
            {
                [sprite1 setPosition:ccp(sprite.boundingBox.size.width/2, 35)];
            }
           [sprite setScale:0.5+0.5*IS_RETINA];
             if (![name isEqualToString:@""])
            [sprite addChild:sprite1];
            return sprite;
        }
            break;
        case 8:
        {
            sprite=[CCSprite spriteWithFile:[NSString stringWithFormat:@"maneken_5%@.png", (IS_IPAD)?@"-iPad":@""]];
            
            CCSprite *sprite1=[CCSprite spriteWithFile:name];
            if (!IS_IPAD)
            {
                sprite1.scale=0.45;
                [sprite1 setPosition:ccp(sprite.boundingBox.size.width/2, 22)];
            }
            else
            {
                sprite1.scale=0.8;
                [sprite1 setPosition:ccp(sprite.boundingBox.size.width/2-3, 42+30*!IS_RETINA)];

            }
             [sprite setScale:0.5+0.5*IS_RETINA];
             if (![name isEqualToString:@""])
            [sprite addChild:sprite1];
            return sprite;
        }
            break;
        case 9:
        {
            sprite=[CCSprite spriteWithFile:[NSString stringWithFormat:@"maneken_5%@.png", (IS_IPAD)?@"-iPad":@""]];
             
            CCSprite *sprite1=[CCSprite spriteWithFile:[NSString stringWithFormat:@"%@%@.png", [[name componentsSeparatedByString:@"."] objectAtIndex:0], (IS_IPAD)?@"-iPad":@""]];
            if (!IS_IPAD)
            {
                sprite1.scale=0.6;
                if ([sprite1 textureRect].size.height>100)
               [sprite1 setTextureRect:CGRectMake([sprite1 textureRect].origin.x, [sprite1 textureRect].origin.y, [sprite1 textureRect].size.width, [sprite1 textureRect].size.height-55)];
                [sprite1 setPosition:ccp(sprite.boundingBox.size.width/2-3-10*IS_IPAD, [sprite boundingBox].size.height/2-[sprite1 boundingBox].size.height/2+25+80*IS_IPAD)];
            }
            else
            {
                sprite1.scale=0.3+0.05*IS_RETINA;
                [sprite1 setPosition:ccp(sprite.boundingBox.size.width/2-10*!IS_RETINA, 142-70*IS_RETINA)];
            }
          
            [sprite setScale:0.5+0.5*IS_RETINA];
             if (![name isEqualToString:@""])
            [sprite addChild:sprite1];
            
            return sprite;
        }
            break;
        case 10:
        {
            sprite=[CCSprite spriteWithFile:[NSString stringWithFormat:@"maneken_5%@.png", (IS_IPAD)?@"-iPad":@""]];
             
            CCSprite *sprite1=[CCSprite spriteWithFile:[NSString stringWithFormat:@"%@%@.png", [[name componentsSeparatedByString:@"."] objectAtIndex:0], (IS_IPAD)?@"-iPad":@""]];
           
            if (!IS_IPAD)
            {
                [sprite1 setPosition:ccp(sprite.boundingBox.size.width/2, 15)];
                if ([name isEqualToString:@"rot.png"]) {
                    sprite1.scale=0.4;
                }
            }
            else
            {
                [sprite1 setScale:0.4];
                  [sprite1 setPosition:ccp(sprite.boundingBox.size.width/2, 35-10*IS_RETINA)];
                 
            }
            [sprite setScale:0.5+0.5*IS_RETINA];
            [sprite addChild:sprite1];
            return sprite;
        }
            break;
        case 11:
        {
            sprite=[CCSprite spriteWithFile:[NSString stringWithFormat:@"maneken_5%@.png", (IS_IPAD)?@"-iPad":@""]];
             [sprite setScale:0.5+0.5*IS_RETINA];
            CCSprite *sprite1=[CCSprite spriteWithFile:name];
            if (!IS_IPAD)
            {
                sprite1.scale=0.35;
                [sprite1 setPosition:ccp(sprite.boundingBox.size.width/2-2, 35)];
            }
            else
            {
                sprite1.scale=0.7+0.15*IS_RETINA;
                [sprite1 setPosition:ccp(sprite.boundingBox.size.width/2+58-62*IS_RETINA, 55+80*!IS_RETINA)];
            }
             if (![name isEqualToString:@""])
            [sprite addChild:sprite1 z:-1];
            return sprite;
        }
            break;
        case 12:
        {
            sprite=[CCSprite spriteWithFile:[NSString stringWithFormat:@"maneken_3%@.png", (IS_IPAD)?@"-iPad":@""]];
             [sprite setScale:0.5+0.5*IS_RETINA];
            CCSprite *sprite1=[CCSprite spriteWithFile:name];
            [sprite1 setPosition:ccp(sprite.boundingBox.size.width/2-2, 20)];
             if (![name isEqualToString:@""])
            [sprite addChild:sprite1];
            return sprite;
        }
            break;
        default:
            break;
    }
}

-(void)creatClosed:(NSInteger)category
{
    int k=0;
    int g=4;
    int h=0;
    
    if (!IS_IPAD)
    {
        if (IS_WIDESCREEN) g=5;
        for (int i=g;i>=g-3;i--)
        {
            for (int j=1;j<=5;j+=2)
            {
                origin_sprite[k]=CGPointMake(53*j, 92*i+(h*15));
                k++;
            }
            h++;
        }
    }
    else
    {
        
        for (int i=4;i>=1;i--)
        {
            for (int j=1;j<=5;j+=2)
            {
                
                origin_sprite[k]=CGPointMake(130*j, 195*i+(h*55));
                k++;
            }
            h++;
        }

    }
    
    for (int i=0;i<12;i++)
    {
        [self removeChildByTag:i+100 cleanup:NO];
    }
   
    for (int i=0;i<category;i++)
    {
        
        CCSprite *spr1;
        if (touchPolk==0)
        {
        spr1=[self createSprite:i+1 name:[polki_sprites objectAtIndex:i]];
        }
        else
        {
            spr1=[self createSprite:touchPolk name:[polki_sprites objectAtIndex:i]];
        }
        [spr1 setPosition:origin_sprite[i]];
        [self addChild:spr1 z:2 tag:i+100];
    }

}

-(NSMutableArray *)arrayDefault
{
    NSMutableArray *arr=[[[NSMutableArray alloc] initWithObjects:@"ochi_centru_8.png", @"gene_01_5.png", @"contur_ochi_2.png", @"palarii_04_1.png", @"haine_03_9.png",@"corpul_10.png", @"mustet_06_6.png", @"ochelari_02_7.png", @"peruca_01_8.png",@"buze_01_8.png", @"urechi_02_10.png", @"tatoo_05_7.png",nil]  autorelease];
    return arr;
}

-(void)closetCategoryCreate
{
    touchPolk=[stateTamagochi bodyCategory];
    [polki_sprites removeAllObjects];
    [polki_sprites addObjectsFromArray:[stateTamagochi selectBody_Shop:[stateTamagochi bodyCategory]]];
    [self creatClosed:[polki_sprites count]];
    page++;

    
    
}

-(ccColor3B)tranformColor:(NSInteger)tag object:(NSInteger)object
{
    NSMutableArray *arr=[[[NSMutableArray alloc] initWithArray:[stateTamagochi colorSkin:tag object:object]] autorelease];
    ccColor3B color={[[arr objectAtIndex:0] floatValue], [[arr objectAtIndex:1] floatValue], [[arr objectAtIndex:2] floatValue]};
    return color;
}
-(id)init
{
    self=[super init];
    if (self)
    {
         [[SimpleAudioEngine sharedEngine] stopAllEffects];
        [[SimpleAudioEngine sharedEngine] setEffectsVolume:[stateTamagochi soundOn]];
        [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume:[stateTamagochi soundOn]];
        [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(closetCategoryCreate) name:@"closetCategory" object:nil];
        _dataShopBody = [[NSMutableArray alloc] init];
        touchPolk=0;
        page=0;
        polki_sprites=[[NSMutableArray alloc] init];
        [polki_sprites addObjectsFromArray:[self arrayDefault]];
        [stateTamagochi setRoom:closetRoom];
        mouth=[stateTamagochi mouthState];
        [[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"giggle.wav"];
        [[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"mtem.wav"];
         [[SimpleAudioEngine sharedEngine] preloadEffect:@"closet.mp3"];
        [[SimpleAudioEngine sharedEngine] setEffectsVolume:[stateTamagochi soundOn]];
        [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume:[stateTamagochi soundOn]];
        
        winSize = [[CCDirector sharedDirector] winSize];
        self.isTouchEnabled=YES;
        
        CCSprite *background = [CCSprite spriteWithFile:[NSString stringWithFormat:@"bg_closet%@.png",IMG_PREFIX]];
        [background setColor:[self tranformColor:[stateTamagochi selectColorSkin:7 room:5] object:0]];
        [background setScale:0.5+0.5*IS_RETINA];
        background.position = ccp(winSize.width/2, winSize.height/2);
        [self addChild:background z:-12];
        
        if (!IS_IPAD)
        {
            btn_back = [CCSprite spriteWithFile:@"btn_back_share.png"];
            btn_back.position = ccp(50, 50+90*IS_WIDESCREEN);
        }
        else
        {
            btn_back = [CCSprite spriteWithFile:@"btn_back_share-iPad.png"];
            btn_back.scale=0.5+0.5*IS_RETINA;
            btn_back.position = ccp(60,200);
        }
        [self addChild:btn_back z:4];
        
        [personaj person].position = ccp(winSize.width/2,20+(55-25*!IS_WIDESCREEN)*[stateTamagochi sizeTamagochi]-50*IS_IPAD);
        [personaj initRects];
        if (IS_WIDESCREEN)
        {
        if ([stateTamagochi sizeTamagochi]>0.8)
            [personaj setScale:0.8];
        }
        else
        {
            if ([stateTamagochi sizeTamagochi]>0.5)
                [personaj setScale:0.5];
        }
        [personaj person].delegate=self;
        [self addChild:[personaj person] z:3];
        [self initRects];
        [self creatClosed:12];
        [self scheduleUpdate];
    }
    
    return self;
}


- (void)update:(float)dt
{

}

-(CGPoint)returnPosition
{
    return CGPointMake(xx, yy);
}

-(float)returnTouchValue
{
    
    switch ([stateTamagochi mouthState]) {
        case mouthStateFun:
        {
            return tamTouch*sin(totalTime);
        }
            break;
        case mouthStateReadyEat:
        {
            return 0.3;
        }
            break;
        case mouthStateEat:
        {
            return 0.25*sin(totalTime);
        }
            break;
        case mouthStateDefault:
        {
            return 0.25*sin(totalTime);
        }
            break;
            
        default:return tamTouch;
            break;
    }
}


-(NSArray *)getArrayBody:(NSString *)str
{
    return [stateTamagochi selectBodyGroup:[str componentsSeparatedByString:@"_"]];
}



-(void)createSelectColors
{
    SelectColorsView *colors=[SelectColorsView node];
    [colors setDelegate:self];
    [colors createColors];
    [self addChild:colors z:2];
        
  }

-(NSMutableArray *)returnCountData
{
   
    return _dataShopBody;
}

-(NSMutableArray *)returnPolki
{
    return polki_sprites;
}
-(int)returnColor
{
    return selectedColor;
}
-(void)setSelectedColor:(NSInteger)color
{
    selectedColor=color;
}

-(void)getShopping:(NSInteger)index
{
    switch (index) {
        case 0:
        {
            [stateTamagochi setShopCategory:sh_body];
            
            [[personaj person] removeFromParentAndCleanup:NO];
            [[CCDirectorIOS sharedDirector] replaceScene:[InMallRoom scene]];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"showBackgroundAlpha" object:nil];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"shopView" object:@"15"];
        }
            break;
        case 1:
        {
            [stateTamagochi setShopCategory:sh_body];
           
            [[personaj person] removeFromParentAndCleanup:NO];
            [[CCDirectorIOS sharedDirector] replaceScene:[InMallRoom scene]];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"showBackgroundAlpha" object:nil];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"shopView" object:[[_dataShopBody objectAtIndex:0] objectAtIndex:1]];
        }
            break;
            
        default:
            break;
    }
}

-(void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    nr_sounds=0;
    UITouch* touch = [touches anyObject];
    CGPoint location = [touch locationInView: [touch view]];
    startTouchPoint=location;
    date_start=[[NSDate date] retain];
    location = [[CCDirector sharedDirector] convertToGL:location];
    
    xx=location.x;
    if (location.y>=90.0)
        yy=location.y;
    else
        yy=90.0;
    touchFun=0;
   
    if (CGRectContainsPoint([btn_back boundingBox],location))
    {
        butTouch=butBack;
    }
    if (CGRectContainsPoint([personaj rectPerson],location))
    {
        [stateTamagochi setTouchObject:tagTamagochi];
    }
    
    for (int i=0;i<12;i++)
    {
        if (CGRectContainsPoint(rect_polk[i],location))
        {
            objTouch=i;
        }
    }
}

-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch* touch = [touches anyObject];
    CGPoint location = [touch locationInView: [touch view]];
    location = [[CCDirector sharedDirector] convertToGL:location];
    
    if ([stateTamagochi touchObject]==tagTamagochi && CGRectContainsPoint([personaj rectPerson],location))
    {
        [stateTamagochi setMouthState:mouthStateFun];
        touchFun+=0.006;
        if (touchFun>=0.05)
        {
            if (!repeatTouches)
            [self fun];
        }
        nr_sounds++;
    }
  
    xx=location.x;
    if (location.y<=90.0)
    yy=90.0;
    else
        yy=location.y;
        
    
}
-(void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [stateTamagochi setMouthState:mouth];
    UITouch* touch = [touches anyObject];
    CGPoint location = [touch locationInView: [touch view]];
    
    endTouchPoint=location;
    float moveDifference=startTouchPoint.x-endTouchPoint.x;
    float differenceY=startTouchPoint.y-endTouchPoint.y;
    if (differenceY<0) differenceY*=-1;
    NSTimeInterval secondsBetween = [[NSDate date] timeIntervalSinceDate:date_start];
    [date_start release];
    date_start=nil;
    if (moveDifference>150.0 && differenceY<40.0 && endTouchPoint.y<150.0 && secondsBetween<0.3)
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"nextRoom" object:@"1"];
        return;
    }
    else if (moveDifference<-150.0 && differenceY<40.0 && endTouchPoint.y<150.0 && secondsBetween<0.3)
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"nextRoom" object:@"2"];
        return;
    }
    
    location = [[CCDirector sharedDirector] convertToGL:location];

    for (int i=0;i<12;i++)
    {
        if (CGRectContainsPoint(rect_polk[i],location) && objTouch==i)
        {
            switch (page) {
                case 0:
                {
                    if ( [[stateTamagochi selectBody_Shop:i+1] count]!=0)
                    {
                        if ([stateTamagochi soundOn])
                            [[SimpleAudioEngine sharedEngine] playEffect:@"closet.mp3" loop:NO];
                        
                        [stateTamagochi setBodyCategory:i+1];
                        [self closetCategoryCreate];
                        
                    }
                }
                    break;
                case 1:
                {
                    
                    if (i+1<=[polki_sprites count])
                    {
                        if (![[polki_sprites objectAtIndex:i] isEqualToString:([stateTamagochi bodyCategory]==10)?@"rot.png":@""] && ![[polki_sprites objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"add%@.png", (IS_IPAD)?@"-iPad":@""]])
                        {
                            [_dataShopBody removeAllObjects];
                            [_dataShopBody addObjectsFromArray:[self getArrayBody:[polki_sprites objectAtIndex:i]]];
                            if ([_dataShopBody count]==0)
                            {
                                [[SimpleAudioEngine sharedEngine] playEffect:@"apply.mp3" loop:NO];
                                selectedColor=i;
                                if ([stateTamagochi bodyCategory]==6)
                                {
                                    int selColEye=[[[[polki_sprites objectAtIndex:i] componentsSeparatedByString:@"_"] objectAtIndex:1] intValue];
                                    [personaj replaceTexturePerson:13 obj:[NSString stringWithFormat:@"yeylid_up_%d%@.png",selColEye, (IS_IPAD)?@"-iPad":@""]];
                                    [personaj replaceTexturePerson:14 obj:[NSString stringWithFormat:@"yeylid_down_%d%@.png",selColEye, (IS_IPAD)?@"-iPad":@""]];
                                }
                                [personaj replaceTexturePerson:[stateTamagochi bodyCategory] obj:[polki_sprites objectAtIndex:i]];
                                [personaj replaceTextureDefault:[stateTamagochi bodyCategory]];
                                [[personaj person] setPositionByIndex:[stateTamagochi bodyCategory]];
                                
                                
                            }
                            else
                            [self createSelectColors];
                        }
                        else if (![[polki_sprites objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"add%@.png", (IS_IPAD)?@"-iPad":@""]])
                        {
                             [[SimpleAudioEngine sharedEngine] playEffect:@"apply.mp3" loop:NO];
                            [personaj replaceTexturePerson:[stateTamagochi bodyCategory] obj:([stateTamagochi bodyCategory]==10)?@"buze_03_4.png":@""];
                            [personaj replaceTextureDefault:[stateTamagochi bodyCategory]];
                            if ([stateTamagochi bodyCategory]==12)
                            {
                                for (int j=0;j<[[personaj tatooShopped] count];j++)
                                {
                                    [[[personaj person] getChildByTag:[[[[[[personaj tatooShopped] objectAtIndex:j] objectAtIndex:0] componentsSeparatedByString:@"_"] objectAtIndex:1] intValue]+21] removeFromParentAndCleanup:YES];
                                }
                                 [[personaj tatooShopped] removeAllObjects];
                            }
                        }
                        else
                        {
                            [self getShopping:0];
                        }
                    }
                }
                default:
                    break;
            }
        }
    }
       
    if (CGRectContainsPoint([btn_back boundingBox],location) && butTouch==butBack)
    {
        switch (page) {
            case 0:
            {
                if ([stateTamagochi soundOn])
                     [[SimpleAudioEngine sharedEngine] playEffect:@"closet.mp3" loop:NO];
                [[NSNotificationCenter defaultCenter]postNotificationName:@"renameRoom" object:@"Bed_room"];
                [[personaj person] removeFromParentAndCleanup:NO];
                [[CCDirectorIOS sharedDirector] replaceScene:[BedRoom scene]];
            }
                break;
            case 1:
            {
                if ([stateTamagochi soundOn])
                     [[SimpleAudioEngine sharedEngine] playEffect:@"closet.mp3" loop:NO];
                
                [polki_sprites removeAllObjects];
                [polki_sprites addObjectsFromArray:[self arrayDefault]];
                page=0;
                touchPolk=0;
               [self creatClosed:12];
              
            }
                break;
            case 2:
            {
                page=0;
                touchPolk=0;
            }
                
            default:
                break;
        }
    }
    
    nr_sounds=0;
    touchFun=0;
    xx=0;
    yy=0;
}

-(void)changePosition:(CGPoint)pos
{
    xx=pos.x;
    yy=pos.y;
}



#pragma CCScrollLayer delegate

- (void) scrollLayerScrollingStarted:(CCScrollLayer *) sender;
{
}
- (void) scrollLayer: (CCScrollLayer *)sender scrolledToPageNumber: (int) page;
{
}

#pragma ScrollLayer delegate

-(void)ccScrollLayerCurrentItem:(int)current scroll:(CCScrollLayer *)scrollLayer category:(int)category
{
  
}

-(void)ccScrollLayerActive:(CCScrollLayer *)scrollLayer active:(BOOL)active
{
    
}

-(void)ccScrollLayerDidEndDragging:(CCScrollLayer *)scrollLayer willDecelerate:(BOOL)willDecelerate
{
   
}

-(void)ccScrollLayerDidScroll:(CCScrollLayer *)scrollLayer
{
    
}

-(void)ccScrollLayerWillBeginDecelerating:(CCScrollLayer *)scrollLayer
{
    
}

-(void)ccScrollLayerWillBeginDragging:(CCScrollLayer *)scrollLayer
{
    
}

-(void)scrollButtonActive:(ScrollLayer *)scrollLayer button:(int)but rind:(int)rind
{

}
#pragma Tamagochi effects


-(void)resetVariables
{
    repeatTouches=NO;
    touchFun=0;
    if ([[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying])
        [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
 
}

-(void)fun
{
    repeatTouches=YES;
    totalTime=0;
    tamTouch=0.3;
    if (![[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying] &&  [stateTamagochi soundOn])
        [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"giggle.wav" loop:NO];
    [self performSelector:@selector(resetVariables) withObject:nil afterDelay:1.57];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]    removeObserver:self name:@"closetCategory" object:nil];
    [polki_sprites release];
  
    [super dealloc];
}

@end
