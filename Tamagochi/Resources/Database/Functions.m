//
//  Functions.m
//  Big Day Test
//
//  Created by Symon on 8/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Functions.h"

@implementation Functions

- (void)encryptDB
{
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDir = [documentPaths objectAtIndex:0];
    NSString *ecDB = [documentDir stringByAppendingPathComponent:@"iopta.data"];
    
    // SQL Query. NOTE THAT DATABASE IS THE FULL PATH NOT ONLY THE NAME
    const char* sqlQ = [[NSString stringWithFormat:@"ATTACH DATABASE '%@' AS iopta KEY '5GsahnQwC7q801KJ1C7y2H7l1vYK4J17';",ecDB] UTF8String];
    
    sqlite3 *unencrypted_DB;
    if (sqlite3_open([self.databasePath UTF8String], &unencrypted_DB) == SQLITE_OK) {
        
        // Attach empty encrypted database to unencrypted database
        sqlite3_exec(unencrypted_DB, sqlQ, NULL, NULL, NULL);
        
        // export database
        sqlite3_exec(unencrypted_DB, "SELECT sqlcipher_export('iopta');", NULL, NULL, NULL);
        
        // Detach encrypted database
        sqlite3_exec(unencrypted_DB, "DETACH DATABASE iopta;", NULL, NULL, NULL);
        
        sqlite3_close(unencrypted_DB);
    }
    else {
        sqlite3_close(unencrypted_DB);
        NSAssert1(NO, @"Failed to open database with message '%s'.", sqlite3_errmsg(unencrypted_DB));
    }
    
    self.databasePath = [documentDir stringByAppendingPathComponent:@"iopta.data"];}


-(void)openDataBase
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    
    //self.databasePath = [documentsDirectory stringByAppendingPathComponent:@"/tamagochi.sqlite"];
    //NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath]  stringByAppendingPathComponent:@"/tamagochi.sqlite"];
    
    self.databasePath = [documentsDirectory stringByAppendingPathComponent:@"/iopta.data"];
    NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath]  stringByAppendingPathComponent:@"/iopta.data"];
    
    if (![fileManager fileExistsAtPath:self.databasePath])
    {
        if ([fileManager copyItemAtPath:defaultDBPath toPath:self.databasePath error:&error])
        {
            NSLog(@"copied database to docs successfully!");
        }
        else {
            NSLog(@"Error description-%@ \n", [error localizedDescription]);
            NSLog(@"Error reason-%@", [error localizedFailureReason]);
        }
    }
    
   // [self encryptDB];
    //[[NSFileManager defaultManager] removeItemAtPath:[documentsDirectory stringByAppendingPathComponent:@"/tamagochi.sqlite"] error: &error];
    
    database= [[FMDatabase alloc] initWithPath:self.databasePath];
    [database open];
    [database setKey:@"5GsahnQwC7q801KJ1C7y2H7l1vYK4J17"];
  
}


-(NSMutableArray *)selectStates
{
    NSMutableArray *array=[[[NSMutableArray alloc] init] autorelease];
    
    
    FMResultSet *result=[database executeQuery:@"select * from tamagochi"];
    while ([result next])
    {
        [array addObject:[result stringForColumn:@"Food"]];
        [array addObject:[result stringForColumn:@"Energy"]];
        [array addObject:[result stringForColumn:@"Health"]];
        [array addObject:[result stringForColumn:@"Fun"]];
        [array addObject:[result stringForColumn:@"Money"]];
        [array addObject:[result stringForColumn:@"Level"]];
        [array addObject:[result stringForColumn:@"Size"]];
        [array addObject:[result stringForColumn:@"Fat"]];
    }
    
    return array;
}

-(NSMutableArray *)selectSkins:(NSInteger)index
{
    NSMutableArray *array=[[[NSMutableArray alloc] init] autorelease];
    FMResultSet *result =[database executeQuery:@"select * from skins_name"];
    while ([result next])
    {
        NSMutableArray *arr=[[[NSMutableArray alloc] init] autorelease];
        [arr addObject:[result stringForColumn:@"ID_Skins_Name"]];
        [arr addObject:[result stringForColumn:@"Name"]];
        [array addObject:arr];
    }
    return array;
}

-(NSMutableArray *)selectSkins_Shop:(NSInteger)index
{
    NSMutableArray *array=[[[NSMutableArray alloc] init] autorelease];
    FMResultSet *result;
    result =[database executeQuery:@"select max(Group_body) AS NAME from body_shop, body_name where body_shop.ID_Name=body_name.ID_Body_Name and body_name.ID_Body_Name=?", [NSString stringWithFormat:@"%d", index]];
    
    return  array;
}

-(NSMutableArray *)selectBody_Shop:(NSInteger)index
{
    
    NSMutableArray *array=[[[NSMutableArray alloc] init] autorelease];
    FMResultSet *result;
    result =[database executeQuery:@"select max(Group_body) AS NAME from body_shop, body_name where body_shop.ID_Name=body_name.ID_Body_Name and body_name.ID_Body_Name=?", [NSString stringWithFormat:@"%d", index]];
    int max;
    while ([result next]) {
            max=[result intForColumn:@"NAME"];
    }

    if (max>0)
    {
        result =[database executeQuery:@"select body_name.Name, body_shop.Group_body, body_shop.Nr_body from body_name, body_shop where body_shop.ID_Name=body_name.ID_Body_Name and body_shop.Quantity_shop<>0 and body_shop.Quantity_shop<9 and body_name.ID_Body_Name=? group by body_shop.Group_body", [NSString stringWithFormat:@"%d", index]];
    }
    else
    {
        result =[database executeQuery:@"select  body_name.Name, body_shop.Group_body, body_shop.Nr_body from body_name, body_shop where body_shop.ID_Name=body_name.ID_Body_Name and body_shop.Quantity_shop<>0 and body_shop.Quantity_shop<9 and body_name.ID_Body_Name=?", [NSString stringWithFormat:@"%d", index]];
    }
    if (max!=0 && (![[[personaj bodyShopped] objectAtIndex:index-1] isEqualToString:@""] || [[personaj tatooShopped] count]!=0))
    {
        [array addObject:([stateTamagochi bodyCategory]==10)?@"rot.png":@""];
        max++;
    }
    int k=0;
    while ([result next])
    {
        k++;
        NSString *str=@"";
        
        if ([[result stringForColumn:@"Group_body"] intValue]!=0)
        {
            str=([[result stringForColumn:@"Group_body"] intValue]>9)?[NSString stringWithFormat:@"_%@",[result stringForColumn:@"Group_body"]]:[NSString stringWithFormat:@"_0%@",[result stringForColumn:@"Group_body"]];

        }
        [array addObject:[NSString stringWithFormat:@"%@%@_%@.png", [result stringForColumn:@"Name"],str,[result stringForColumn:@"Nr_body"]]];
    }
    if (k==0)
    {
        [array removeAllObjects];
    }
    if ((max==0 && [array count]<10) || [array count]<max )
    {
        [array addObject:[NSString stringWithFormat:@"add%@.png", (IS_IPAD)?@"-iPad":@""]];
    }
    return array;
}

-(NSMutableArray *)selectBodyGroup:(NSArray *)arr
{
    NSMutableArray *array=[[[NSMutableArray alloc] init] autorelease];
    FMResultSet *result=[database executeQuery:@"select body_name.Name, body_shop.Group_body, body_shop.Nr_body from body_name, body_shop where body_shop.ID_Name=body_name.ID_Body_Name and body_shop.Quantity_shop<>0 and body_shop.Quantity_shop<10 and body_name.Name like ? and body_shop.Group_body=?", [NSString stringWithFormat:@"%@", [arr objectAtIndex:0]],[NSString stringWithFormat:@"%d", [[arr objectAtIndex:1] intValue]]];
    while ([result next])
    {
        NSMutableArray *arr=[[[NSMutableArray alloc] init] autorelease];
        [arr addObject:[result stringForColumn:@"Name"]];
        
        if ([[result stringForColumn:@"Group_body"] length]>1)
            [arr addObject:[result stringForColumn:@"Group_body"]];
        else
            [arr addObject:[NSString stringWithFormat:@"0%@",[result stringForColumn:@"Group_body"]]];
        
        [arr addObject:[result stringForColumn:@"Nr_body"]];
        [array addObject:arr];
    }
    return array;
}

-(int)returnPriceSkin:(NSInteger)index
{
    FMResultSet *result=[database executeQuery:@"select skins_shop.Price from skins_shop where skins_shop.ID_Skin=? and skins_shop.Unlocked=1",[NSString stringWithFormat:@"%d", index]];
    
    while ([result next]) {
        return [result intForColumn:@"Price"];
    }
    return 0;

}

-(NSInteger)selectNameSkin:(NSInteger)tag
{
      FMResultSet *result=[database executeQuery:@"select ID_Name from skins_shop where ID_Skin=?",[NSString stringWithFormat:@"%d", tag]];
    
    while ([result next]) {
        return [result intForColumn:@"ID_Name"];
    }
    return 0;
}
-(NSMutableArray *)colorSkin:(NSInteger)index object:(NSInteger)object
{
    NSMutableArray *array=[[[NSMutableArray alloc] init] autorelease];
    FMResultSet *result=[database executeQuery:@"select * from HSVColors where ID_Color=? and ID_Name=?",[NSString stringWithFormat:@"%d", index], [NSString stringWithFormat:@"%d", 1]];
    
    while ([result next]) {
        [array addObject:[result stringForColumn:@"H"]];
        [array addObject:[result stringForColumn:@"S"]];
        [array addObject:[result stringForColumn:@"V"]];
    }
    return array;
}

-(NSInteger)selectColorSkin:(NSInteger)tag room:(NSInteger)room
{
    
    FMResultSet *result=[database executeQuery:@"select skins_shop.Selected_color from skins_shop where skins_shop.ID_Name=? and Group_skin=?",[NSString stringWithFormat:@"%d", tag], [NSString stringWithFormat:@"%d", room]];
    
    while ([result next]) {
        return [result intForColumn:@"Selected_color"];
    }
    return 0;
}

-(void)updateColorSkin:(NSInteger)tag color:(NSInteger)color
{
    [database executeUpdate:@"update skins_shop set Selected_color=? where ID_Skin=?", [NSString stringWithFormat:@"%d",color], [NSString stringWithFormat:@"%d",tag]];
}

-(int)returnPrice:(NSString *)name group:(NSString *)group nr:(NSInteger)nr
{
    FMResultSet *result=[database executeQuery:@"select body_shop.Price from body_shop, body_name where body_name.Name like ? and body_shop.ID_Name=body_name.ID_Body_Name  and  body_shop.Group_body=? and body_shop.Nr_body=? and body_shop.Unlocked=1", [NSString stringWithFormat:@"%@", name], [NSString stringWithFormat:@"%@", ([group length]>0)?[group substringFromIndex:1]:@"0"], [NSString stringWithFormat:@"%d", nr]];
   
    while ([result next]) {
        return [result intForColumn:@"Price"];
    }
    return 0;
}

-(int)returnLevel:(NSString *)name group:(NSString *)group nr:(NSInteger)nr
{
    FMResultSet *result=[database executeQuery:@"select body_shop.Level from body_shop, body_name where body_name.Name like ? and body_shop.ID_Name=body_name.ID_Body_Name  and  body_shop.Group_body=? and body_shop.Nr_body=?", [NSString stringWithFormat:@"%@", name], [NSString stringWithFormat:@"%@", ([group length]>0)?[group substringFromIndex:1]:@"0"], [NSString stringWithFormat:@"%d", nr]];
    
    while ([result next]) {
        return [result intForColumn:@"Level"];
    }
    return 0;
}


-(int)returnIDName:(NSString *)name
{
    FMResultSet *result=[database executeQuery:@"select ID_Body_Name from body_name where Name like ?", [NSString stringWithFormat:@"%@", name]];
    while ([result next]) {
        return [result intForColumn:@"ID_Body_Name"];
    }
    return 0;
}

-(void)unlockColor:(NSInteger)index color:(NSInteger)color group:(NSString*)group
{
   
    switch ([stateTamagochi shopCategory]) {
        case sh_body:
        {
            FMResultSet *result =[database executeQuery:@"select ID_Name from body_shop where ID_Body_Shop=?", [NSString stringWithFormat:@"%d", index]];
            int id_name;
            while ([result next])
            {
                id_name=[result intForColumn:@"ID_Name"];
            }
            [database executeUpdate:@"update body_shop set Unlocked=1 where ID_Name=? and Nr_body=? and Group_body=?", [NSString stringWithFormat:@"%d",id_name], [NSString stringWithFormat:@"%d",color], [NSString stringWithFormat:@"%@", ([group length]>0)?[group substringFromIndex:1]:@"0"]];
        }
            break;
        case sh_eat:
        {
             [database executeUpdate:@"update eat_shop set Unlocked=1 where ID_Product=?", [NSString stringWithFormat:@"%d",index]];
        }
            break;
            
        case sh_skins:
        {
            [database executeUpdate:@"update skins_shop set Unlocked=1 where ID_Skin=?", [NSString stringWithFormat:@"%d",index]];
        }
            break;
        default:
            break;
    }
    
}

-(NSString *)getPriceBodyShop:(NSString *)index group:(NSString *)group
{
    switch ([stateTamagochi shopCategory]) {
        case sh_body:
        {
            FMResultSet *result =[database executeQuery:@"select min(Price) as min, max(Price) as max from body_shop, body_name where body_name.Name like ? and body_shop.ID_Name=body_name.ID_Body_Name and body_shop.Group_body=?", [NSString stringWithFormat:@"%@", index], ([group length]>0)?[group substringFromIndex:1]:@"0"];
            while ([result next])
            {
                int min=[result intForColumn:@"min"];
                int max=[result intForColumn:@"max"];
                if (min!=max)
                    return [NSString stringWithFormat:@"%d - %d", min,max];
                else
                    return [NSString stringWithFormat:@"%d", max];
            }

        }
            break;
        case sh_skins:
        {
            FMResultSet *result =[database executeQuery:@"select min(Price) as min, max(Price) as max from skins_shop, skins_name where skins_name.Name like ? and skins_shop.ID_Name=skins_name.ID_Skins_Name and skins_shop.Group_skin=?", [NSString stringWithFormat:@"%@", index], [group substringFromIndex:1]];
            while ([result next])
            {
                int min=[result intForColumn:@"min"];
                int max=[result intForColumn:@"max"];
                if (min!=max)
                    return [NSString stringWithFormat:@"%d - %d", min,max];
                else
                    return [NSString stringWithFormat:@"%d", max];
            }
        }
            
        default:
            break;
    }
      return @"0";
}

-(NSMutableArray *)selectBodyShop:(NSInteger)category
{
    
    NSMutableArray *array=[[[NSMutableArray alloc] init] autorelease];
    FMResultSet *result;
    
    switch (category) {
        case sh_potions:
        {
           result =[database executeQuery:@"select potions_shop.Percent,potions_shop.Group_potion, potions_shop.Price , potions_shop.Quantity_shop, potions_shop.ID_Potion, potions_name.Name from potions_shop, potions_name where potions_shop.Group_potion=potions_name.ID_Name"];
            while ([result next])
            {
                NSMutableArray *array1=[[[NSMutableArray alloc] init] autorelease];
                [array1 addObject:@"potion"];
                [array1 addObject:[NSString stringWithFormat:@"_0%@",[result stringForColumn:@"Group_potion"]]];
                [array1 addObject:[result stringForColumn:@"Price"]];
                [array1 addObject:[result stringForColumn:@"Quantity_shop"]];
                [array1 addObject:[result stringForColumn:@"ID_Potion"]];
                [array1 addObject:@""];
                [array1 addObject:@"0"];
                if ([[result stringForColumn:@"Group_potion"] isEqualToString:@"1"])
                    [array1 addObject:[NSString stringWithFormat:@"%@ %@%% %@",NSLocalizedString(@"potion_01_1", nil), [result stringForColumn:@"Percent"], NSLocalizedString(@"potion_01_2", nil)]];
                else
                {
                    NSString *str=[NSString stringWithFormat:@"potion_0%@_1",[result stringForColumn:@"Group_potion"]];
                    [array1 addObject:[NSString stringWithFormat:@"%@",NSLocalizedString(str, nil)]];
                }
                [array1 addObject:@"1"];
                [array addObject:array1];
            }
        }
            break;
            
        case sh_eat:
        {
            result =[database executeQuery:@"select eat_name.Name, eat_shop.Nr_eat, eat_shop.kCal, eat_shop.Price, eat_shop.Level, eat_shop.Quantity_shop, eat_shop.ID_Product, eat_shop.Unlocked from eat_shop, eat_name where eat_shop.ID_Name=eat_name.ID_Eat_Name and eat_shop.ID_Name=? order by Level", [NSString stringWithFormat:@"%d", [stateTamagochi eatCategory]]];
          
            while ([result next])
            {
                NSMutableArray *array1=[[[NSMutableArray alloc] init] autorelease];
                [array1 addObject:[result stringForColumn:@"Name"]];
                if ([[result stringForColumn:@"Nr_eat"] length]==1)
                    [array1 addObject:[NSString stringWithFormat:@"_0%@",[result stringForColumn:@"Nr_eat"]]];
                else
                    [array1 addObject:[NSString stringWithFormat:@"_%@",[result stringForColumn:@"Nr_eat"]]];
                [array1 addObject:[result stringForColumn:@"Price"]];
                [array1 addObject:[result stringForColumn:@"Quantity_shop"]];
                [array1 addObject:[result stringForColumn:@"ID_Product"]];
                [array1 addObject:@""];
                [array1 addObject:[result stringForColumn:@"Level"]];
                [array1 addObject:[NSString stringWithFormat:@"%ld %%", lroundf([[result stringForColumn:@"kCal"] intValue]/20.0/[stateTamagochi sizeTamagochi])]];
                [array1 addObject:[result stringForColumn:@"Unlocked"]];
                [array addObject:array1];
            }
        }
            break;
        case sh_body:
        {
            result =[database executeQuery:@"select max(Group_body) as Max_N from body_shop, body_name where body_shop.ID_Name=body_name.ID_Body_Name and body_name.ID_Body_Name=?", [NSString stringWithFormat:@"%d", [stateTamagochi bodyCategory]]];
            int max;
            while ([result next]) {
                max=[result intForColumn:@"Max_N"];
            }
            
            if (max>0)
            {
                NSString *query=@"";

                for (int i=1;i<=max;i++)
                {
                    query=[NSString stringWithFormat:@"%@ select body_shop.Price as Price, body_name.Name as Name, body_shop.Group_body as Group_body, body_shop.Nr_body as Nr_body , body_shop.Level as Level, body_shop.Quantity_shop as Quantity_shop, body_shop.ID_Body_Shop as ID_Body_Shop, body_shop.Unlocked as Unlocked from body_shop, body_name where body_shop.ID_Name=body_name.ID_Body_Name and body_name.ID_Body_Name=%@ and body_shop.Group_body=%d and body_shop.Nr_body=%d %@", query, [NSString stringWithFormat:@"%d", [stateTamagochi bodyCategory]], i,1 + arc4random() % (10 - 1), (i==max)?@"":@" union "];
                }
            
                
                result =[database executeQuery:query];
                
            }
            else
                result =[database executeQuery:@"select body_shop.Price, body_name.Name, body_shop.Group_body, body_shop.Nr_body, body_shop.Level, body_shop.Quantity_shop, body_shop.ID_Body_Shop, body_shop.ID_Name, body_shop.Unlocked as Unlocked from body_shop, body_name where body_shop.ID_Name=body_name.ID_Body_Name and body_name.ID_Body_Name=?", [NSString stringWithFormat:@"%d", [stateTamagochi bodyCategory]]];
            
           
            while ([result next])
            {
                NSMutableArray *array1=[[[NSMutableArray alloc] init] autorelease];
                [array1 addObject:[result stringForColumn:@"Name"]];
                NSString *str=@"";
                if ([[result stringForColumn:@"Group_body"] intValue]!=0)
                {
                    str=([[result stringForColumn:@"Group_body"] intValue]>9)?[NSString stringWithFormat:@"_%@",[result stringForColumn:@"Group_body"]]:[NSString stringWithFormat:@"_0%@",[result stringForColumn:@"Group_body"]];
                }
                [array1 addObject:str];
                [array1 addObject:[result stringForColumn:@"Price"]];
                [array1 addObject:[result stringForColumn:@"Quantity_shop"]];
                [array1 addObject:[result stringForColumn:@"ID_Body_Shop"]];
                [array1 addObject:[NSString stringWithFormat:@"_%@",[result stringForColumn:@"Nr_body"]]];
                [array1 addObject:[result stringForColumn:@"Level"]];
                [array1 addObject:([result intForColumn:@"Unlocked"]==0)?NSLocalizedString(@"Locked", nil):@""];
                [array1 addObject:[result stringForColumn:@"Unlocked"]];
                [array addObject:array1];
            }
           [self sortArray:array];
        }
            break;
        case sh_skins:
        {
            result =[database executeQuery:@"select skins_shop.ID_Skin, skins_name.Name, skins_shop.Group_skin, skins_shop.Price,  skins_shop.Level , skins_shop.Unlocked from skins_shop, skins_name where skins_shop.ID_Name=skins_name.ID_Skins_Name and skins_shop.Group_skin=? order by Level", [NSString stringWithFormat:@"%d", [stateTamagochi skinCategory]]];
            
            while ([result next])
            {
                NSMutableArray *array1=[[[NSMutableArray alloc] init] autorelease];
                [array1 addObject:[result stringForColumn:@"Name"]];
                [array1 addObject:[NSString stringWithFormat:@"_%@",[result stringForColumn:@"Group_skin"]]];
                [array1 addObject:[result stringForColumn:@"Price"]];
                [array1 addObject:@""];
                [array1 addObject:[result stringForColumn:@"ID_Skin"]];
                [array1 addObject:@""];
                [array1 addObject:[result stringForColumn:@"Level"]];
                [array1 addObject:([result intForColumn:@"Unlocked"]==0)?NSLocalizedString(@"Locked", nil):@""];
                [array1 addObject:[result stringForColumn:@"Unlocked"]];
                [array addObject:array1];
            }
        }
            break;
        default:
            break;
    }
    
    return array;
}


-(void)sortArray:(NSMutableArray *)array
{
    int min=0;
    int size=[array count];
    int k;
    for (int i=0;i<size-1; i++)
    {
        min=[[[array objectAtIndex:i] objectAtIndex:6] intValue];
        k=i;
        for (int j=i+1;j<size;j++)
            if ([[[array objectAtIndex:j] objectAtIndex:6] intValue]<min)
            {
                min=[[[array objectAtIndex:j] objectAtIndex:6] intValue];
                k=j;
            }
        if  ([[[array objectAtIndex:i] objectAtIndex:6] intValue]>min)
            {
                NSMutableArray *aux=[[[NSMutableArray alloc] initWithArray:[array objectAtIndex:k]] autorelease];
                [array replaceObjectAtIndex:k withObject:[array objectAtIndex:i]];
                [array replaceObjectAtIndex:i withObject:aux];
            }
    }
}
-(NSMutableArray *)selectCoinsShop:(NSInteger)index
{
    NSMutableArray *array1=[[[NSMutableArray alloc] init] autorelease];
    FMResultSet *result=[database executeQuery:@"select *  from coins_shop where ID_Coins=?", [NSString stringWithFormat:@"%d",index+1]];
    while ([result next])
    {
        [array1 addObject:[result stringForColumn:@"Nr_Coins"]];
        [array1 addObject:[result stringForColumn:@"Sale"]];
        [array1 addObject:[result stringForColumn:@"Price"]];
    }
    return array1;

}

-(NSMutableArray *)selectShoppedColors:(NSInteger)index
{
    NSMutableArray *arr=[[[NSMutableArray alloc] init] autorelease];
    switch ([stateTamagochi shopCategory]) {
        case sh_body:
        {
            FMResultSet *result=[database executeQuery:@"select Group_body, ID_Name from body_shop where ID_Body_Shop=?", [NSString stringWithFormat:@"%d",index]];
            int group, id_n;
            while ([result next])
            {
                group=[result intForColumn:@"Group_body"];
                id_n=[result intForColumn:@"ID_Name"];
            }
            
            result=[database executeQuery:@"select Nr_body from body_shop where ID_Name=? and Group_body=? and Quantity_shop>0", [NSString stringWithFormat:@"%d",id_n], [NSString stringWithFormat:@"%d",group]];
            
            while ([result next])
            {
                [arr addObject:[result stringForColumn:@"Nr_body"]];
            }

        }
            break;
        case sh_skins:
        {
            FMResultSet *result=[database executeQuery:@"select Colors_shop from skins_shop where ID_Skin=?", [NSString stringWithFormat:@"%d",index]];
            while ([result next])
            {
                [arr addObjectsFromArray:[[result stringForColumn:@"Colors_shop"] componentsSeparatedByString:@","]];
            }
        }
        default:
            break;
    }
        return arr;
     
}
-(void)shopProduct:(NSInteger)index index2:(NSInteger)index2
{
    switch ([stateTamagochi shopCategory]) {
        case sh_potions:
        {
             [database executeUpdate:@"update potions_shop set Quantity_shop=Quantity_shop+1 where ID_Potion=?", [NSString stringWithFormat:@"%d",index]];
        }
            break;
        case sh_body:
        {
            FMResultSet *result;
            result=[database executeQuery:@"select ID_Name,Group_body, Quantity_shop from body_shop where ID_Body_Shop=?", [NSString stringWithFormat:@"%d",index]];
            int str1, str2;
            while ([result next])
            {
                str1=[result intForColumn:@"ID_Name"];
                str2=[result intForColumn:@"Group_body"];
            }
            if (index2!=0)
            {
                [database executeUpdate:@"update body_shop set Quantity_shop=Quantity_shop+1 where ID_Name=? and Group_body=? and Nr_body=? and Quantity_shop=0",[NSString stringWithFormat:@"%d",str1], [NSString stringWithFormat:@"%d",str2], [NSString stringWithFormat:@"%d",index2]];
            }
            
        }
            break;
        case sh_eat:
        {
            [database executeUpdate:@"update eat_shop set Quantity_shop=Quantity_shop+1 where ID_Product=?", [NSString stringWithFormat:@"%d",index]];

            
            
        }
            break;
        case sh_skins:
        {
            NSMutableArray *arr=[[[NSMutableArray alloc] initWithArray:[self selectShoppedColors:index]] autorelease];
            BOOL contine=NO;
            for (int i=0;i<[arr count];i++)
            {
                if ([[arr objectAtIndex:i] intValue]==index2)
                {
                    contine=YES;
                    break;
                }
            }
            if (!contine)
            {
                [database executeUpdate:@"update skins_shop set Colors_shop=Colors_shop || ? where ID_Skin=?", [NSString stringWithFormat:@",%d",index2],[NSString stringWithFormat:@"%d",index]];
            }
           
            [database executeUpdate:@"update skins_shop set Selected_color=? where ID_Skin=?", [NSString stringWithFormat:@"%d",index2],[NSString stringWithFormat:@"%d",index]];
            
            
        }
            break;
            
        default:
            break;
    }
}

-(NSInteger)getPriceProduct:(NSInteger)index
{
    int price=0;
    
    switch ([stateTamagochi shopCategory]) {
        case sh_potions:
        {
            FMResultSet *result=[database executeQuery:@"select Price from potions_shop where ID_Potion=?", [NSString stringWithFormat:@"%d",index]];
            while ([result next])
            {
                price=[result intForColumn:@"Price"];
            }

        }
            break;
        case sh_body:
        {
            FMResultSet *result=[database executeQuery:@"select Price from body_shop where ID_Body_Shop=?", [NSString stringWithFormat:@"%d",index]];
            while ([result next])
            {
                price=[result intForColumn:@"Price"];
            }

        }
            break;
        case sh_eat:
        {
            FMResultSet *result=[database executeQuery:@"select Price from eat_shop where ID_Product=?", [NSString stringWithFormat:@"%d",index]];
            while ([result next])
            {
                price=[result intForColumn:@"Price"];
            }

        }
            break;
        case sh_skins:
        {
            FMResultSet *result=[database executeQuery:@"select Price from skins_shop where ID_Skin=?", [NSString stringWithFormat:@"%d",index]];
            while ([result next])
            {
                price=[result intForColumn:@"Price"];
            }
        }
            
        default:
            break;
    }
    
    return price;
}
-(void)insertNameBody:(NSString *)str
{
     [database executeUpdate:@"insert into body_shop(Name) values(?)", [NSString stringWithFormat:@"%@",str]];
}

-(void)insertNameBodyIopta:(NSString *)str price:(NSInteger)prc group:(NSInteger)group nr:(NSInteger)nr level:(NSInteger)lev
{
    FMResultSet *result;
    
    result=[database executeQuery:@"select ID_Eat_Name from eat_name where Name like ?", [NSString stringWithFormat:@"%@", str]];
    while ([result next])
    {
        str=[result stringForColumn:@"ID_Eat_Name"];
    }

    [database executeUpdate:@"insert into eat_shop(ID_Name, Price, Nr_group, Nr_eat, Level, Quantity_shop, kCal) values(?,?,?,?,?,0,120)", [NSString stringWithFormat:@"%@",str], [NSString stringWithFormat:@"%d",prc], [NSString stringWithFormat:@"%d",group],[NSString stringWithFormat:@"%d",nr], [NSString stringWithFormat:@"%d",lev]];
}

-(void)tamagochiEat:(int)nr category:(int)category
{
    if (category!=2)
         [database executeUpdate:@"update eat_shop set Quantity_shop=Quantity_shop-1 where ID_Product=?", [NSString stringWithFormat:@"%d",nr]];
    else
         [database executeUpdate:@"update potions_shop set Quantity_shop=Quantity_shop-1 where ID_Potion=?", [NSString stringWithFormat:@"%d",nr]];
}

-(void)updateFunny:(float)number
{
     [database executeUpdate:@"update tamagochi set Fun=?", [NSString stringWithFormat:@"%f",number]];
}

-(void)updateFood:(float)number
{
    [database executeUpdate:@"update tamagochi set Food=?", [NSString stringWithFormat:@"%f",number]];
}

-(void)updateEnergy:(float)number
{
    [database executeUpdate:@"update tamagochi set Energy=?", [NSString stringWithFormat:@"%f",number]];
}

-(void)updateHealth:(float)number
{
    [database executeUpdate:@"update tamagochi set Health=?", [NSString stringWithFormat:@"%f",number]];
}

-(void)updateMoney:(int)number
{
    [database executeUpdate:@"update tamagochi set Money=?", [NSString stringWithFormat:@"%d",number]];
}

-(void)updateLevel:(int)number
{
    [database executeUpdate:@"update eat_shop set Unlocked=1 where Level=?", [NSString stringWithFormat:@"%d",[stateTamagochi level]]];
    [database executeUpdate:@"update body_shop set Unlocked=1 where Level=?", [NSString stringWithFormat:@"%d",[stateTamagochi level]]];
    [database executeUpdate:@"update skins_shop set Unlocked=1 where Level=?", [NSString stringWithFormat:@"%d",[stateTamagochi level]]];
    [database executeUpdate:@"update tamagochi set Level=?", [NSString stringWithFormat:@"%d",number]];
}

-(void)updateSizeLevel:(int)number
{
    [database executeUpdate:@"update tamagochi set Size=?", [NSString stringWithFormat:@"%d",number]];
}
-(void)deleteFood:(int)category food:(int)number
{
    [database executeUpdate:@"delete from eat_shop where Nr_group=? and ID_Product=", [NSString stringWithFormat:@"%d",number]];
}

-(NSMutableArray*)selectFood:(int)category
{
    NSMutableArray *array=[[[NSMutableArray alloc] init] autorelease];
    
    if (category!=2)
    {
        NSString *str=@"";
        if (category==4)
            str=@"select eat_name.Name, eat_shop.Nr_eat, eat_shop.kCal, eat_shop.Quantity_Shop, eat_shop.ID_Product  from eat_shop, eat_name where ID_Name=ID_Eat_Name and (ID_Name=2 or ID_Name=4) and Quantity_shop<>0";
        else
            str=[NSString stringWithFormat:@"select eat_name.Name, eat_shop.Nr_eat, eat_shop.kCal, eat_shop.Quantity_Shop, eat_shop.ID_Product  from eat_shop, eat_name where ID_Name=ID_Eat_Name and ID_Name=%d and Quantity_shop<>0 ", category];
    FMResultSet *result=[database executeQuery:str];
    while ([result next])
    {
        NSMutableArray *arr=[[[NSMutableArray alloc] init] autorelease];
        [arr addObject:[result stringForColumn:@"Name"]];
        [arr addObject:[NSString stringWithFormat:@"%ld", lroundf([[result stringForColumn:@"kCal"] intValue]/[stateTamagochi sizeTamagochi])]];
        if ([[result stringForColumn:@"Nr_eat"] length]==1)
            [arr addObject:[NSString stringWithFormat:@"0%@",[result stringForColumn:@"Nr_eat"]]];
        else
            [arr addObject:[result stringForColumn:@"Nr_eat"]];
        [arr addObject:[result stringForColumn:@"Quantity_shop"]];
        [arr addObject:[result stringForColumn:@"ID_Product"]];
        [array addObject:arr];
    }
    }
    else
    {
        FMResultSet *result=[database executeQuery:@"select potions_shop.ID_Potion, potions_shop.Group_potion, potions_shop.Percent, potions_shop.Quantity_shop, potions_name.Name  from potions_shop, potions_name where potions_shop.Group_potion=potions_name.ID_Name and  Quantity_shop<>0"];
        while ([result next])
        {
            NSMutableArray *arr=[[[NSMutableArray alloc] init] autorelease];
            [arr addObject:[result stringForColumn:@"Name"]];
            [arr addObject:[result stringForColumn:@"Percent"]];
            [arr addObject:[result stringForColumn:@"Group_potion"]];
            [arr addObject:[result stringForColumn:@"Quantity_shop"]];
            [arr addObject:[result stringForColumn:@"ID_Potion"]];
            [array addObject:arr];
        }
    }

    
    return array;
}

-(void)updateSleep
{
      [database executeUpdate:@"update stateOut set Sleep=?", [NSString stringWithFormat:@"%d",[stateTamagochi sleep]]];
}
-(void)updateStateOut
{
      [database executeUpdate:@"update stateOut set Sleep=?, Time=?, GMT=?", [NSString stringWithFormat:@"%d",[stateTamagochi sleep]], [NSString stringWithFormat:@"%@",[NSDate date] ], [NSString stringWithFormat:@"%d",[stateTamagochi convertToGMT:[NSDate date]]]];
}

-(NSMutableArray *)returnStateOut
{
     NSMutableArray *array=[[[NSMutableArray alloc] init] autorelease];
    FMResultSet *result=[database executeQuery:@"select *  from stateOut"];
    while ([result next])
    {
        [array addObject:[result stringForColumn:@"Sleep"]];
        [array addObject:[result stringForColumn:@"Time"]];
        [array addObject:[result stringForColumn:@"GMT"]];
    }

    return  array;
}

-(void)updateNextLevel:(float)points
{
    [database executeUpdate:@"update tamagochi set Next_level=?", [NSString stringWithFormat:@"%f", points]];
}

-(void)updateAdult:(int)points
{
    [database executeUpdate:@"update tamagochi set Adult=Adult+?", [NSString stringWithFormat:@"%d", points]];
}

-(float)nextLevel
{
    FMResultSet *result=[database executeQuery:@"select Next_level from tamagochi"];
    while ([result next]) {
        return [[result stringForColumn:@"Next_level"] floatValue];
    }
    return 0.0;
}
-(int)adult
{
    FMResultSet *result=[database executeQuery:@"select Adult from tamagochi"];
    while ([result next]) {
        return [result intForColumn:@"Adult"];
    }
    return 0;
}

-(NSMutableArray *)selectOpenedNextLevel
{
    NSMutableArray *array=[[[NSMutableArray alloc] init] autorelease];
    FMResultSet *result=[database executeQuery:@"select skins_Name.Name, skins_shop.Group_skin from skins_shop, skins_name where skins_shop.ID_Name=skins_name.ID_Skins_Name and skins_shop.Level=?", [NSString stringWithFormat:@"%d", [stateTamagochi level]]];
    while ([result next])
    {
        NSMutableArray *arr=[[[NSMutableArray alloc] init] autorelease];
        [arr addObject:[NSString stringWithFormat:@"%@_%@_prev%@.png", [result stringForColumn:@"Name"], [result stringForColumn:@"Group_skin"], (IS_IPAD)?@"-iPad":@""]];
        [arr addObject:[NSString stringWithFormat:@"%@_%@", [result stringForColumn:@"Name"], [result stringForColumn:@"Group_skin"] ]];
        [array addObject:arr];
    }
    
    result=[database executeQuery:@"select body_name.Name, body_shop.Group_body from body_shop, body_name where body_shop.ID_Name=body_name.ID_Body_Name and body_shop.Level=? group by Name", [NSString stringWithFormat:@"%d", [stateTamagochi level]]];
    while ([result next])
    {
        NSMutableArray *arr=[[[NSMutableArray alloc] init] autorelease];
        if ([[result stringForColumn:@"Group_body"] intValue]!=0)
         [arr addObject:[NSString stringWithFormat:@"%@_%@%@_%d%@.png", [result stringForColumn:@"Name"],([[result stringForColumn:@"Group_body"] length]>1)?@"":@"0",[result stringForColumn:@"Group_body"],1 + arc4random() % (10 - 1), (IS_IPAD)?@"-iPad":@""]];
        else
            [arr addObject:[NSString stringWithFormat:@"%@_%d%@.png", [result stringForColumn:@"Name"],1 + arc4random() % (10 - 1), (IS_IPAD)?@"-iPad":@""]];

        [arr addObject:[NSString stringWithFormat:@"%@_%@%@", [result stringForColumn:@"Name"],([[result stringForColumn:@"Group_body"] length]>1 )?@"":@"0",[result stringForColumn:@"Group_body"]]];
        [array addObject:arr];
    }
    
    result=[database executeQuery:@"select eat_name.Name, eat_shop.Nr_eat from eat_shop, eat_name where eat_shop.ID_Name=eat_name.ID_Eat_Name and eat_shop.Level=?", [NSString stringWithFormat:@"%d", [stateTamagochi level]]];
    while ([result next])
    {
        NSMutableArray *arr=[[[NSMutableArray alloc] init] autorelease];
        [arr addObject:[NSString stringWithFormat:@"%@_%@%@.png", [result stringForColumn:@"Name"], ([[result stringForColumn:@"Nr_eat"] length]==1)?@"0":@"",[result stringForColumn:@"Nr_eat"]]];
        [arr addObject:[NSString stringWithFormat:@"%@_%@%@", [result stringForColumn:@"Name"], ([[result stringForColumn:@"Nr_eat"] length]==1)?@"0":@"",[result stringForColumn:@"Nr_eat"]]];
        [array addObject:arr];
    }


    return  array;
}

-(void)updateBodyShopped:(NSMutableArray *)body
{
    NSString *str=[body objectAtIndex:0];
    for (int i=1;i<[body count];i++)
    {
        str=[NSString stringWithFormat:@"%@,%@", str, [body objectAtIndex:i]];
    }
    [database executeUpdate:@"update tamagochi set Body_shopped=?", [NSString stringWithFormat:@"%@", str]];
}

-(NSArray*)selectBodyShopped
{
    FMResultSet *result=[database executeQuery:@"select Body_shopped from tamagochi"];
    NSString *str=@",";
    while ([result next]) {
        str=[result stringForColumn:@"Body_shopped"] ;
    }
    return [str componentsSeparatedByString:@","];
}

-(void)updateTatooShopped:(NSMutableArray *)body
{
    NSString *str=@"";
    if ([body count]>0)
        str=[body objectAtIndex:0];
    for (int i=1;i<[body count];i++)
    {
        str=[NSString stringWithFormat:@"%@,%@", str, [body objectAtIndex:i]];
    }
    [database executeUpdate:@"update tamagochi set Tatto_shopped=?", [NSString stringWithFormat:@"%@", str]];
}

-(NSMutableArray *)selectTatooShopped
{
    FMResultSet *result=[database executeQuery:@"select Tatto_shopped from tamagochi"];
    NSString *str=@"";
    
    while ([result next]) {
        str=[result stringForColumn:@"Tatto_shopped"] ;
    }
    
    NSMutableArray *array=[[[NSMutableArray alloc] init] autorelease];
    [array addObjectsFromArray:[str componentsSeparatedByString:@","]];
    NSMutableArray *arr=[[[NSMutableArray alloc] init] autorelease];
    if ([array count]>1)
    {
        for (int i=0;i<[array count];i+=3)
        {
            NSMutableArray *arr1=[[[NSMutableArray alloc] init] autorelease];
            [arr1 addObject:[array objectAtIndex:i]];
            [arr1 addObject:[array objectAtIndex:i+1]];
            [arr1 addObject:[array objectAtIndex:i+2]];
            [arr addObject:arr1];
        }
    }
    return arr;
}

-(void)updateDirt:(float)nr
{
     [database executeUpdate:@"update tamagochi set dirtFood=?", [NSString stringWithFormat:@"%f", nr]];
}
-(float)selectDirt
{
    FMResultSet *result=[database executeQuery:@"select dirtFood from tamagochi"];
    NSString *str;
    while ([result next]) {
        str=[result stringForColumn:@"dirtFood"] ;
    }
    
    return [str floatValue];
}

-(float)selectFat
{
    FMResultSet *result=[database executeQuery:@"select Fat from tamagochi"];
    NSString *str;
    while ([result next]) {
        str=[result stringForColumn:@"Fat"] ;
    }
    
    return [str floatValue];
}

-(void)updateFat
{
    [database executeUpdate:@"update tamagochi set Fat=?", [NSString stringWithFormat:@"%f", [stateTamagochi fat]]];
}

-(NSMutableArray *)firstPlayGame
{
    NSMutableArray *array=[[[NSMutableArray alloc] init] autorelease];
    FMResultSet *result=[database executeQuery:@"select *  from first_play_game"];
    while ([result next])
    {
        [array addObject:[result stringForColumn:@"Kitchenroom_start"]];
        [array addObject:[result stringForColumn:@"Game_play"]];
        [array addObject:[result stringForColumn:@"Sleep_start"]];
        [array addObject:[result stringForColumn:@"Bathroom_start"]];
        [array addObject:[result stringForColumn:@"Health_first"]];
        [array addObject:[result stringForColumn:@"Closed_start"]];
    }
    
    return  array;
}

-(void)updateFirstStart:(int)category
{
    switch (category) {
        case 0:
        {
            [database executeUpdate:@"update first_play_game set Kitchenroom_start=1"];
        }
            break;
        case 1:
        {
            [database executeUpdate:@"update first_play_game set Game_play=1"];
        }
            break;
        case 2:
        {
            [database executeUpdate:@"update first_play_game set Sleep_start=1"];
        }
            break;
        case 3:
        {
            [database executeUpdate:@"update first_play_game set Bathroom_start=1"];
        }
            break;
        case 4:
        {
            [database executeUpdate:@"update first_play_game set Health_first=1"];
        }
            break;
        case 5:
        {
            [database executeUpdate:@"update first_play_game set Closed_start=1"];
        }
            break;
            
        default:
            break;
    }
}


-(void)dealloc
{
     [database release];
    [super dealloc];
   
}

// TODO: GAMES



-(BOOL)updateGameWithStageId:(NSNumber*)idStage withTime:(NSString*)bestTime withScore:(NSNumber*)score withCoins:(NSNumber*)coins withPosition:(NSNumber*)bestPosition{
    NSString* name = [[NSUserDefaults standardUserDefaults] objectForKey:@"player"];
    FMResultSet *result = [database executeQuery:@"SELECT * FROM GAMES WHERE ID_STAGE=? ORDER BY SCORE DESC LIMIT 1;", idStage];
    int max=0;
    while ([result next])
    {
        max=[result intForColumn:@"SCORE"];
    }
   
    if(!name) name = @"ME";
    if ([database executeUpdate:@"INSERT INTO GAMES (POSITION ,COINS, TIME, SCORE, ID_STAGE,NAME) VALUES(?,?,?,?,?,?);", bestPosition, coins, bestTime, score, idStage,name]) {
        NSLog(@"add new iopta");
        if ([score intValue]>max)
            return YES;
        return NO;
    }else return NO;
    
}

- (NSInteger)getScoreForStageId:(NSNumber*)idStage {
    NSString* name = [[NSUserDefaults standardUserDefaults] objectForKey:@"player"];
    if(!name) name = @"ME";
    FMResultSet *res = [database executeQuery:@"SELECT SCORE FROM GAMES WHERE ID_STAGE=? AND NAME =?", idStage,name];
    while ([res next]) {
        return  [res intForColumn:@"SCORE"];
    }
    NSLog(@"add new");
    return [self insertEmptyLineForStageId:idStage];
}

- (NSInteger)getPostionForStageID:(NSNumber *)idStage {
    NSString* name = [[NSUserDefaults standardUserDefaults] objectForKey:@"player"];
    if(!name) name = @"ME";
    FMResultSet *result = [database executeQuery:@"SELECT POSITION FROM GAMES WHERE ID_STAGE=? AND NAME =?", idStage,name];
    while ([result next]) {
        return  [result intForColumn:@"POSITION"];
    }
    NSLog(@"add new");
    return [self insertEmptyLineForStageId:idStage];
}

- (NSMutableArray*)getAllGameStatisticsForStageID:(NSNumber *)idStage{
    FMResultSet *result = [database executeQuery:@"SELECT * FROM GAMES WHERE ID_STAGE=? AND SCORE > 0 ORDER BY SCORE DESC", idStage];
    NSMutableArray *dict = [[[NSMutableArray alloc] init] autorelease];
    while ([result next]) {
        [dict addObject:[result resultDictionary]];
    }
    return dict;
}

- (NSDictionary*)getAllGameStatisticsForStageID:(NSNumber *)idStage forPlayer:(NSString*)player{
    FMResultSet *result = [database executeQuery:@"SELECT * FROM GAMES WHERE ID_STAGE=? AND NAME=? ORDER BY SCORE DESC LIMIT 1;", idStage,player];
    while ([result next]) {
        return [result resultDictionary];
    }
    return nil;
}

- (BOOL)insertEmptyLineForStageId:(NSNumber*)idStage {
    NSNumber* defaultValue = [NSNumber numberWithInt:0];
    NSString* name = [[NSUserDefaults standardUserDefaults] objectForKey:@"player"];
    if(!name) name = @"ME";
    if ([database executeUpdate:@"INSERT INTO GAMES (POSITION, COINS, TIME, SCORE, ID_STAGE,NAME) VALUES(?,?,?,?,?,?);", defaultValue, defaultValue, defaultValue, defaultValue, idStage, name])
    {
        return 0;
    }else {
        return NO;
    }
}

- (BOOL)addPlayer:(NSString*)player{
   if (![player isEqualToString:@""])
    return ![database executeUpdate:@"INSERT INTO PLAYER (ID_PLAYER, STAGE) VALUES(?,?);",player,@"0"];
    return NO;
}

- (BOOL)setStageForPlayer:(NSString*)player forStage:(NSInteger)stageId{
    NSInteger stage = [self getStageForPlayer:player];
    if (stageId<stage) {
        stage = stage;
    }else{
        stage = stage + 1;
    }
    return [database executeUpdate:@"UPDATE PLAYER SET STAGE=? WHERE ID_PLAYER=? ;",[NSNumber numberWithInt:stage],player];
}

-(NSInteger)getStageForPlayer:(NSString*)player{
    FMResultSet *result = [database executeQuery:@"SELECT STAGE FROM PLAYER WHERE ID_PLAYER=?", player];
    while ([result next]) {
        return  [result intForColumn:@"STAGE"];
    }
    return [self addPlayer:player];
}
//END

@end
