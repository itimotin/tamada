//
//  MainViewController.h
//  Tamagochi
//
//  Created by Sergiu Moțipan on 7/2/13.
//
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "iCarousel.h"
#import "CustomCell.h"
#import "ShopCoins.h"
#import "SettingsView.h"
#import "NextLevelView.h"
#import "ShareView.h"
#import "HighScoreCell.h"
#import "SimpleAudioEngine.h"
#import "cocos2d.h"

@interface MainViewController  : UIViewController <iCarouselDataSource, iCarouselDelegate, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, UIDocumentInteractionControllerDelegate, UITextFieldDelegate>
{
    CCDirectorIOS	*director_;
    IBOutlet UILabel *l_score;
    IBOutlet UILabel *l_room;
    IBOutlet UILabel *l_lvl;
    IBOutlet UIImageView *i_health;
    IBOutlet UIImageView *i_energy;
    IBOutlet UIImageView *i_life;
    IBOutlet UIImageView *i_funny;
    IBOutlet UIView *menuView;
    int nrPage;
    IBOutlet UITableView *tableHighscore;
    IBOutlet UITableView *tableGames;
    IBOutlet UITableView *tableShop;
    IBOutlet UITableView *tableEat;
    IBOutlet UIView *highScoreView;
    IBOutlet ShareView *shareView;
    IBOutlet UIView *selectColor;
    IBOutlet UIView *statsView;
    IBOutlet UIView *shopView;
    IBOutlet UIView *viewEats;
    IBOutlet UIImageView *stats_slide_1;
    IBOutlet UIImageView *stats_slide_2;
    IBOutlet UIImageView *stats_slide_3;
    IBOutlet UIImageView *stats_slide_4;
    IBOutlet UILabel *stats_lab_1;
    IBOutlet UILabel *stats_lab_2;
    IBOutlet UILabel *stats_lab_3;
    IBOutlet UILabel *stats_lab_4;
    IBOutlet UIView *viewBodyShop;
    IBOutlet UIButton *btnBackShop;
    IBOutlet UILabel *shopName;
    int selectedColor;
    IBOutlet UILabel *priceProduct;
    IBOutlet UIButton *btnBuy;
    IBOutlet UILabel *lbl_btnBuy;
    IBOutlet UIImageView *imageBuy;
    CGPoint startPos;
    IBOutlet UIButton *btn_Eat_1;
    IBOutlet UIButton *btn_Eat_2;
    IBOutlet UIButton *btn_Eat_3;
    IBOutlet UIButton *btn_Eat_4;
    int otherPage;
    CGMutablePathRef path;
    CGPoint newPoint;
    IBOutlet UIView *skinsView;
    IBOutlet UIButton *btnCloseColors;
    IBOutlet UIView *selectColorsView;
    IBOutlet UIButton *changebleColor;
    IBOutlet UIView *unlockView;
    IBOutlet UITextView *locked_Text;
    
    int lockedObject;
    IBOutlet UILabel *titleUnlockBtn;
    IBOutlet UIButton *unlockBtn;
    IBOutlet UIImageView *unlockIcon;
    IBOutlet UILabel *titileWaitBtn;
    IBOutlet UIButton *waitBtn;
    IBOutlet UIButton *btnNext;
    IBOutlet UIButton *btnBack;
    IBOutlet ShopCoins *viewCoins;
    IBOutlet SettingsView *viewSettings;
    IBOutlet NextLevelView *nextLevelView;
    IBOutlet UIButton *mallTap;
    IBOutlet UIView *viewPanelButonsStatistic;
    IBOutlet UILabel *skinsLabGame;
    IBOutlet UILabel *skinsLabBath;
    IBOutlet UILabel *skinsLabStudio;
    IBOutlet UILabel *skinsKitchen;
    IBOutlet UILabel *skinsLabBed;
    IBOutlet UIView *viewTV;
    IBOutlet UIButton *btn_settings;
    BOOL showCoins;
    IBOutlet UIView *tapCoinsView;
    
    IBOutlet UILabel *states_Food;
    IBOutlet UILabel *states_Health;
    IBOutlet UILabel *states_Energy;
    IBOutlet UILabel *states_Fun;
    IBOutlet UIButton *btn_ShareHighScore;
    
    //TODO: add labels
    IBOutlet UILabel *labelStage;
    IBOutlet UILabel *labelName;
    IBOutlet UILabel *labelScore;
    IBOutlet UILabel *labelTime;
    IBOutlet UILabel *btn_Play;
    IBOutlet UILabel *btn_Share;
    
    IBOutlet UILabel *lbl_Stage;
    IBOutlet UITextField *textFieldWithNamePlayer;
    int scoreShare;

   
}

@property (nonatomic, assign) NSMutableArray *dataShopBody;
@property (nonatomic, retain) CCRenderTexture *rt;
@property (strong) NSIndexPath *rowTapped;

- (IBAction)updateShopObject:(id)sender forEvent:(UIEvent *)event;
- (IBAction)selectEat:(id)sender;

- (IBAction)back:(id)sender;
- (IBAction)next:(id)sender;
- (IBAction)mallTaped:(id)sender;
- (IBAction)closeHighScore:(id)sender forEvent:(UIEvent *)event;
- (IBAction)closeStats:(id)sender;
- (IBAction)closeShop:(id)sender;
-(void)showShopView;
- (IBAction)tapView:(id)sender;
-(IBAction)updateShopColor:(id)sender;
- (IBAction)skinsSelect:(id)sender;
- (IBAction)unlockColor:(id)sender;
- (IBAction)settings:(id)sender;
- (IBAction)backDown:(id)sender;
- (IBAction)closeTV:(id)sender;
- (IBAction)shopCoinsView:(id)sender;


// TODO: add Action
- (IBAction)actionSelectStage:(id)sender;
// TODO: add View PanelStatistic

@end
