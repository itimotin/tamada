//
//  VSSilentSwitch.h
//  VSSilentSwitch
//
//  Created by Josh Pressnell on 12/27/11.
//  Copyright (c) 2011 Verietas Software. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^SilentSwitchCheckCompletionBlock)(Boolean isSilent);

@interface VSSilentSwitch : NSObject

+ (void)setPhoneCallsForceMute:(Boolean)forceMute;
+ (void)isOnSilent:(SilentSwitchCheckCompletionBlock)completion;

@end
