#import <GameKit/GameKit.h>
#import "cocos2d.h"
#import "Main.h"

@interface Game : Main
{
	CGPoint poo_pos;
	ccVertex2F poo_vel;
	ccVertex2F poo_acc;	

	float currentPlatformY;
	int currentPlatformTag, currentCoinTag;
	float currentMaxPlatformStep;
	int currentBonusPlatformIndex;
	int currentBonusType;
	int platformCount;
	
	BOOL gameSuspended;
	BOOL pooLookingRight;
    BOOL isJump;
    BOOL isFinish;
    BOOL forNonIpad;
	
	int score;
    
    int currentBackgroundID;
    int tagForParashute;
    float bonusAccelerateBackground;
}

@property (strong) CCTMXTiledMap *tileMap;
@property (strong) CCTMXLayer *background;

+ (CCScene *)scene;
@end
