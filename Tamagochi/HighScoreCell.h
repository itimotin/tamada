//
//  HighScoreCell.h
//  Tamagochi
//
//  Created by Timotin Vanea on 2/24/14.
//
//

#import <UIKit/UIKit.h>

@interface HighScoreCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *namePlayer;
@property (retain, nonatomic) IBOutlet UILabel *scorePl;
@property (retain, nonatomic) IBOutlet UILabel *timePlayer;
@property (retain, nonatomic) IBOutlet UILabel *numberPlayer;
@property (retain, nonatomic) IBOutlet UILabel *scorePlayer;
@end
