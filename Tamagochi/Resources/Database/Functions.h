//
//  Functions.h
//  Big Day Test
//
//  Created by Symon on 8/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "FMResultSet.h"

@interface Functions : NSObject
{
    FMDatabase *database;
    BOOL isfirstbd;
  
}
@property(nonatomic, retain) NSString *databasePath;

-(void)openDataBase;
-(NSMutableArray *)selectStates;
-(void)updateFunny:(float)number;
-(void)updateEnergy:(float)number;
-(void)updateFood:(float)number;
-(void)updateHealth:(float)number;
-(void)updateMoney:(int)number;
-(void)updateLevel:(int)number;
-(void)tamagochiEat:(int)nr category:(int)category;
-(NSMutableArray*)selectFood:(int)category;
-(void)insertNameBody:(NSString *)str;
-(void)insertNameBodyIopta:(NSString *)str price:(NSInteger)prc group:(NSInteger)group nr:(NSInteger)nr level:(NSInteger)lev;
-(NSMutableArray *)selectBody_Shop:(NSInteger)index;
-(NSMutableArray *)selectBodyGroup:(NSArray *)arr;
-(NSMutableArray *)selectBodyShop:(NSInteger)category;
-(void)shopProduct:(NSInteger)index index2:(NSInteger)index2;
-(NSMutableArray *)selectShoppedColors:(NSInteger)index;
-(int)returnPrice:(NSString *)name group:(NSString *)group nr:(NSInteger)nr;
-(int)returnIDName:(NSString *)name;
-(NSMutableArray *)selectSkins:(NSInteger)index;
-(int)returnPriceSkin:(NSInteger)index;
-(NSMutableArray *)colorSkin:(NSInteger)index object:(NSInteger)object;
-(NSInteger)selectColorSkin:(NSInteger)tag room:(NSInteger)room;
-(void)updateColorSkin:(NSInteger)tag color:(NSInteger)color;
-(NSInteger)selectNameSkin:(NSInteger)tag;
-(NSInteger)getPriceProduct:(NSInteger)index;
-(void)unlockColor:(NSInteger)index color:(NSInteger)color group:(NSString *)group;
-(NSMutableArray *)selectCoinsShop:(NSInteger)index;
-(NSString *)getPriceBodyShop:(NSString *)index group:(NSString *)group;
-(void)updateStateOut;
-(NSMutableArray *)returnStateOut;
-(void)updateNextLevel:(float)points;
-(float)nextLevel;
-(int)adult;
-(int)returnLevel:(NSString *)name group:(NSString *)group nr:(NSInteger)nr;
-(NSMutableArray *)selectOpenedNextLevel;
-(void)updateBodyShopped:(NSMutableArray *)body;
-(NSArray*)selectBodyShopped;
-(void)updateDirt:(float)nr;
-(float)selectDirt;
-(void)updateTatooShopped:(NSMutableArray *)body;
-(NSMutableArray *)selectTatooShopped;
-(void)updateSizeLevel:(int)number;
-(float)selectFat;
-(void)updateFat;
-(NSMutableArray *)firstPlayGame;
-(void)updateFirstStart:(int)category;
-(void)updateSleep;

// TODO: games
- (BOOL)updateGameWithStageId:(NSNumber*)idStage withTime:(NSString*)bestTime withScore:(NSNumber*)score withCoins:(NSNumber*)coins withPosition:(NSNumber*)bestPosition;
- (NSInteger)getScoreForStageId:(NSNumber*)idStage;
- (NSInteger)getPostionForStageID:(NSNumber*)idStage;
- (BOOL)insertEmptyLineForStageId:(NSNumber*)idStage;
- (NSMutableArray*)getAllGameStatisticsForStageID:(NSNumber *)idStage;
- (NSDictionary*)getAllGameStatisticsForStageID:(NSNumber *)idStage forPlayer:(NSString*)player;
- (BOOL)addPlayer:(NSString*)player;

-(NSInteger)getStageForPlayer:(NSString*)player;
- (BOOL)setStageForPlayer:(NSString*)player forStage:(NSInteger)stageId;
//END
@end
