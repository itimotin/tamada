//
//  FirstScene.h
//  Tamagochi
//
//  Created by Sergiu Moțipan on 6/4/13.
//
//

#import "CCLayer.h"
#import "cocos2d.h"
#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES2/gl.h>
#import "Box2D.h"
#import "GLES-Render.h"
#import "PouNode.h"
#import "SimpleAudioEngine.h"

#define PTM_RATIO 32

@interface MallRoom : CCLayer< PouNodeDelegate>
{
    CGSize winSize;
    CCSprite *leftBottom;
   
    GLESDebugDraw *m_debugDraw;
    int nr_sounds;
    float touchFun;
    int mouth;
    ALuint sound;
    
    CGPoint startTouchPoint;
    CGPoint endTouchPoint;
    
    NSDate *date_start;
    
}
+(CCScene *) scene;
@end
