//
//  SelectColors.m
//  Tamagochi
//
//  Created by Sergiu Moțipan on 10/24/13.
//
//

#import "SelectColors.h"

@implementation SelectColorsView

- (id)init {
    if ((self = [super init])) {
        colorLayer=[CCLayerColor layerWithColor:ccc4(0, 0, 0, 160)];
        [self addChild:colorLayer];
        backColors=[CCSprite spriteWithFile:[NSString stringWithFormat:@"bg_dulap%@.png", (IS_IPAD)?@"-iPad":@""]];
        backColors.scale=0.5+0.5*IS_RETINA;
        [backColors setPosition:ccp(colorLayer.boundingBox.size.width/2, colorLayer.boundingBox.size.height/2)];
        [colorLayer addChild:backColors z:1];
        nr_colors=0;
        indexTatoo=-1;
    }
    return self;
}


-(void)createColors
{
    
    int j=0;
    nr_colors=[[_delegate returnCountData] count];
    if (nr_colors==1)
    {
        if ([stateTamagochi bodyCategory]!=12)
        {
            [personaj replaceTexturePerson:[stateTamagochi bodyCategory] obj:[NSString stringWithFormat:@"%@_%@_%@%@.png", [[[_delegate returnCountData] objectAtIndex:0] objectAtIndex:0], [[[_delegate returnCountData] objectAtIndex:0] objectAtIndex:1], [[[_delegate returnCountData] objectAtIndex:0] objectAtIndex:2], (IS_IPAD)?@"-iPad":@""]];
            [personaj replaceTextureDefault:[stateTamagochi bodyCategory]];
    
            if ([stateTamagochi bodyCategory]==4)
            {
                [[[personaj person] getChildByTag:9] removeFromParentAndCleanup:NO];
            }
            else if ([stateTamagochi bodyCategory]==9)
                [[[personaj person] getChildByTag:4] removeFromParentAndCleanup:NO];
        }
        else
        {
             [[[personaj person] getChildByTag:5] removeFromParentAndCleanup:NO];
             [personaj replaceTatooPerson:[NSString stringWithFormat:@"%@_%@_%@%@.png", [[[_delegate returnCountData] objectAtIndex:0] objectAtIndex:0], [[[_delegate returnCountData] objectAtIndex:0] objectAtIndex:1], [[[_delegate returnCountData] objectAtIndex:0] objectAtIndex:2], (IS_IPAD)?@"-iPad":@""]];
             [personaj replaceTatooDefault:[[[_delegate returnCountData] objectAtIndex:0] objectAtIndex:1]];
        }
    }
    if (nr_colors<10) nr_colors++;
    for (int i=0;i<nr_colors;i++)
    {
        CCSprite *sprite;
        
        if (i==nr_colors-1 && [[_delegate returnCountData] count]!=10)
        {
          CCSprite  *sprite1=[CCSprite spriteWithFile:[NSString stringWithFormat:@"add%@.png", (IS_IPAD && IS_RETINA)?@"-iPad":@""]];
           sprite1.scale=0.825-0.14*IS_IPAD+0.025*IS_RETINA;
            buttons[i]=sprite1;
            buttons[i].tag=11;
        }
        else
        {
          
            sprite =[CCSprite spriteWithFile:[NSString stringWithFormat:@"color_%@%@.png", [[[_delegate returnCountData] objectAtIndex:i] objectAtIndex:2], (IS_IPAD)?@"-iPad":@""]];
         
            if ([stateTamagochi bodyCategory]!=12)
            {
                if ([[NSString stringWithFormat:@"%@_%@_%@%@.png",  [[[_delegate returnCountData] objectAtIndex:i] objectAtIndex:0], [[[_delegate returnCountData] objectAtIndex:i] objectAtIndex:1],     [[[_delegate returnCountData] objectAtIndex:i] objectAtIndex:2], (IS_IPAD)?@"-iPad":@"" ] isEqualToString:[[personaj bodyShopped] objectAtIndex:[stateTamagochi bodyCategory]-1]])
                {
                    CCSprite *sprite1=[CCSprite spriteWithFile:@"selected_yes.png"];
                    [sprite1 setPosition:ccp(sprite.boundingBox.size.width/2, sprite.boundingBox.size.height/2)];
                    [sprite addChild:sprite1 z:1 tag:1];
                }
            }
            else
            {
                for (int j=0;j<[[personaj tatooShopped] count];j++)
                {
                    if ([[NSString stringWithFormat:@"%@_%@_%@%@.png",  [[[_delegate returnCountData] objectAtIndex:i] objectAtIndex:0], [[[_delegate returnCountData] objectAtIndex:i] objectAtIndex:1],     [[[_delegate returnCountData] objectAtIndex:i] objectAtIndex:2], (IS_IPAD)?@"-iPad":@"" ] isEqualToString:[[[personaj tatooShopped] objectAtIndex:j] objectAtIndex:0]])
                    {
                        CCSprite *sprite1=[CCSprite spriteWithFile:@"selected_yes.png"];
                        [sprite1 setPosition:ccp(sprite.boundingBox.size.width/2, sprite.boundingBox.size.height/2)];
                        [sprite addChild:sprite1 z:1 tag:1];
                        indexTatoo=j;
                    }
                }
            }
            sprite.scale=0.5+0.5*IS_RETINA;

            buttons[i]=sprite;
            buttons[i].tag=[[[[_delegate returnCountData] objectAtIndex:i] objectAtIndex:2] intValue];
        }
        
       if (IS_IPAD)
           [buttons[i] setPosition:CGPointMake(colorLayer.boundingBox.size.width/3.2+j*buttons[i] .boundingBox.size.width/0.8, colorLayer.boundingBox.size.height/2.0+buttons[i] .boundingBox.size.height/2+10-(buttons[i] .boundingBox.size.height+10)*(i>4))];
        else
            [buttons[i] setPosition:CGPointMake(colorLayer.boundingBox.size.width/5+j*50, colorLayer.boundingBox.size.height/2+buttons[i] .boundingBox.size.height/2+10-(buttons[i] .boundingBox.size.height+10)*(i>4))];
                j++;
        if (j>4) j=0;
        [colorLayer addChild:buttons[i]  z:5];
    }
    
}

- (void)touchUpInSide:(id)sender
{
 
    if (![sender getChildByTag:1] && [sender tag]!=11)
    {
        
        NSString *text=@"";
        int selColEye=0;
        if (sender)
        {
            text=[NSString stringWithFormat:@"%@_%@_%d%@.png",[[[_delegate returnCountData] objectAtIndex:0] objectAtIndex:0], [[[_delegate returnCountData] objectAtIndex:0] objectAtIndex:1], [sender tag], (IS_IPAD)?@"-iPad":@""];
            selColEye=[sender tag];
        }
        else
        {
            text= [NSString stringWithFormat:@"%@%@.png",[[_delegate returnPolki] objectAtIndex:[_delegate returnColor]], (IS_IPAD)?@"-iPad":@""];
            selColEye=[[[[[_delegate returnPolki] objectAtIndex:[_delegate returnColor]] componentsSeparatedByString:@"_"] objectAtIndex:1] intValue];
        }
        
        
        if (![[personaj person] getChildByTag: [stateTamagochi bodyCategory]] && [stateTamagochi bodyCategory]!=12)
        {
            [[personaj person] createChildByTag:[stateTamagochi bodyCategory] nameTexture:text];
        }
        else
        {
            
            if ([stateTamagochi bodyCategory]==6)
            {
                [[[personaj person] getChildByTag:16] setTexture:[[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"yeylid_up_%d%@.png",selColEye, (IS_IPAD)?@"-iPad":@""]]];
                [[[personaj person] getChildByTag:17] setTexture:[[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"yeylid_down_%d%@.png",selColEye, (IS_IPAD)?@"-iPad":@""]]];
            }
            else  if ([stateTamagochi bodyCategory]!=12)
            {
            CCTexture2D* texture = [[CCTextureCache sharedTextureCache] addImage:text];
            [[[personaj person] getChildByTag: [stateTamagochi bodyCategory]] setTexture:texture];
            [[[personaj person] getChildByTag: [stateTamagochi bodyCategory]] setTextureRect:CGRectMake(0.0f, 0.0f, [texture contentSize].width,[texture contentSize].height)];
            [[personaj person] setPositionByIndex:[stateTamagochi bodyCategory]];
            }
            else
            {
                [[[personaj person] getChildByTag:5] removeFromParentAndCleanup:NO];
                [personaj replaceTatooPerson:text];
                [personaj replaceTatooDefault:[[text componentsSeparatedByString:@"_"] objectAtIndex:1]];
            }
            
            
        }
        if ([stateTamagochi bodyCategory]!=12)
            [personaj replaceTexturePerson:[stateTamagochi bodyCategory] obj:text];
        if ([stateTamagochi bodyCategory]==6)
        {
            [personaj replaceTexturePerson:13 obj:[NSString stringWithFormat:@"yeylid_up_%d%@.png",  selColEye,  (IS_IPAD)?@"-iPad":@""]];
            [personaj replaceTexturePerson:14 obj:[NSString stringWithFormat:@"yeylid_down_%d%@.png",  selColEye,  (IS_IPAD)?@"-iPad":@""]];
        }
        if ([stateTamagochi bodyCategory]==4)
        {
            [[[personaj person] getChildByTag:9] removeFromParentAndCleanup:NO];
        }
        else if ([stateTamagochi bodyCategory]==9)
            [[[personaj person] getChildByTag:4] removeFromParentAndCleanup:NO];
        
        if ([stateTamagochi bodyCategory]!=12)
        [self removeFromParentAndCleanup:NO];
        else
        {
            [self createColors];
        }
         [[SimpleAudioEngine sharedEngine] playEffect:@"apply.mp3" loop:NO];
        [_delegate setSelectedColor:[sender tag]];
    }
    else if ([sender tag]!=11)
    {
        
        [sender  removeAllChildrenWithCleanup:YES];
        if ([stateTamagochi bodyCategory]!=12)
        {
            [personaj replaceTexturePerson:[stateTamagochi bodyCategory] obj:([stateTamagochi bodyCategory]==10)?@"rot.png":@""];
            [personaj replaceTextureDefault:[stateTamagochi bodyCategory]];
        }
        else
        {
            [personaj deleteTatoo:[[[personaj tatooShopped] objectAtIndex:indexTatoo] objectAtIndex:0]];
            [[personaj tatooShopped] removeObjectAtIndex:indexTatoo];
            indexTatoo=-1;
        }
    }
    else
    {
        [_delegate getShopping:1];
    }
   
}

#pragma mark touch handler registration

- (void)onEnter {
    [super onEnter];
    [[[CCDirector sharedDirector] touchDispatcher] addTargetedDelegate:self priority:2 swallowsTouches:YES];
}

- (void)onExit {
    [super onExit];
    [[[CCDirector sharedDirector] touchDispatcher] removeDelegate:self];
}

-(BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
     CGPoint location=[[CCDirector sharedDirector] convertToGL:[touch locationInView:touch.view]];
    if (CGRectContainsPoint([personaj rectPerson], location) && [stateTamagochi bodyCategory]==12)
    {
        path=CGPathCreateMutable();
        CGPathMoveToPoint(path, NULL, [[personaj person] getChildByTag:6].position.x+50, [[personaj person] getChildByTag:6].position.y+25);
        CGPathAddLineToPoint(path, NULL, [[personaj person] getChildByTag:6].position.x+120, [[personaj person] getChildByTag:6].position.y+180);
        CGPathAddLineToPoint(path, NULL, [[personaj person] getChildByTag:6].position.x+190, [[personaj person] getChildByTag:6].position.y+25);
        CGPathCloseSubpath(path);
        startPos=location;
    }
    
    if (!CGRectContainsPoint([backColors boundingBox], location) && (!CGRectContainsPoint([personaj rectPerson], location) || [stateTamagochi bodyCategory]!=12))
    {
        
        selectedObject=1;
    }
    return YES;
}

-(void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
    CGPoint location=[[CCDirector sharedDirector] convertToGL:[touch locationInView:touch.view]];
    if (!CGRectContainsPoint([backColors boundingBox], location) && (!CGRectContainsPoint([personaj rectPerson], location) || [stateTamagochi bodyCategory]!=12) && selectedObject==1)
    {
        [personaj replaceTextureDefault:5];
        [self removeFromParentAndCleanup:YES];
        [personaj repositioningTatoo];
    }
    
    for (int i=0;i<nr_colors;i++)
    {
        if (CGRectContainsPoint([buttons[i] boundingBox], location))
        {
            [self touchUpInSide:buttons[i]];
        }
    }
}

-(void)ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event
{
    CGPoint location=[[CCDirector sharedDirector] convertToGL:[touch locationInView:touch.view]];
    
   

    
   
    
    if (CGRectContainsPoint([personaj rectPerson], location))
    {
        if (indexTatoo!=-1)
        {
            int tagObj= [[[[[[personaj tatooShopped] objectAtIndex:indexTatoo] objectAtIndex:0] componentsSeparatedByString:@"_"] objectAtIndex:1] intValue]+21;
            
            
            CGPoint loc1=[[[personaj person] getChildByTag:tagObj] position];
            loc1.x=loc1.x+[[[personaj person] getChildByTag:6] boundingBox].size.width/2;
            loc1.y=loc1.y+[[[personaj person] getChildByTag:6] boundingBox].size.height/2;
            newPoint=loc1;
            CGPoint moveDifference=CGPointZero;
            
            CGPoint loc=[[[personaj person] getChildByTag:3] position];
            loc.x=loc.x+[[[personaj person] getChildByTag:6] boundingBox].size.width/3;
            loc.y=loc.y+[[[personaj person] getChildByTag:6] boundingBox].size.height/4;

            
            moveDifference = ccpSub(location, startPos);
            newPoint=ccpAdd(newPoint, moveDifference);
        
            if (CGPathContainsPoint(path, NULL,newPoint, NO) && !CGRectContainsPoint(CGRectMake(loc.x, loc.y, [[[personaj person] getChildByTag:3] boundingBox].size.width, [[[personaj person] getChildByTag:3] boundingBox].size.height),newPoint))
            {
                [[personaj person] setPositionTatoo:tagObj position:ccpAdd([[[personaj person] getChildByTag:tagObj] position], moveDifference)];
            }
            newPoint=ccpAdd(newPoint, moveDifference);
            startPos=location;

        }
    }
}



@end
