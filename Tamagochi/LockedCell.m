//
//  LockedCell.m
//  Tamagochi
//
//  Created by Sergiu Moțipan on 11/26/13.
//
//

#import "LockedCell.h"

@implementation LockedCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_lockedText release];
    [_waitButton release];
    [_unlockButton release];
    [_titleUnlockBtn release];
    [_titleWaitBtn release];
    [_imgLock release];
    [super dealloc];
}
@end
