//
//  RotNode.h
//  Tamagochi
//
//  Created by Sergiu Moțipan on 2/4/14.
//
//

#import "CCNode.h"
#import "SimpleAudioEngine.h"

@class RotNode;

@protocol RotNodeDelegate
@required
-(float)returnTouchValue;
@end
@interface RotNode : CCNode
{
    id<RotNodeDelegate, NSObject> delegate;
    float totalTime;
    BOOL jos, defaultRot;
    float casca, openMouth;
}
@property (nonatomic, assign) id<RotNodeDelegate, NSObject> delegate;
@property (nonatomic, assign) CCSprite *rotUp;
@property (nonatomic, assign) CCSprite *rotDown;
@property (nonatomic, assign) CCSprite *teethUp;
@property (nonatomic, assign) CCSprite *teethDown;
@property (nonatomic, assign) CCSprite *rot;

-(void)setTexture:(CCTexture2D*)texture;
-(void)setTextureRect:(CGRect)rect;
@end
