//
//  FirstScene.h
//  Tamagochi
//
//  Created by Sergiu Moțipan on 6/4/13.
//
//

#import "CCLayer.h"
#import "cocos2d.h"
#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES2/gl.h>
#import "GLES-Render.h"
#import "PouNode.h"
#import <RotNode.h>
#import "SimpleAudioEngine.h"

#define PTM_RATIO 32

@interface StudioRoom : CCLayer< PouNodeDelegate, RotNodeDelegate>
{
  
  
    CGSize winSize;
    CCSprite *microfon;
    CCLabelTTF *ofOn;
   
      int nr_sounds;
    float touchFun;
    int mouth;
    float disp, ener;
    
    CGPoint startTouchPoint;
    CGPoint endTouchPoint;
    NSDate *date_start;
    
    
}
+(CCScene *) scene;
@end
