//
//  MKStoreManager.m
//
//  Created by Mugunth Kumar on 17-Oct-09.
//  Copyright 2009 Mugunth Kumar. All rights reserved.
//  mugunthkumar.com
//

#import "MKStoreManager.h"


@implementation MKStoreManager

@synthesize purchasableObjects;
@synthesize storeObserver;
@synthesize delegate;

// all your features should be managed one and only by StoreManager
static NSString *featureAId = @"com.razin.paramon.2000coins";
static NSString *featureBId = @"com.razin.paramon.4000coins";
static NSString *featureCId = @"com.razin.paramon.15999coins";
static NSString *featureDId = @"com.razin.paramon.49999coins";
BOOL featureAPurchased;
BOOL featureBPurchased;
BOOL featureCPurchased;
BOOL featureDPurchased;

static MKStoreManager* _sharedStoreManager; // self

- (void)dealloc {
	
	[_sharedStoreManager release];
	[storeObserver release];
	[super dealloc];
}

+ (BOOL) featureAPurchased {
	
	return featureAPurchased;
}

+ (BOOL) featureBPurchased {
	
	return featureBPurchased;
}

+ (MKStoreManager*)sharedManager
{
	@synchronized(self) {
		
        if (_sharedStoreManager == nil) {
			
            [[self alloc] init]; // assignment not done here
			_sharedStoreManager.purchasableObjects = [[NSMutableArray alloc] init];			
			//[_sharedStoreManager requestProductData];
			
			[MKStoreManager loadPurchases];
			_sharedStoreManager.storeObserver = [[MKStoreObserver alloc] init];
			[[SKPaymentQueue defaultQueue] addTransactionObserver:_sharedStoreManager.storeObserver];
        }
    }
    return _sharedStoreManager;
}


#pragma mark Singleton Methods

+ (id)allocWithZone:(NSZone *)zone

{	
    @synchronized(self) {
		
        if (_sharedStoreManager == nil) {
			
            _sharedStoreManager = [super allocWithZone:zone];			
            return _sharedStoreManager;  // assignment and return on first allocation
        }
    }
	
    return nil; //on subsequent allocation attempts return nil	
}


- (id)copyWithZone:(NSZone *)zone
{
    return self;	
}

- (id)retain
{	
    return self;	
}

- (unsigned)retainCount
{
    return UINT_MAX;  //denotes an object that cannot be released
}

- (void)release
{
    //do nothing
}

- (id)autorelease
{
    return self;	
}


- (void) requestProductData
{
	SKProductsRequest *request= [[SKProductsRequest alloc] initWithProductIdentifiers: 
								 [NSSet setWithObjects: featureAId, featureBId, nil]]; // add any other product here
	request.delegate = self;
	[request start];
}


- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    [purchasableObjects removeAllObjects];
	[purchasableObjects addObjectsFromArray:response.products];
    
    if ([purchasableObjects count]>0)
    {
        SKProduct *product = [purchasableObjects objectAtIndex:0];
        NSLog(@"Products Available!");
        [self purchase:product];
		NSLog(@"Feature: %@, Cost: %f, ID: %@",[product localizedTitle],
			  [[product price] doubleValue], [product productIdentifier]);
    }
    else
    {
        [self paymentCanceled];
        NSLog(@"Products Not Available!");
    }
    
	
	[request autorelease];
}

- (IBAction)purchase:(SKProduct *)product{
    SKPayment *payment = [SKPayment paymentWithProduct:product];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}
- (void) buyFeatureA
{
   
    featured=0;
	[self buyFeature:featureAId];
}
- (void) buyFeatureC
{
    featured=2;
	[self buyFeature:featureCId];
}

- (void) buyFeatureD
{
    featured=3;
	[self buyFeature:featureDId];
}

- (void) buyFeature:(NSString*) featureId
{

	if ([SKPaymentQueue canMakePayments])
	{
        SKProductsRequest *productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObject:featureId]];
    
        productsRequest.delegate = self;
        [productsRequest start];
    }
	else
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"MyApp" message:@"You are not authorized to purchase from AppStore"
													   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
	}
}

- (void) buyFeatureB
{
    featured=1;
	[self buyFeature:featureBId];
}

-(void)paymentCanceled
{
    [delegate failed:@""];
}

- (void) failedTransaction: (SKPaymentTransaction *)transaction
{
    [delegate failed:transaction.payment.productIdentifier];
	
	//NSString *messageToBeShown = [NSString stringWithFormat:@"Reason: %@, You can try: %@", [transaction.error localizedFailureReason], [transaction.error localizedRecoverySuggestion]];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unable to complete your purchase" message:@""
												   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
	[alert show];
	[alert release];
}

-(void) provideContent: (NSString*) productIdentifier
{
   
	if([productIdentifier isEqualToString:featureAId])
	{
		featureAPurchased = YES;
        [delegate productAPurchased];
	}

	if([productIdentifier isEqualToString:featureBId])
	{
		featureBPurchased = YES;
        [delegate productBPurchased];
	}
    
    if([productIdentifier isEqualToString:featureCId])
	{
		featureCPurchased = YES;
        [delegate productCPurchased];
	}
	
    if([productIdentifier isEqualToString:featureDId])
	{
		featureDPurchased = YES;
        [delegate productDPurchased];
	}
	[MKStoreManager updatePurchases];
}


+(void) loadPurchases 
{
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];	
	featureAPurchased = [userDefaults boolForKey:featureAId]; 
	featureBPurchased = [userDefaults boolForKey:featureBId]; 	
}


+(void) updatePurchases
{
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	[userDefaults setBool:featureAPurchased forKey:featureAId];
	[userDefaults setBool:featureBPurchased forKey:featureBId];
}
@end
