#import "Game.h"
#import "Main.h"
#import "Highscores.h"


@interface Game (Private)
- (void)initPlatforms;
- (void)initPlatform;
- (void)startGame;
- (void)resetPlatforms;
- (void)resetPlatform;
- (void)resetpoo;
- (void)resetBonus;
- (void)step:(ccTime)dt;
- (void)jump;
- (void)jumpBonus;
- (void)showHighscores;
- (CCSprite*)changePlatformImage:(CCSprite*)platformSprite  withTag:(NSInteger)tag;
- (void)changePlatformXPosition;
- (void)moveBackgroundWith:(float)delta;
- (void)initCoins;
- (void)resetCoins;
- (void)resetCoinWithYPos:(NSInteger)yPosCoin stepByCoin:(float)stepByCoin;
@end


@implementation Game

+ (CCScene *)scene
{
    CCScene *game = [CCScene node];
    
    Game *layer = [Game node];

    [game addChild:layer];
    
    return game;
}

- (id)init {
	if(![super init]) return nil;
    
	gameSuspended = YES;
    forNonIpad = NO;
    bonusAccelerateBackground = 1.0;
    
//	CCSpriteBatchNode *batchNode = (CCSpriteBatchNode *)[self getChildByTag:kSpriteManager];
//    [self initCoins];
	[self initPlatforms];
	
    CCTexture2D *texture = [[CCTextureCache sharedTextureCache] addImage:@"pou.png"];
    CCSprite *spritePoo = [CCSprite spriteWithTexture:texture];
	[self addChild:spritePoo z:4 tag:kPoo];
	
    CCSprite *bonus;
	for(int i=0; i<kNumBonuses; i++) {
		bonus = [CCSprite spriteWithFile:[NSString stringWithFormat:@"%@20.png",MODEL]];
		[self addChild:bonus z:4 tag:kBonusStartTag+i];
		bonus.visible = NO;
	}
	
	CCLabelBMFont *scoreLabel = [CCLabelBMFont labelWithString:@"0" fntFile:@"bitmapFont.fnt"];
	[self addChild:scoreLabel z:5 tag:kScoreLabel];
	scoreLabel.position = ccp(WIDTH_DEVICE/2,HEIGHT_DEVICE-WIDTH_DEVICE/4);

	[self schedule:@selector(step:)];
	
	self.touchEnabled = NO;
	self.accelerometerEnabled = YES;

	[[UIAccelerometer sharedAccelerometer] setUpdateInterval:(1.0 / kFPS)];
	
	[self startGame];
	
	return self;
    
}

- (void)dealloc {
//	NSLog(@"Game::dealloc");
	[super dealloc];
}

- (void)initPlatforms {
//	NSLog(@"initPlatforms");
	
	currentPlatformTag = kPlatformsStartTag;
	while(currentPlatformTag < kPlatformsStartTag + kNumPlatforms) {
		[self initPlatform];
		currentPlatformTag++;
	}
	
	[self resetPlatforms];
}

- (void)initCoins {
    currentCoinTag = kCoinBonusTag;
	while(currentCoinTag < kCoinBonusTag + kOrdCoin) {
        CCSprite *coin = [CCSprite spriteWithFile:[NSString stringWithFormat:@"%@19.png",MODEL]];
        [self addChild:coin z:4 tag:currentCoinTag];
		currentCoinTag++;
	}
	
	[self resetCoins];

}

- (void)initPlatform {
    CCSprite *platform;
    int k = random()%3;
	switch(k) {
		case 0: {platform = [CCSprite spriteWithFile:[NSString stringWithFormat:@"%@1.png",MODEL]];} break;
		case 1: {platform = [CCSprite spriteWithFile:[NSString stringWithFormat:@"%@2.png",MODEL]];} break;
        case 2: {platform = [CCSprite spriteWithFile:[NSString stringWithFormat:@"%@3.png",MODEL]];} break;
	}
    
	[self addChild:platform z:3 tag:currentPlatformTag];
    
}

- (void)startGame {
    isFinish = NO;
    forNonIpad = NO;

    currentBackgroundID = 2;
	score = 0;
	
	[self resetClouds];
	[self resetPlatforms];
	[self resetpoo];
	[self resetBonus];
	
	[[UIApplication sharedApplication] setIdleTimerDisabled:YES];
	gameSuspended = NO;
}

- (void)resetPlatforms {
	currentPlatformY = -1;
	currentPlatformTag = kPlatformsStartTag;
	currentMaxPlatformStep = WIDTH_DEVICE/4;
	currentBonusPlatformIndex = 0;
	currentBonusType = 0;
	platformCount = 0;
	while(currentPlatformTag < kPlatformsStartTag + kNumPlatforms) {
		[self resetPlatform];
		currentPlatformTag++;
	}
}

- (void)resetPlatform {
	if(currentPlatformY < 0) {
		currentPlatformY = 30.0f;
	} else {
		currentPlatformY += random() % (int)(currentMaxPlatformStep - kMinPlatformStep) + kMinPlatformStep;
		if(currentMaxPlatformStep < kMaxPlatformStep+(score * 0.00001)) {
			currentMaxPlatformStep += 0.5f;
		}
	}
    
	CCSprite *platform = (CCSprite*)[self getChildByTag:currentPlatformTag];
    platform = [self changePlatformImage:platform withTag:currentPlatformTag];
	if(random()%2==0) {
        platform.scaleX = -1.0f;
    }else{
        platform.scaleX = 1.0f;
    }
	float x;
	CGSize size = platform.contentSize;
	if(currentPlatformY == 30.0f) {
		x = WIDTH_DEVICE/2;
	} else {
		x = random() % ((int)WIDTH_DEVICE -(int)size.width) + size.width/2;
	}
//	x = WIDTH_DEVICE/2;
	platform.position = ccp(x,currentPlatformY);
	platformCount++;
	if(platformCount == currentBonusPlatformIndex) {
		CCSprite *bonus = (CCSprite*)[self getChildByTag:kBonusStartTag+currentBonusType];
		bonus.position = ccp(x,currentPlatformY+(bonus.contentSize.height*2));
		bonus.visible = YES;
	}
}

//- (void)resetCoins{
//	currentCoinTag = kCoinBonusTag;
//	float stepByCoin = WIDTH_DEVICE/10;
//    
//    while(currentCoinTag < kCoinBonusTag + kOrdCoin) {
//		[self resetCoinWithStep:stepByCoin];
//		currentCoinTag++;
//	}
//    
//}
//
//- (void)resetCoinWithStep:(float)stepByCoin {
//
//	if(yPosCoin < 0) {
//		yPosCoin = 30.0f;
//	} else {
//		yPosCoin += random() % (int)(stepByCoin - kMinPlatformStep) + kMinPlatformStep;
//		if(stepByCoin < kMaxPlatformStep+(score * 0.00001)) {
//            stepByCoin += 0.5f;
//		}
//	}
//    
//	CCSprite *coin = (CCSprite*)[self getChildByTag:currentCoinTag];
//	float x;
//	CGSize size = coin.contentSize;
//	if(yPosCoin == 30.0f) {
//		x = WIDTH_DEVICE/2;
//	} else {
//		x = random() % ((int)WIDTH_DEVICE -(int)size.width) + size.width/2;
//	}
//	coin.position = ccp(x,yPosCoin);
//    NSLog(@"%d:%d, => %f : %f",coin.tag ,currentCoinTag ,coin.position.x ,coin.position.y);
//}

- (CCSprite*)changePlatformImage :(CCSprite*)platformSprite  withTag:(NSInteger)tag{
    int k =0;
    if (currentBackgroundID >= 2 && currentBackgroundID <4 ) {
        if (random()%2==1) {
            k=5;
        }
        else{
            k=4;
        }
    }else if (currentBackgroundID >= 4 && currentBackgroundID <5){
        switch (random()%3) {
            case 0:
                k=6;
                break;
            case 1:
                k=7;
                break;
            case 2:
                k=8;
                break;
        }
    }else if (currentBackgroundID >= 5 && currentBackgroundID <6){
                k=9;
    }else if (currentBackgroundID >= 6 && currentBackgroundID <8){
        switch (random()%3) {
            case 0:
                k=10;
                break;
            case 1:
                k=11;
                break;
            case 2:
                k=12;
                break;
        }
    }else if (currentBackgroundID >= 8 && currentBackgroundID <9){
        switch (random()%4) {
            case 0:
                k=13;
                break;
            case 1:
                k=14;
                break;
            case 2:
                k=15;
                break;
            case 3:
                k=16;
                break;
        }
    }else{
        switch (random()%2) {
            case 0:
                k=17;
                break;
            case 1:
                k=18;
                break;
        }
    }
    if (currentBackgroundID >=3) {
 
        
        CCTexture2D *texture = [[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"%@%d.png",MODEL,k]];
        CCSprite *spritePlatform = [CCSprite spriteWithTexture:texture];
        spritePlatform.position = platformSprite.position;
        
        [self removeChild:platformSprite cleanup:YES];
        [self addChild:spritePlatform z:3 tag:tag];

        return spritePlatform;
    }
    return platformSprite;
}
- (void)resetpoo {
    currentBackgroundID = 2;
//	NSLog(@"resetpoo");

//	CCSpriteBatchNode *batchNode = (CCSpriteBatchNode*)[self getChildByTag:kSpriteManager];
	CCSprite *poo = (CCSprite*)[self getChildByTag:kPoo];
	
	poo_pos.x = WIDTH_DEVICE/2;
	poo_pos.y = WIDTH_DEVICE/2;
	poo.position = poo_pos;
	
	poo_vel.x = 0;
	poo_vel.y = 0;
	
	poo_acc.x = 0;
	poo_acc.y = -550.0f;
	
	pooLookingRight = YES;
	poo.scaleX = 1.0f;
}

- (void)resetBonus {

	CCSprite *bonus = (CCSprite*)[self getChildByTag:kBonusStartTag+currentBonusType];
	bonus.visible = NO;
	currentBonusPlatformIndex += random() % (kMaxBonusStep - kMinBonusStep) + kMinBonusStep;

	if(score < 10000) {
        currentBonusType = random() % 3;
	} else if(score < 50000) {
		currentBonusType = random() % 2;
	} else if(score < 100000) {
		currentBonusType = random() % 3;
	} else {
		currentBonusType = random() % 2 + 2;
	}
}



- (void)step:(ccTime)dt {
	[super step:dt];
	
	if(gameSuspended) return;

//	CCSpriteBatchNode *batchNode = (CCSpriteBatchNode*)[self getChildByTag:kSpriteManager];
	CCSprite *poo = (CCSprite*)[self getChildByTag:kPoo];
	
	poo_pos.x += poo_vel.x * dt;
	
	if(poo_vel.x < -30.0f && pooLookingRight) {
		pooLookingRight = NO;
		poo.scaleX = -1.0f;
	} else if (poo_vel.x > 30.0f && !pooLookingRight) {
		pooLookingRight = YES;
		poo.scaleX = 1.0f;
	}

	CGSize poo_size = poo.contentSize;
	float max_x = WIDTH_DEVICE-poo_size.width/2;
	float min_x = 0+poo_size.width/2;
	
	if(poo_pos.x>max_x) poo_pos.x = max_x;
	if(poo_pos.x<min_x) poo_pos.x = min_x;
	
	poo_vel.y += poo_acc.y * dt;
	poo_pos.y += poo_vel.y * dt;
	
	CCSprite *bonus = (CCSprite*)[self getChildByTag:kBonusStartTag+currentBonusType];
	if(bonus.visible) {
		CGPoint bonus_pos = bonus.position;
		float range = 20.0f;
		if(poo_pos.x > bonus_pos.x - range && poo_pos.x < bonus_pos.x + range && poo_pos.y > bonus_pos.y - range && poo_pos.y < bonus_pos.y + range) {
            
            CCTexture2D *texture = nil;
			switch(currentBonusType) {
				case kBonus5:{  score += 5000; texture =[[CCTextureCache sharedTextureCache] addImage:@"poo.png"];}  break;
				case kBonus10:{ score += 10000; texture =[[CCTextureCache sharedTextureCache] addImage:@"aPoo.png"];} break;
				case kBonus50:{ score += 50000; texture =[[CCTextureCache sharedTextureCache] addImage:@"pink.png"];} break;
				case kBonus100:{ score += 100000; texture= [[CCTextureCache sharedTextureCache] addImage:@"new_iph20.png"];} break;
			}
            [self jumpBonus];
            isJump = YES;
            
            [(CCSprite *)[self getChildByTag:kPoo] setTexture:texture];
     
            
			NSString *scoreStr = [NSString stringWithFormat:@"%d",score];
			CCLabelBMFont *scoreLabel = (CCLabelBMFont*)[self getChildByTag:kScoreLabel];
			[scoreLabel setString:scoreStr];
			id a1 = [CCScaleTo actionWithDuration:0.2f scaleX:1.5f scaleY:0.8f];
			id a2 = [CCScaleTo actionWithDuration:0.2f scaleX:1.0f scaleY:1.0f];
			id a3 = [CCSequence actions:a1,a2,a1,a2,a1,a2,nil];
			[scoreLabel runAction:a3];
			[self resetBonus];
		}
	}
	
	int t;
	
	if(poo_vel.y < 0) {
		
		for(t= kPlatformsStartTag; t < kPlatformsStartTag + kNumPlatforms; t++) {
			CCSprite *platform = (CCSprite*)[self getChildByTag:t];

			CGSize platform_size = platform.contentSize;
			CGPoint platform_pos = platform.position;
			
			max_x = platform_pos.x - platform_size.width/2 - 10;
			min_x = platform_pos.x + platform_size.width/2 + 10;
			float min_y = platform_pos.y + (platform_size.height+poo_size.height)/2 - kPlatformTopPadding;
			
			if(poo_pos.x > max_x &&
			   poo_pos.x < min_x &&
			   poo_pos.y > platform_pos.y &&
			   poo_pos.y < min_y) {
				[self jump];
			}
		}
		
		if(poo_pos.y < -poo_size.height/2) {
			[self showHighscores];
		}
		
        if (isJump) {
            CCSprite *poo = (CCSprite *)[self getChildByTag:kPoo];
            CCTexture2D *texture = [[CCTextureCache sharedTextureCache] addImage:@"pou.png"];
            [poo setTexture:texture];
            
        }
        isJump = NO;

	} else if(poo_pos.y > HEIGHT_DEVICE/2) {
		float delta = poo_pos.y - HEIGHT_DEVICE/2;
        if (isFinish) {
            poo_pos.y += 0.000001;
            if (poo_pos.y> HEIGHT_DEVICE- WIDTH_DEVICE/2) {
                gameSuspended = YES;
                CCLabelTTF *labelWin = [CCLabelTTF labelWithString:@"YOU ARE WINNER!" fontName:@"Marker Felt" fontSize:40];
                [self addChild:labelWin z:5 tag:112233];
                labelWin.position = ccp(WIDTH_DEVICE/2,HEIGHT_DEVICE/2);
                while(currentPlatformTag < kPlatformsStartTag + kNumPlatforms) {
                   CCSprite *spritePlatform = (CCSprite *)[self getChildByTag:currentPlatformTag];
                    [self removeChild:spritePlatform cleanup:YES];
                    currentPlatformTag++;
                }
//                [self showHighscores];
            }
        }else{
            poo_pos.y = HEIGHT_DEVICE/2;
            [self moveBackgroundWith:delta];
        }
		currentPlatformY -= delta;
		
        for(t= kPlatformsStartTag; t < kPlatformsStartTag + kNumPlatforms; t++) {
			CCSprite *platform = (CCSprite*)[self getChildByTag:t];
			CGPoint pos = platform.position;
			pos = ccp(pos.x,pos.y - delta);
            
			if(pos.y < - platform.contentSize.height/2) {
				currentPlatformTag = t;
				[self resetPlatform]; 
			} else {
				platform.position = pos;
			}
		}
		
		if(bonus.visible) {
			CGPoint pos = bonus.position;
			pos.y -= delta;
			if(pos.y < -bonus.contentSize.height/2) {
				[self resetBonus];
			} else {
				bonus.position = pos;
			}
		}
		
		score += (int)delta;

		NSString *scoreStr = [NSString stringWithFormat:@"%d",score];

		CCLabelBMFont *scoreLabel = (CCLabelBMFont*)[self getChildByTag:kScoreLabel];
		[scoreLabel setString:scoreStr];
	}
    
// mishcarea platformelor pe axa x
    [self changePlatformXPosition];
    
//trecerea lui Poo prin perete
    if (currentBackgroundID > 8) {
        if (poo_pos.x < 0 + poo_size.width/2+1) {
            poo_pos.x = WIDTH_DEVICE-poo_size.width/2-1;
        }else if (poo_pos.x > WIDTH_DEVICE-poo_size.width/2-1){
            poo_pos.x = poo_size.width/2+1;
        }
    }

	poo.position = poo_pos;
    
}

- (void)moveBackgroundWith:(float)delta{
    CCSprite *background = (CCSprite *)[self getChildByTag:kBackgroundFirst];
    CCSprite *background1 = (CCSprite *)[self getChildByTag:kBackgroundFirst+1];

    CGPoint positionBackgr = background.position;
    CGPoint positionBackgr1 = background1.position;
    
    positionBackgr.y -= delta * 0.009 * bonusAccelerateBackground;
    positionBackgr1.y -= delta * 0.009 * bonusAccelerateBackground;
    if (positionBackgr.y < -512) {
        if (currentBackgroundID < 10) {
            currentBackgroundID++;
            CCTexture2D *texture = [[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"_%@%d.png",MODEL,currentBackgroundID]];
            [background setTexture:texture];
            background.position = CGPointMake(WIDTH_DEVICE/2,1024+((1024-2)/2));
            background1.position = CGPointMake(WIDTH_DEVICE/2,1024/2);

        }else{
            if (IS_IPAD) {
                isFinish = YES;
            }else{
                forNonIpad = YES;
                CCTexture2D *texture = [[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"_%@%d.png",MODEL,currentBackgroundID]];
                [background setTexture:texture];
                background.position = CGPointMake(WIDTH_DEVICE/2,1024+((1024-2)/2));
                background1.position = CGPointMake(WIDTH_DEVICE/2,1024/2);
            }

        }
    }else if (positionBackgr1.y < -512) {
        if (currentBackgroundID < 10) {
            currentBackgroundID++;
            CCTexture2D *texture = [[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"_%@%d.png",MODEL,currentBackgroundID]];
            [background1 setTexture:texture];
            background1.position = CGPointMake(WIDTH_DEVICE/2,1024+((1024-2)/2));
            background.position = CGPointMake(WIDTH_DEVICE/2,1024/2);
        }else{
            if (IS_IPAD) {
                isFinish = YES;
                
            }else{
                forNonIpad = YES;
                CCTexture2D *texture = [[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"_%@%d.png",MODEL,currentBackgroundID]];
                [background setTexture:texture];
                background.position = CGPointMake(WIDTH_DEVICE/2,1024+((1024-2)/2));
                background1.position = CGPointMake(WIDTH_DEVICE/2,1024/2);
            }
        }
    }else{
        if (positionBackgr.y > positionBackgr1.y) {
            background.position = positionBackgr;
            background1.position = positionBackgr1;
        }else{
            background1.position = positionBackgr1;
            background.position = positionBackgr;
        }
    if ( forNonIpad && (positionBackgr1.y < (HEIGHT_DEVICE-512))){
        isFinish = YES;
    }
    }
}



- (void)changePlatformXPosition{
    if (currentBackgroundID >= 4) {
        for(int t= kPlatformsStartTag; t < kPlatformsStartTag + kNumPlatforms; t++) {
            CCSprite *platform = (CCSprite*)[self getChildByTag:t];
                CGPoint posPlatform = platform.position;
            if (posPlatform.x <= HEIGHT_DEVICE) {
                int k = random()% (20-t) + t;
                if (currentBackgroundID >=9) {
                    if (platform.scaleX < 0) {
                        posPlatform.x += 0.01f * k ;
                    }else {
                        posPlatform.x -= 0.01f * k;
                    }

                }else if (currentBackgroundID >8) {
                    if (platform.scaleX < 0) {
                        posPlatform.x += 0.0019f * k;
                    }else {
                        posPlatform.x -= 0.0019f * k;
                    }
                }else if (currentBackgroundID >6){
                    if (platform.scaleX < 0) {
                        posPlatform.x += 0.001f * k;
                    }else {
                        posPlatform.x -= 0.001f * k;
                    }
                }else{
                    posPlatform.x += 0.0008f * k;
                }

                if (currentBackgroundID >= 5 && currentBackgroundID <7) {
                    if(posPlatform.x > (WIDTH_DEVICE-platform.contentSize.width/2)) {
                        platform.scaleX = 1;
                        
                    }
                    if (posPlatform.x < platform.contentSize.width/2) {
                        platform.scaleX = -1;
                    }
                    posPlatform.y -=0.5;
                }
                
                if(posPlatform.x > WIDTH_DEVICE ) {
                    posPlatform.x = 0;
                }
                if (posPlatform.x < 0) {
                    posPlatform.x = WIDTH_DEVICE;
                }
                platform.position = posPlatform;
            }
        }
    }
}

//- (void)setViewPointCenter:(CGPoint) position {
//
//    CGSize winSize = [CCDirector sharedDirector].winSize;
//    
//    int x = MAX(position.x, winSize.width/2);
//    int y = MAX(position.y, winSize.height/2);
//    x = MIN(x, (_tileMap.mapSize.width * _tileMap.tileSize.width) - winSize.width / 2);
//    y = MIN(y, (_tileMap.mapSize.height * _tileMap.tileSize.height) - winSize.height/2);
//    CGPoint actualPosition = ccp(x, y);
//    
//    CGPoint centerOfView = ccp(winSize.width/2, winSize.height/2);
//    CGPoint viewPoint = ccpSub(centerOfView, actualPosition);
//    self.position = viewPoint;
//}


- (void)jump {
    bonusAccelerateBackground = 1.0;
	poo_vel.y = (HEIGHT_DEVICE-WIDTH_DEVICE/2) + fabsf(poo_vel.x);
}

- (void)jumpBonus {
    switch(currentBonusType) {
        case kBonus5: {
            poo_vel.y = 2*(HEIGHT_DEVICE-WIDTH_DEVICE/2)+ fabsf(poo_vel.x+2);
            bonusAccelerateBackground = 3;
        }
            break;
        case kBonus10: {
            poo_vel.y = 3*(HEIGHT_DEVICE-WIDTH_DEVICE/2)+ fabsf(poo_vel.x+2);
            bonusAccelerateBackground = 4;
        }
            break;
        case kBonus50:{
            poo_vel.y = 4*(HEIGHT_DEVICE-WIDTH_DEVICE/2)+ fabsf(poo_vel.x+2);
            bonusAccelerateBackground = 6;
        }
            break;
        case kBonus100: {
            poo_vel.y = 5*(HEIGHT_DEVICE-WIDTH_DEVICE/2)+ fabsf(poo_vel.x+2);
            bonusAccelerateBackground = 7;
        }
            break;
    }
}

- (void)showHighscores {
	gameSuspended = YES;
	[[UIApplication sharedApplication] setIdleTimerDisabled:NO];
	
	[[CCDirector sharedDirector] replaceScene:
     [CCTransitionFade transitionWithDuration:1 scene:[Highscores sceneWithScore:score] withColor:ccWHITE]];
}


- (void)accelerometer:(UIAccelerometer*)accelerometer didAccelerate:(UIAcceleration*)acceleration {
	if(gameSuspended) return;
	float accel_filter = 0.1f;
	poo_vel.x = poo_vel.x * accel_filter + acceleration.x * (1.0f - accel_filter) * (WIDTH_DEVICE * 3);
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
//	NSLog(@"alertView:clickedButtonAtIndex: %i",buttonIndex);

	if(buttonIndex == 0) {
		[self startGame];
	} else {
		[self startGame];
	}
}

@end
