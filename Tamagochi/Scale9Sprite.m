//
//  Scale9Sprite.m
//  Funglish
//
//  Created by Mohit Sud on 12-05-23.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "Scale9Sprite.h"

@implementation Scale9Sprite

@synthesize opacity, color;

enum positions {
    pCentre = 0,
    pTop,
    pLeft,
    pRight,
    pBottom,
    pTopRight,
    pTopLeft,
    pBottomRight,
    pBottomLeft
};

CGSize baseSize;
CGRect resizableRegion;

-(id) initWithFile:(NSString*)file centreRegion:(CGRect)centreRegion {
    
    if( (self=[super init]) ) {
        
        scale9Image = [[CCSpriteBatchNode alloc] initWithFile:file capacity:9];
        CGSize imageSize = scale9Image.texture.contentSize;
        
        NSLog(@">>>>>>>>> Image size %0.2f x %0.2f",imageSize.width,imageSize.height);
        //Set up centre sprite
        centre = [[CCSprite alloc] initWithTexture:scale9Image.texture rect:centreRegion];
        [scale9Image addChild:centre z:0 tag:pCentre];
        
        //top
        top = [[CCSprite alloc]
               initWithTexture:scale9Image.texture
               rect:CGRectMake(centreRegion.origin.x,
                               0,
                               centreRegion.size.width,
                               centreRegion.origin.y)
               ];
        
        [scale9Image addChild:top z:1 tag:pTop];
        
        //bottom
        bottom = [[CCSprite alloc]
                  initWithTexture:scale9Image.texture
                  rect:CGRectMake(centreRegion.origin.x,
                                  centreRegion.origin.y + centreRegion.size.height,
                                  centreRegion.size.width,
                                  imageSize.height - (centreRegion.origin.y + centreRegion.size.height))
                  ];
        
        [scale9Image addChild:bottom z:1 tag:pBottom];
        
        //left
        left = [[CCSprite alloc]
                initWithTexture:scale9Image.texture
                rect:CGRectMake(0,
                                centreRegion.origin.y,
                                centreRegion.origin.x,
                                centreRegion.size.height)
                ];
        
        [scale9Image addChild:left z:1 tag:pLeft];
        
        //right
        right = [[CCSprite alloc]
                 initWithTexture:scale9Image.texture
                 rect:CGRectMake(centreRegion.origin.x + centreRegion.size.width,
                                 centreRegion.origin.y,
                                 imageSize.width - (centreRegion.origin.x + centreRegion.size.width),
                                 centreRegion.size.height)
                 ];
        
        [scale9Image addChild:right z:1 tag:pRight];
        
        //top left
        topLeft = [[CCSprite alloc]
                   initWithTexture:scale9Image.texture
                   rect:CGRectMake(0,
                                   0,
                                   centreRegion.origin.x,
                                   centreRegion.origin.y)
                   ];
        
        [scale9Image addChild:topLeft z:2 tag:pTopLeft];
        
        //top right
        topRight = [[CCSprite alloc]
                    initWithTexture:scale9Image.texture
                    rect:CGRectMake(centreRegion.origin.x + centreRegion.size.width,
                                    0,
                                    imageSize.width - (centreRegion.origin.x + centreRegion.size.width),
                                    centreRegion.origin.y)
                    ];
        
        [scale9Image addChild:topRight z:2 tag:pTopRight];
        
        //bottom left
        bottomLeft = [[CCSprite alloc]
                      initWithTexture:scale9Image.texture
                      rect:CGRectMake(0,
                                      centreRegion.origin.y + centreRegion.size.height,
                                      centreRegion.origin.x,
                                      imageSize.height - (centreRegion.origin.y + centreRegion.size.height))
                      ];
        
        [scale9Image addChild:bottomLeft z:2 tag:pBottomLeft];
        
        //bottom right
        bottomRight = [[CCSprite alloc]
                       initWithTexture:scale9Image.texture
                       rect:CGRectMake(centreRegion.origin.x + centreRegion.size.width,
                                       centreRegion.origin.y + centreRegion.size.height,
                                       imageSize.width - (centreRegion.origin.x + centreRegion.size.width),
                                       imageSize.height - (centreRegion.origin.y + centreRegion.size.height))
                       ];
        
        [scale9Image addChild:bottomRight z:2 tag:pBottomRight];
        baseSize = imageSize;
        resizableRegion = centreRegion;
        [self setContentSize:imageSize];
        [self addChild:scale9Image];
    }
    return self;
}

-(void) dealloc
{
    [topLeft release];
    [top release];
    [topRight release];
    
    ;
    [centre release];
    
    ;
    [bottomLeft release];
    [bottom release];
    [bottomRight release];
    [scale9Image release];
    [super dealloc];
}

-(void) setContentSize:(CGSize)size
{
    
    [super setContentSize:size];
    
    float sizableWidth = size.width - topLeft.contentSize.width - topRight.contentSize.width;
    float sizableHeight = size.height - topLeft.contentSize.height - bottomRight.contentSize.height;
    float horizontalScale = sizableWidth/centre.contentSize.width;
    float verticalScale = sizableHeight/centre.contentSize.height;
    
    centre.scaleX = horizontalScale;
    centre.scaleY = verticalScale;
    
    float rescaledWidth = centre.contentSize.width * horizontalScale;
    float rescaledHeight = centre.contentSize.height * verticalScale;
    
    //Position corners
    //[self setAnchorPoint:CGPointMake(0.5f,0.5f)];
    
    float despx = size.width*0.5f;
    float despy = size.height*0.5f;
    
    //Position corners
    [topLeft setPosition:CGPointMake(-rescaledWidth/2 - topLeft.contentSize.width/2 +despx, rescaledHeight/2 + topLeft.contentSize.height*0.5 + despy) ];
    
    [topRight setPosition:CGPointMake(rescaledWidth/2 + topRight.contentSize.width/2 +despx, rescaledHeight/2 + topRight.contentSize.height*0.5 + despy)];
    [bottomLeft setPosition:CGPointMake(-rescaledWidth/2 - bottomLeft.contentSize.width/2 + despx, -rescaledHeight/2 - bottomLeft.contentSize.height*0.5 + despy)];
    [bottomRight setPosition:CGPointMake(rescaledWidth/2 + bottomRight.contentSize.width/2 + despx, -rescaledHeight/2 + -bottomRight.contentSize.height*0.5 + despy)];
    top.scaleX = horizontalScale;
    [top setPosition:CGPointMake(0+despx,rescaledHeight/2 + topLeft.contentSize.height*0.5 + despy)];
    bottom.scaleX = horizontalScale;
    [bottom setPosition:CGPointMake(0+despx,-rescaledHeight/2 - bottomLeft.contentSize.height*0.5 + despy)];
    left.scaleY = verticalScale;
    
    
    right.scaleY = verticalScale;
    
    ;
    [centre setPosition:CGPointMake(despx, despy)];
    
}

-(void) draw {
    [scale9Image draw];
}

@end