//
//  Ball.h
//  Tennis
//
//  Created by Sergiu Motipan on 10/22/12.
//
//

#import "CCLayer.h"
#import "cocos2d.h"
#import "Box2D.h"
#import "GLES-Render.h"
#import "SimpleAudioEngine.h" 
#import "CCActionManager.h"
// Add to top of file
#import "MyContactListener.h"

// Add inside @interface



#define PTM_RATIO 32
#define k_bufferSize 10

@class Ball;

@protocol BallDelegate
@required
-(void)changePosition:(CGPoint)pos;
@end
@interface Ball : CCLayer
{
    MyContactListener *_contactListener;
    id<BallDelegate, NSObject> delegate;
    b2Body* _groundBody;
    b2World *_world;
    b2Body * ballBody;
    b2Fixture *_bottomFixture;
    b2Fixture *_leftFixture;
    b2Fixture *_rightFixture;
    b2Fixture *_topFixture;
    b2Fixture *_ballFixture;

    b2MouseJoint *_mouseJoint;
   
    CCSprite *ball;
    CCSprite *morisca;
    CCSprite *morisca_spate;

    b2AABB aabb;
    GLESDebugDraw *m_debugDraw;
    CCAnimation *animation;
    CCSprite *umbra;
    CGSize s;
    int k;
    float startRez;
    ALuint soundBack;
    float startFun;
}
@property (nonatomic, assign) id<BallDelegate, NSObject> delegate;
@property (nonatomic, assign) b2Body * ballBody;
@property (nonatomic, assign) BOOL start;
+(CCScene *)scene;
-(void)initPhysic;
-(void)stopHelicopter;
@end
