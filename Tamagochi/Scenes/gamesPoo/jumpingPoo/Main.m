#import "Main.h"
#import <mach/mach_time.h>

#define RANDOM_SEED() srandom((unsigned)(mach_absolute_time() & 0xFFFFFFFF))

@interface Main(Private)
- (void)initClouds;
- (void)initCloud;
@end


@implementation Main

- (id)init {
//	NSLog(@"Main::init");
	
	if(![super init]) return nil;

    
	RANDOM_SEED();
    
	CCSpriteBatchNode *batchNode = [CCSpriteBatchNode batchNodeWithFile:@"sprites.png" capacity:10];
	[self addChild:batchNode z:-1 tag:kSpriteManager];


 
    CCSprite *background, *background1 = nil;
    CCTexture2D *texture = [[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"_%@1.png", MODEL]];
    CCTexture2D *texture1 = [[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"_%@2.png", MODEL]];
//    if (IS_IPAD) {
    NSLog(@"%f - %f   %@", WIDTH_DEVICE, HEIGHT_DEVICE, MODEL);
        background1 = [CCSprite spriteWithTexture:texture1 rect:CGRectMake(0, 0, WIDTH_DEVICE, 1024)];
        background1.position = CGPointMake(WIDTH_DEVICE/2.0, 1024+((1024-2)/2));
        background = [CCSprite spriteWithTexture:texture rect:CGRectMake(0, 0, WIDTH_DEVICE , 1024)];
        background.position = CGPointMake(WIDTH_DEVICE/2.0,1024/2);
//    }else{
//        if (IS_FOUR_INCH) {
//            background1 = [CCSprite spriteWithTexture:texture1 rect:CGRectMake(0, 0, WIDTH_DEVICE, 1024)];
//            background1.position = CGPointMake(WIDTH_DEVICE/2.0, HEIGHT_DEVICE+((HEIGHT_DEVICE-2)/2));
//            background = [CCSprite spriteWithTexture:texture rect:CGRectMake(0, 0, WIDTH_DEVICE , 1024)];
//            background.position = CGPointMake(WIDTH_DEVICE/2.0,HEIGHT_DEVICE/2);
//        }else{
//            background1 = [CCSprite spriteWithTexture:texture1 rect:CGRectMake(0, 0, WIDTH_DEVICE, 1024)];
//            background1.position = CGPointMake(WIDTH_DEVICE/2.0, HEIGHT_DEVICE+((HEIGHT_DEVICE-2)/2));
//            background = [CCSprite spriteWithTexture:texture rect:CGRectMake(0, 0, WIDTH_DEVICE , 1024)];
//            background.position = CGPointMake(WIDTH_DEVICE/2.0,HEIGHT_DEVICE/2);
//        }
//    }
    
//    background = [CCSprite spriteWithFile:@"1.jpg"];

	[self addChild:background z:-2 tag:kBackgroundFirst];
    [self addChild:background1 z:-2 tag:kBackgroundFirst+1];


//	[self initClouds];
//	[self schedule:@selector(step:)];
	
	return self;
}

- (void)dealloc {
    
	[super dealloc];
}

- (void)initClouds {
	NSLog(@"initClouds");
	
	currentCloudTag = kCloudsStartTag;
	while(currentCloudTag < kCloudsStartTag + kNumClouds) {
		[self initCloud];
		currentCloudTag++;
	}

	[self resetClouds];
}

- (void)initCloud {
	CGRect rect;
	switch(random()%3) {
		case 0: rect = CGRectMake(336,16,256,108); break;
		case 1: rect = CGRectMake(336,128,257,110); break;
		case 2: rect = CGRectMake(336,240,252,119); break;
	}	
	
	CCSpriteBatchNode *batchNode = (CCSpriteBatchNode*)[self getChildByTag:kSpriteManager];
	CCSprite *cloud = [CCSprite spriteWithTexture:[batchNode texture] rect:rect];
	[batchNode addChild:cloud z:3 tag:currentCloudTag];
	cloud.opacity = 128;
}

- (void)resetClouds {
//	NSLog(@"resetClouds");
	
	currentCloudTag = kCloudsStartTag;
	
	while(currentCloudTag < kCloudsStartTag + kNumClouds) {
		[self resetCloud];

		CCSpriteBatchNode *batchNode = (CCSpriteBatchNode*)[self getChildByTag:kSpriteManager];
		CCSprite *cloud = (CCSprite*)[batchNode getChildByTag:currentCloudTag];
		CGPoint pos = cloud.position;
		pos.y -= 480.0f;
		cloud.position = pos;
		
		currentCloudTag++;
	}
}

- (void)resetCloud {
	
	CCSpriteBatchNode *batchNode = (CCSpriteBatchNode*)[self getChildByTag:kSpriteManager];
	CCSprite *cloud = (CCSprite*)[batchNode getChildByTag:currentCloudTag];
	
	float distance = random()%20 + 5;
	
	float scale = 5.0f / distance;
	cloud.scaleX = scale;
	cloud.scaleY = scale;
	if(random()%2==1) cloud.scaleX = -cloud.scaleX;
	
	CGSize size = cloud.contentSize;
	float scaled_width = size.width * scale;
	float x = random()%(320+(int)scaled_width) - scaled_width/2;
	float y = random()%(480-(int)scaled_width) + scaled_width/2 + 480;
	
	cloud.position = ccp(x,y);
}

- (void)step:(ccTime)dt {
//	CCSpriteBatchNode *batchNode = (CCSpriteBatchNode*)[self getChildByTag:kSpriteManager];
//    
//    int t;
//	for(t= kCloudsStartTag; t < kCloudsStartTag + kNumClouds; t++) {
//		CCSprite *cloud = (CCSprite*)[batchNode getChildByTag:t];
//		CGPoint pos = cloud.position;
//		CGSize size = cloud.contentSize;
//		pos.x += 0.2f * cloud.scaleY;
//		if(pos.x > 320 + size.width/2) {
//            pos.x = -size.width/2;
//		}
//		cloud.position = pos;
//	}
}

@end
