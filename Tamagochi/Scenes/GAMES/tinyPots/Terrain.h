//
//  Terrain.h
//  Tamagochi
//
//  Created by iVanea! on 9/26/13.
//
//

#import "CCNode.h"
#import "cocos2d.h"
#import "Box2D.h"
#import "Constants.h"


#define kCoinTag 100

enum {
    kCoinStep = 30,
    kCoinOrd = 30
};

@interface Terrain : CCNode{
    float _offsetXf;
}

@property (retain, nonatomic) CCSprite * stripes;
@property (nonatomic, assign) float offsetXf;
@property (readonly) CGPoint theDistanceOfWay;
@property (readonly) uint_fast16_t coinBank;

- (void) setOffsetX:(float)newOffsetX Y:(float)y;

- (id)initWithWorld:(b2World *)world withStageID:(uint_fast8_t)idStage withBoss:(BOOL)isBoss;
- (void) coinCollisionWithBodyX:(float)bodyX Y:(float)bodyY;

- (BOOL) isGoodPositionToDive:(CGPoint)enemyPos indexHill:(int8)indexHill;
- (BOOL) isTimeToStopAtPosition:(CGPoint)enemyPos;
- (BOOL) isEndOfWay;
- (BOOL) isTimeToChangeBackgr;
- (BOOL) increaseVelocity;
- (void) resetTerain;
- (void) selectWayFromStageID:(uint_fast8_t)idStage withBoss:(BOOL)isBoss;

@end