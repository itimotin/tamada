//
//  Hero.h
//  Tamagochi
//
//  Created by iVanea! on 9/26/13.
//
//

#import "CCSprite.h"
#import "Box2D.h"
#import "PouNode.h"
#import "Constants.h"

#define kPerfectTakeOffVelocityY 2.0f

@class TinyPoo;
class HeroContactListener;

@interface Hero : CCSprite<PouNodeDelegate>{
    TinyPoo *_game;
    HeroContactListener *_contactListener;
    uint_fast8_t _nPerfectSlides;
}

@property (nonatomic, retain) TinyPoo *game;
@property (readonly) BOOL awake;
@property (readonly) uint_fast8_t nPerfectSlides;
@property (readwrite) uint_fast8_t winPosition;


- (void)update;
+ (id) heroWithGame:(TinyPoo*)game;
- (id) initWithGame:(TinyPoo*)game;

- (void) resetVelocityY;

- (void) removeHero;
- (void) pouRealSize;

- (void)wake;
- (void)sleep;

- (void) dive;
- (void)diveForce;
- (void)limitVelocity:(int)velocity;
- (void) wakeContinue;
- (void)hit;
- (void)resetHero;
- (void)setWinnerPosition:(int_fast8_t)winnerPosition;
- (void) resetWithPosition:(CGPoint)bodyPosition;
@end
