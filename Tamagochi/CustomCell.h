//
//  CustomCell.h
//  Tamagochi
//
//  Created by Sergiu Moțipan on 9/2/13.
//
//

#import <UIKit/UIKit.h>

@interface CustomCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UIImageView *gameImage;
@property (retain, nonatomic) IBOutlet UILabel *nameGame;
@property (retain, nonatomic) IBOutlet UIButton *btnGame;

@end
