//
//  NextLevCell.m
//  Tamagochi
//
//  Created by Sergiu Moțipan on 1/10/14.
//
//

#import "NextLevCell.h"

@implementation NextLevCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_labImg release];
    [_name release];
    [super dealloc];
}
@end
