
#import "TinyPoo.h"
#import "Enemy.h"
#import "Box2D.h"
#import "Constants.h"

@interface Enemy(){
    int_fast8_t minVelocityX;
	int_fast8_t minVelocityY;
}

- (void) createBox2DBodyWithPosition:(CGPoint)bodyPosition;
- (void) resetWithPosition:(CGPoint)bodyPosition;

@end

@implementation Enemy

@synthesize game = _game;
@synthesize sprite = _sprite;
@synthesize awake = _awake;
@synthesize diving = _diving;

#pragma mark INIT
+ (id) heroWithGame:(TinyPoo*)game withName:(NSString *)bodyName withPosition:(CGPoint)bodyPosition {
	return [[self alloc] initWithGame:game withName:bodyName withPosition:bodyPosition];
}

- (id) initWithGame:(TinyPoo*)game withName:(NSString *)bodyName withPosition:(CGPoint)bodyPosition {
	
	if ((self = [super init])) {
        minVelocityX = 4;
        minVelocityY = -40;
		self.game = game;
		
#ifndef DRAW_BOX2D_WORLD
		self.sprite = [CCSprite spriteWithFile:[NSString stringWithFormat:@"%@.png", bodyName]];
        
		[self addChild:_sprite];
#endif
		_body = NULL;
		_radius = RADIUS_POU;

//		_contactListener = new HeroContactListener(self);
//		_game.world->SetContactListener(_contactListener);

		[self resetWithPosition:bodyPosition];
	}
	return self;
}

//- (void) dealloc {
//
//	self.game = nil;
//	
//#ifndef DRAW_BOX2D_WORLD
//	self.sprite = nil;
//#endif
//
//	delete _contactListener;
//	[super dealloc];
//}

- (void) resetWithPosition:(CGPoint)bodyPosition {
	_flying = NO;
	_diving = NO;
	if (_body) {
		_game.world->DestroyBody(_body);
	}
	[self createBox2DBodyWithPosition:bodyPosition];
	[self updateNode];
	[self sleep];
}

#pragma mark -
#pragma mark CREATE BODY
- (void) createBox2DBodyWithPosition:(CGPoint)bodyPosition {
	
	b2BodyDef bd;
    bd.type = b2_dynamicBody;
	bd.linearDamping = 0.00f;
	bd.fixedRotation = true;

	
	// start position
	CGPoint p = ccp(bodyPosition.x - _radius, bodyPosition.y + _radius/2);
	bd.position.Set(p.x / PTM_RATIO, p.y / PTM_RATIO);
	_body = _game.world->CreateBody(&bd);
	
	b2CircleShape shape;
	shape.m_radius = _radius / PTM_RATIO;
	
	b2FixtureDef fd;
	fd.shape = &shape;
    fd.filter.groupIndex = -1;
	fd.density = .9f;
	fd.restitution = 0.1; // bounce
	fd.friction = 0.005;
	
	_body->CreateFixture(&fd);
}

- (void) sleep {
	_awake = NO;
	_body->SetActive(false);
}

- (void) wake {
	_awake = YES;
	_body->SetActive(true);
	_body->ApplyLinearImpulse(b2Vec2(1,2), _body->GetPosition());
}

- (void) wakeContinue{
	_awake = YES;
	_body->SetActive(true);
}

#pragma mark DIVE
- (void) timeToDive:(int8)forceDive{
    
	_body->ApplyForce(b2Vec2(3*forceDive,-33*forceDive),_body->GetPosition());
    minVelocityX = 4;
    minVelocityY = -40;
}

#pragma mark LINEAR VELOCITY
- (void) simpleVelocity{
	const float minVelX = 5;
    
	b2Vec2 vel = _body->GetLinearVelocity();
	if (vel.x < minVelX) {
		vel.x = minVelX;
	}
	if (vel.y < minVelocityY) {
		vel.y = minVelocityY;
	}
	_body->SetLinearVelocity(vel);
}

#pragma mark UPDATE
- (void) updateNode {
	
	CGPoint p;
	p.x = _body->GetPosition().x * PTM_RATIO;
    
    if (p.x > 800) {
        if (minVelocityY > -60) {
            minVelocityY--;
        }
    }
 
    p.y = _body->GetPosition().y * PTM_RATIO;
	
	// CCNode position and rotation
	self.position = p;
	b2Vec2 vel = _body->GetLinearVelocity();
	float angle = atan2f(vel.y, vel.x);

	self.rotation = -1 * CC_RADIANS_TO_DEGREES(angle);
	
	// TEMP: sleep if below the screen
	if (p.y < -_radius && _awake) {
//		[self sleep];
	}
    
}

#pragma mark - Reset Enemy
- (void)resetEnemyWithPosition:(CGPoint)position{
    _flying = NO;
	_diving = NO;
    minVelocityY = -40;
    
	if (_body) {
		_game.world->DestroyBody(_body);
	}
	[self createBox2DBodyWithPosition:position];
    [self updateNode];
	[self sleep];
}
@end
