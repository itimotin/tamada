#ifdef GL_ES
precision mediump float;
#endif

varying vec2 v_texCoord;
uniform sampler2D u_texture0;
uniform float u_time;
uniform float u_color;
uniform float u_type;


#define epsilon 0.0001
// 1
const float speed = 2.0;
const float bendFactor = 0.5;
void main()
{

//vec4 textureColor = texture2D(u_texture0, fract(vec2(v_texCoord.x , v_texCoord.y)));
//    
    //float ratio=1.0;
//  
//    vec4 color;
//	float sq = 32.0;
//	
//	vec2 pos = v_texCoord;//+ (resolution / vec2(2.0));
//	
//	pos.x = sq * ((pos.x / sq)-fract(pos.x / sq));
//	pos.y = sq * ((pos.y / sq)-fract(pos.y / sq));	
//	
//	if (   gl_FragCoord.x > pos.x - sq/2.0
//	    && gl_FragCoord.x < pos.x + sq/2.0
//	    && gl_FragCoord.y < pos.y + sq/2.0
//	    && gl_FragCoord.y > pos.y - sq/2.0) color = vec4(1.0,0.0,1.0,1.0);
//	
	//gl_FragColor = vec4(color, 1.0 );


 //  gl_FragColor = textureColor;
    
//  lowp vec4 textureColor = texture2D(u_texture0, fract(vec2(v_texCoord.x , v_texCoord.y)));
//    lowp float gray = dot(textureColor, vec4(3.0, 1.0, 2.0, 0.0));
 //   gl_FragColor = vec4( textureColor.r ,  gray * ratio + textureColor.g   * (1.0 - ratio), textureColor.b, textureColor.a);
    //gl_FragColor=vec4(1.0,1.0,0,1);
    
    vec2 onePixel = vec2(1.0 / 480.0, 1.0 / 320.0);
    
    // 3
    vec2 texCoord = v_texCoord;
    
    // 4
    vec4 color;
    color.rgb = vec3(0.5);
    color -= texture2D(u_texture0, texCoord - onePixel) * 5.0;
    color += texture2D(u_texture0, texCoord + onePixel) * 5.0;
    // 5
    color.rgb = vec3((color.r + color.g + color.b) / 3.0);
    gl_FragColor = vec4(color.rgb, 1);
}