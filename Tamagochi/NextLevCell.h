//
//  NextLevCell.h
//  Tamagochi
//
//  Created by Sergiu Moțipan on 1/10/14.
//
//

#import <UIKit/UIKit.h>

@interface NextLevCell : UITableViewCell
{
 
    
}
@property (retain, nonatomic) IBOutlet UILabel *name;
@property (retain, nonatomic) IBOutlet UIImageView *labImg;

@end
