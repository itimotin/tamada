attribute vec4 a_position;							
attribute vec2 a_texCoord;
attribute vec4 a_color;
attribute vec4 a_Vertex;
uniform mat4 u_MVPMatrix;
uniform float u_mouseX;
uniform float u_mouseY;


#ifdef GL_ES
varying lowp vec4 v_fragmentColor;
varying mediump vec2 v_texCoord;					
#else												
varying vec4 v_fragmentColor;						
varying vec2 v_texCoord;							
#endif												





void main()											
{
        vec4 thisPos = a_Vertex;
        float thisY = thisPos.y;
        float thisZ = thisPos.z;
        float thisX = thisPos.x;
        float thisW = thisPos.w;
       
        thisPos.y+= u_mouseY*1.3;
        thisPos.x+= u_mouseX*1.3;
       
        gl_Position=u_MVPMatrix*thisPos;
        
        v_fragmentColor = a_color;
        v_texCoord = a_texCoord;
    
  }