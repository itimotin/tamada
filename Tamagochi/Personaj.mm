//
//  Personaj.m
//  Tamagochi
//
//  Created by Sergiu Moțipan on 9/30/13.
//
//

#import "Personaj.h"

static Personaj *stateManager=nil;

@implementation Personaj

+(id)sharedInstance
{
    
    if (!stateManager)
        stateManager = [[super allocWithZone:NULL] init];
    return stateManager;
}

-(id)init
{
    self=[super init];
    if (self)
    {
     
        _person = [PouNode node];
        _person.tag=tagTamagochi;
        [_person setTamSize:CGSizeMake(_person.tamSize.width*0.7, _person.tamSize.height*0.9)];
        _tatooShopped=[[NSMutableArray alloc] init];
        _bodyShopped=[[NSMutableArray alloc] initWithObjects:[NSString stringWithFormat:@"ochi_centru_7%@.png", (IS_IPAD)?@"-iPad":@""], @"gene_01_1.png",[NSString stringWithFormat:@"contur_ochi_7%@.png", (IS_IPAD)?@"-iPad":@""], @"", @"", [NSString stringWithFormat:@"corpul_10%@.png", (IS_IPAD)?@"-iPad":@""], @"", @"", @"", @"buze_03_4.png", @"", @"",[NSString stringWithFormat:@"yeylid_up_10%@.png", (IS_IPAD)?@"-iPad":@""],[NSString stringWithFormat:@"yeylid_down_10%@.png", (IS_IPAD)?@"-iPad":@""], nil];
    }
    return self;
}

-(void)replaceTexturePerson:(NSInteger)index obj:(NSString *)str
{
    if (index==4)
        [_bodyShopped replaceObjectAtIndex:8 withObject:@""];
    if (index==9)
        [_bodyShopped replaceObjectAtIndex:3 withObject:@""];
    [_bodyShopped replaceObjectAtIndex:index-1 withObject:str];
    [stateTamagochi updateBodyShopped:_bodyShopped];
}

-(void)replaceTatooPerson:(NSString*)obj
{
    int number=-1;
        for (int i=0;i<[_tatooShopped count];i++)
        {
            if ([[[[[_tatooShopped objectAtIndex:i] objectAtIndex:0] componentsSeparatedByString:@"_"] objectAtIndex:1] isEqualToString:[[obj componentsSeparatedByString:@"_"] objectAtIndex:1]])
            {
                number=i;
            }
        }
    if (number!=-1)
    {
       CGPoint p=[_person getPositionTatoo:[[[obj componentsSeparatedByString:@"_"] objectAtIndex:1] intValue]+21];
        [_tatooShopped replaceObjectAtIndex:number withObject:[NSMutableArray arrayWithObjects:obj,[NSString stringWithFormat:@"%f", p.x],[NSString stringWithFormat:@"%f", p.y], nil]];
    }
    else
    {
        CGPoint p=[_person getPositionTatoo:[[[obj componentsSeparatedByString:@"_"] objectAtIndex:1] intValue]+21];
        [_tatooShopped addObject:[NSMutableArray arrayWithObjects:obj,[NSString stringWithFormat:@"%f", p.x],[NSString stringWithFormat:@"%f", p.y], nil]];
    }
    [stateTamagochi updateTatooShopped:_tatooShopped];
}

-(void)repositioningTatoo
{
    for (int i=0; i<[_tatooShopped count]; i++) {
        CGPoint p=[_person getPositionTatoo:[[[[[_tatooShopped objectAtIndex:i] objectAtIndex:0] componentsSeparatedByString:@"_"] objectAtIndex:1] intValue]+21];
        [[_tatooShopped objectAtIndex:i]  replaceObjectAtIndex:1 withObject:[NSString stringWithFormat:@"%f", p.x]];
        [[_tatooShopped objectAtIndex:i]  replaceObjectAtIndex:2 withObject:[NSString stringWithFormat:@"%f", p.y]];
    }
     [stateTamagochi updateTatooShopped:_tatooShopped];
}
-(void)replaceTatooDefault:(NSString*)str
{
    int index=[str intValue]+21;
    int number=-1;
    for (int i=0;i<[_tatooShopped count];i++)
    {
        if ([[[[[_tatooShopped objectAtIndex:i] objectAtIndex:0] componentsSeparatedByString:@"_"] objectAtIndex:1] isEqualToString:str])
        {
            number=i;
        }
    }
    if (number==-1 )
    {
        [[_person getChildByTag:index] removeFromParentAndCleanup:YES];
    }
    else if (number!=-1)
    {
        if (![[_person getChildByTag:index] parent])
        {
            CGPoint p=CGPointMake([[[_tatooShopped objectAtIndex:number] objectAtIndex:1] floatValue], [[[_tatooShopped objectAtIndex:number] objectAtIndex:2] floatValue]);
            [_person createTatooChild:index nameTexture:[[_tatooShopped objectAtIndex:number] objectAtIndex:0] position:p]  ;
        }
        else
        {
            CCTexture2D* texture =[[CCTextureCache sharedTextureCache] addImage:[[_tatooShopped objectAtIndex:number] objectAtIndex:0]];
            [[_person getChildByTag:index] setTexture:texture];
            [[_person getChildByTag:index] setTextureRect:CGRectMake(0.0f, 0.0f, [texture contentSize].width,[texture contentSize].height)];
            [_person setPositionByIndex:index];
        }
    }
}

-(void)deleteTatoo:(NSString *)str
{
    int index=[[[str componentsSeparatedByString:@"_"] objectAtIndex:1] intValue]+21;
    [[_person getChildByTag:index] removeFromParentAndCleanup:YES];
}

-(void)replaceTextureDefault:(NSInteger)index
{
    
    if ([[_bodyShopped objectAtIndex:index-1] isEqualToString:@""])
    {
        [[_person getChildByTag:index] removeFromParentAndCleanup:YES];
    }
    else
    {
        if (index==6)
        {
            [(CCSprite *)[[personaj person] getChildByTag:16] setTexture:[[CCTextureCache sharedTextureCache] addImage:[_bodyShopped objectAtIndex:12]]];
            [(CCSprite *)[[personaj person] getChildByTag:17] setTexture:[[CCTextureCache sharedTextureCache] addImage:[_bodyShopped objectAtIndex:13]]];
        }

        if (![[_person getChildByTag:index] parent])
        {
            [_person createChildByTag:index nameTexture:[_bodyShopped objectAtIndex:index-1]];
        }
        else
        {
            CCTexture2D* texture =[[CCTextureCache sharedTextureCache] addImage:[_bodyShopped objectAtIndex:index-1]];
            [(CCSprite *)[_person getChildByTag:index] setTexture:texture];
            [(CCSprite *)[_person getChildByTag:index] setTextureRect:CGRectMake(0.0f, 0.0f, [texture contentSize].width,[texture contentSize].height)];
            [_person setPositionByIndex:index];
        }
        
    }
}

-(void)replaceDefaultAllTatoo
{
    for (int i=0;i<[_tatooShopped count];i++)
    {
        [self replaceTatooDefault:[[[[_tatooShopped objectAtIndex:i] objectAtIndex:0] componentsSeparatedByString:@"_"] objectAtIndex:1] ];
    }
}
-(void)replaceDefaultAllBody
{
    for (int i=1;i<=12;i++)
    {
        [self replaceTextureDefault:i];
     //   [_person setPositionByIndex:i];
    }
}


-(void)initRects
{
    [_person setScale:[stateTamagochi sizeTamagochi]];
     _rectPerson=CGRectMake( _person.position.x-_person.tamSize.width/2, _person.position.y-_person.tamSize.height/2, _person.tamSize.width, _person.tamSize.height);
}

-(void)setScale:(float)scale
{
    [_person setScale:scale];
    /*
    for (int i=1;i<=40;i++)
    {
        if (i!=3 && i!=1 && i!=16 && i!=17 && i!=2 && [_person getChildByTag:i])
        {
            [[_person getChildByTag:i] setScale:scale];
        }
       // [[[_person children] objectAtIndex:i] setScale:scale];
    }
     */
}

-(void)dealloc
{
    [_bodyShopped release];
    [_tatooShopped release];
    [super dealloc];
}
@end
