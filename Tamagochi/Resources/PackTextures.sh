#!/bin/sh

TP="/usr/local/bin/TexturePacker"
cd ${PROJECT_DIR}/${PROJECT}

if [ "${ACTION}" = "clean" ]; then
echo "cleaning..."

rm -f Resources/sprites*.pvr.ccz
rm -f Resources/sprites*.plist


# ....
# add all files to be removed in clean phase
# ....
else
#ensure the file exists
if [ -f "${TP}" ]; then
echo "building..."
# create assets
${TP} --smart-update \
--format cocos2d \
--texture-format pvr2ccz \
--main-extension "-hd" \
--autosd-variant 0.5:-hd \
--data Resources/sprites-hd.plist \
--sheet Resources/sprites-hd.pvr.ccz \
--dither-fs-alpha \
--opt RGBA4444 \
TextureTamagochi_Art/*.png

# ....
# add other sheets to create here
# ....

exit 0
else
#if here the TexturePacker command line file could not be found
echo "TexturePacker tool not installed in ${TP}"
echo "skipping requested operation."
exit 1
fi

fi
exit 0