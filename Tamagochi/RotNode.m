//
//  RotNode.m
//  Tamagochi
//
//  Created by Sergiu Moțipan on 2/4/14.
//
//

#import "RotNode.h"

@implementation RotNode
@synthesize delegate;

- (void)performBlock:(void (^)(void))block afterDelay:(NSTimeInterval)delay
{
    int64_t delta = (int64_t)(1.0e9 * delay);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delta), dispatch_get_main_queue(), block);
}

-(void)setTexture:(CCTexture2D*)texture
{
    int count=[[texture.imageName componentsSeparatedByString:@"_"] count];
    int length=[[[texture.imageName componentsSeparatedByString:@"_"] objectAtIndex:count-1] length];
    
    if ([[[texture.imageName componentsSeparatedByString:@"_"] objectAtIndex:1]  intValue]==3)
    {
        defaultRot=YES;
        [_rot setTexture:[[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"%@_rot%@.png",[texture.imageName substringToIndex:[texture.imageName length]-4], (IS_IPAD)?@"-iPad":@""]]];
        if(_rotUp.opacity!=0.0)
        {
            _rotUp.opacity=0.0;
            _rotDown.opacity=0.0;
            _teethUp.opacity=0.0;
            _teethDown.opacity=0.0;
        }
     
        return;
    }
    [_rotUp setTexture:[[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"%@_lips_up%@.png",[texture.imageName substringToIndex:[texture.imageName length]-4], (IS_IPAD)?@"-iPad":@""]]];
    [_rotDown setTexture:[[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"%@_lips_down%@.png",[texture.imageName substringToIndex:[texture.imageName length]-4], (IS_IPAD)?@"-iPad":@""]]];
    [_rot setTexture:[[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"%@down_rot%@.png",[texture.imageName substringToIndex:[texture.imageName length]-length], (IS_IPAD)?@"-iPad":@""]]];
    [_teethUp setTexture:[[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"%@teeth_up%@.png",[texture.imageName substringToIndex:[texture.imageName length]-length], (IS_IPAD)?@"-iPad":@""]]];
    [_teethDown setTexture:[[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"%@teeth_down%@.png",[texture.imageName substringToIndex:[texture.imageName length]-length], (IS_IPAD)?@"-iPad":@""]]];
    
    if(_rotUp.opacity!=255.0)
    {
        _rotUp.opacity=255.0;
        _rotDown.opacity=255.0;
        _teethUp.opacity=255.0;
        _teethDown.opacity=255.0;
    }
}
-(void)setTextureRect:(CGRect)rect
{
   // NSLog(@"rect iopta");
}

- (id)init
{
    self=[super init];
    if (self)
    {
        if ([stateTamagochi food]>=20)
            [stateTamagochi setMouthState:mouthStateDefault];
        else
            [stateTamagochi setMouthState:mouthHungry];
        
        defaultRot=NO;
        casca=0.2;
        _rot=[CCSprite spriteWithFile:@"buze_02_down_rot.png"];
        _rot.tag=5;
        [self addChild:_rot];
        
        _rotUp=[CCSprite spriteWithFile:@"buze_02_4_lips_up.png"];
        [_rotUp setPosition:ccp(0, 0)];
        _rotUp.tag=1;
        [self addChild:_rotUp z:3];
        
        _rotDown=[CCSprite spriteWithFile:@"buze_02_4_lips_down.png"];
        [_rotDown setPosition:ccp(0, 0)];
        _rotDown.tag=2;
        [self addChild:_rotDown z:2];
        
        _teethUp=[CCSprite spriteWithFile:@"buze_02_teeth_up.png"];
        [_teethUp setPosition:ccp(0, 0)];
        _teethUp.tag=3;
        [self addChild:_teethUp z:1];
        
        _teethDown=[CCSprite spriteWithFile:@"buze_02_teeth_down.png"];
        [_teethDown setPosition:ccp(0, 0)];
        _teethDown.tag=4;
        [self addChild:_teethDown z:1];
        
        [self guraDownY:0.2 X:0.7 time:0.3 position:1.0];
        [self scheduleUpdate];
    }
    return self;
}


-(void)guraDownY:(float)y X:(float)x time:(float)timp position:(float)position
{
    jos=YES;
   
    id action_teeth_UP1=[CCScaleBy actionWithDuration:timp-0.1 scaleX:x-0.2 scaleY:y-0.2];
    id action_teeth_UP2=[CCMoveTo actionWithDuration:timp position:ccp(0, 15*position)];
    CCSpawn *action_teeth_UP=[CCSpawn actions:action_teeth_UP1, action_teeth_UP2, nil];
    [_teethUp runAction:action_teeth_UP];
    
    id action_teeth_Down1=[CCScaleTo actionWithDuration:timp-0.1 scaleX:x scaleY:y-0.2];
    id action_teeth_Down2=[CCMoveTo actionWithDuration:timp position:ccp(0, 5*position)];
    CCSpawn *action_teeth_Down=[CCSpawn actions:action_teeth_Down1, action_teeth_Down2, nil];
    [_teethDown runAction:action_teeth_Down];
    
    id action_rot_Down1=[CCScaleBy actionWithDuration:timp scaleX:x scaleY:y+0.1];
    id action_rot_Down2=[CCMoveTo actionWithDuration:timp position:ccp(0, 10*position)];
    CCSpawn *action_rot_Down=[CCSpawn actions:action_rot_Down1, action_rot_Down2, nil];
    [_rotDown runAction:action_rot_Down];
    
    id action_rot1=[CCScaleBy actionWithDuration:timp scaleX:x scaleY:y+0.1];
    id action_rot2=[CCMoveTo actionWithDuration:timp position:ccp(0, 10*position)];
    CCSpawn *action_rot=[CCSpawn actions:action_rot1, action_rot2, nil];
    [_rot runAction:action_rot];
    
    id action_rot_Up1=[CCScaleBy actionWithDuration:timp scaleX:x scaleY:y+0.5];
    id action_rot_Up2=[CCMoveTo actionWithDuration:timp position:ccp(0, 5*position)];
    CCSpawn *action_rot_Up=[CCSpawn actions:action_rot_Up1, action_rot_Up2, nil];
    [_rotUp runAction:action_rot_Up];
    
}


-(void)guraUpY:(float)y X:(float)x time:(float)timp
{
    openMouth=y;
    jos=NO;
   
    id action_teeth_UP11=[CCScaleTo actionWithDuration:timp scaleX:x scaleY:y];
    id action_teeth_UP22=[CCMoveTo actionWithDuration:timp position:ccp(0, 0)];
    CCSpawn *action_teeth_UP_1=[CCSpawn actions:action_teeth_UP11, action_teeth_UP22, nil];
    [_teethUp runAction:action_teeth_UP_1];
    
    id action_teeth_Down11=[CCScaleTo actionWithDuration:timp scaleX:x scaleY:y];
    id action_teeth_Down22=[CCMoveTo actionWithDuration:timp position:ccp(0, 0)];
    CCSpawn *action_teeth_Down_1=[CCSpawn actions:action_teeth_Down11, action_teeth_Down22, nil];
    [_teethDown runAction:action_teeth_Down_1];
    
    id action_rot_Down11=[CCScaleTo actionWithDuration:timp scaleX:x scaleY:y];
    id action_rot_Down22=[CCMoveTo actionWithDuration:timp position:ccp(0, 0)];
    CCSpawn *action_rot_Down_1=[CCSpawn actions:action_rot_Down11, action_rot_Down22, nil];
    [_rotDown runAction:action_rot_Down_1];
    
    id action_rot11=[CCScaleTo actionWithDuration:timp scaleX:x scaleY:y];
    id action_rot22=[CCMoveTo actionWithDuration:timp position:ccp(0, 0)];
    CCSpawn *action_rot_1=[CCSpawn actions:action_rot11, action_rot22, nil];
    [_rot runAction:action_rot_1];
    
    id action_rot_Up11=[CCScaleTo actionWithDuration:timp scaleX:x scaleY:y];
    id action_rot_Up22=[CCMoveTo actionWithDuration:timp position:ccp(0, 0)];
    CCSpawn *action_rot_Up_1=[CCSpawn actions:action_rot_Up11, action_rot_Up22, nil];
    [_rotUp runAction:action_rot_Up_1];
   
}


- (void)update:(float)dt
{
    totalTime += dt;
    switch ([stateTamagochi mouthState]) {
        case mouthHungry:
        {
           
            if (totalTime>=10.0)
            {
                totalTime=0.0;
                [self guraUpY:1.0 X:1.0 time:0.2];
                [self performBlock:^{
                    [self guraDownY:0.2 X:0.7 time:0.3 position:1.0];
                } afterDelay:0.8];
            }
        }
            break;
            
        case mouthStateDefault:
        {
            
            if (totalTime>=20.0)
            {
                totalTime=0.0;
                [self guraUpY:0.8 X:0.8 time:0.2];
                
                [self performBlock:^{
                    [self guraDownY:0.2 X:0.8 time:0.3 position:0.8];
                } afterDelay:0.8];
            }
        }
            break;
        case mouthStateFun:
        {
            if (jos && _rot.numberOfRunningActions==0)
            {
                [self guraUpY:0.8 X:1.0 time:0.5];
             
                [self performBlock:^{
                    [self guraDownY:0.2 X:0.7 time:0.2 position:0.8];
                } afterDelay:1.5];
            }

        }
            break;
        case mouthStateReadyEat:
        {
            if (jos)
            {
                [_rot stopAllActions];
                [_rotDown stopAllActions];
                [_rotUp stopAllActions];
                [_teethDown stopAllActions];
                [_teethUp stopAllActions];
                [self guraUpY:1.0 X:1.0 time:0.1];
            }
        }
            break;
        case mouthStateReadyEatCancel:
        {
            if (!jos)
            {
                [_rot stopAllActions];
                [_rotDown stopAllActions];
                [_rotUp stopAllActions];
                [_teethDown stopAllActions];
                [_teethUp stopAllActions];
                [self guraDownY:0.2 X:0.7 time:0.2 position:1.0];
            }
            if ([stateTamagochi food]>=20)
                [stateTamagochi setMouthState:mouthStateDefault];
            else
                [stateTamagochi setMouthState:mouthHungry];
            totalTime=0;
            
        }
            break;
        case mouthStateEat:
        {
            [self guraDownY:0.2 X:0.7 time:0.4 position:1.0];
            
            switch ([stateTamagochi eatCategory]) {
                case 1:
                {
                     if ([stateTamagochi soundOn])
                         [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"eating.mp3" loop:NO];
                    
                    [self performBlock:^{
                        [self guraUpY:1.0 X:1.0 time:0.3];
                        
                    } afterDelay:0.4];
                    [self performBlock:^{
                        [self guraDownY:0.2 X:0.7 time:0.4 position:1.0];
                        
                         if ([stateTamagochi soundOn])
                             [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"eating.mp3" loop:NO];
                        
                    } afterDelay:0.7];
                }
                    break;
                case 2:
                {
                    NSLog(@"222");
                }
                    break;
                case 3:
                {
                     if ([stateTamagochi soundOn])
                         [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"eating_poison.wav" loop:NO];
                }
                    break;
                case 4:
                {
                    if ([stateTamagochi soundOn])
                        [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"eating_dessert.mp3" loop:NO];
                    
                    [self performBlock:^{
                        [self guraUpY:1.0 X:1.0 time:0.3];
                        
                    } afterDelay:0.4];
                    [self performBlock:^{
                        [self guraDownY:0.2 X:0.7 time:0.4 position:1.0];
                        
                         if ([stateTamagochi soundOn])
                             [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"eating_dessert.mp3" loop:NO];
                        
                    } afterDelay:0.7];

                   
                }
                    
                default:
                    break;
            }
          
           
            if ([stateTamagochi food]>=20)
                [stateTamagochi setMouthState:mouthStateDefault];
            else
                [stateTamagochi setMouthState:mouthHungry];
            totalTime=0;
           
            
        }
            break;
        case mouthSpeak:
        {
            if ([delegate returnTouchValue]!=casca)
            {
                
                if (casca>0.3) casca=0.3;
                
                if (jos && _rot.numberOfRunningActions==0)
                {
                    [self guraUpY:0.4+casca X:0.7 time:0.15];
                    [self performBlock:^{
                        [self guraDownY:0.2 X:0.7 time:0.15 position:0.4+casca];
                    } afterDelay:0.15];
    
                }
            casca=[delegate returnTouchValue];
            }
        }
            break;

        default:
            break;
    }
}
@end
