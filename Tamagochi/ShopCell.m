//
//  ShopCell.m
//  Tamagochi
//
//  Created by Sergiu Moțipan on 10/3/13.
//
//

#import "ShopCell.h"

@implementation ShopCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_imageBody release];
    [_nameBody release];
    [_descBody release];
    [_nameButton release];
    [_buttonBody release];
    [_quantity release];
    [_priceBody release];
    [_money release];
    [_lockImage release];
    [super dealloc];
}
@end
