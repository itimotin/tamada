attribute vec4 a_position;							
attribute vec2 a_texCoord;
attribute vec4 a_color;
uniform mat4 u_MVPMatrix;
uniform float u_longX;
uniform float u_longY;


#ifdef GL_ES
varying lowp vec4 v_fragmentColor;
varying mediump vec2 v_texCoord;					
#else												
varying vec4 v_fragmentColor;						
varying vec2 v_texCoord;			
#endif												





void main()											
{

vec2 center=vec2(0.0,0.0);
mat2 rot=mat2(1.0,0.0,0.5,1.0);
mat2 rot1=mat2(1.0,0.0,-0.5,1.0);

    float height =  v_texCoord.x/(-3.5);
    float height1 = v_texCoord.y/1.8;
    // 3
    float offset1 = pow(1.1-height1, 2.5);
    float offset2 = pow(1.0-height1, 2.5);
    // 4 multiply by sin since it gives us nice bending
    offset1 *= (sin(u_longX  * 0.06));
    offset2 *= (sin(u_longY  * 0.07));
  
    vec2 modifiedTexCoord = fract(vec2(v_texCoord.x+offset1, v_texCoord.y-offset2));
    gl_Position=u_MVPMatrix*a_position;
    
    v_fragmentColor = a_color;
   // v_texCoord = (a_texCoord-modifiedTexCoord)*rot*rot1;
    v_texCoord = (a_texCoord);
}