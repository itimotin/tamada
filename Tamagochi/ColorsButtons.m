//
//  ColorsButtons.m
//  Tamagochi
//
//  Created by Sergiu Moțipan on 10/9/13.
//
//

#import "ColorsButtons.h"

@implementation ColorsButtons

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)dealloc {
    [_col1 release];
    [_col2 release];
    [_col3 release];
    [_col4 release];
    [_col5 release];
    [_col6 release];
    [_col7 release];
    [_col8 release];
    [_col9 release];
    [_col10 release];
    [super dealloc];
}
@end
