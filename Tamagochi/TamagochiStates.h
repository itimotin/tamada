//
//  TamagochiStates.h
//  Tamagochi
//
//  Created by Sergiu Moțipan on 6/28/13.
//
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "Functions.h"
#import "SimpleAudioEngine.h"
#import <AVFoundation/AVFoundation.h>

enum timeCategory
{
    time_food=2,
    time_health=28,
    time_fun=7,
    time_energy=5
};


enum objectsTags
{
    tagTamagochi=0,
    tagDefault=1,
    tagStergar=2,
    tagStishka=3,
    tagSoap=4,
    tagMicrofon=5,
    tagCashti=6,
    tagFood = 7,
    tagLamp=8,
    tagMall =9,
    tagTV=10
};

enum stateMouth {
    mouthStateDefault =0,
    mouthStateFun = 1,
    mouthStateReadyEat = 2,
    mouthStateEat = 3,
    mouthSpeak = 4,
    mouthHungry =5,
    mouthSatieted =6,
    mouthStateReadyEatCancel=7,
    mouthStateNotEat=8
    };

enum stateEyes {
    eyeStateDefault =0,
    eyeStateSad = 1,
    eyeStateSuffering = 2,
    eyeStateShower = 3,
    eyeStateErase = 4
};

enum states {
    stateDefault =0,
    stateDirt = 1,
    stateMiddleDirt = 2,
    stateSlowDirt = 3
};

enum room {
    gameRoom =0,
    bathRoom = 1,
    studioRoom = 2,
    kitchenRoom = 3,
    bedRoom=4,
    mallRoom=5,
    otherRoom = 6,
    closetRoom = 7
};

enum categoryScroll {
    s_default = 0,
    s_animatedSprite = 1,
    s_createNewSprite = 2
};
enum categoryShop
{
    sh_default=0,
    sh_potions=1,
    sh_skins =2,
    sh_eat =3,
    sh_body =4
    
};

@interface TamagochiStates : NSObject
{
    Functions *database;
   
}
@property (nonatomic) int state;
@property (nonatomic) int mouthState;
@property (nonatomic) int eyeState;
@property (nonatomic) int touchObject;
@property (nonatomic) int room;
@property (nonatomic) int level;
@property (nonatomic) float health;
@property (nonatomic) float funny;
@property (nonatomic) float food;
@property (nonatomic) float energy;
@property (nonatomic) float fat;
@property (nonatomic) float sizeTamagochi;
@property (nonatomic) int moneyGame;
@property (nonatomic) int kCalFoods;
@property (nonatomic) int shopCategory;
@property (nonatomic) int eatCategory;
@property (nonatomic) int bodyCategory;
@property (nonatomic) int skinCategory;
@property (nonatomic) float nextLevelPoints;
@property (nonatomic) int adult;
@property (nonatomic) BOOL sleep;
@property (nonatomic) BOOL mouthSad;
@property (nonatomic) BOOL soundOn;
@property (nonatomic) BOOL musicOn;
@property (nonatomic) BOOL notificationsOn;
@property (nonatomic) float dirtFood;
@property (nonatomic) float healthFood;
@property (nonatomic) float sweetsFood;
@property (nonatomic) int sizeLevel;
@property (nonatomic) int procentYears;
@property (nonatomic, retain) NSArray *years;
@property (nonatomic) BOOL kitchenStart;
@property (nonatomic) BOOL gameStart;
@property (nonatomic) BOOL sleepStart;
@property (nonatomic) BOOL bathStart;
@property (nonatomic) BOOL closetStart;
@property (nonatomic) BOOL healthStart;
@property (nonatomic) BOOL startRecord;
@property (nonatomic) BOOL nameRoomTap;
@property (nonatomic) BOOL loaded;
@property (nonatomic) float sleepONBed;


+(id)sharedInstance;

-(void)selectStatesTamagochi;
-(NSMutableArray *)selectBody_Shop:(NSInteger)index;
-(NSMutableArray *)selectBodyGroup:(NSArray *)arr;
-(void)updateFun;
-(void)insertNameB:(NSString *)str;
-(void)insertNameBodyIopta:(NSString *)str price:(NSInteger)prc group:(NSInteger)group nr:(NSInteger)nr level:(NSInteger)lev;
-(void)updateFat;
-(void)updateFood;
-(void)updateEnergy;
-(void)updateHealth;
-(void)updateLevel;
-(void)updateMoney;
-(void)updateDirt;
-(void)deleteFood:(int)category food:(int)nr;
-(NSMutableArray*)selectFoods:(int)category;
-(void)eatTamagochi:(int)nr category:(int)category;
-(NSMutableArray *)selectBodyShop:(NSInteger)category;
-(void)shopProduct:(NSInteger)index index2:(NSInteger)index2;
-(NSMutableArray *)selectShoppedColors:(NSInteger)index;
-(int)returnPrice:(NSString *)name group:(NSString *)group nr:(NSInteger)nr;
-(int)returnIDName:(NSString *)name;
-(NSMutableArray *)selectSkins:(NSInteger)index;
-(int)returnPriceSkin:(NSInteger)index;
-(NSMutableArray *)colorSkin:(NSInteger)index object:(NSInteger)object;
-(NSInteger)selectColorSkin:(NSInteger)tag room:(NSInteger)room;
-(void)updateColorSkin:(NSInteger)tag color:(NSInteger)color;
-(NSInteger)selectNameSkin:(NSInteger)tag;
-(NSInteger)getPriceProduct:(NSInteger)index;
-(void)unlockColor:(NSInteger)index color:(NSInteger)color group:(NSString *)group;
-(NSMutableArray *)selectCoinsShop:(NSInteger)index;
-(NSString *)getPriceBodyShop:(NSString *)index group:(NSString *)group;
-(void)createNotificationsOnExit;
-(void)removeNotificationsOnEnter;
-(void)cancelLocalNotification:(NSString*)notificationID;
-(void)createLocalNotification:(NSString *)notificationID message:(NSString *)text;
-(void)updateStateOut;
-(NSMutableArray *)returnStateOut;
-(NSInteger) convertToGMT:(NSDate*)sourceDate;
-(void)createNotificationsOrder;
-(void)addToNextLevel:(int)category;
-(int)returnLevel:(NSString *)name group:(NSString *)group nr:(NSInteger)nr;
-(NSMutableArray *)selectOpenedNextLevel;
-(void)updateBodyShopped:(NSMutableArray*)array;
-(void)updateSizeLevel;
-(NSMutableArray *)firstPlayGame;
-(void)createNotificationsFirstPlayGame;
-(void)updateFirstStart:(int)category;
-(void)updateSleep;
-(void)firstStart;



- (BOOL)updateGameWithStageId:(NSNumber*)idStage withTime:(NSString*)bestTime withScore:(NSNumber*)score withCoins:(NSNumber*)coins withPosition:(NSNumber*)bestPosition;
- (NSInteger)getScoreForStageId:(NSNumber*)idStage;
- (NSInteger)getPostionForStageID:(NSNumber*)idStage;
- (NSMutableArray*)getAllGameStatisticsForStageID:(NSNumber *)idStage;
- (BOOL)addPlayer:(NSString*)player;
-(NSInteger)getStageForPlayer:(NSString*)player;
- (NSDictionary*)getAllGameStatisticsForStageID:(NSNumber *)idStage forPlayer:(NSString*)player;
- (BOOL)setStageForPlayer:(NSString*)player forStage:(NSInteger)stageId;

@end
