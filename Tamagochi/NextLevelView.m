//
//  NextLevelView.m
//  Tamagochi
//
//  Created by Sergiu Moțipan on 12/26/13.
//
//

#import "NextLevelView.h"
#import "NextLevCell.h"

@implementation NextLevelView
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)reloadLabels
{
    _dataOpened=[[NSMutableArray alloc] init];
    [title setText:NSLocalizedString(@"level_up", nil)];
    [level setText:[NSString stringWithFormat:@"%@ %d!",NSLocalizedString(@"level", nil), [stateTamagochi level]]];
    [summ setText:[NSString stringWithFormat:@"+ %d", [stateTamagochi level]*10]];
    [labOpen setText:NSLocalizedString(@"you_open", nil)];
    [_dataOpened removeAllObjects];
    [_dataOpened addObjectsFromArray:[stateTamagochi selectOpenedNextLevel]];
    if ([_dataOpened count]>0)
    {
        labOpen.alpha=1.0;
        tableOpened.alpha=1.0;
    }
    else
    {
        labOpen.alpha=0.0;
         tableOpened.alpha=0.0;
    }
    [tableOpened reloadData];
}

- (IBAction)share:(id)sender {
    
}

- (void)dealloc {
    [title release];
    [level release];
    [summ release];
    [tableOpened release];
    [labOpen release];
    [lab_share release];
    [super dealloc];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_dataOpened count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier;
    CellIdentifier = [NSString stringWithFormat:@"NextLevCell%@", (IS_IPAD)?@"_iPad":@""];
    NextLevCell *cell = (NextLevCell *) [tableOpened dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:[NSString stringWithFormat:@"NextLevCell%@", (IS_IPAD)?@"_iPad":@""] owner:self options:nil];
        
        for (id currentObject in topLevelObjects) {
            if ([currentObject isKindOfClass:[NextLevCell class]]) {
                cell = (NextLevCell *) currentObject;
                break;
            }
        }
    }
    cell.name.text=NSLocalizedString([[_dataOpened objectAtIndex:indexPath.row] objectAtIndex:1], nil);
    cell.labImg.image=[UIImage imageNamed:[[_dataOpened objectAtIndex:indexPath.row] objectAtIndex:0]];
    [cell.name sizeToFit];
    if (cell.name.frame.size.width>195)
    {
        [cell.name setFrame:CGRectMake(cell.name.frame.origin.x, cell.name.frame.origin.y, 195, cell.name.frame.size.height)];
        cell.name.adjustsFontSizeToFitWidth=YES;
        cell.name.minimumFontSize=9.0;
    }
    [cell.labImg setFrame:CGRectMake(cell.name.frame.size.width+cell.name.frame.origin.x+15, cell.labImg.frame.origin.y, cell.labImg.frame.size.width, cell.labImg.frame.size.height)];
      return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
        return 50+50*IS_IPAD;
}

- (IBAction)close:(id)sender {
    [_dataOpened release];
    [self removeFromSuperview];
}
@end
