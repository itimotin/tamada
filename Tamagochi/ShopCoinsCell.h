//
//  ShopCoinsCell.h
//  Tamagochi
//
//  Created by Sergiu Moțipan on 11/28/13.
//
//

#import <UIKit/UIKit.h>

@interface ShopCoinsCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *coins;
@property (retain, nonatomic) IBOutlet UILabel *sale;
@property (retain, nonatomic) IBOutlet UILabel *name_btn;
@property (retain, nonatomic) IBOutlet UILabel *price;
@property (retain, nonatomic) IBOutlet UIButton *btnShopCoins;
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *loading;

@end
