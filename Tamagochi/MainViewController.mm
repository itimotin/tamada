//
//  MainViewController.m
//  Tamagochi
//
//  Created by Sergiu Moțipan on 7/2/13.
//
//

#import "MainViewController.h"
#import "GamesLabScene.h"
#import "BathRoomScene.h"
#import "StudioRoom.h"
#import "KitchenRoom.h"
#import "BedRoom.h"
#import "MallRoom.h"
#import "Game.h"
#import "ShopCell.h"
#import "ClosetRoom.h"
#import "LockedCell.h"
#import "Social/Social.h"
#import "Accounts/Accounts.h"
//TODO: add Import headers
#import "Constants.h"
#import "TinyPoo.h"


@interface MainViewController ()
{
    NSMutableArray *items;
    NSTimer *scrollTimer;
    NSTimeInterval lastTime;
    iCarousel *carousel;
    
    // TODO: Add THIS
    uint_fast8_t indexGame;
    int_least8_t selectedStage;
    NSMutableArray *highscores;
	int currentScore;
	int currentScorePosition;
    NSString *currentPlayer;
    //end

}

@end

#define SCROLL_SPEED 1

@implementation MainViewController

- (void)performBlock:(void (^)(void))block afterDelay:(NSTimeInterval)delay
{
    int64_t delta = (int64_t)(1.0e9 * delay);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delta), dispatch_get_main_queue(), block);
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)setupCocos2D {
    
    CCGLView *glView = [CCGLView viewWithFrame:CGRectMake(0, 40+40*IS_IPAD, [self.view bounds].size.width, [self.view bounds].size.height-(40+40*IS_IPAD))
								   pixelFormat:kEAGLColorFormatRGBA8
								   depthFormat:GL_DEPTH_COMPONENT24_OES
							preserveBackbuffer:NO
									sharegroup:nil
								 multiSampling:NO
							   numberOfSamples:0];
    
    [glView setMultipleTouchEnabled:NO];
    
	CCDirector *director = [CCDirector sharedDirector];
	
	director.wantsFullScreenLayout = YES;
    
	[director setDisplayStats:NO];

	[director setAnimationInterval:1.0/60];

	[director setView:glView];

	[director setProjection:kCCDirectorProjection2D];

	if( ! [director enableRetinaDisplay:YES] )
		CCLOG(@"Retina Display Not supported");

	//[CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA8888];

	CCFileUtils *sharedFileUtils = [CCFileUtils sharedFileUtils];
	[sharedFileUtils setEnableFallbackSuffixes:NO];				// Default: NO. No fallback suffixes are going to be used
	[sharedFileUtils setiPhoneRetinaDisplaySuffix:@"-hd"];		// Default on iPhone RetinaDisplay is "-hd"
	[sharedFileUtils setiPadSuffix:@"-ipad"];					// Default on iPad is "ipad"
	[sharedFileUtils setiPadRetinaDisplaySuffix:@"-ipadhd"];
	
	//[CCTexture2D PVRImagesHavePremultipliedAlpha:YES];
    [self.view insertSubview:director.view atIndex:0];
    if ([[[stateTamagochi returnStateOut] objectAtIndex:0] intValue]==1)
    {
        nrPage=4;
        [l_room setText:NSLocalizedString(@"Bed_room", nil)];
        [director runWithScene:[BedRoom scene]];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"sleeping" object:nil];
    }
    else
    if ([stateTamagochi food]<20)
    {
        nrPage=3;
        [l_room setText:NSLocalizedString(@"Kitchen_room", nil)];
        [director runWithScene:[KitchenRoom scene]];
    }
    else if ([stateTamagochi state]==stateDirt || [stateTamagochi state]==stateSlowDirt)
    {
         nrPage=1;
         [l_room setText:NSLocalizedString(@"Bath_room", nil)];
         [director runWithScene:[BathRoomScene scene]];
    }
    else if ([stateTamagochi health]<20)
    {
        [mallTap setEnabled:NO];
        nrPage=5;
        [l_room setText:NSLocalizedString(@"Mall_room", nil)];
        [director runWithScene:[InMallRoom scene]];
    }
    else if ([stateTamagochi energy]<20)
    {
        nrPage=4;
        [l_room setText:NSLocalizedString(@"Bed_room", nil)];
        [director runWithScene:[BedRoom scene]];
    }
    else
    {
        nrPage=0;
        [l_room setText:NSLocalizedString(@"Game_room", nil)];
        [director runWithScene:[GameLabScene scene]];
    }
   
}

-(void)setStates
{
    float life;
    float food;
    float fun;
    float energy;
    float min, max;
    if (!IS_IPAD)
    {
        life=[stateTamagochi health]/(1.7+[stateTamagochi health]/57);
        food=[stateTamagochi food]/(1.7+[stateTamagochi food]/57);
        fun=[stateTamagochi funny]/(1.7+[stateTamagochi funny]/57);
        energy=[stateTamagochi energy]/(1.7+[stateTamagochi energy]/57);
        
        if (food>37.0) food=37.0;
        if (life>37.0) life=37.0;
        if (fun>37.0) fun=37.0;
        if (energy>37.0) energy=37.0;
        
        min=0;
        max=35;
    }
    else
    {
        life=[stateTamagochi health]/(0.12+[stateTamagochi health]/100);
        food=[stateTamagochi food]/(0.12+[stateTamagochi food]/100);
        if (food>89) food=89;
        fun=[stateTamagochi funny]/(0.12+[stateTamagochi funny]/100);
        energy=[stateTamagochi energy]/(0.12+[stateTamagochi energy]/100);
        min=1;
        max=89;
    }
    
    [i_life setFrame:CGRectMake(i_life.frame.origin.x, min+(max-life),  i_life.frame.size.width, life)];
    [i_health setFrame:CGRectMake(i_health.frame.origin.x, min+(max-food),  i_health.frame.size.width, food)];
    [i_funny setFrame:CGRectMake(i_funny.frame.origin.x, min+(max-fun),  i_funny.frame.size.width, fun)];
    [i_energy setFrame:CGRectMake(i_energy.frame.origin.x, min+(max-energy),  i_energy.frame.size.width, energy)];
    [l_lvl setText:[NSString stringWithFormat:@"%d",  [stateTamagochi level]]];
    [l_score setText:[NSString stringWithFormat:@"%d",  [stateTamagochi moneyGame]]];
}

-(void)openTV
{
    if ([stateTamagochi soundOn])
        [[SimpleAudioEngine sharedEngine] playEffect:@"btn_press.mp3" loop:NO];
    
    if (!viewTV.superview)
    {
        [viewTV setFrame:CGRectMake(0, 0, WIDTH_DEVICE, HEIGHT_DEVICE)];
        [self.view insertSubview:viewTV atIndex:1];
    }
    else
    {
        [viewTV removeFromSuperview];
    }
}

-(void)loadDataArray
{
    [_dataShopBody removeAllObjects];
    [_dataShopBody addObjectsFromArray:[stateTamagochi selectBodyShop:[stateTamagochi shopCategory]]];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    
    [super viewDidAppear:YES];
    if (![stateTamagochi loaded]) {
        [stateTamagochi setLoaded:YES];
        [stateTamagochi removeNotificationsOnEnter];
       // [self firstStart];
        //TODO: LABELS
        [self completeLabels];
    } else {
        return;
    }
    
}

- (void)viewDidLoad
{
    [[SimpleAudioEngine sharedEngine] preloadEffect:@"btn_press.mp3"];
    [[SimpleAudioEngine sharedEngine] preloadEffect:@"next_btn.wav"];
    [[SimpleAudioEngine sharedEngine] preloadEffect:@"buy.wav"];
    
    selectedColor=0;
    otherPage=0;
    lockedObject=-1;
    _dataShopBody = [[NSMutableArray alloc] init];
    [self loadDataArray];
   
    [[UIApplication sharedApplication] setStatusBarOrientation:UIDeviceOrientationPortrait animated:NO];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(menuTapped:)];
    [menuView addGestureRecognizer:tap];
    
    carousel=[[iCarousel alloc] initWithFrame:CGRectMake(80, -175, 150, 150)];
    carousel.type = iCarouselTypeCylinder;
    [carousel setUserInteractionEnabled:NO];
    carousel.delegate=self;
    carousel.dataSource=self;
    [self.view insertSubview:carousel atIndex:0];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(carouselMove:) name:@"moveCarousel" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(setStates) name:@"updateStates" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(openTV) name:@"openTV" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(showShopView:) name:@"shopView" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(nextLevelShow) name:@"nextLevel" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(renameNameRoom:) name:@"renameRoom" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(nextRoom:) name:@"nextRoom" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(enableButtons) name:@"enableButtons" object:nil];
  
    //TODO: closeGame NOTIFICATION
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(closeGame) name:@"closeGame" object:nil];
    //

    
    [shopView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
    [self.view insertSubview:shopView atIndex:3];
    UITapGestureRecognizer *tapRecognizer;
    tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(nameRoomTap:)];
    tapRecognizer.numberOfTapsRequired = 1;
    tapRecognizer.numberOfTouchesRequired = 1;
    [l_room addGestureRecognizer:tapRecognizer];
    [stateTamagochi selectStatesTamagochi];
    
    
    [self setStates];
    [stateTamagochi updateEnergy];
    [self setupCocos2D];
    [stateTamagochi updateFood];
    [stateTamagochi updateHealth];
    [stateTamagochi updateFun];
    [stateTamagochi updateSizeLevel];
    
    [[[personaj person] tamagochi] runAction:[CCScaleTo actionWithDuration:0.2 scaleX:[stateTamagochi fat] scaleY:[[personaj person] tamagochi].scaleY]];
    if ([[personaj person] getChildByTag:5])
    [[[personaj person] haine] runAction:[CCScaleTo actionWithDuration:0.2 scaleX:[stateTamagochi fat] scaleY:[[personaj person] haine].scaleY]];
   
    [stateTamagochi firstStart];
    [self.view bringSubviewToFront:viewPanelButonsStatistic];
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)removeViewsGameRoom
{
     [highScoreView setFrame:CGRectMake(0, -HEIGHT_DEVICE, 320, HEIGHT_DEVICE)];
     [shareView setFrame:CGRectMake(0, -HEIGHT_DEVICE, 320, HEIGHT_DEVICE)];
     [viewTV removeFromSuperview];
}

-(void)renameNameRoom:(NSNotification *)notif
{
    [l_room setText:NSLocalizedString(notif.object, nil)];
}
-(void)nameRoomTap:(UITapGestureRecognizer *)recognizer
{
   [stateTamagochi setNameRoomTap:YES];
   if (nrPage==4 && [CCDirector sharedDirector].runningScene.tag==10)
   {
        if ([stateTamagochi soundOn])
            [[SimpleAudioEngine sharedEngine] playEffect:@"btn_press.mp3" loop:NO];
       
        [[personaj person] removeFromParentAndCleanup:NO];
        [l_room setText:NSLocalizedString(@"Bed_room", nil)];
        [[CCDirectorIOS sharedDirector] replaceScene:[BedRoom scene]];
        return;
   }
   
    if (nrPage==5 && [CCDirector sharedDirector].runningScene.tag==11)
    {
        if ([stateTamagochi soundOn])
             [[SimpleAudioEngine sharedEngine] playEffect:@"btn_press.mp3" loop:NO];
        
        [viewCoins setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
        [viewEats setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
        [selectColor setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
        [shopView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
        [viewBodyShop setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
        [skinsView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
        
        [self setTexturesDefault];
        [[personaj person] removeFromParentAndCleanup:NO];
        [l_room setText:NSLocalizedString(@"Mall_room", nil)];
        [[CCDirectorIOS sharedDirector] replaceScene:[MallRoom scene]];
    }
    
    [self performBlock:^{
        [stateTamagochi setNameRoomTap:NO];
    } afterDelay:0.2];
}
-(void)reloadViewData
{
    for (int i=1;i<=10; i++)
    {
        [(UIButton *)[selectColor viewWithTag:i] setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    }
}
-(void)showColors:(NSIndexPath*)index
{
    if ([stateTamagochi  shopCategory]==sh_skins && nrPage==5)
    {
        [tapCoinsView setUserInteractionEnabled:NO];
        mallTap.enabled=NO;
        btnNext.enabled=NO;
        btnBack.enabled=NO;
        btn_settings.enabled=NO;
        menuView.userInteractionEnabled=NO;
        nrPage=[stateTamagochi skinCategory]-2;
        [self next:nil];
    }
  
    NSMutableArray *arr;
    
    [self reloadViewData];
  
    if (index)
    {
        ShopCell *cell = (ShopCell*)[tableShop cellForRowAtIndexPath:index];
        arr=[NSMutableArray arrayWithArray:[stateTamagochi selectShoppedColors:(cell)?cell.buttonBody.tag:[[[_dataShopBody objectAtIndex:index.row] objectAtIndex:4]intValue]]];
    }
    else
    {
         arr=[NSMutableArray arrayWithArray:[stateTamagochi selectShoppedColors:[[[_dataShopBody objectAtIndex:0] objectAtIndex:4] intValue]]];
    }
    
    for (int i=0;i<[arr count]; i++)
    {
        [(UIButton *)[selectColor viewWithTag:[[arr objectAtIndex:i] intValue]] setImage:[UIImage imageNamed:@"selected_yes.png"] forState:UIControlStateNormal];
    }
    selectedColor=0;
    if ([stateTamagochi shopCategory]!=sh_potions && [stateTamagochi shopCategory]!=sh_eat)
    {
        
    if (!selectColor.superview)
        [self.view insertSubview:selectColor atIndex:4];
    [selectColor setFrame:CGRectMake(0, 0, WIDTH_DEVICE, HEIGHT_DEVICE)];
        
    unlockView.alpha=0.0;
    [btnBuy setEnabled:NO];
    //[lbl_btnBuy setText:NSLocalizedString(@"Buy", nil)];
    [priceProduct setText:@"0"];
    
    [selectColorsView setFrame:CGRectMake(selectColorsView.frame.origin.x, HEIGHT_DEVICE-selectColorsView.frame.size.height, selectColorsView.frame.size.width, selectColorsView.frame.size.height)];
     
    int category= [stateTamagochi selectNameSkin:[[[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:4]intValue]];
        
    if ([stateTamagochi shopCategory]==sh_skins && (category==2 || category==5 || category==8))
    {
        [selectColorsView setFrame:CGRectMake(selectColorsView.frame.origin.x, selectColorsView.frame.origin.y-150, selectColorsView.frame.size.width, selectColorsView.frame.size.height)];
    }
    else
        [btnBuy setEnabled:NO];
        
    [self closeShop:selectColor];
    shopName.text=@"";
    if ([stateTamagochi shopCategory]!=sh_skins)
    {
        [changebleColor setBackgroundImage:[UIImage imageNamed:@"color_7.png"] forState:UIControlStateNormal];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"showBackgroundAlpha" object:@"2"];
    }
    else
        [changebleColor setBackgroundImage:[UIImage imageNamed:@"color_7 copy.png"] forState:UIControlStateNormal];
    }
}

-(void)changeCellInTable:(NSIndexPath *)index table:(UITableView *)table
{
    lockedObject=[index row];
    [table beginUpdates];
    [table reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationRight];
    [table endUpdates];
  
}

- (IBAction)unlockObject:(id)sender forEvent:(UIEvent *)event
{
    switch ([sender tag]) {
        case 0:
        {
            if ([stateTamagochi moneyGame]-1999>0)
            {
                lockedObject=-1;
                [stateTamagochi unlockColor:[[[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:4] intValue] color:0 group:@"0"];
                [stateTamagochi setMoneyGame:([[l_score text] intValue]-1999)];
                [stateTamagochi updateMoney];
                [self loadDataArray];
                [[SimpleAudioEngine sharedEngine] playEffect:@"unlock.wav" loop:NO];
            }

        }
            break;
        case 3:
        {
            lockedObject=-1;
        }
            break;
        case 4:
        {
            nrPage=-1;
            [self next:nil];
            
            [self performBlock:^{
                [self openTV];
               
            }afterDelay:0.7];
          
            return;
        }
            break;
        case 5:
        {
            showCoins=NO;
            if (!viewCoins.superview)
                [self.view insertSubview:viewCoins atIndex:1];
            viewCoins.viewBackground.alpha=0.0;
             [viewCoins.btnBack setHidden:NO];
            [viewCoins refresh];
            if ([stateTamagochi shopCategory]==sh_eat)
                 [viewEats setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE)];
            if ([stateTamagochi shopCategory]==sh_skins)
                [shopView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE)];
            [viewCoins setFrame:CGRectMake(0, 0, WIDTH_DEVICE, HEIGHT_DEVICE)];
          //  [[NSNotificationCenter defaultCenter]postNotificationName:@"showBackgroundAlpha" object:@"1"];
            return;
        }
            
        default:
            break;
    }
    
    UIButton *btn =(UIButton*)sender;
    UITouch *touch = [[event touchesForView:btn] anyObject];
  
    switch ([stateTamagochi shopCategory]) {
        case sh_eat:
        {
            NSIndexPath *index=[tableEat indexPathForRowAtPoint:[touch locationInView: tableEat]];
            if (lockedObject!=-1)
            {
                LockedCell *cell = (LockedCell *)[tableEat cellForRowAtIndexPath:index];
                cell.lockedText.text=[NSString stringWithFormat:@"%@ %@ %d %@. %@",NSLocalizedString(@"deneg_noli", nil), NSLocalizedString(@"deneg_skoliko", nil), 1999-[stateTamagochi moneyGame], NSLocalizedString(@"deneg_skoliko_1", nil), NSLocalizedString(@"deneg_net", nil)];
                cell.lockedText.textColor=[UIColor orangeColor];
                cell.lockedText.font=[UIFont fontWithName:@"Marker Felt" size:12];
                cell.lockedText.textAlignment=UITextAlignmentCenter;
                cell.waitButton.alpha=1.0;
                [cell.unlockButton setFrame:CGRectMake(cell.unlockButton.frame.origin.x+10, cell.unlockButton.frame.origin.y, cell.unlockButton.frame.size.width, cell.unlockButton.frame.size.height)];
                cell.unlockButton.tag=5;
                cell.titleUnlockBtn.text=NSLocalizedString(@"Shop_c", nil);
                cell.titleWaitBtn.text=NSLocalizedString(@"Play_g", nil);
                cell.titleUnlockBtn.frame=cell.unlockButton.frame;
                cell.imgLock.alpha=0.0;
                cell.titleWaitBtn.alpha=1.0;

            }
            else
            {
                if ([sender tag]!=3)
                {
                    [tableEat beginUpdates];
                    [tableEat reloadData];
                    [tableEat endUpdates];
                }
                else
                      [tableEat reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationLeft];
            }
        }
            break;
        case sh_skins:
        {
            NSIndexPath *index=[tableShop indexPathForRowAtPoint:[touch locationInView: tableShop]];
            if (lockedObject!=-1)
            {
                LockedCell *cell = (LockedCell *)[tableShop cellForRowAtIndexPath:index];
                cell.lockedText.text=[NSString stringWithFormat:@"%@ %@ %d %@. %@",NSLocalizedString(@"deneg_noli", nil), NSLocalizedString(@"deneg_skoliko", nil), 1999-[stateTamagochi moneyGame], NSLocalizedString(@"deneg_skoliko_1", nil), NSLocalizedString(@"deneg_net", nil)];
                cell.lockedText.textColor=[UIColor orangeColor];
                cell.lockedText.font=[UIFont fontWithName:@"Marker Felt" size:12];
                cell.lockedText.textAlignment=UITextAlignmentCenter;
                cell.waitButton.alpha=1.0;
                [cell.unlockButton setFrame:CGRectMake(cell.unlockButton.frame.origin.x+10, cell.unlockButton.frame.origin.y, cell.unlockButton.frame.size.width, cell.unlockButton.frame.size.height)];
                cell.unlockButton.tag=5;
                cell.titleUnlockBtn.text=NSLocalizedString(@"Shop_c", nil);
                cell.titleWaitBtn.text=NSLocalizedString(@"Play_g", nil);
                cell.titleUnlockBtn.frame=cell.unlockButton.frame;
                cell.imgLock.alpha=0.0;
                cell.titleWaitBtn.alpha=1.0;
            }
            else
            {
                if ([sender tag]!=3)
                {
                    [tableShop beginUpdates];
                    [tableShop reloadData];
                    [tableShop endUpdates];
                }
                else
                    [tableShop reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationLeft];

            }
        }
            break;
            
        default:
            break;
    }
   
}

- (IBAction)updateShopObject:(id)sender forEvent:(UIEvent *)event {

    UIButton *btn =(UIButton*)sender;
    UITouch *touch = [[event touchesForView:btn] anyObject];
    if ([stateTamagochi shopCategory]!=sh_eat)
        _rowTapped = [[tableShop indexPathForRowAtPoint:[touch locationInView: tableShop]] retain];
    else
        _rowTapped = [[tableEat indexPathForRowAtPoint:[touch locationInView: tableEat]] retain];
    
    if ([[[sender titleLabel] text] isEqualToString:@"Locked"])
    {
         [[SimpleAudioEngine sharedEngine] playEffect:@"btn_press.mp3" loop:NO];
        [tableEat reloadData];
        [self changeCellInTable:_rowTapped table:tableEat];
        return;
    }
    
    if (([stateTamagochi shopCategory]==sh_potions || [stateTamagochi shopCategory]==sh_eat ) && ![[[_dataShopBody objectAtIndex:0] objectAtIndex:1] isEqualToString:@""])
    {
        [self updateShopColor:nil];
    }
}

- (IBAction)selectEat:(id)sender {
     if ([stateTamagochi soundOn])
         [[SimpleAudioEngine sharedEngine] playEffect:@"btn_press.mp3" loop:NO];
    
    lockedObject=-1;
    [stateTamagochi setEatCategory:[sender tag]];
    [self loadDataArray];
    [tableEat reloadData];
    [tableEat setContentOffset:CGPointZero animated:NO];
    switch ([sender tag]) {
        case 1:
        {
            [btn_Eat_1 setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"selectFood_back%@.png", (IS_IPAD)?@"-iPad":@""]] forState:UIControlStateNormal];
            [btn_Eat_2 setBackgroundImage:nil forState:UIControlStateNormal];
            [btn_Eat_3 setBackgroundImage:nil forState:UIControlStateNormal];
            [btn_Eat_4 setBackgroundImage:nil forState:UIControlStateNormal];
        }
            break;
        case 2:
        {
            [btn_Eat_2 setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"selectFood_back%@.png", (IS_IPAD)?@"-iPad":@""]] forState:UIControlStateNormal];
            [btn_Eat_1 setBackgroundImage:nil forState:UIControlStateNormal];
            [btn_Eat_3 setBackgroundImage:nil forState:UIControlStateNormal];
            [btn_Eat_4 setBackgroundImage:nil forState:UIControlStateNormal];
        }
            break;
        case 3:
        {
            [btn_Eat_3 setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"selectFood_back%@.png", (IS_IPAD)?@"-iPad":@""]] forState:UIControlStateNormal];
            [btn_Eat_2 setBackgroundImage:nil forState:UIControlStateNormal];
            [btn_Eat_1 setBackgroundImage:nil forState:UIControlStateNormal];
            [btn_Eat_4 setBackgroundImage:nil forState:UIControlStateNormal];
        }
            break;
        case 4:
        {
            [btn_Eat_4 setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"selectFood_back%@.png", (IS_IPAD)?@"-iPad":@""]] forState:UIControlStateNormal];
            [btn_Eat_2 setBackgroundImage:nil forState:UIControlStateNormal];
            [btn_Eat_3 setBackgroundImage:nil forState:UIControlStateNormal];
            [btn_Eat_1 setBackgroundImage:nil forState:UIControlStateNormal];
        }
            break;
            
        default:
            break;
    }
}

-(void)shopSkins:(id)sender
{
    if ([sender imageForState:UIControlStateNormal]==[UIImage imageNamed:@"selected_yes.png"])
    {
        if ([stateTamagochi selectColorSkin:[stateTamagochi selectNameSkin:[[[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:4] intValue]] room:[[[[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:1] substringFromIndex:1] intValue]]==[sender tag])
                imageBuy.image=[UIImage imageNamed:@"selected_yes.png"];
        else
                imageBuy.image=[UIImage imageNamed:@""];

        unlockView.alpha=0.0;
        [priceProduct setHidden:YES];
        [lbl_btnBuy setText:@"Owned"];
        btnBuy.tag=1;
    }
    else
    {

        int price= [stateTamagochi returnPriceSkin:[[[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:4] intValue]];
        [priceProduct setHidden:NO];
        if (price>0)
        {
            unlockView.alpha=0.0;
            imageBuy.image=[UIImage imageNamed:@"money_3.png"];
            [lbl_btnBuy setText:NSLocalizedString(@"Buy", nil)];
            btnBuy.tag=0;
            priceProduct.text=[NSString stringWithFormat:@"%d",price];
        }
        else
        {
            btnBuy.tag=0;
            [btnBuy setEnabled:NO];
            unlockView.alpha=1.0;
            waitBtn.alpha=0.0;
            titileWaitBtn.alpha=0.0;
            unlockBtn.tag=11;
            [unlockBtn setFrame:CGRectMake(WIDTH_DEVICE/2-unlockBtn.frame.size.width/2, unlockBtn.frame.origin.y, unlockBtn.frame.size.width, unlockBtn.frame.size.height)];
            titleUnlockBtn.frame=CGRectMake(unlockBtn.frame.origin.x+10, unlockBtn.frame.origin.y, unlockBtn.frame.size.width-20, unlockBtn.frame.size.height);
            titleUnlockBtn.text=NSLocalizedString(@"Unlock", nil);
            locked_Text.text=NSLocalizedString(@"Locked_text", nil);
            locked_Text.textColor=[UIColor orangeColor];
            locked_Text.font=[UIFont fontWithName:@"Marker Felt" size:16];
            locked_Text.textAlignment=UITextAlignmentCenter;
            imageBuy.image=[UIImage imageNamed:@"lock.png"];
            [lbl_btnBuy setText:NSLocalizedString(@"Locked", nil)];
            priceProduct.text=@"";
        }
    }
    selectedColor=[sender tag];
     [[NSNotificationCenter defaultCenter]postNotificationName:@"skinColor" object:[NSString stringWithFormat:@"%d, %d", [sender tag],[stateTamagochi selectNameSkin:[[[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:4] intValue]]]];
}

-(IBAction)changeColorSelected:(id)sender {
    [btnBuy setEnabled:YES];
   [[SimpleAudioEngine sharedEngine] playEffect:@"apply.mp3" loop:NO];
    if ([stateTamagochi shopCategory]==sh_skins)
    {
        [self shopSkins:(id)sender];
        return;
    }

    if ([sender imageForState:UIControlStateNormal]==[UIImage imageNamed:@"selected_yes.png"])
    {
        [priceProduct setHidden:YES];
        [lbl_btnBuy setText:@"Owned"];
        btnBuy.tag=1;
       
            if ([[[personaj bodyShopped] objectAtIndex:[stateTamagochi bodyCategory]-1] isEqualToString:[NSString stringWithFormat:@"%@%@_%d.png",[[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:0], [[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:1], [sender tag]]])
            {
                imageBuy.image=[UIImage imageNamed:@"selected_yes.png"];
            }
            else
            {
                imageBuy.image=[UIImage imageNamed:@""];
            }
        unlockView.alpha=0.0;
    }
    else
    {
    
        btnBuy.tag=0;
        int price= [stateTamagochi returnPrice:[[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:0] group:[[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:1] nr:[sender tag]];
        [priceProduct setHidden:NO];
        if (price>0)
        {
            unlockView.alpha=0.0;
            imageBuy.image=[UIImage imageNamed:@"money_3.png"];
            [lbl_btnBuy setText:NSLocalizedString(@"Buy", nil)];
            priceProduct.text=[NSString stringWithFormat:@"%d",price];
        }
        else
        {
            [btnBuy setEnabled:NO];
            unlockView.alpha=1.0;
            waitBtn.alpha=0.0;
            titileWaitBtn.alpha=0.0;
            unlockIcon.alpha=1.0;
            titleUnlockBtn.text=NSLocalizedString(@"Unlock", nil);
            unlockBtn.tag=11;
            [unlockBtn setFrame:CGRectMake(WIDTH_DEVICE/2-unlockBtn.frame.size.width/2, unlockBtn.frame.origin.y, unlockBtn.frame.size.width, unlockBtn.frame.size.height)];
            unlockBtn.imageView.image=[UIImage imageNamed:@"btn_lock.png"];
            titleUnlockBtn.frame=CGRectMake(unlockBtn.frame.origin.x+10, unlockBtn.frame.origin.y, unlockBtn.frame.size.width-20, unlockBtn.frame.size.height);
            locked_Text.text=NSLocalizedString(@"Locked_text", nil);
            locked_Text.textColor=[UIColor orangeColor];
            locked_Text.font=[UIFont fontWithName:@"Marker Felt" size:18];
            locked_Text.textAlignment=UITextAlignmentCenter;
            imageBuy.image=[UIImage imageNamed:@"lock.png"];
            [lbl_btnBuy setText:NSLocalizedString(@"Locked", nil)];
            priceProduct.text=[NSString stringWithFormat:@"%d",[stateTamagochi returnLevel:[[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:0] group:[[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:1] nr:[sender tag]]];
        }
    }
 
    
    if (![[personaj person] getChildByTag: [stateTamagochi bodyCategory]] && [stateTamagochi bodyCategory]!=12)
    {
    
        [[personaj person] createChildByTag:[stateTamagochi bodyCategory] nameTexture:[NSString stringWithFormat:@"%@%@_%d.png",[[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:0], [[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:1], [sender tag]]];
        
    }
    else
    {
        
        if ([stateTamagochi bodyCategory]!=12)
        {
            if ([stateTamagochi bodyCategory]==6)
            {
                [[[personaj person] getChildByTag:16] setTexture:[[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"yeylid_up_%d%@.png",[sender tag], (IS_IPAD)?@"-iPad":@""]]];
                [[[personaj person] getChildByTag:17] setTexture:[[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"yeylid_down_%d%@.png",[sender tag], (IS_IPAD)?@"-iPad":@""]]];
            }
            CCTexture2D* texture = [[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"%@%@_%d.png",[[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:0], [[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:1], [sender tag]]];
            [[[personaj person] getChildByTag: [stateTamagochi bodyCategory]] setTexture:texture];
            [[[personaj person] getChildByTag: [stateTamagochi bodyCategory]] setTextureRect:CGRectMake(0.0f, 0.0f, [texture contentSize].width,[texture contentSize].height)];
            [[personaj person] setPositionByIndex:[stateTamagochi bodyCategory]];
        }
        else
        {
            int tags=21+[[[[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:1] substringFromIndex:1] intValue];
            if (![[personaj person] getChildByTag: tags])
            {
                [[personaj person] createTatooChild:tags nameTexture:[NSString stringWithFormat:@"%@%@_%d.png",[[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:0], [[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:1], [sender tag]] position:CGPointZero
                 
             ];
            }
            else
            {
                CCTexture2D* texture = [[CCTextureCache sharedTextureCache] addImage:[NSString stringWithFormat:@"%@%@_%d.png",[[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:0], [[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:1], [sender tag]]];
                [[[personaj person] getChildByTag: tags] setTexture:texture];
                [[[personaj person] getChildByTag: tags] setTextureRect:CGRectMake(0.0f, 0.0f, [texture contentSize].width,[texture contentSize].height)];
                [[personaj person] setPositionByIndex:[stateTamagochi bodyCategory]];
            }
        }
    }
  
    if ([stateTamagochi bodyCategory]==4)
    {
        [[[personaj person] getChildByTag:9] removeFromParentAndCleanup:NO];
    }
    else if ([stateTamagochi bodyCategory]==9)
        [[[personaj person] getChildByTag:4] removeFromParentAndCleanup:NO];
    else if ([stateTamagochi bodyCategory]==12)
        [[[personaj person] getChildByTag:5] removeFromParentAndCleanup:NO];
    selectedColor=[sender tag];
    
}

-(void)alertShow:(NSString *)title message:(NSString *)message
{
   
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    [alert release];
}
-(IBAction)updateShopColor:(id)sender {
   

    if (!_rowTapped)
    {
    if ([sender tag]==0)
    {
        if ([[l_score text] intValue]-[stateTamagochi getPriceProduct:[[[_dataShopBody objectAtIndex:0] objectAtIndex:4] intValue]]>=0)
        {
            if ([stateTamagochi soundOn])
                 [[SimpleAudioEngine sharedEngine] playEffect:@"buy.wav" loop:NO];
            
            [stateTamagochi shopProduct:[[[_dataShopBody objectAtIndex:0] objectAtIndex:4] intValue]  index2:selectedColor];
            [stateTamagochi setMoneyGame:([[l_score text] intValue]-[stateTamagochi getPriceProduct:[[[_dataShopBody objectAtIndex:0] objectAtIndex:4] intValue]])];
            [stateTamagochi updateMoney];
            
        }
        else
        {
           // [self alertShow:@"Deneg noli" message:@"igraite!!!"];
            NSLog(@"iopta nu se poate, nai bani 1");
            return;
        }
    }
        
    }
    else 
    {
       
        ShopCell *cell;
        if ([stateTamagochi shopCategory]!=sh_eat)
            cell= (ShopCell*)[tableShop cellForRowAtIndexPath:_rowTapped];
        else
            cell= (ShopCell*)[tableEat cellForRowAtIndexPath:_rowTapped];
    if ([sender tag]==0)
    {
        int price= [stateTamagochi getPriceProduct:(cell)?cell.buttonBody.tag:[[[_dataShopBody objectAtIndex:_rowTapped.row] objectAtIndex:4]intValue]];
        if ([[l_score text] intValue]-price>=0)
        {
            if ([stateTamagochi soundOn])
                [[SimpleAudioEngine sharedEngine] playEffect:@"buy.wav" loop:NO];
            
            [stateTamagochi shopProduct:(cell)?cell.buttonBody.tag:[[[_dataShopBody objectAtIndex:_rowTapped.row] objectAtIndex:4]intValue] index2:selectedColor];
            [stateTamagochi setMoneyGame:([[l_score text] intValue]-price)];
            [stateTamagochi updateMoney];
        }
        else
        {
            unlockView.alpha=1.0;
            waitBtn.alpha=1.0;
            titileWaitBtn.alpha=1.0;
            unlockBtn.tag=12;
            [unlockBtn setFrame:CGRectMake(WIDTH_DEVICE/2+5, unlockBtn.frame.origin.y, unlockBtn.frame.size.width, unlockBtn.frame.size.height)];
            titleUnlockBtn.frame=CGRectMake(unlockBtn.frame.origin.x+10, unlockBtn.frame.origin.y, unlockBtn.frame.size.width-20, unlockBtn.frame.size.height);
            locked_Text.text=[NSString stringWithFormat:@"%@ %@ %d %@. %@",NSLocalizedString(@"deneg_noli", nil), NSLocalizedString(@"deneg_skoliko", nil), price-[[l_score text] intValue], NSLocalizedString(@"deneg_skoliko_1", nil), NSLocalizedString(@"deneg_net", nil)];
            locked_Text.textColor=[UIColor orangeColor];
            locked_Text.font=[UIFont fontWithName:@"Marker Felt" size:18];
            locked_Text.textAlignment=UITextAlignmentCenter;
            unlockIcon.alpha=0.0;
            titleUnlockBtn.text=NSLocalizedString(@"Shop_c", nil);
            titileWaitBtn.text=NSLocalizedString(@"Play_g", nil);
            [[SimpleAudioEngine sharedEngine] playEffect:@"reflection.mp3" loop:NO];
            if (!sender)
                [self alertShow:NSLocalizedString(@"deneg_noli", nil) message:[NSString stringWithFormat:@"%@ %d %@. \n\n %@",NSLocalizedString(@"deneg_skoliko", nil), price-[[l_score text] intValue], NSLocalizedString(@"deneg_skoliko_1", nil), NSLocalizedString(@"deneg_net", nil)  ]];
            return;
        }
    }
    [self loadDataArray];
   
    if ([stateTamagochi shopCategory]==sh_potions || [stateTamagochi shopCategory]==sh_eat)
    {
        if ([stateTamagochi shopCategory]!=sh_eat)
            [tableShop reloadRowsAtIndexPaths:@[_rowTapped] withRowAnimation:UITableViewRowAnimationNone];
        else
            [tableEat reloadRowsAtIndexPaths:@[_rowTapped] withRowAnimation:UITableViewRowAnimationNone];
    }
    }

    if (selectedColor!=0 && [stateTamagochi shopCategory]!=sh_skins)
    {
                if ([stateTamagochi bodyCategory]!=12)
        [personaj replaceTexturePerson:[stateTamagochi bodyCategory] obj:[NSString stringWithFormat:@"%@%@_%d.png", [[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:0], [[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:1], selectedColor]];
        else
            [personaj replaceTatooPerson:[NSString stringWithFormat:@"%@%@_%d.png", [[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:0], [[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:1], selectedColor]];
        if ([stateTamagochi bodyCategory]==6)
        {
            [personaj replaceTexturePerson:13 obj:[NSString stringWithFormat:@"yeylid_up_%d%@.png",  selectedColor,  (IS_IPAD)?@"-iPad":@""]];
            [personaj replaceTexturePerson:14 obj:[NSString stringWithFormat:@"yeylid_down_%d%@.png",  selectedColor,  (IS_IPAD)?@"-iPad":@""]];
        }
        [[personaj person] setPositionByIndex:[stateTamagochi bodyCategory]];
    }
    
    if (selectedColor!=0 && [stateTamagochi shopCategory]==sh_skins)
    {
        [stateTamagochi updateColorSkin:[[[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:4] intValue] color:selectedColor];
    }
    [priceProduct setHidden:YES];
    [lbl_btnBuy setText:@"Owned"];
    imageBuy.image=[UIImage imageNamed:@"selected_yes.png"];
    [self showColors:_rowTapped];
}

- (IBAction)skinsSelect:(id)sender {
    if ([stateTamagochi soundOn])
         [[SimpleAudioEngine sharedEngine] playEffect:@"btn_press.mp3" loop:NO];
    
    [stateTamagochi setSkinCategory:[sender tag]];
    [self loadDataArray];
    [skinsView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE)];
    if ([stateTamagochi shopCategory]!=sh_potions && ![[[_dataShopBody objectAtIndex:0] objectAtIndex:1] isEqualToString:@""])
    {
        lockedObject=-1;
        [tableShop reloadData];
        [tableShop setContentOffset:CGPointZero animated:NO];
        [btnBackShop setHidden:NO];
        [shopView setFrame:CGRectMake(0, 0, WIDTH_DEVICE, HEIGHT_DEVICE)];
    }
    else
    {
        _rowTapped=nil;
        [self showColors:nil];
    }
}

- (IBAction)unlockColor:(id)sender {
    switch ([sender tag]) {
        case 0:
        {
            if ([stateTamagochi bodyCategory]!=12)
                [personaj replaceTextureDefault: [stateTamagochi bodyCategory]];
            else
                [personaj replaceTatooDefault:[[[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:1] substringFromIndex:1]];
            if ([stateTamagochi bodyCategory]==4)
                [personaj replaceTextureDefault: 9];
            else if ([stateTamagochi bodyCategory]==9)
                [personaj replaceTextureDefault: 4];
            else if ([stateTamagochi bodyCategory]==12)
                [personaj replaceTextureDefault: 5];
            nrPage=-1;
            [self next:nil];
            
            [self performBlock:^{
                [[NSNotificationCenter defaultCenter]postNotificationName:@"playGames" object:nil];
            }afterDelay:0.7];
        }
            break;
        case 11:
        {
            if ( [stateTamagochi moneyGame]-1999>0)
            {
                [stateTamagochi unlockColor:[[[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:4]intValue] color:selectedColor group:[[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:1]];
                int price;
                if ([stateTamagochi shopCategory]==sh_body)
                    price=[stateTamagochi returnPrice:[[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:0] group:[[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:1] nr:selectedColor];
                else if ([stateTamagochi shopCategory]==sh_skins)
                    price= [stateTamagochi returnPriceSkin:[[[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:4] intValue]];
                [self loadDataArray];
                
                [tableShop reloadData];
                [stateTamagochi setMoneyGame:([[l_score text] intValue]-1999)];
                [stateTamagochi updateMoney];
                [btnBuy setEnabled:YES];
                unlockView.alpha=0.0;
                imageBuy.image=[UIImage imageNamed:@"money_3.png"];
                [lbl_btnBuy setText:NSLocalizedString(@"Buy", nil)];
                priceProduct.text=[NSString stringWithFormat:@"%d",price];
                [[SimpleAudioEngine sharedEngine] playEffect:@"unlock.wav" loop:NO];
            }
            else
            {
                waitBtn.alpha=1.0;
                titileWaitBtn.alpha=1.0;
                unlockBtn.tag=12;
                [unlockBtn setFrame:CGRectMake(WIDTH_DEVICE/2+5, unlockBtn.frame.origin.y, unlockBtn.frame.size.width, unlockBtn.frame.size.height)];
                titleUnlockBtn.frame=CGRectMake(unlockBtn.frame.origin.x+10, unlockBtn.frame.origin.y, unlockBtn.frame.size.width-20, unlockBtn.frame.size.height);
                locked_Text.text=[NSString stringWithFormat:@"%@ %@ %d %@. %@",NSLocalizedString(@"deneg_noli", nil), NSLocalizedString(@"deneg_skoliko", nil), 1999-[stateTamagochi moneyGame], NSLocalizedString(@"deneg_skoliko_1", nil), NSLocalizedString(@"deneg_net", nil)];
                locked_Text.textColor=[UIColor orangeColor];
                locked_Text.font=[UIFont fontWithName:@"Marker Felt" size:18];
                locked_Text.textAlignment=UITextAlignmentCenter;
                unlockIcon.alpha=0.0;
                titleUnlockBtn.text=NSLocalizedString(@"Shop_c", nil);
                titileWaitBtn.text=NSLocalizedString(@"Play_g", nil);
            }

        }
            break;
        case 12:
        {
            showCoins=NO;
            if (!viewCoins.superview)
                [self.view insertSubview:viewCoins atIndex:1];
            viewCoins.viewBackground.alpha=0.0;
            [viewCoins refresh];
            [viewCoins.btnBack setHidden:NO];
            [viewCoins setFrame:CGRectMake(0, 0, WIDTH_DEVICE, HEIGHT_DEVICE)];
            [selectColor setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE)];
        }
            break;
            
        default:
            break;
    }
   }

- (IBAction)settings:(id)sender {
     if ([stateTamagochi soundOn])
         [[SimpleAudioEngine sharedEngine] playEffect:@"btn_press.mp3" loop:NO];
    
    if (viewCoins.frame.origin.y==0) [viewCoins setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
    if (nrPage==5)
    {
        
        
        [[NSNotificationCenter defaultCenter]postNotificationName:@"showBackgroundAlpha" object:@"1"];
        [viewCoins setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
        [viewEats setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
        [selectColor setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
         
        if (shopView.frame.origin.y==0) [shopView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
     
        [viewBodyShop setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
         
        [skinsView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
         
        [self setTexturesDefault];
        
    }
    
   if (statsView.superview)
       [statsView removeFromSuperview];
    if (!viewSettings.superview)
        [self.view insertSubview:viewSettings atIndex:3];
    if (shareView.superview)
       [shareView removeFromSuperview];
    [viewSettings setFrame:CGRectMake(0, 0, WIDTH_DEVICE, HEIGHT_DEVICE)];
    [[viewSettings switchMusic] setOn:[stateTamagochi musicOn]];
    [[viewSettings switchSound] setOn:[stateTamagochi soundOn]];
    [[viewSettings switchNotifications] setOn:[stateTamagochi notificationsOn]];
    [viewSettings back:nil];
}

- (IBAction)backDown:(id)sender {
    mallTap.userInteractionEnabled=NO;
    btnNext.userInteractionEnabled=NO;
    btnBack.userInteractionEnabled=NO;
    menuView.userInteractionEnabled=NO;
    btn_settings.userInteractionEnabled=NO;
}

- (IBAction)closeTV:(id)sender {
    if ([stateTamagochi soundOn])
        [[SimpleAudioEngine sharedEngine] playEffect:@"btn_press.mp3" loop:NO];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"closeTelevizor" object:nil];
    [viewTV removeFromSuperview];
}

- (IBAction)shopCoinsView:(id)sender {
    if ([stateTamagochi soundOn])
        [[SimpleAudioEngine sharedEngine] playEffect:@"btn_press.mp3" loop:NO];
    showCoins=YES;
    if (!viewCoins.superview)
        [self.view insertSubview:viewCoins atIndex:3];
    viewCoins.viewBackground.alpha=0.6;
    [viewCoins.btnBack setHidden:YES];
    [viewCoins refresh];
    [viewCoins setFrame:CGRectMake(0, 0, WIDTH_DEVICE, HEIGHT_DEVICE)];
    
    if (viewSettings.frame.origin.y==0)  [viewSettings setFrame:CGRectMake(0, HEIGHT_DEVICE+20, WIDTH_DEVICE, HEIGHT_DEVICE)];
    if (shareView.frame.origin.y==0) [shareView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
    if (statsView.superview)  [statsView removeFromSuperview];
    if (nrPage==5)
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"showBackgroundAlpha" object:@"1"];
        if (selectColor.frame.origin.y==0) [selectColor setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE)];
        if (shopView.frame.origin.y==0) [shopView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE)];
        if (viewBodyShop.frame.origin.y==0) [viewBodyShop setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE)];
        if (viewEats.frame.origin.y==0) [viewEats setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE)];
        if (skinsView.frame.origin.y==0) [skinsView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE)];
        if (shareView.frame.origin.y==0) [shareView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE)];
    }
     
    
}

-(void)nextLevelShow
{
    if (!nextLevelView.superview)
    {
        [[SimpleAudioEngine sharedEngine] playEffect:@"next_level.mp3" loop:NO];
        [nextLevelView setFrame:CGRectMake(0, 0, WIDTH_DEVICE, HEIGHT_DEVICE)];
        [self.view insertSubview:nextLevelView atIndex:4];
    }
    [nextLevelView reloadLabels];
}

- (IBAction)back:(id)sender {
    
    [textFieldWithNamePlayer resignFirstResponder];
    
    if (sender)
        [stateTamagochi setShopCategory:sh_default];
    
    if (sender &&  [stateTamagochi soundOn])
        [[SimpleAudioEngine sharedEngine] playEffect:@"next_btn.wav" loop:NO];
    [[personaj person] removeFromParentAndCleanup:NO];
    
    nrPage--;
    if (nrPage<=-1) nrPage=5;
    
    switch (nrPage) {
        case 0:
        {
            [mallTap setEnabled:YES];
            [l_room setText:NSLocalizedString(@"Game_room", nil)];
            [[CCDirectorIOS sharedDirector] replaceScene:[CCTransitionSlideInL transitionWithDuration:0.5 scene:[GameLabScene scene]]];
        }
            break;
        case 1:
        {
            [[[personaj person] getChildByTag:18] removeFromParentAndCleanup:YES];
            [self removeViewsGameRoom];
            [l_room setText:NSLocalizedString(@"Bath_room", nil)];
            [[CCDirectorIOS sharedDirector] replaceScene:[CCTransitionSlideInL transitionWithDuration:0.5 scene:[BathRoomScene scene]]];
        }
            break;
        case 2:
        {
            [l_room setText:NSLocalizedString(@"Studio_room", nil)];
            [[CCDirectorIOS sharedDirector] replaceScene:[CCTransitionSlideInL transitionWithDuration:0.5 scene:[StudioRoom scene]]];
        }
            break;
        case 3:
        {
            [stateTamagochi setSleep:NO];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"timerStop" object:nil];
            [viewEats setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
            [carousel setFrame:CGRectMake((WIDTH_DEVICE-150)/2, -175, 150, 150)];
            [l_room setText:NSLocalizedString(@"Kitchen_room", nil)];
            [[CCDirectorIOS sharedDirector] replaceScene:[CCTransitionSlideInL transitionWithDuration:0.5 scene:[KitchenRoom scene]]];
        }
            break;
        case 4:
        {
            [mallTap setEnabled:YES];
            [self setTexturesDefault];
            [selectColor setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
            [viewCoins setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
            [shopView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
            [viewBodyShop setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
            [skinsView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
            [viewEats setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
            //start scrolling
            
            [l_room setText:NSLocalizedString(@"Bed_room", nil)];
            [[CCDirectorIOS sharedDirector] replaceScene:[CCTransitionSlideInL transitionWithDuration:0.5 scene:[BedRoom scene]]];
        }
            break;
        case 5:
        {
            [[[personaj person] getChildByTag:18] removeFromParentAndCleanup:YES];
            [mallTap setEnabled:NO];
            [self removeViewsGameRoom];
            [stateTamagochi setSleep:NO];
            [carousel setFrame:CGRectMake((WIDTH_DEVICE-150)/2, -175, 150, 150)];
            [l_room setText:NSLocalizedString(@"Mall_room", nil)];
            [[CCDirectorIOS sharedDirector] replaceScene:[CCTransitionSlideInL transitionWithDuration:0.5 scene:[MallRoom scene]]];
        }

            
        default:
            break;
    }
    if (sender)
        [self performSelector:@selector(enableButtons) withObject:nil afterDelay:0.5];
        //[self performBlock:^{ [self enableButtons];} afterDelay:0.5];
}

-(void)enableButtons
{
    btnNext.userInteractionEnabled=YES;
    btnBack.userInteractionEnabled=YES;
    mallTap.userInteractionEnabled=YES;
    btn_settings.userInteractionEnabled=YES;
    menuView.userInteractionEnabled=YES;
}

- (IBAction)next:(id)sender {
     [textFieldWithNamePlayer resignFirstResponder];
    if (sender &&  [stateTamagochi soundOn])
        [[SimpleAudioEngine sharedEngine] playEffect:@"next_btn.wav" loop:NO];
    if (sender)
        [stateTamagochi setShopCategory:sh_default];
    
    float timp=0.0;
    if ([sender tag]==1)
        timp=0.5;
    nrPage++;
    [[personaj person] removeFromParentAndCleanup:NO];
    if (nrPage==6) nrPage=0;
    switch (nrPage) {
        case 0:
        {
            [mallTap setEnabled:YES];
            [self setTexturesDefault];
            [viewCoins setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
            [viewEats setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
            [selectColor setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
            [shopView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
            [viewBodyShop setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
            [skinsView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
            [l_room setText:NSLocalizedString(@"Game_room", nil)];
            [[CCDirectorIOS sharedDirector] replaceScene:[CCTransitionSlideInR transitionWithDuration:timp scene:[GameLabScene scene]]];
        }
            break;
        case 1:
        {
            [self removeViewsGameRoom];
            [l_room setText:NSLocalizedString(@"Bath_room", nil)];
            [[CCDirectorIOS sharedDirector] replaceScene:[CCTransitionSlideInR transitionWithDuration:timp scene:[BathRoomScene scene]]];
        }
            break;
        case 2:
        {
             [l_room setText:NSLocalizedString(@"Studio_room", nil)];
             [[CCDirectorIOS sharedDirector] replaceScene:[CCTransitionSlideInR transitionWithDuration:timp scene:[StudioRoom scene]]];
        }
            break;
        case 3:
        {
            [[[personaj person] getChildByTag:18] removeFromParentAndCleanup:YES];
            [carousel setFrame:CGRectMake((WIDTH_DEVICE-150)/2, -175, 150, 150)];
            [l_room setText:NSLocalizedString(@"Kitchen_room", nil)];
            [[CCDirectorIOS sharedDirector] replaceScene:[CCTransitionSlideInR transitionWithDuration:timp scene:[KitchenRoom scene]]];
        }
            break;
        case 4:
        {
            [viewEats setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
            [mallTap setEnabled:YES];
            [l_room setText:NSLocalizedString(@"Bed_room", nil)];
            [[CCDirectorIOS sharedDirector] replaceScene:[CCTransitionSlideInR transitionWithDuration:timp scene:[BedRoom scene]]];
        }
            break;
        case 5:
        {
             [[[personaj person] getChildByTag:18] removeFromParentAndCleanup:YES];
             [mallTap setEnabled:NO];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"timerStop" object:nil];
             [self removeViewsGameRoom];
             [stateTamagochi setSleep:NO];
             [carousel setFrame:CGRectMake((WIDTH_DEVICE-150)/2, -175, 150, 150)];
             [l_room setText:NSLocalizedString(@"Mall_room", nil)];
             [[CCDirectorIOS sharedDirector] replaceScene:[CCTransitionSlideInR transitionWithDuration:timp scene:[MallRoom scene]]];
        }

        default:
            break;
    }
    if (sender)
        [self performSelector:@selector(enableButtons) withObject:nil afterDelay:0.5];
}

-(void)nextRoom:(NSNotification *)notif
{
    if ([notif.object isEqualToString:@"1"])
        [self next:btnNext];
    else
        [self back:btnBack];
}
- (IBAction)mallTaped:(id)sender {
     if ([stateTamagochi soundOn])
         [[SimpleAudioEngine sharedEngine] playEffect:@"btn_press.mp3" loop:NO];
    
    [self performSelector:@selector(enableButtons) withObject:nil afterDelay:0.5];
    
    if (nrPage!=5)
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"timerStop" object:nil];
        [stateTamagochi setSleep:NO];
        [carousel setFrame:CGRectMake((WIDTH_DEVICE-150)/2, -175, 150, 150)];
        [mallTap setEnabled:NO];
        [self setTexturesDefault];
        [self removeViewsGameRoom];
        if (viewCoins.frame.origin.y==0) [viewCoins setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE)];
        if (viewEats.frame.origin.y==0) [viewEats setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE)];
        if (selectColor.frame.origin.y==0)  [selectColor setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE)];
        if (shopView.frame.origin.y==0)  [shopView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE)];
        if (viewBodyShop.frame.origin.y==0)  [viewBodyShop setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE)];
        [[[personaj person] getChildByTag:18] removeFromParentAndCleanup:YES];
        [[personaj person] removeFromParentAndCleanup:NO];
        [l_room setText:NSLocalizedString(@"Mall_room", nil)];
        [[CCDirectorIOS sharedDirector] replaceScene:[MallRoom scene]];
        nrPage=5;
    }
}

- (IBAction)closeHighScore:(id)sender forEvent:(UIEvent *)event{
     if ([stateTamagochi soundOn])
         [[SimpleAudioEngine sharedEngine] playEffect:@"btn_press.mp3" loop:NO];
    
    switch ([sender tag]) {
        case 0:
        {
             [textFieldWithNamePlayer resignFirstResponder];
            [highScoreView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE)];
            [shareView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE)];
            [nextLevelView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE)];
            if ([stateTamagochi room]==gameRoom)
                [self openTV];
            //TODO: enableButtonsGame
            [self enableButtonsGame];
            //end
        }
            break;
        case 1:
        {
            //TODO: alege joaca
            if ([stateTamagochi health]>20.0 && [stateTamagochi energy]>20.0) {
                if ([[textFieldWithNamePlayer text] length]!=0)
                {
                    [stateTamagochi addPlayer:[textFieldWithNamePlayer text] ];
                    [self chooseGameAnimation];
                    viewPanelButonsStatistic.hidden = YES;
                    viewPanelButonsStatistic.userInteractionEnabled = NO;
                }
                else
                    [self alertShow:NSLocalizedString(@"attention", nil) message:NSLocalizedString(@"textEmpty", nil)];
            
            }else{
                [self alertShow:NSLocalizedString(@"attention", nil) message:NSLocalizedString(@"pou_sick_energy", nil)];
            }
            //END

        }
            break;
        case 2:
        {
            // highscore button press action
        }
            break;
        case 3:
        {
            // share button press action
            
            [shareView setFrame:CGRectMake(0, 0, WIDTH_DEVICE, HEIGHT_DEVICE)];
            if (!shareView.superview)
                [self.view insertSubview:shareView atIndex:4];
            [shareView.lab_Share_Title setText:NSLocalizedString(@"Share_how", nil)];
            shareView.scoreShare=[NSString stringWithFormat:@"My HighScore in Paramon is %d! \n You can to beat my record of %d points in %@ game in Paramon?", scoreShare,scoreShare, labelStage.text];
            shareView.scoreShareTwitter=[NSString stringWithFormat:@"My HighScore in Paramon is %d! \nYou can to beat my record of %d points in %@?", scoreShare,scoreShare, labelStage.text];
            shareView.btn_CloseShare.tag=0;
            UIGraphicsBeginImageContext(CGSizeMake(320,85));
            [viewPanelButonsStatistic drawViewHierarchyInRect:CGRectMake(0, 0, 320, 85) afterScreenUpdates:YES];
            UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            shareView.imageBar=image;
        
        }
            break;
        case 4:
        {
             [shareView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE)];
        }
            break;
        case 5:
        {
            [shareView setFrame:CGRectMake(0, 0, WIDTH_DEVICE, HEIGHT_DEVICE)];
            if (!shareView.superview)
                [self.view insertSubview:shareView atIndex:4];
            [shareView.lab_Share_Title setText:NSLocalizedString(@"Share_how", nil)];
            
            NSString *str1;
            
            if ([stateTamagochi sizeLevel]<=7)
            {
                
                str1=[[stateTamagochi years] objectAtIndex:0];
            }
            else if ([stateTamagochi sizeLevel]<=17)
            {
                str1=[[stateTamagochi years] objectAtIndex:1];
            }
            else
            {
                str1=[[stateTamagochi years] objectAtIndex:2];
            }
            shareView.scoreShareTwitter=[NSString stringWithFormat:@"Hey. I play Paramon. I have reached %d level, my size is: %@.\n Join us and you!\n", [stateTamagochi level],str1];
            shareView.scoreShare=[NSString stringWithFormat:@"Hey. I play Paramon. I have reached %d level, my size is: %@.\n Join us and you!\n", [stateTamagochi level],str1];
            shareView.btn_CloseShare.tag=7;
            
            
            UIGraphicsBeginImageContext(CGSizeMake(320,85));
            [viewPanelButonsStatistic drawViewHierarchyInRect:CGRectMake(0, 0, 320, 85) afterScreenUpdates:YES];
            UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            shareView.imageBar=image;
         
        }
            break;
        case 6:
        {
            [shareView setFrame:CGRectMake(0, 0, WIDTH_DEVICE, HEIGHT_DEVICE)];
            if (!shareView.superview)
                [self.view insertSubview:shareView atIndex:4];
            [shareView.lab_Share_Title setText:NSLocalizedString(@"Share_how", nil)];
            NSString *str=@"!", *str1=@"!";
            if (nextLevelView.dataOpened.count>0)
            {
                
                
                str=[NSString stringWithFormat:@" and unlock %d items!",nextLevelView.dataOpened.count];
                
                NSString *str2=@"";
                for (int i=0;i<nextLevelView.dataOpened.count;i++)
                {
                    str2=[NSString stringWithFormat:@"%@\n%d.%@", str2, i+1, NSLocalizedString([[nextLevelView.dataOpened objectAtIndex:i] objectAtIndex:1], nil)];
                }
                
                
                str1=[NSString stringWithFormat:@" and unlock the:%@",str2];
            }
            shareView.scoreShareTwitter=[NSString stringWithFormat:@"Hooray, I have reached (%d) level%@", [stateTamagochi level] , str];
            shareView.scoreShare=[NSString stringWithFormat:@"Hooray, I have reached (%d) level%@", [stateTamagochi level] , str1];
            shareView.btn_CloseShare.tag=0;
            UIGraphicsBeginImageContext(CGSizeMake(320,85));
            [viewPanelButonsStatistic drawViewHierarchyInRect:CGRectMake(0, 0, 320, 85) afterScreenUpdates:YES];
            UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            shareView.imageBar=image;
        }
            break;
        case 7:
        {
            [textFieldWithNamePlayer resignFirstResponder];
            [highScoreView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE)];
            [shareView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE)];
            [nextLevelView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE)];
        
            //TODO: enableButtonsGame
            [self enableButtonsGame];
            //end
        }
            break;
        default:
            break;
    }
    
}

- (NSString*)formattedStringForDuration:(NSTimeInterval)duration
{
    NSInteger minutes = floor(duration/60);
    NSInteger seconds = round(duration - minutes * 60);
    return [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
}

- (IBAction)closeStats:(id)sender {
     if ([stateTamagochi soundOn])
         [[SimpleAudioEngine sharedEngine] playEffect:@"btn_press.mp3" loop:NO];
    
    [UIView animateWithDuration:0.0
                    animations:^{ [statsView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE)];}
                    completion:^(BOOL finished) {
                        float lat=140;
                        if (IS_IPAD)
                            lat=329;
                        [stats_slide_1 setFrame:CGRectMake(stats_slide_1.frame.origin.x, stats_slide_1.frame.origin.y,lat, stats_slide_1.frame.size.height)];
                        [stats_slide_2 setFrame:CGRectMake(stats_slide_2.frame.origin.x, stats_slide_2.frame.origin.y, lat, stats_slide_2.frame.size.height)];
                        [stats_slide_3 setFrame:CGRectMake(stats_slide_3.frame.origin.x, stats_slide_3.frame.origin.y, lat, stats_slide_3.frame.size.height)];
                        [stats_slide_4 setFrame:CGRectMake(stats_slide_4.frame.origin.x, stats_slide_4.frame.origin.y, lat, stats_slide_4.frame.size.height)];
                        [statsView removeFromSuperview];
                    }];
}


-(void)showShopView:(NSNotification *)notif
{
    if ([stateTamagochi soundOn])
         [[SimpleAudioEngine sharedEngine] playEffect:@"btn_press.mp3" loop:NO];
    
    selectedColor=0;
    lockedObject=-1;
    [self loadDataArray];
   
    switch ([stateTamagochi shopCategory]) {
        case sh_potions:
        {
            otherPage=0;
            if (notif.object)
            {
                nrPage=5;
                otherPage=3;
            }
            [btnBackShop setHidden:YES];
             [tableShop reloadData];
             [tableShop setContentOffset:CGPointZero animated:NO];
             [shopView setFrame:CGRectMake(0, 0, WIDTH_DEVICE, HEIGHT_DEVICE)];
        }
            break;
        case sh_body:
        {
            path=CGPathCreateMutable();
            CGPathMoveToPoint(path, NULL, [[personaj person] getChildByTag:6].position.x+50, [[personaj person] getChildByTag:6].position.y+25);
            CGPathAddLineToPoint(path, NULL, [[personaj person] getChildByTag:6].position.x+120, [[personaj person] getChildByTag:6].position.y+180);
            CGPathAddLineToPoint(path, NULL, [[personaj person] getChildByTag:6].position.x+190, [[personaj person] getChildByTag:6].position.y+25);
            CGPathCloseSubpath(path);
            
            if (!viewBodyShop.superview)
                [self.view insertSubview:viewBodyShop atIndex:3];
            [viewBodyShop setFrame:CGRectMake(0, 0, WIDTH_DEVICE, HEIGHT_DEVICE)];
            otherPage=0;
        
            if ([notif.object isEqualToString:@"15"])
            {
               
                otherPage=1;
                nrPage=5;
                [l_room setText:NSLocalizedString(@"Mall_room", nil)];
                [mallTap setEnabled:NO];
                [self loadDataArray];
                [viewBodyShop setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE)];
                if ([stateTamagochi shopCategory]!=sh_potions && ![[[_dataShopBody objectAtIndex:0] objectAtIndex:1] isEqualToString:@""])
                {
                   [tableShop reloadData];
                     [tableShop setContentOffset:CGPointZero animated:NO];
                    [btnBackShop setHidden:YES];
                    [shopView setFrame:CGRectMake(0, 0, WIDTH_DEVICE, HEIGHT_DEVICE)];
                }
                else
                {
                    _rowTapped=nil;
                    [self showColors:nil];
                }
            }
            else if (notif.object)
            {
                
                otherPage=2;
             
                nrPage=5;
                [l_room setText:NSLocalizedString(@"Mall_room", nil)];
                [mallTap setEnabled:NO];
                [stateTamagochi setShopCategory:sh_body];
                [self loadDataArray];
                int index=0;
                for (int i=0;i<[_dataShopBody count];i++)
                {
                    if ([[[[_dataShopBody objectAtIndex:i] objectAtIndex:1] substringFromIndex:1] intValue]==[notif.object intValue])
                    {
                        index=i;
                    }
                }
                _rowTapped=[[NSIndexPath indexPathForRow:index inSection:1] retain];
                [tableShop reloadData];
                [tableShop setContentOffset:CGPointZero animated:NO];
                [self showColors:_rowTapped];
                [viewBodyShop setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE)];
                [shopView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE)];
            }
            else
                otherPage=0;
        }
            break;
        case sh_eat:
        {
            if (!viewEats.superview)
                [self.view insertSubview:viewEats atIndex:3];
            [tableEat reloadData];
            [tableShop reloadData];
            [tableEat setContentOffset:CGPointZero animated:NO];
            [viewEats setFrame:CGRectMake(0, 0, WIDTH_DEVICE, HEIGHT_DEVICE)];
            if (notif.object)
            {
                otherPage=1;
                nrPage=5;
                [mallTap setEnabled:NO];
                [l_room setText:NSLocalizedString(@"Mall_room", nil)];
                switch ([notif.object intValue]) {
                case 1:
                {
                    [self selectEat:btn_Eat_1];
                }
                    break;
                case 3:
                {
                    [self selectEat:btn_Eat_3];
                }
                    break;
                case 4:
                {
                    [self selectEat:btn_Eat_4];
                }
                default:
                    break;
            }
            }
            else
                otherPage=0;
        }
            break;
        case sh_skins:
        {
            otherPage=0;
            if (!skinsView.superview)
                [self.view insertSubview:skinsView atIndex:3];
            [skinsView setFrame:CGRectMake(0, 0, WIDTH_DEVICE, HEIGHT_DEVICE)];
            [skinsKitchen setText:NSLocalizedString(@"Kitchen_room", nil)];
            [skinsLabBath setText:NSLocalizedString(@"Bath_room", nil)];
            [skinsLabBed setText:NSLocalizedString(@"Bed_room", nil)];
            [skinsLabGame setText:NSLocalizedString(@"Game_room", nil)];
            [skinsLabStudio setText:NSLocalizedString(@"Studio_room", nil)];
        }
            break;


            
        default:
            break;
    }
   
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer btnClose:(UITouch *)touch {
    if ( [gestureRecognizer isMemberOfClass:[UITapGestureRecognizer class]] ) {
        // Return NO for views that don't support Taps
    } else if ( [gestureRecognizer isMemberOfClass:[UISwipeGestureRecognizer class]] ) {
        // Return NO for views that don't support Swipes
    }
    
    return YES;
}
- (IBAction)tapView:(id)sender {
     if ([stateTamagochi soundOn])
         [[SimpleAudioEngine sharedEngine] playEffect:@"btn_press.mp3" loop:NO];
    if ([[sender view] tag]!=15)
    {
    [stateTamagochi setBodyCategory:[[sender view] tag]];
    [self loadDataArray];
    [viewBodyShop setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE)];
        if ([stateTamagochi shopCategory]!=sh_potions && ![[[_dataShopBody objectAtIndex:0] objectAtIndex:1] isEqualToString:@""])
        {
            [tableShop reloadData];
            [btnBackShop setHidden:NO];
            [shopView setFrame:CGRectMake(0, 0, WIDTH_DEVICE, HEIGHT_DEVICE)];
        }
        else
        {
            _rowTapped=nil;
            [self showColors:nil];
        }
  
    }
    else
    {
        [selectColor removeFromSuperview];
    }

}
- (IBAction)closeShop:(id)sender {
    
     if ([stateTamagochi soundOn])
         [[SimpleAudioEngine sharedEngine] playEffect:@"btn_press.mp3" loop:NO];
    
    switch ([sender tag]) {
        case 0:
        {
        
            //[shopView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE)];
            
        }
            break;
        case 1:
        {
            [[NSNotificationCenter defaultCenter]postNotificationName:@"showBackgroundAlpha" object:@"1"];
            [viewBodyShop setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
            [shopView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
            if (otherPage!=0)
            {
                [[personaj person] removeFromParentAndCleanup:NO];
                if (otherPage!=3)
                {
                    [mallTap setEnabled:YES];
                    nrPage=4;
                    [l_room setText:NSLocalizedString(@"Closed_room", nil)];
                    [[CCDirectorIOS sharedDirector] replaceScene:[ClosetRoom scene]];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"closetCategory" object:nil];
                }
                else
                {
                     [mallTap setEnabled:YES];
                    nrPage=3;
                    [l_room setText:NSLocalizedString(@"Kitchen_room", nil)];
                    [[CCDirectorIOS sharedDirector] replaceScene:[KitchenRoom scene]];
                }
            }

        }
            break;
        case 2:
        {
            [shopView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
            if ([stateTamagochi shopCategory]!=sh_skins)
                [viewBodyShop setFrame:CGRectMake(0, 0, WIDTH_DEVICE, HEIGHT_DEVICE)];
            else
                [skinsView setFrame:CGRectMake(0, 0, WIDTH_DEVICE, HEIGHT_DEVICE)];
        }
            break;
        
        case 11:
        {
             [shopView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
        }
            break;
        case 12:
        {
            if (stateTamagochi)
            if (![[[_dataShopBody objectAtIndex:0] objectAtIndex:1] isEqualToString:@""])
            {
                if (otherPage>1)
                {
                    [[personaj person] removeFromParentAndCleanup:NO];
                    [mallTap setEnabled:YES];
                    [l_room setText:NSLocalizedString(@"Closed_room", nil)];
                    nrPage=4;
                    [[CCDirectorIOS sharedDirector] replaceScene:[ClosetRoom scene]];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"closetCategory" object:nil];
                }
                else
                {
                    [shopView setFrame:CGRectMake(0, 0, WIDTH_DEVICE, HEIGHT_DEVICE)];
                }
            }
            else
            {
                if (otherPage==0)
                {
                    [shopView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
                    [viewBodyShop setFrame:CGRectMake(0, 0, WIDTH_DEVICE, HEIGHT_DEVICE)];
                }
                else
                {
                    [[personaj person] removeFromParentAndCleanup:NO];
                     [mallTap setEnabled:YES];
                    nrPage=4;
                    [l_room setText:NSLocalizedString(@"Closed_room", nil)];
                    [[CCDirectorIOS sharedDirector] replaceScene:[ClosetRoom scene]];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"closetCategory" object:nil];
                }
            }
            [selectColor setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
            [priceProduct setHidden:NO];
            [lbl_btnBuy setText:NSLocalizedString(@"Buy", nil)];
            imageBuy.image=[UIImage imageNamed:@"money_3.png"];
            if ([stateTamagochi shopCategory]==sh_body)
            {
                [[personaj person] setZOrder:1];
                if ([stateTamagochi bodyCategory]!=12)
                    [personaj replaceTextureDefault: [stateTamagochi bodyCategory]];
                else
                    [personaj replaceTatooDefault:[[[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:1] substringFromIndex:1]];
                if ([stateTamagochi bodyCategory]==4)
                    [personaj replaceTextureDefault: 9];
                else if ([stateTamagochi bodyCategory]==9)
                    [personaj replaceTextureDefault: 4];
                else if ([stateTamagochi bodyCategory]==12)
                    [personaj replaceTextureDefault: 5];
            }
            else
            {
            
                btnNext.enabled=YES;
                btnBack.enabled=YES;
                btn_settings.enabled=YES;
                [tapCoinsView setUserInteractionEnabled:YES];
                [self enableButtons];
                nrPage=5;
                [[[personaj person] getChildByTag:18] removeFromParentAndCleanup:YES];
                [[personaj person] removeFromParentAndCleanup:NO];
                [l_room setText:NSLocalizedString(@"InMall_room", nil)];
                 [mallTap setEnabled:NO];
                [[CCDirectorIOS sharedDirector] replaceScene:[InMallRoom scene]];
                 [[NSNotificationCenter defaultCenter]postNotificationName:@"showBackgroundAlpha" object:@"0"];
              
            }
        }
            break;
        case 13:
        {
            [[NSNotificationCenter defaultCenter]postNotificationName:@"showBackgroundAlpha" object:@"1"];
            [viewEats setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
            if (otherPage!=0)
            {
                 [mallTap setEnabled:YES];
                [[personaj person] removeFromParentAndCleanup:NO];
                nrPage=3;
                [[CCDirectorIOS sharedDirector] replaceScene:[KitchenRoom scene]];
                [l_room setText:NSLocalizedString(@"Kitchen_room", nil)];
            }
        }
            break;
        case 14:
        {
            [[NSNotificationCenter defaultCenter]postNotificationName:@"showBackgroundAlpha" object:@"1"];
            [skinsView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
        }
            break;
        case 15:
        {
            [skinsView setFrame:CGRectMake(0, 0, WIDTH_DEVICE, HEIGHT_DEVICE)];
            [selectColor setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
            [priceProduct setHidden:NO];
            [lbl_btnBuy setText:NSLocalizedString(@"Buy", nil)];
            imageBuy.image=[UIImage imageNamed:@"money_3.png"];
        }
            break;
        case 16:
        {
           // if (!showCoins) [[NSNotificationCenter defaultCenter]postNotificationName:@"showBackgroundAlpha" object:@"0"];
            [viewCoins setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
            if ([stateTamagochi shopCategory]==sh_eat)
                [viewEats setFrame:CGRectMake(0, 0, WIDTH_DEVICE, HEIGHT_DEVICE)];
            if ([stateTamagochi shopCategory]==sh_skins)
            {
              
                if (nrPage!=5)
                    [selectColor setFrame:CGRectMake(0, 0, WIDTH_DEVICE, HEIGHT_DEVICE)];
                else
                {
                    [shopView setFrame:CGRectMake(0,0, WIDTH_DEVICE, HEIGHT_DEVICE)];
                }
            }
             if ([stateTamagochi shopCategory]==sh_body)
                [selectColor setFrame:CGRectMake(0, 0, WIDTH_DEVICE, HEIGHT_DEVICE)];
        }
            break;
        case 17:
        {
            
            [viewCoins setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
            if (showCoins) break;
            
            [[NSNotificationCenter defaultCenter]postNotificationName:@"showBackgroundAlpha" object:@"1"];
            if ([stateTamagochi shopCategory]==sh_eat)
            {
            
                [viewEats setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
                if (otherPage!=0)
                {
                     [mallTap setEnabled:YES];
                    nrPage=3;
                    [[personaj person] removeFromParentAndCleanup:NO];
                    [[CCDirectorIOS sharedDirector] replaceScene:[KitchenRoom scene]];
                    [l_room setText:NSLocalizedString(@"Kitchen_room", nil)];
                }
                     break;
            }
       
            if ([stateTamagochi shopCategory]==sh_skins)
            {
                [viewBodyShop setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
                [shopView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
                if (nrPage!=5) [self closeShop:btnCloseColors];
                 break;
            }
            
            if ([stateTamagochi shopCategory]==sh_body)
            {
                UIButton *but=[[[UIButton alloc] init] autorelease];
                but.tag=1;
                [[personaj person] setZOrder:1];
                [self setTexturesDefault];
                [self closeShop:but];
                break;
            }

        }
        default:
            break;
    }
}

-(void)setTexturesDefault
{
    if ([stateTamagochi bodyCategory]!=12)
        [personaj replaceTextureDefault: [stateTamagochi bodyCategory]];
    else
        [personaj replaceTatooDefault:[[[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:1] substringFromIndex:1]];
    if ([stateTamagochi bodyCategory]==4)
        [personaj replaceTextureDefault: 9];
    else if ([stateTamagochi bodyCategory]==9)
        [personaj replaceTextureDefault: 4];
    else if ([stateTamagochi bodyCategory]==12)
        [personaj replaceTextureDefault: 5];
  
}

- (void)menuTapped:(UITapGestureRecognizer *)gr {
    
     if ([stateTamagochi soundOn])
         [[SimpleAudioEngine sharedEngine] playEffect:@"btn_press.mp3" loop:NO];
    
    if (viewCoins.frame.origin.y==0) [viewCoins setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
    if (shareView.frame.origin.y==0) [shareView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
    if (!statsView.superview)
    {
        states_Energy.text=NSLocalizedString(@"potion_03", nil);
        states_Food.text=NSLocalizedString(@"sitosti", nil);
        states_Fun.text=NSLocalizedString(@"potion_05", nil);
        states_Health.text=NSLocalizedString(@"potion_01", nil);
        if (viewSettings.frame.origin.y==0)
           [viewSettings setFrame:CGRectMake(0, HEIGHT_DEVICE+20, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
        if (nrPage==5)
        {
            [[NSNotificationCenter defaultCenter]postNotificationName:@"showBackgroundAlpha" object:@"1"];
            if (selectColor.frame.origin.y==0) [selectColor setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
            if (shopView.frame.origin.y==0)[shopView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
            if (viewBodyShop.frame.origin.y==0)[viewBodyShop setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
            if (viewEats.frame.origin.y==0)[viewEats setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
            if (skinsView.frame.origin.y==0)[skinsView setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
            if (viewCoins.frame.origin.y==0)[viewCoins setFrame:CGRectMake(0, -HEIGHT_DEVICE, WIDTH_DEVICE, HEIGHT_DEVICE-20)];
        }
        [statsView setFrame:CGRectMake(0, 0, WIDTH_DEVICE, HEIGHT_DEVICE)];
        [self.view insertSubview:statsView atIndex:3];
        float food=[stateTamagochi food];
        if (food>100.0)
            food=100.0;
        [stats_slide_1 setFrame:CGRectMake(stats_slide_1.frame.origin.x, stats_slide_1.frame.origin.y, (stats_slide_1.frame.size.width*food)/100.0, stats_slide_1.frame.size.height)];
        [stats_slide_2 setFrame:CGRectMake(stats_slide_2.frame.origin.x, stats_slide_2.frame.origin.y, (stats_slide_2.frame.size.width*[stateTamagochi energy])/100.0, stats_slide_2.frame.size.height)];
        [stats_slide_3 setFrame:CGRectMake(stats_slide_3.frame.origin.x, stats_slide_3.frame.origin.y, (stats_slide_3.frame.size.width*[stateTamagochi health])/100.0, stats_slide_3.frame.size.height)];
        [stats_slide_4 setFrame:CGRectMake(stats_slide_4.frame.origin.x, stats_slide_4.frame.origin.y, (stats_slide_4.frame.size.width*[stateTamagochi funny])/100.0, stats_slide_4.frame.size.height)]   ;
    
        [stats_lab_1 setText:[NSString stringWithFormat:@"%.0f", food]];
        [stats_lab_2 setText:[NSString stringWithFormat:@"%.0f", [stateTamagochi energy]]];
        [stats_lab_3 setText:[NSString stringWithFormat:@"%.0f", [stateTamagochi health]]];
        [stats_lab_4 setText:[NSString stringWithFormat:@"%.0f", [stateTamagochi funny]]];
    }
}

#pragma mark -
#pragma mark iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    //return the total number of items in the carousel
    return 4;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    if (view == nil)
    {
             if (!IS_IPAD)
        {
             view = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 130, 107)] autorelease];
            ((UIImageView *)view).image = [UIImage imageNamed:[NSString stringWithFormat:@"star_%d.png", index+1]];
            
        }
        else
        {
            view = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 330, 407)] autorelease];
            ((UIImageView *)view).image = [UIImage imageNamed:[NSString stringWithFormat:@"star_%d-iPad.png", index+1]];
        }
        view.contentMode = UIViewContentModeCenter;
    }
    
    
    return view;
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    switch (option)
    {
        case iCarouselOptionSpacing:
            return value * 1.1;
        default:
            return value;
    }
}

#pragma mark -
#pragma mark Autoscroll

- (void)startScrolling
{
    [scrollTimer invalidate];
    scrollTimer = [NSTimer scheduledTimerWithTimeInterval:1.0/60.0
                                                        target:self
                                                      selector:@selector(scrollStep)
                                                      userInfo:nil
                                                       repeats:YES];
    
}

- (void)stopScrolling
{
    [scrollTimer invalidate];
    scrollTimer = nil;
}

- (void)scrollStep
{
    //calculate delta time
    NSTimeInterval now = [NSDate timeIntervalSinceReferenceDate];
    float delta = lastTime - now;
    lastTime = now;
    
    //don't autoscroll when user is manipulating carousel
    if (!carousel.dragging && !carousel.decelerating)
    {
        //scroll carousel
        carousel.scrollOffset += delta * (float)(0.5);
    }
}

-(void)carouselMove:(NSNotification *)notif
{
    float height=60;
    if ([notif.object isEqualToString:@"off"])
        height=-175;
    [UIView animateWithDuration:0.5
        animations:^{
           if (height==60) [self startScrolling];
            else
                 [self stopScrolling];
            [carousel setFrame:CGRectMake((WIDTH_DEVICE-150)/2, height, 150, 150)];
            }
        completion:^(BOOL finished){}
     ];
}



-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch* touch = [touches anyObject];
    CGPoint location = [touch locationInView: [touch view]];
       location = [[CCDirector sharedDirector] convertToGL:location];
    if (CGRectContainsPoint([personaj rectPerson], location) && selectColor.frame.origin.y==0 && [stateTamagochi bodyCategory]==12)
    {
        startPos=location;
        newPoint=startPos;
    }
    [textFieldWithNamePlayer resignFirstResponder];
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch* touch = [touches anyObject];
    CGPoint location = [touch locationInView: [touch view]];
    
    location = [[CCDirector sharedDirector] convertToGL:location];
    
    if (CGRectContainsPoint([personaj rectPerson], location) && selectColor.frame.origin.y==0 && [stateTamagochi bodyCategory]==12)
    {
        int tagObj=21+[[[[_dataShopBody objectAtIndex:[_rowTapped row]] objectAtIndex:1] substringFromIndex:1] intValue];
        CGPoint loc1=[[[personaj person] getChildByTag:tagObj] position];
        loc1.x=loc1.x+[[[personaj person] getChildByTag:6] boundingBox].size.width/2;
        loc1.y=loc1.y+[[[personaj person] getChildByTag:6] boundingBox].size.height/2;
        newPoint=loc1;
        CGPoint moveDifference=CGPointZero;
        moveDifference = ccpSub(location, startPos);
        newPoint=ccpAdd(newPoint, moveDifference);
        CGPoint loc=[[[personaj person] getChildByTag:3] position];
        loc.x=loc.x+[[[personaj person] getChildByTag:6] boundingBox].size.width/3;
        loc.y=loc.y+[[[personaj person] getChildByTag:6] boundingBox].size.height/4;
      
        if (CGPathContainsPoint(path, NULL,newPoint, NO) && !CGRectContainsPoint(CGRectMake(loc.x, loc.y, [[[personaj person] getChildByTag:3] boundingBox].size.width, [[[personaj person] getChildByTag:3] boundingBox].size.height),newPoint))
        {
            //[[[personaj person] getChildByTag:tagObj] setPosition:ccpAdd([[[personaj person] getChildByTag:tagObj] position], moveDifference)];
            [[personaj person] setPositionTatoo:tagObj position:ccpAdd([[[personaj person] getChildByTag:tagObj] position], moveDifference)];
        }
        newPoint=ccpAdd(newPoint, moveDifference);
        startPos=location;
    }
}
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch* touch = [touches anyObject];
    CGPoint location = [touch locationInView: [touch view]];
  
}

- (UIImage *)imageNamed:(NSString *)name withColor:(UIColor *)color
{
    // load the image
    UIImage *img = [UIImage imageNamed:name];
    
    // begin a new image context, to draw our colored image onto
    UIGraphicsBeginImageContext(img.size);
    
    // get a reference to that context we created
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // set the fill color
    [color setFill];
    
    // translate/flip the graphics context (for transforming from CG* coords to UI* coords
    CGContextTranslateCTM(context, 0, img.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    
    // set the blend mode to color burn, and the original image
    CGContextSetBlendMode(context, kCGBlendModeColorBurn);
    CGRect rect = CGRectMake(0, 0, img.size.width, img.size.height);
    CGContextDrawImage(context, rect, img.CGImage);
    
    // set a mask that matches the shape of the image, then draw (color burn) a colored rectangle
    CGContextClipToMask(context, rect, img.CGImage);
    CGContextAddRect(context, rect);
    CGContextDrawPath(context,kCGPathFill);
    
    // generate a new UIImage from the graphics context we drew onto
    UIImage *coloredImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //return the color-burned image
    return coloredImg;
}


#pragma TableView delegate - Games

#pragma mark - UItableViewDelegates


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView  numberOfRowsInSection:(NSInteger)section
{
    if (tableView == tableHighscore) {
        
        int nr_rind= [stateTamagochi getAllGameStatisticsForStageID:[NSNumber numberWithInt:((indexGame)?selectedStage:indexGame)]].count;
       
        
        return nr_rind;
    }

    if (tableView==tableGames)
        return 2;
    return [_dataShopBody count];
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
     static NSString *CellIdentifier;
    if (_tableView==tableHighscore) {
        if (!IS_IPAD)
            CellIdentifier = @"HighCell";
        else
            CellIdentifier = @"HighCell_iPad";
        
        HighScoreCell *cell = (HighScoreCell *) [tableHighscore dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:[NSString stringWithFormat:@"HighScoreCell%@", (IS_IPAD)?@"_iPad":@""] owner:self options:nil];
            
            for (id currentObject in topLevelObjects) {
                if ([currentObject isKindOfClass:[HighScoreCell class]]) {
                    cell = (HighScoreCell *) currentObject;
                    break;
                }
            }
        }
        for (id obj in cell.subviews) {
            if ([obj respondsToSelector:@selector(setDelaysContentTouches:)]) {
                [obj setDelaysContentTouches:NO];
            }
        }
        
        NSDictionary *highscore = [[stateTamagochi getAllGameStatisticsForStageID:[NSNumber numberWithInt:((indexGame)?selectedStage:indexGame)]] objectAtIndex:indexPath.row];
        NSString *player = [highscore objectForKey:@"NAME"];
        int score = [[highscore objectForKey:@"SCORE"] integerValue];
        int coins = [[highscore objectForKey:@"COINS"] integerValue];
        float time = [[highscore objectForKey:@"TIME"] doubleValue];
        
        NSString *formatedTime = [self formattedStringForDuration:time];
        //cell.namePlayer.text=[NSString stringWithFormat:@"%@",player];
        cell.scorePl.text=[NSString stringWithFormat:@"%d",score];
        cell.namePlayer.text=[NSString stringWithFormat:@"%@", player];
        cell.scorePlayer.text=[NSString stringWithFormat:@"%@:  %d",NSLocalizedString(@"coins", nil),coins];
        cell.timePlayer.text=[NSString stringWithFormat:@"%@:  %@",NSLocalizedString(@"time", nil), formatedTime];
        cell.numberPlayer.text=[NSString stringWithFormat:@"%d. ",indexPath.row+1];
        [cell.numberPlayer sizeToFit];
        [cell. namePlayer sizeToFit];
        if (cell.namePlayer.frame.size.width>140) [cell.namePlayer setFrame:CGRectMake(cell.namePlayer.frame.origin.x, 0, 140, cell.namePlayer.frame.size.height)];
        [cell.namePlayer setFrame:CGRectMake(cell.numberPlayer.frame.origin.x+cell.numberPlayer.frame.size.width+5, 0, cell.namePlayer.frame.size.width, cell.namePlayer.frame.size.height)];
        [cell.scorePl sizeToFit];
        [cell.scorePl setFrame:CGRectMake(cell.namePlayer.frame.origin.x+cell.namePlayer.frame.size.width+5, 0, cell.scorePl.frame.size.width, cell.scorePl.frame.size.height)];
        if (indexPath.row==0)
        {
            [btn_ShareHighScore setEnabled:YES];
            scoreShare=score;
        }
        cell.backgroundColor=[UIColor clearColor];
       
        return cell;
    }

    if (_tableView==tableGames)
    {
        if (!IS_IPAD)
            CellIdentifier = @"CustomCell";
        else
            CellIdentifier = @"CustomCell_iPad";
        CustomCell *cell = (CustomCell *) [tableGames dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:[NSString stringWithFormat:@"CustomCell%@", (IS_IPAD)?@"_iPad":@""] owner:self options:nil];
            
            for (id currentObject in topLevelObjects) {
                if ([currentObject isKindOfClass:[CustomCell class]]) {
                    cell = (CustomCell *) currentObject;
                    break;
                }
            }
        }
        for (id obj in cell.subviews) {
            if ([obj respondsToSelector:@selector(setDelaysContentTouches:)]) {
                [obj setDelaysContentTouches:NO];
            }
        }
        cell.gameImage.image=[UIImage imageNamed:[NSString stringWithFormat:@"game_%d.png", indexPath.row+1]];
        cell.nameGame.text=[NSString stringWithFormat:@"%@", (indexPath.row==0)?@"Para Jump":@"Para in Space"];
        cell.backgroundColor=[UIColor clearColor];

        return cell;
    }
    else
    {
        
        if (lockedObject!=[indexPath row])
        {
        if (!IS_IPAD)
            CellIdentifier =@"ShopCell";
        else
             CellIdentifier =@"ShopCell_iPad";
            
        ShopCell *cell;
        if (_tableView==tableShop)
        cell= (ShopCell *) [tableShop dequeueReusableCellWithIdentifier:CellIdentifier];
        else
            if (_tableView==tableEat)
          cell= (ShopCell *) [tableEat dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {                                                               
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:[NSString stringWithFormat:@"ShopCell%@", (IS_IPAD)?@"_iPad":@""] owner:self options:nil];
            
            for (id currentObject in topLevelObjects) {
                if ([currentObject isKindOfClass:[ShopCell class]]) {
                    cell = (ShopCell *) currentObject;
                    break;
                }
        }
        }
        for (id obj in cell.subviews) {
            if ([obj respondsToSelector:@selector(setDelaysContentTouches:)]) {
                [obj setDelaysContentTouches:NO];
            }
        }
        if ([stateTamagochi shopCategory]!=sh_potions && [stateTamagochi shopCategory]!=sh_eat  && ![[[_dataShopBody objectAtIndex:0] objectAtIndex:1] isEqualToString:@""])
        {
            [cell.buttonBody setEnabled:NO];
            [cell.buttonBody setHidden:YES];
            cell.tag=[[[_dataShopBody objectAtIndex:indexPath.row] objectAtIndex:4]intValue];
            [cell.quantity setHidden:YES];
            [cell.nameButton setHidden:YES];
            if ([[[_dataShopBody objectAtIndex:indexPath.row] objectAtIndex:8] intValue]==0)
            {
                cell.priceBody.frame=CGRectMake(cell.priceBody.frame.origin.x+15, cell.priceBody.frame.origin.y, cell.priceBody.frame.size.width, cell.priceBody.frame.size.height);

                cell.lockImage.image=[UIImage imageNamed:@"lock_red.png"];
                cell.lockImage.alpha=1.0;
                cell.money.image=[UIImage imageNamed:@""];
            }
            else
            {
                 cell.priceBody.frame=CGRectMake(182, cell.priceBody.frame.origin.y, cell.priceBody.frame.size.width, cell.priceBody.frame.size.height);
                cell.lockImage.alpha=0.0;
                cell.nameButton.text=NSLocalizedString(@"buy", nil);
                cell.money.image=[UIImage imageNamed:@"money_2.png"];
            }
        }
        else
        {
         
            [cell.buttonBody setEnabled:YES];
            [cell.buttonBody setTitle:@"Unlocked" forState:UIControlStateNormal];
            cell.nameButton.alpha=1.0;
            if ([[[_dataShopBody objectAtIndex:indexPath.row] objectAtIndex:8] intValue]==0)
            {
                cell.priceBody.frame=CGRectMake(cell.priceBody.frame.origin.x+15, cell.priceBody.frame.origin.y, cell.priceBody.frame.size.width, cell.priceBody.frame.size.height);
                [cell.buttonBody setImage:[UIImage imageNamed:@"buy_red_.png"] forState:UIControlStateNormal];
                
                cell.money.image=[UIImage imageNamed:@""];
                cell.nameButton.alpha=0.0;
                cell.lockImage.alpha=1.0;
                cell.lockImage.image=[UIImage imageNamed:@"lock_1.png"];
                [cell.buttonBody setTitle:@"Locked" forState:UIControlStateNormal];
                cell.priceBody.text=[NSString stringWithFormat:@"%d %@", [[[_dataShopBody objectAtIndex:indexPath.row] objectAtIndex:6] intValue], NSLocalizedString(@"lvl", nil)];
            }
            else
            {
                
                cell.priceBody.frame=CGRectMake(182, cell.priceBody.frame.origin.y, cell.priceBody.frame.size.width, cell.priceBody.frame.size.height);
                [cell.buttonBody setImage:[UIImage imageNamed:@"bay.png"] forState:UIControlStateNormal];
                cell.lockImage.alpha=0.0;
                cell.nameButton.text=NSLocalizedString(@"buy", nil);
                cell.money.image=[UIImage imageNamed:@"money_2.png"];
            }
            if ([[[_dataShopBody objectAtIndex:indexPath.row] objectAtIndex:3] intValue]==0)
            {
                cell.quantity.alpha=0;
            }
            else
            {
                cell.quantity.alpha=1;
                cell.quantity.text=[NSString stringWithFormat:@"x %@", [[_dataShopBody objectAtIndex:indexPath.row] objectAtIndex:3]];
            }
        }
          
        NSString *str=[NSString stringWithFormat:@"%@%@",[[_dataShopBody objectAtIndex:indexPath.row]  objectAtIndex:0],[[_dataShopBody objectAtIndex:indexPath.row]  objectAtIndex:1]];
        cell.nameBody.text=NSLocalizedString(str, nil);
            
        cell.imageBody.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@%@%@%@.png", [[_dataShopBody objectAtIndex:indexPath.row]  objectAtIndex:0],[[_dataShopBody objectAtIndex:indexPath.row]  objectAtIndex:1], [[_dataShopBody objectAtIndex:indexPath.row]  objectAtIndex:5], ([stateTamagochi shopCategory]==sh_skins)?@"_prev":@""]];
        cell.descBody.text=[NSString stringWithFormat:@"%@ ", [[_dataShopBody objectAtIndex:indexPath.row] objectAtIndex:7]];
        cell.buttonBody.tag=[[[_dataShopBody objectAtIndex:indexPath.row] objectAtIndex:4]intValue];
            if ([[[_dataShopBody objectAtIndex:indexPath.row] objectAtIndex:8] intValue]==1)
            {
                cell.priceBody.text=([stateTamagochi shopCategory]==sh_body || [stateTamagochi shopCategory]==sh_skins)?[NSString stringWithFormat:@"%@", [stateTamagochi getPriceBodyShop:[[_dataShopBody objectAtIndex:indexPath.row]  objectAtIndex:0] group:[[_dataShopBody objectAtIndex:indexPath.row]  objectAtIndex:1]]]:[NSString stringWithFormat:@"%@", [[_dataShopBody objectAtIndex:indexPath.row] objectAtIndex:2]];
            }
            else
            {
                cell.priceBody.text=[NSString stringWithFormat:@"%d %@", [[[_dataShopBody objectAtIndex:indexPath.row] objectAtIndex:6] intValue], NSLocalizedString(@"lvl", nil)];
            }
        cell.backgroundColor=[UIColor clearColor];
        return cell;
        }
        else
        {
            
            
            CellIdentifier =[NSString stringWithFormat:@"LockedCell%@", (IS_IPAD)?@"_iPad":@""];
            LockedCell *cell;
            if (_tableView==tableShop)
                cell= (LockedCell *) [tableShop dequeueReusableCellWithIdentifier:CellIdentifier];
            else
                if (_tableView==tableEat)
                    cell= (LockedCell *) [tableEat dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:[NSString stringWithFormat:@"LockedCell%@", (IS_IPAD)?@"_iPad":@""] owner:self options:nil];
                
                for (id currentObject in topLevelObjects) {
                    if ([currentObject isKindOfClass:[LockedCell class]]) {
                        cell = (LockedCell *) currentObject;
                        break;
                    }
                }
                for (id obj in cell.subviews) {
                    if ([obj respondsToSelector:@selector(setDelaysContentTouches:)]) {
                        [obj setDelaysContentTouches:NO];
                    }
                }
                cell.lockedText.text=NSLocalizedString(@"Locked_text", nil);
                cell.lockedText.textColor=[UIColor orangeColor];
                cell.lockedText.font=[UIFont fontWithName:@"Marker Felt" size:12];
                cell.lockedText.textAlignment=UITextAlignmentCenter;
                cell.waitButton.alpha=0.0;
                [cell.unlockButton setFrame:CGRectMake(cell.unlockButton.frame.origin.x-10, cell.unlockButton.frame.origin.y, cell.unlockButton.frame.size.width, cell.unlockButton.frame.size.height)];
                cell.unlockButton.tag=0;
                cell.titleUnlockBtn.text=NSLocalizedString(@"Unlock", nil);
                cell.titleUnlockBtn.frame=CGRectMake(cell.unlockButton.frame.origin.x+5, cell.unlockButton.frame.origin.y, cell.unlockButton.frame.size.width-10, cell.unlockButton.frame.size.height);
                //cell.titleUnlockBtn.frame=cell.unlockButton.frame;
                cell.imgLock.alpha=1.0;
                cell.titleWaitBtn.alpha=0.0;
            }
            return cell;
        }
    }
}

-(void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==tableGames)
    {
        CustomCell *cell = (CustomCell *)[tableGames cellForRowAtIndexPath:indexPath];
        [cell.btnGame setHighlighted:YES];
    }

}
-(void)tableView:(UITableView *)tableView didUnhighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==tableGames)
    {
        CustomCell *cell = (CustomCell *)[tableGames cellForRowAtIndexPath:indexPath];
        [cell.btnGame setHighlighted:NO];
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==tableGames)
    {
        [self openTV];
        indexGame=indexPath.row;
        
        UIButton *btnLeft = (UIButton*)[highScoreView viewWithTag:99];
        btnLeft.transform = CGAffineTransformMakeRotation( ( 90 * M_PI ) / 180 );
        UIButton *btnRight = (UIButton*)[highScoreView viewWithTag:100];
        btnRight.transform = CGAffineTransformMakeRotation( ( 270 * M_PI ) / 180 );
        
        btnLeft.hidden = btnRight.hidden = YES;//labelStage.hidden = labelName.hidden = YES;
        
        if (indexGame > 0) {
            btnLeft.hidden = btnRight.hidden = NO;
            //            labelStage.hidden = labelName.hidden = NO;
        }
        
        [self completeLabels];
        
        if (!highScoreView.superview)
            [self.view insertSubview:highScoreView atIndex:1];
        btn_Play.text=NSLocalizedString(@"Play", nil);
        btn_Share.text=NSLocalizedString(@"Share", nil);
        [highScoreView setFrame:CGRectMake(0, 0, WIDTH_DEVICE, HEIGHT_DEVICE)];
        [btn_ShareHighScore setEnabled:NO];
        scoreShare=0;
        
        
    }
    else if ([stateTamagochi shopCategory]!=sh_potions && [stateTamagochi shopCategory]!=sh_eat  && lockedObject==-1 && tableView!=tableHighscore)
    {
        _rowTapped=[indexPath retain];
        ShopCell *cell = (ShopCell *)[tableShop cellForRowAtIndexPath:indexPath];
        if ([cell.descBody.text isEqualToString:NSLocalizedString(@"Locked ",nil)] && [stateTamagochi shopCategory]!=sh_body)
        {
            [self changeCellInTable:_rowTapped table:tableShop];
        }
        else
            [self showColors:indexPath];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == tableHighscore) {
        return 50;
    }
    if (tableView==tableShop || tableView==tableEat)
        return 75+75*IS_IPAD;
    return 80+60*IS_IPAD;
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"updateStates" object:nil];
    [[NSNotificationCenter defaultCenter]      removeObserver:self name:@"openTV" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"moveCarousel" object:nil];
    [[NSNotificationCenter defaultCenter]    removeObserver:self name:@"shopView" object:nil];
    [[NSNotificationCenter defaultCenter]   removeObserver:self name:@"nextLevel" object:nil];
    [[NSNotificationCenter defaultCenter]  removeObserver:self name:@"renameRoom" object:nil];
    [[NSNotificationCenter defaultCenter]  removeObserver:self name:@"nextRoom" object:nil];
    [[NSNotificationCenter defaultCenter]  removeObserver:self name:@"enableButtons" object:nil];
    
    //TODO: dealloc CLOSEGAME
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"closeGame" object:nil];
    //END notif
    
     otherPage=0;
    [carousel release];
    carousel.delegate=nil;
    carousel.dataSource=nil;
    [l_score release];
    [l_room release];
    [l_lvl release];
    [i_health release];
    [i_energy release];
    [i_life release];
    [i_funny release];
    [menuView release];
    [tableGames release];
    [highScoreView release];
    [shareView release];
    [statsView release];
    [stats_slide_1 release];
    [stats_slide_2 release];
    [stats_slide_3 release];
    [stats_slide_4 release];
    [stats_lab_1 release];
    [stats_lab_2 release];
    [stats_lab_3 release];
    [stats_lab_4 release];
    [shopView release];
    [tableShop release];
    [viewBodyShop release];
    [btnBackShop release];
    [selectColor release];
    [shopName release];
    [priceProduct release];
    [btnBuy release];
    [lbl_btnBuy release];
    [imageBuy release];
    [tableEat release];
    [btn_Eat_1 release];
    [btn_Eat_2 release];
    [btn_Eat_1 release];
    [btn_Eat_2 release];
    [btn_Eat_3 release];
    [btn_Eat_4 release];
    [viewEats release];
    [skinsView release];
    [btnCloseColors release];
    [selectColorsView release];
    [changebleColor release];
    [unlockView release];
    [locked_Text release];
    [titleUnlockBtn release];
    [unlockBtn release];
    [unlockIcon release];
    [titileWaitBtn release];
    [waitBtn release];
    [btnNext release];
    [btnBack release];
    [mallTap release];
    [viewPanelButonsStatistic release];
    [skinsLabGame release];
    [skinsLabBath release];
    [skinsLabStudio release];
    [skinsKitchen release];
    [skinsLabBed release];
    [viewTV release];
     [tableHighscore release];
    [btn_settings release];
    [tapCoinsView release];
    [states_Food release];
    [states_Energy release];
    [states_Health release];
    [states_Fun release];
    [btn_Play release];
    [btn_Share release];
    [lbl_Stage release];
    [super dealloc];
}
- (void)viewDidUnload {
    [tableHighscore release];
    tableHighscore = nil;
    [l_score release];
    l_score = nil;
    [l_room release];
    l_room = nil;
    [l_lvl release];
    l_lvl = nil;
    [i_health release];
    i_health = nil;
    [i_energy release];
    i_energy = nil;
    [i_life release];
    i_life = nil;
    [i_funny release];
    i_funny = nil;
    [menuView release];
    menuView = nil;
    [tableGames release];
    tableGames = nil;
    [highScoreView release];
    highScoreView = nil;
    [shareView release];
    shareView = nil;
    [statsView release];
    statsView = nil;
    [stats_slide_1 release];
    stats_slide_1 = nil;
    [stats_slide_2 release];
    stats_slide_2 = nil;
    [stats_slide_3 release];
    stats_slide_3 = nil;
    [stats_slide_4 release];
    stats_slide_4 = nil;
    [stats_lab_1 release];
    stats_lab_1 = nil;
    [stats_lab_2 release];
    stats_lab_2 = nil;
    [stats_lab_3 release];
    stats_lab_3 = nil;
    [stats_lab_4 release];
    stats_lab_4 = nil;
    [shopView release];
    shopView = nil;
    [tableShop release];
    tableShop = nil;
    [viewBodyShop release];
    viewBodyShop = nil;
    [btnBackShop release];
    btnBackShop = nil;
    [selectColor release];
    selectColor = nil;
    [shopName release];
    shopName = nil;
    [priceProduct release];
    priceProduct = nil;
    [btnBuy release];
    btnBuy = nil;
    [lbl_btnBuy release];
    lbl_btnBuy = nil;
    [imageBuy release];
    imageBuy = nil;
    [tableEat release];
    tableEat = nil;
    [btn_Eat_1 release];
    btn_Eat_1 = nil;
    [btn_Eat_2 release];
    btn_Eat_2 = nil;
    [btn_Eat_1 release];
    btn_Eat_1 = nil;
    [btn_Eat_2 release];
    btn_Eat_2 = nil;
    [btn_Eat_3 release];
    btn_Eat_3 = nil;
    [btn_Eat_4 release];
    btn_Eat_4 = nil;
    [viewEats release];
    viewEats = nil;
    [skinsView release];
    skinsView = nil;
    [btnCloseColors release];
    btnCloseColors = nil;
    [selectColorsView release];
    selectColorsView = nil;
    [changebleColor release];
    changebleColor = nil;
    [unlockView release];
    unlockView = nil;
    [locked_Text release];
    locked_Text = nil;
    [titleUnlockBtn release];
    titleUnlockBtn = nil;
    [unlockBtn release];
    unlockBtn = nil;
    [unlockIcon release];
    unlockIcon = nil;
    [titileWaitBtn release];
    titileWaitBtn = nil;
    [waitBtn release];
    waitBtn = nil;
    [btnNext release];
    btnNext = nil;
    [btnBack release];
    btnBack = nil;
    [mallTap release];
    mallTap = nil;
    [viewPanelButonsStatistic release];
    viewPanelButonsStatistic = nil;
    [skinsLabGame release];
    skinsLabGame = nil;
    [skinsLabBath release];
    skinsLabBath = nil;
    [skinsLabStudio release];
    skinsLabStudio = nil;
    [skinsKitchen release];
    skinsKitchen = nil;
    [skinsLabBed release];
    skinsLabBed = nil;
    [viewTV release];
    viewTV = nil;
    [btn_settings release];
    btn_settings = nil;
    [tapCoinsView release];
    tapCoinsView = nil;
    [states_Food release];
    states_Food = nil;
    [states_Energy release];
    states_Energy = nil;
    [states_Health release];
    states_Health = nil;
    [states_Fun release];
    states_Fun = nil;
    [btn_Play release];
    btn_Play = nil;
    [btn_Share release];
    btn_Share = nil;
    [lbl_Stage release];
    lbl_Stage = nil;
    [super viewDidUnload];
}

//TODO: tot pentru Games
// TODO: pina la END
#pragma mark - ANIMATION
- (void)chooseGameAnimation {
    
     [self chooseGame];
    
    
    /*
    [UIView animateWithDuration:.001
                     animations:^{
                         [[CCDirector sharedDirector] view].alpha = 0;
                     }
                     completion:^(BOOL finished){
                         [UIView animateWithDuration:.3
                                          animations:^{
                                              [self chooseGame];
                                          }
                                          completion:^(BOOL finished){
                                              [UIView animateWithDuration:.3
                                                               animations:^{
                                                                   [[CCDirector sharedDirector] view].alpha = 1;
                                                               }
                                                               completion:^(BOOL finished){
                                                                   
                                                               }];
                                              
                                          }];
                     }];
     */
    
}

- (void)chooseGame{
    switch (indexGame) {
        case 0:
        {
            [[personaj person] removeFromParentAndCleanup:NO];
            [[[CCDirector sharedDirector] view] setFrame:CGRectMake(0, 0, WIDTH_DEVICE, HEIGHT_DEVICE)];
            
            [[CCDirector sharedDirector] replaceScene:[CCTransitionCrossFade transitionWithDuration:.1f scene:[Game scene] ]  ];
        }
            break;
        case 1:
            
            [[[CCDirector sharedDirector] view] setTransform:CGAffineTransformMakeRotation(3*M_PI_2)];
            [[[CCDirector sharedDirector] view] setFrame:CGRectMake(0, 0, WIDTH_DEVICE, HEIGHT_DEVICE)];
            [[CCDirector sharedDirector] replaceScene: [CCTransitionCrossFade transitionWithDuration:0.1 scene:[TinyPoo sceneWithStage:selectedStage]]];
            break;
        default:
            break;
    }
    [self removeViewsGameRoom];
}


// TODO: SELECT STAGe

// TODO: SELECT STAGe
- (IBAction)actionSelectStage:(id)sender {
    [textFieldWithNamePlayer resignFirstResponder];
    switch ([sender tag]) {
        case 99:
            //left
            if (selectedStage<2) {
                selectedStage = 10;
            }else
                selectedStage--;
            
            
            break;
        case 100:
            //right
            if (selectedStage>9) {
                selectedStage = 1;
            }else
                selectedStage++;
            break;
        default:
            break;
    }
    
    NSInteger stage = [stateTamagochi getStageForPlayer:currentPlayer];
    if (stage==0) {
        stage =1;
    }
    
    if (selectedStage % 2 ==0) {
        lbl_Stage.text = [NSString stringWithFormat:@"%@    %i",NSLocalizedString(@"boss", nil), selectedStage - selectedStage/2];
    }else{
        lbl_Stage.text = [NSString stringWithFormat:@"%@    %i",NSLocalizedString(@"stage", nil),selectedStage - selectedStage/2];
    }
    
    
    if (selectedStage <= stage) {
        [self enableButtonsGame];
    }else   {
        [self disableButtons];
    }
    [btn_ShareHighScore setEnabled:NO];
    [tableHighscore reloadData];
    //    [self addToViewHighscores];
    
}

- (void)disableButtons {
    for (UIButton *btn in [highScoreView subviews]) {
        if ([btn isKindOfClass: UIButton.class] && btn.tag > 0 && btn.tag < 4) {
            btn.enabled = NO;
        }
    }
}

- (void)enableButtonsGame {
    for (UIButton *btn in [highScoreView subviews]) {
        if ([btn isKindOfClass: UIButton.class] && btn.tag > 0 && btn.tag < 3) {
            btn.enabled = YES;
        }
    }
    
}
//END
// TODO: complete LABELS
// TODO: complete LABELS
- (void)completeLabels{
    NSInteger stage = 1;
    if (stage< [stateTamagochi getStageForPlayer:currentPlayer]) {
        stage = [stateTamagochi getStageForPlayer:currentPlayer];
    }
    
    selectedStage = stage;
    
    if (indexGame>0) {
        labelStage.text = @"Para in Space";
        if (selectedStage%2==0) {
            lbl_Stage.text = [NSString stringWithFormat:@"%@   %d",NSLocalizedString(@"boss", nil),selectedStage-(selectedStage/2)];
        }else{
            lbl_Stage.text = [NSString stringWithFormat:@"%@   %d",NSLocalizedString(@"stage", nil),selectedStage-(selectedStage/2)];
        }
    }else{
        lbl_Stage.text=@"";
        labelStage.text = @"Para Jump";
    }
    [self enableButtons];
    [self loadCurrentPlayer];
    
    [tableHighscore reloadData];
    [self enableButtonsGame];
    //    [self addToViewHighscores];
}
- (void)loadCurrentPlayer {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	
	currentPlayer = nil;
	currentPlayer = [defaults objectForKey:@"player"];
    
	if(!currentPlayer)
        currentPlayer = @"Paramon";
	
	[defaults setObject:currentPlayer forKey:@"player"];
    [defaults synchronize];
    textFieldWithNamePlayer.text = currentPlayer;
    
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	if (textField.text.length ==0) {
        textField.text = [defaults objectForKey:@"player"];
    }
	[defaults setObject:textField.text forKey:@"player"];
    currentPlayer = textField.text;
    [self completeLabels];
    [textField resignFirstResponder];
}




//END TODO


- (void)closeGame{
    
    [tableHighscore reloadData];
    [highScoreView setFrame:CGRectMake(0, 0, WIDTH_DEVICE, HEIGHT_DEVICE)];
    [[[CCDirector sharedDirector] view] setFrame: CGRectMake(0, 40+40*IS_IPAD, WIDTH_DEVICE, HEIGHT_DEVICE-(40+40*IS_IPAD))];
    [[CCDirector sharedDirector] replaceScene:[GameLabScene scene]];
    [self completeLabels];
    viewPanelButonsStatistic.hidden = NO;
    viewPanelButonsStatistic.userInteractionEnabled = YES;
}

@end
