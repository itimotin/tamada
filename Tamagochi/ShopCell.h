//
//  ShopCell.h
//  Tamagochi
//
//  Created by Sergiu Moțipan on 10/3/13.
//
//

#import <UIKit/UIKit.h>

@interface ShopCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UIImageView *imageBody;
@property (retain, nonatomic) IBOutlet UILabel *nameBody;
@property (retain, nonatomic) IBOutlet UILabel *descBody;
@property (retain, nonatomic) IBOutlet UILabel *nameButton;
@property (retain, nonatomic) IBOutlet UIButton *buttonBody;
@property (retain, nonatomic) IBOutlet UILabel *quantity;
@property (retain, nonatomic) IBOutlet UILabel *priceBody;
@property (retain, nonatomic) IBOutlet UIImageView *money;
@property (retain, nonatomic) IBOutlet UIImageView *lockImage;
- (IBAction)updateShopObject:(id)sender;

@end
