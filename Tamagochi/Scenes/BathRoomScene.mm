//
//  FirstScene.m
//  Tamagochi
//
//  Created by Sergiu Moțipan on 6/4/13.
//
//

#import "BathRoomScene.h"



@implementation BathRoomScene
{

    int touchObject;
    int timp;
    int timp_2;
    int mouse;
    int oche_mouse;
    int n_mouse;
    int eye_pos;
    float totalTime,yy,xx, tamTouch;
    BOOL repeatTouches;
    float limit;
    CCSprite *spuma1;
    CCSprite *spuma2;
    
    float soapErase;
    float stergarErase;
    float soapErase2;
    NSTimeInterval previousTimestamp;
    
}
- (void)performBlock:(void (^)(void))block afterDelay:(NSTimeInterval)delay
{
    int64_t delta = (int64_t)(1.0e9 * delay);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delta), dispatch_get_main_queue(), block);
}

+(CCScene *) scene
{
	CCScene *scene = [CCScene node];
	BathRoomScene *layer = [BathRoomScene node];
	[scene addChild: layer];
	return scene;
}

-(CCAction*)initAnimations
{
    id action1;
    id action2;
    if (!IS_RETINA)
    {
        action1 = [CCScaleTo actionWithDuration:0.4 scale:0.55];
        action2 = [CCScaleTo actionWithDuration:0.4 scale:0.5];
    }
    else
    {
        action1 = [CCScaleBy actionWithDuration:0.4 scale:1.1];
        action2 = [CCScaleTo actionWithDuration:0.4 scale:1.0];
    }
    id  animationObjects = [CCSequence actions:action1, action2, nil];
    return [CCRepeatForever actionWithAction:animationObjects];
}

-(ccColor3B)tranformColor:(NSInteger)tag object:(NSInteger)object
{
    NSMutableArray *arr=[[[NSMutableArray alloc] initWithArray:[stateTamagochi colorSkin:tag object:object]] autorelease];
    ccColor3B color={[[arr objectAtIndex:0] floatValue], [[arr objectAtIndex:1] floatValue], [[arr objectAtIndex:2] floatValue]};
    return color;
  
}

-(void)initSprites
{
    CCSprite *background = [CCSprite spriteWithFile:[NSString stringWithFormat:@"bg_2%@.png",IMG_PREFIX]];
    background.tag=1;
    [background setColor:[self tranformColor:[stateTamagochi selectColorSkin:1 room:2] object:0]];
    background.position = ccp(winSize.width/2, winSize.height/2+30);
    [background setScale:0.5+0.5*IS_RETINA];
    [self addChild:background z:-13];
   
    
    CCSprite *foot = [CCSprite spriteWithFile:[NSString stringWithFormat:@"foot_2%@.png",(IS_IPAD)?@"-iPad":@""]];
    foot.tag=2;
    [foot setColor:[self tranformColor:[stateTamagochi selectColorSkin:2 room:2] object:0]];
    foot.position = ccp(winSize.width/2, foot.boundingBox.size.height/2);
    [foot setScale:0.5+0.5*IS_RETINA];
    [self addChild:foot z:-13];

    CCSprite *cuier = [CCSprite spriteWithFile:[NSString stringWithFormat:@"cuier%@.png", (IS_IPAD)?@"-iPad":@""]];
    [cuier setScale:0.5+0.5*IS_RETINA];
    cuier.position = ccp(8+5*IS_IPAD, 353+290*IS_IPAD);
    [self addChild:cuier z:-7];
    CCSprite *vanna = [CCSprite spriteWithFile:[NSString stringWithFormat:@"vanna%@.png", (IS_IPAD)?@"-iPad":@""]];
    [vanna setScale:0.5+0.5*IS_RETINA];
    vanna.position = ccp(winSize.width/2, 90+78*IS_IPAD);
    [self addChild:vanna z:-8];
    
    CCSprite *top_vanna = [CCSprite spriteWithFile:[NSString stringWithFormat:@"top_vanna%@.png", (IS_IPAD)?@"-iPad":@""]];
    [top_vanna setScale:0.5+0.5*IS_RETINA];
    top_vanna.position = ccp(winSize.width/2,vanna.boundingBox.size.height+vanna.boundingBox.origin.y);
    [self addChild:top_vanna z:-12];
    
    CCSprite *washbasin = [CCSprite spriteWithFile:[NSString stringWithFormat:@"washbasin%@.png", (IS_IPAD)?@"-iPad":@""]];
    [washbasin setScale:0.5+0.5*IS_RETINA];
    washbasin.position = ccp(winSize.width-washbasin.boundingBox.size.width/2,winSize.height/2.8);
    [self addChild:washbasin z:-9];
    
    CCSprite *board = [CCSprite spriteWithFile:[NSString stringWithFormat:@"board%@.png", (IS_IPAD)?@"-iPad":@""]];
    [board setScale:0.5+0.5*IS_RETINA];
    board.position = ccp(winSize.width/2,board.boundingBox.size.height/2);
    [self addChild:board z:-9];
    
    soap = [CCSprite spriteWithFile:[NSString stringWithFormat:@"soap_2%@.png", (IS_IPAD)?@"-iPad":@""]];
    soap.tag=5;
    [soap setColor:[self tranformColor:[stateTamagochi selectColorSkin:5 room:2] object:0]];
    [soap setScale:0.5+0.5*IS_RETINA];
    soap.position = ccp(90+150*IS_IPAD, 30+10*IS_IPAD);
    [self addChild:soap z:-6];
    
    spuma1 = [CCSprite spriteWithFile:[NSString stringWithFormat:@"spuma_1%@.png", (IS_IPAD)?@"-iPad":@""]];
   
    [spuma1 setScale:0.5+0.5*IS_RETINA];
    spuma1.position = ccp(winSize.width/2, 135+130*IS_IPAD);
    [self addChild:spuma1 z:-7];
    
    spuma2 = [CCSprite spriteWithFile:[NSString stringWithFormat:@"spuma_2%@.png", (IS_IPAD)?@"-iPad":@""]];
    [spuma2 setScale:0.5+0.5*IS_RETINA];
    spuma2.position = ccp(winSize.width/2-10, 50+40*IS_IPAD);
    [self addChild:spuma2 z:-7];
    [spuma1 setScaleY:0.0];
    [spuma2 setScaleY:0.0];
    
    stishka = [CCSprite spriteWithFile:[NSString stringWithFormat:@"stishca%@.png", (IS_IPAD)?@"-iPad":@""]];
    stishka.tag=6;
    [stishka setScale:0.5+0.5*IS_RETINA];
    stishka.position = ccp(winSize.width/2-2, winSize.height-70-105*IS_IPAD);
    [self addChild:stishka z:-10];
    
    if ([stateTamagochi state]==stateDirt && [stateTamagochi shopCategory]!=sh_skins)
    [stishka runAction:[self initAnimations]];
    
    
    cranik = [CCSprite spriteWithFile:[NSString stringWithFormat:@"cranik%@.png", (IS_IPAD)?@"-iPad":@""]];
    [cranik setScale:0.5+0.5*IS_RETINA];
    cranik.position = ccp(winSize.width/2+3, winSize.height-65-65*IS_IPAD);
    [self addChild:cranik z:-10];
    
   
    CCSprite *draperie = [CCSprite spriteWithFile:[NSString stringWithFormat:@"draperie_2%@.png", IMG_PREFIX]];
    draperie.tag=3;
    [draperie setColor:[self tranformColor:[stateTamagochi selectColorSkin:3 room:2] object:0]];
    [draperie setScale:0.5+0.5*IS_RETINA];
    draperie.position = ccp(winSize.width/2, winSize.height-130);
    [draperie setScale:0.5+0.5*IS_RETINA];
    [self addChild:draperie z:-8];
    
    
    self.isTouchEnabled=YES;
    
    stergar = [CCSprite spriteWithFile:[NSString stringWithFormat:@"stergar_2%@.png", (IS_IPAD)?@"-iPad":@""]];
    stergar.tag=4;
    [stergar setColor:[self tranformColor:[stateTamagochi selectColorSkin:4 room:2] object:0]];
    [stergar setScale:0.5+0.5*IS_RETINA];
    stergar.position = CGPointMake(25+26*IS_IPAD, 280+202*IS_IPAD);
   // stergar.tag=tagStergar;
    [self addChild:stergar z:-6];
      
    touchObject=tagDefault;
    
    rainEmitter = [CCParticleRain node];
    [rainEmitter setPosition:CGPointMake(50, 40)];
    rainEmitter.life = 0;
    if (!IS_IPAD){
    if (!IS_WIDESCREEN)
        rainEmitter.sourcePosition=CGPointMake(105, 285);
    else
        rainEmitter.sourcePosition=CGPointMake(105, 365);
    }
    else
    {
        rainEmitter.sourcePosition=CGPointMake(winSize.width/2-55, winSize.height/2+200);
    }
    
    rainEmitter.posVar=CGPointMake(40+45*IS_IPAD, 0);
    rainEmitter.emissionRate=200.0f;
    rainEmitter.texture = [[CCTextureCache sharedTextureCache] addImage: @"picatura.png"];
    rainEmitter.startSize = 4.0f+4.0*IS_IPAD;
    rainEmitter.endSize=6.0f+6.0*IS_IPAD;
    [self addChild: rainEmitter z:-9];
}

-(id)init
{
    self=[super init];
    if (self)
    {
        [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
        [[SimpleAudioEngine sharedEngine] stopAllEffects];
        [[SimpleAudioEngine sharedEngine] setEffectsVolume:[stateTamagochi soundOn]];
        [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume:[stateTamagochi soundOn]];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(changeColor:) name:@"skinColor" object:nil];
        [stateTamagochi setRoom:bathRoom];
        stateEye=[stateTamagochi eyeState];
        mouth=[stateTamagochi mouthState];
        
        soapErase=0.0;
        soapErase2=0.0;
        stergarErase=0.0;
        [[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"giggle.wav"];
        [[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"mtem.wav"];
        [[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"dush.mp3"];
        [[SimpleAudioEngine sharedEngine] preloadEffect:@"brush-long.mp3"];
        [[SimpleAudioEngine sharedEngine] preloadEffect:@"stergar.mp3"];
        [[SimpleAudioEngine sharedEngine] setEffectsVolume:[stateTamagochi soundOn]];
        [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume:[stateTamagochi soundOn]];
        [self initAnimations];
        winSize = [[CCDirector sharedDirector] winSize];
       
        [personaj person].position = ccp(winSize.width/2, 140+40*[stateTamagochi sizeTamagochi]+170*IS_IPAD);
        [personaj initRects];
        [personaj person].delegate=self;
        [self addChild:[personaj person] z:-11];
      
        [self performSelectorOnMainThread:@selector(initSprites) withObject:nil waitUntilDone:YES];
        [self scheduleUpdate];
 
    }
    return self;
}

-(void)changeColor:(NSNotification *)notif
{
    [(CCSprite *)[self getChildByTag:[[[notif.object componentsSeparatedByString:@","] objectAtIndex:1] intValue]] setColor:[self tranformColor:[[[notif.object componentsSeparatedByString:@","] objectAtIndex:0] intValue] object:[[[notif.object componentsSeparatedByString:@","] objectAtIndex:1] intValue]]];
}

- (void)update:(float)dt
{
}

-(float)returnAlpha
{
    return soapErase2;
}

-(CGPoint)returnPosition
{
    return CGPointMake(xx, yy);
}

-(float)returnTouchValue
{
    
    switch ([stateTamagochi mouthState]) {
        case mouthStateFun:
        {
            return tamTouch*sin(totalTime);
        }
            break;
        case mouthStateReadyEat:
        {
            return 0.3;
        }
            break;
        case mouthStateEat:
        {
            return 0.25*sin(totalTime);
        }
            break;
       
            
        default:return tamTouch;
            break;
    }
}

-(CGFloat)distanceFromYwoPoint:(CGPoint)y other:(CGPoint)x
{
  return  sqrt(pow((x.x - y.x), 2.0) + pow((x.y - y.y), 2.0));
}


-(void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    nr_sounds=0;
    UITouch* touch = [touches anyObject];
    CGPoint location = [touch locationInView: [touch view]];
    startTouchPoint=location;
    previousTimestamp = event.timestamp;
    date_start=[[NSDate date] retain];
    
    location = [[CCDirector sharedDirector] convertToGL:location];
    xx=location.x;
    yy=location.y;
    touchFun=0;
    if (CGRectContainsPoint([stergar boundingBox], location) ) {
        if (rainEmitter.life==0)
        {
           
            sound_stergar=[[SimpleAudioEngine sharedEngine] playEffect:@"stergar.mp3" loop:YES];
            [stateTamagochi setTouchObject:tagStergar];
            if (CGPointEqualToPoint(stergar.position, CGPointMake(25+26*IS_IPAD, 280+202*IS_IPAD)))
            {
                [stergar stopAllActions];
                [stergar runAction:[CCScaleBy actionWithDuration:0.5 scaleX:3.5-stergar.scaleX scaleY:0.5+0.5*IS_RETINA]];
            }
        }
        else
        {
            [stishka stopAllActions];
            [stishka setScale:0.5+0.5*IS_RETINA];
            [stishka runAction:[self initAnimations]];
        }
    }
    else
    if (CGRectContainsPoint([personaj rectPerson],location))
    {
        [stateTamagochi setTouchObject:tagTamagochi];
    }
    
    if (CGRectContainsPoint([stishka boundingBox],location))
    {
        [stishka pauseSchedulerAndActions];
        [stishka setScale:0.5+0.5*IS_RETINA];
        if (rainEmitter.life!=0)
        {
            [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
            if (![stateTamagochi bathStart])
            {
                [[personaj person] addMoney:50];
                [stateTamagochi setBathStart:YES];
                [stateTamagochi updateFirstStart:3];
            }
            if (CGPointEqualToPoint(soap.position, CGPointMake(90+150*IS_IPAD, 30+10*IS_IPAD)))
                [soap stopAllActions];
            [soap setScale:1.0];
            rainEmitter.life=0;
            [stateTamagochi setEyeState:stateEye];
            [cranik runAction:[CCRotateBy actionWithDuration:0.5 angle:360]];
            [spuma1 runAction:[CCScaleBy actionWithDuration:1.5 scaleX:0.5+0.5*IS_RETINA scaleY:0.0]];
            [spuma2 runAction:[CCScaleBy actionWithDuration:2.5 scaleX:0.5+0.5*IS_RETINA scaleY:0.0]];
            [spuma1 runAction:[CCSequence actions: [CCFadeOut actionWithDuration:3.5],[CCFadeIn actionWithDuration:0.0], nil]];
            [spuma2 runAction:[CCSequence actions: [CCFadeOut actionWithDuration:4.5],[CCFadeIn actionWithDuration:0.0], nil]];
            soapErase=0.0;
            soapErase2=0.0;
        }
        else
        {
            [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"dush.mp3" loop:YES];
            [self performBlock:^{

                if ([(CCSprite *)[[[personaj person] getChildByTag:6]  getChildByTag:40] opacity]==0.0)
                {
                    [[[[personaj person] getChildByTag:6]  getChildByTag:40]  runAction:[CCFadeIn actionWithDuration:1.0]];
                }
            } afterDelay:1.5];

            if (!IS_IPAD) {
           
            if (!IS_WIDESCREEN)
                rainEmitter.life=1.3;
            else
                rainEmitter.life=1.7;
            }
            else
                rainEmitter.life=2.7;
            [cranik runAction:[CCRotateBy actionWithDuration:0.5 angle:-360]];
            if ([soap numberOfRunningActions]==0)
            [soap runAction:[self initAnimations]];
            [stateTamagochi setEyeState:eyeStateShower];
        }
        [stateTamagochi setTouchObject:tagStishka];
    }
    
    if (CGRectContainsPoint([soap boundingBox],location))
    {
        [soap stopAllActions];
        [soap setScale:1.0];
        if (rainEmitter.life!=0)
        {
            [stateTamagochi setTouchObject:tagSoap];
            sound_soap=[[SimpleAudioEngine sharedEngine] playEffect:@"brush-long.mp3" loop:YES];
        }
        else
        {
            [stishka stopAllActions];
            [stishka setScale:0.5+0.5*IS_RETINA];
            [stishka runAction:[self initAnimations]];
        }
    }
}

-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch* touch = [touches anyObject];
    CGPoint location = [touch locationInView: [touch view]];
    CGPoint prevLocation = [touch previousLocationInView:[touch view]];
    CGFloat distanceFromPrevious = [self distanceFromYwoPoint:location other:prevLocation];
    NSTimeInterval timeSincePrevious = event.timestamp - previousTimestamp;
    previousTimestamp = event.timestamp;
    CGFloat speed = distanceFromPrevious/timeSincePrevious;
    
    location = [[CCDirector sharedDirector] convertToGL:location];

    if ([stateTamagochi touchObject]==tagTamagochi && CGRectContainsPoint([personaj rectPerson],location) && rainEmitter.life==0.0)
    {
        [stateTamagochi setMouthState:mouthStateFun];
        touchFun+=0.006;
        if (touchFun>=0.05)
        {
            if (!repeatTouches)
            [self fun];
        }
        nr_sounds++;
    }
    else
    if ([stateTamagochi touchObject]==tagStergar)
    {
        if ([stateTamagochi soundOn])
        {
            float v=speed/200.0;
            if (v>1.0) v=1.0;
            [[SimpleAudioEngine sharedEngine] setEffectsVolume:v];
        }
        [stergar setPosition:location];
        if (CGRectContainsPoint([personaj rectPerson], location) && speed>1000.0)
        {
           
            [stateTamagochi setEyeState:eyeStateErase];
        
            soapErase2+=0.02;
            if (soapErase2>=1.0)
                soapErase2=1.0;
        if (soapErase2>0.7  && [(CCSprite *)[[[personaj person] getChildByTag:6]  getChildByTag:40] opacity]==255.0)
        {
            if ([stateTamagochi state]==stateSlowDirt)
            {
                [[personaj person] addMoney:15];
                if ([stateTamagochi funny]<100.0)
                {
                    [stateTamagochi setNextLevelPoints:[stateTamagochi nextLevelPoints] +3.0*(1.3-[stateTamagochi sizeTamagochi])];
                    [stateTamagochi addToNextLevel:4];
                }
            }
            [stateTamagochi setState:stateDefault];
            [[[[personaj person] getChildByTag:6]  getChildByTag:40]  runAction:[CCFadeOut actionWithDuration:1.0]];
        }
        }
        else
        {
            [[SimpleAudioEngine sharedEngine] setEffectsVolume:0];
        }
    }
    else if ([stateTamagochi touchObject]==tagSoap)
    {
        if ([stateTamagochi soundOn])
        {
            float v=speed/200.0;
            if (v>1.0) v=1.0;
            [[SimpleAudioEngine sharedEngine] setEffectsVolume:v];
        }
        [soap setPosition:location];
        if (CGRectContainsRect([personaj rectPerson],[soap boundingBox]) && speed>1000.0 && spuma1.numberOfRunningActions==0)
        {
            [stateTamagochi setEyeState:eyeStateErase];
            soapErase+=0.025+0.025*IS_RETINA;
            if (soapErase>=0.4+0.4*IS_RETINA)
            {
                soapErase=0.4+0.4*IS_RETINA;
                soapErase2+=0.025+0.025*IS_RETINA;
                   if (soapErase2>=0.5+0.5*IS_RETINA)
                       soapErase2=0.5+0.5*IS_RETINA;
                if (soapErase2>0.6 && [stateTamagochi state]==stateDirt)
                {
                    [stateTamagochi setDirtFood:0.0];
                    [stateTamagochi updateDirt];
                  
                    [[personaj person] addMoney:5];
                    [stateTamagochi setNextLevelPoints:[stateTamagochi nextLevelPoints] +5.0*(1.3-[stateTamagochi sizeTamagochi])];
                    [stateTamagochi addToNextLevel:4];
                    [stateTamagochi setHealth:[stateTamagochi health]+5.0];
                    [stateTamagochi updateHealth];
                    [[[[personaj person] getChildByTag:6] getChildByTag:39] runAction:[CCFadeOut actionWithDuration:1.0]];
                    [stateTamagochi setState:stateSlowDirt];
                }
            }
                [spuma1 setScaleY:soapErase];
                [spuma2 setScaleY:soapErase2];
        }
        else{
            [[SimpleAudioEngine sharedEngine] setEffectsVolume:0.0];
        }
    }
    xx=location.x;
    yy=location.y;
}
-(void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch* touch = [touches anyObject];
    CGPoint location = [touch locationInView: [touch view]];
    
    endTouchPoint=location;
    float moveDifference=startTouchPoint.x-endTouchPoint.x;
    float differenceY=startTouchPoint.y-endTouchPoint.y;
    if (differenceY<0) differenceY*=-1;
    NSTimeInterval secondsBetween = [[NSDate date] timeIntervalSinceDate:date_start];
    [date_start release];
    date_start=nil;
    if (moveDifference>200.0 && differenceY<40.0 && endTouchPoint.y<150.0 && secondsBetween<0.3)
        [[NSNotificationCenter defaultCenter]postNotificationName:@"nextRoom" object:@"1"];
    else if (moveDifference<-200.0 && differenceY<40.0 && endTouchPoint.y<150.0 && secondsBetween<0.3)
        [[NSNotificationCenter defaultCenter]postNotificationName:@"nextRoom" object:@"2"];
    
    location = [[CCDirector sharedDirector] convertToGL:location];
    nr_sounds=0;
    touchFun=0;
    xx=0;
    yy=0;
    if ([stateTamagochi touchObject]==tagStergar)
    {
        [[SimpleAudioEngine sharedEngine] stopEffect:sound_stergar];
        [stergar runAction:[CCMoveTo actionWithDuration:0.5 position:CGPointMake(25+26*IS_IPAD, 280+202*IS_IPAD)]];
        [stergar runAction:[CCScaleTo actionWithDuration:0.5 scale:0.5+0.5*IS_RETINA]];
    }
    else if ([stateTamagochi touchObject]==tagSoap)
    {
        [[SimpleAudioEngine sharedEngine] stopEffect:sound_soap];
         [soap stopAllActions];
         [soap runAction:[CCMoveTo actionWithDuration:0.5 position:CGPointMake(90+150*IS_IPAD, 30+10*IS_IPAD)]];
    }
    if ([stateTamagochi touchObject]==tagStishka && rainEmitter.life==0)
    {
        if ([stateTamagochi state]==stateSlowDirt)
        [stergar runAction:[self initAnimations]];
        [stateTamagochi setEyeState:stateEye];
        [stateTamagochi updateEnergy];
    }
    else
        [stateTamagochi setEyeState:eyeStateShower];
    
    touchObject=tagDefault;
    [stateTamagochi setMouthState:mouth];
    [stateTamagochi setTouchObject:tagDefault];
    [[SimpleAudioEngine sharedEngine] setEffectsVolume:[stateTamagochi soundOn]];
}

-(void)changePosition:(CGPoint)pos
{
    xx=pos.x;
    yy=pos.y;
}

#pragma Tamagochi effects

-(void)resetVariables
{
    repeatTouches=NO;
    touchFun=0;
    if ([[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying])
        [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
}

-(void)fun
{
    repeatTouches=YES;
    totalTime=0;
    tamTouch=0.3;
    if (![[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying] &&  [stateTamagochi soundOn])
        [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"giggle.wav" loop:NO];
    [self performSelector:@selector(resetVariables) withObject:nil afterDelay:1.57];
}

-(void)dealloc
{
    [[SimpleAudioEngine sharedEngine] stopEffect:sound_stergar];
    [[SimpleAudioEngine sharedEngine] stopEffect:sound_soap];
    [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
    
    [stateTamagochi setEyeState:stateEye];
    [[NSNotificationCenter defaultCenter]    removeObserver:self name:@"skinColor" object:nil];
    [super dealloc];
}
@end
