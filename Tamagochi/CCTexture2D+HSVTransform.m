#import "CCTexture2D+HSVTransform.h"

static CCTexture2DPixelFormat defaultAlphaPixelFormat_ = kCCTexture2DPixelFormat_Default;

@implementation CCTexture2D (CCTexture2D_HSVTransform)

#ifdef __IPHONE_OS_VERSION_MAX_ALLOWED
- (id) initWithImage:(UIImage *)uiImage transform:(HSVTransform*) transform
#elif defined(__MAC_OS_X_VERSION_MAX_ALLOWED)
- (id) initWithImage:(CGImageRef)CGImage transform:(HSVTransform*) transform
#endif
{
    NSUInteger                POTWide, POTHigh;
    CGContextRef            context = nil;
    void*                    data = nil;;
    CGColorSpaceRef            colorSpace;
    void*                    tempData;
    unsigned int*            inPixel32;
    unsigned short*            outPixel16;
    BOOL                    hasAlpha;
    CGImageAlphaInfo        info;
    CGSize                    imageSize;
    CCTexture2DPixelFormat    pixelFormat;
    
#ifdef __IPHONE_OS_VERSION_MAX_ALLOWED
    CGImageRef    CGImage = uiImage.CGImage;
#endif
    
    if(CGImage == NULL) {
        CCLOG(@"cocos2d: CCTexture2D. Can't create Texture. UIImage is nil");
        [self release];
        return nil;
    }
    
    CCConfiguration *conf = [CCConfiguration sharedConfiguration];
    
#if CC_TEXTURE_NPOT_SUPPORT
    if( [conf supportsNPOT] ) {
        POTWide = CGImageGetWidth(CGImage);
        POTHigh = CGImageGetHeight(CGImage);
        
    } else
#endif
    {
        POTWide = ccNextPOT(CGImageGetWidth(CGImage));
        POTHigh = ccNextPOT(CGImageGetHeight(CGImage));
    }
    
    NSUInteger maxTextureSize = [conf maxTextureSize];
    if( POTHigh > maxTextureSize || POTWide > maxTextureSize ) {
        CCLOG(@"cocos2d: WARNING: Image (%lu x %lu) is bigger than the supported %ld x %ld",
              (long)POTWide, (long)POTHigh,
              (long)maxTextureSize, (long)maxTextureSize);
        [self release];
        return nil;
    }
    
    info = CGImageGetAlphaInfo(CGImage);
    hasAlpha = ((info == kCGImageAlphaPremultipliedLast) || (info == kCGImageAlphaPremultipliedFirst) || (info == kCGImageAlphaLast) || (info == kCGImageAlphaFirst) ? YES : NO);
    
    size_t bpp = CGImageGetBitsPerComponent(CGImage);
    colorSpace = CGImageGetColorSpace(CGImage);
    
    if(colorSpace) {
        if(hasAlpha || bpp >= 8)
            pixelFormat = defaultAlphaPixelFormat_;
        else {
            CCLOG(@"cocos2d: CCTexture2D: Using RGB565 texture since image has no alpha");
            pixelFormat = kCCTexture2DPixelFormat_RGB565;
        }
    } else {
        // NOTE: No colorspace means a mask image
        CCLOG(@"cocos2d: CCTexture2D: Using A8 texture since image is a mask");
        pixelFormat = kCCTexture2DPixelFormat_A8;
    }
    
    imageSize = CGSizeMake(CGImageGetWidth(CGImage), CGImageGetHeight(CGImage));
    
    // Create the bitmap graphics context
    
    switch(pixelFormat) {
        case kCCTexture2DPixelFormat_RGBA8888:
        case kCCTexture2DPixelFormat_RGBA4444:
        case kCCTexture2DPixelFormat_RGB5A1:
            colorSpace = CGColorSpaceCreateDeviceRGB();
            data = malloc(POTHigh * POTWide * 4);
            info = hasAlpha ? kCGImageAlphaPremultipliedLast : kCGImageAlphaNoneSkipLast;
            //            info = kCGImageAlphaPremultipliedLast;  // issue #886. This patch breaks BMP images.
            context = CGBitmapContextCreate(data, POTWide, POTHigh, 8, 4 * POTWide, colorSpace, info | kCGBitmapByteOrder32Big);
            CGColorSpaceRelease(colorSpace);
            break;
            
        case kCCTexture2DPixelFormat_RGB565:
            colorSpace = CGColorSpaceCreateDeviceRGB();
            data = malloc(POTHigh * POTWide * 4);
            info = kCGImageAlphaNoneSkipLast;
            context = CGBitmapContextCreate(data, POTWide, POTHigh, 8, 4 * POTWide, colorSpace, info | kCGBitmapByteOrder32Big);
            CGColorSpaceRelease(colorSpace);
            break;
        case kCCTexture2DPixelFormat_A8:
            data = malloc(POTHigh * POTWide);
            info = kCGImageAlphaOnly;
            context = CGBitmapContextCreate(data, POTWide, POTHigh, 8, POTWide, NULL, info);
            break;
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid pixel format"];
    }
    
    CGContextClearRect(context, CGRectMake(0, 0, POTWide, POTHigh));
    CGContextTranslateCTM(context, 0, POTHigh - imageSize.height);
    CGContextDrawImage(context, CGRectMake(0, 0, CGImageGetWidth(CGImage), CGImageGetHeight(CGImage)), CGImage);
    
    // HSV shift
    unsigned int* pix32 = (unsigned int*) data;
    for(unsigned int i = 0; i < POTWide * POTHigh; ++i, ++pix32)
    {
        *pix32 = HSVTransformRGBA8(transform, *pix32);
    }
    
    // Repack the pixel data into the right format
    
    if(pixelFormat == kCCTexture2DPixelFormat_RGB565) {
        //Convert "RRRRRRRRRGGGGGGGGBBBBBBBBAAAAAAAA" to "RRRRRGGGGGGBBBBB"
        tempData = malloc(POTHigh * POTWide * 2);
        inPixel32 = (unsigned int*)data;
        outPixel16 = (unsigned short*)tempData;
        for(unsigned int i = 0; i < POTWide * POTHigh; ++i, ++inPixel32)
            *outPixel16++ = ((((*inPixel32 >> 0) & 0xFF) >> 3) << 11) | ((((*inPixel32 >> 8) & 0xFF) >> 2) << 5) | ((((*inPixel32 >> 16) & 0xFF) >> 3) << 0);
        free(data);
        data = tempData;
        
    }
    else if (pixelFormat == kCCTexture2DPixelFormat_RGBA4444) {
        //Convert "RRRRRRRRRGGGGGGGGBBBBBBBBAAAAAAAA" to "RRRRGGGGBBBBAAAA"
        tempData = malloc(POTHigh * POTWide * 2);
        inPixel32 = (unsigned int*)data;
        outPixel16 = (unsigned short*)tempData;
        for(unsigned int i = 0; i < POTWide * POTHigh; ++i, ++inPixel32)
            *outPixel16++ =
            ((((*inPixel32 >> 0) & 0xFF) >> 4) << 12) | // R
            ((((*inPixel32 >> 8) & 0xFF) >> 4) << 8) | // G
            ((((*inPixel32 >> 16) & 0xFF) >> 4) << 4) | // B
            ((((*inPixel32 >> 24) & 0xFF) >> 4) << 0); // A
        
        free(data);
        data = tempData;
        
    }
    else if (pixelFormat == kCCTexture2DPixelFormat_RGB5A1) {
        //Convert "RRRRRRRRRGGGGGGGGBBBBBBBBAAAAAAAA" to "RRRRRGGGGGBBBBBA"
        tempData = malloc(POTHigh * POTWide * 2);
        inPixel32 = (unsigned int*)data;
        outPixel16 = (unsigned short*)tempData;
        for(unsigned int i = 0; i < POTWide * POTHigh; ++i, ++inPixel32)
            *outPixel16++ =
            ((((*inPixel32 >> 0) & 0xFF) >> 3) << 11) | // R
            ((((*inPixel32 >> 8) & 0xFF) >> 3) << 6) | // G
            ((((*inPixel32 >> 16) & 0xFF) >> 3) << 1) | // B
            ((((*inPixel32 >> 24) & 0xFF) >> 7) << 0); // A
        
        free(data);
        data = tempData;
    }
    self = [self initWithData:data pixelFormat:pixelFormat pixelsWide:POTWide pixelsHigh:POTHigh contentSize:imageSize];
    
    // should be after calling super init
    hasPremultipliedAlpha_ = (info == kCGImageAlphaPremultipliedLast || info == kCGImageAlphaPremultipliedFirst);
    
    CGContextRelease(context);
    [self releaseData:data];
    
    return self;
}

@end