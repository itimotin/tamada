#ifdef GL_ES
precision mediump float;
#endif

varying vec2 v_texCoord;
varying  vec4 v_colorVarying;
uniform sampler2D u_texture0;
uniform sampler2D u_texture1;
uniform float u_time;

#define epsilon 0.0001
// 1
const float speed = 2.0;
const float bendFactor = 0.5;
void main()
{
    float ratio=0.5;  
    vec4 normalColor = texture2D(u_texture0, fract(vec2(v_texCoord.x, v_texCoord.y))).rgba;
    gl_FragColor = normalColor;
    
   // lowp vec4 textureColor = texture2D(u_texture0, fract(vec2(v_texCoord.x+offset , v_texCoord.y)));
   // lowp float gray = dot(textureColor, vec4(2.5, 1.0, 1.0, 0.0));
  //  gl_FragColor = vec4(gray * ratio + textureColor.r * (1.0 - ratio),  textureColor.g , textureColor.b, textureColor.a);
   // gl_FragColor=vec4(1.0,1.0,0,1);
}