attribute vec4 a_position;							
attribute vec2 a_texCoord;
attribute vec4 a_color;
attribute vec4 a_Vertex;

uniform mat4 u_MVPMatrix;
uniform float u_mouseX;
uniform float u_mouseY;
uniform float u_touch;
uniform float u_time;
uniform float u_type;
varying float v_touch;	

#ifdef GL_ES
varying lowp vec4 v_fragmentColor;
varying mediump vec2 v_texCoord;					
#else												
varying vec4 v_fragmentColor;						
varying vec2 v_texCoord;							
#endif												





void main()											
{
    v_touch=u_touch*6.5;
    vec4 thisPos = a_Vertex;
    float thisY = thisPos.y;
    float thisZ = thisPos.z;
    float thisX = thisPos.x;
    float thisW = thisPos.w;

    thisPos.y+= u_mouseY/2.5;
   //  thisPos.x+=15.6+ u_mouseX/1.5-u_touch*15.6;
     thisPos.x+=15.6+u_mouseX/1.5-u_touch*45.6;
     thisPos.w+=0.4-u_touch;
    float left=u_mouseX/90.5;
    if (u_touch==0.0)
    {
        left=0.0;
    }
    
    gl_Position=u_MVPMatrix*thisPos;
    mat2 rot;
    if (u_type>30.0)
        rot=mat2(1.2-u_touch,left,(0.0),(1.2-u_touch));
    else
        rot=mat2(1.2-u_touch,left,(0.2-u_touch),-1.2+u_touch);
        
    
  
    
    v_fragmentColor = a_color;
    v_texCoord = (a_texCoord)*rot;
}