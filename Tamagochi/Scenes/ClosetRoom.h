//
//  FirstScene.h
//  Tamagochi
//
//  Created by Sergiu Moțipan on 6/4/13.
//
//

#import "CCLayer.h"
#import "cocos2d.h"
#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES2/gl.h>
#import "Box2D.h"
#import "GLES-Render.h"
#import "PouNode.h"
#import "SimpleAudioEngine.h"
#import "CCScrollLayer.h"
#import "FGScrollLayer.h"
#import "ScrollLayer.h"
#import <SelectColors.h>

enum buttonsTags {
    butBack = 0,
    butBody =1,
    butDefault = 2,
    butBody1=3
};


#define PTM_RATIO 32

@interface ClosetRoom : CCLayer< PouNodeDelegate,CCTargetedTouchDelegate, SelectColorViewDelegate>
{
    
	ScrollLayer* scrollTable;
    CGSize winSize;
    int nr_sounds;
    float touchFun;
    NSMutableArray *polki_sprites;
    int touchPolk;
    int page;
    int butTouch;
    int selectedColor;
    CCLayerColor *colorLayer;
    CCSprite *backColors;
    int objTouch;
    int mouth;
    
    CGPoint startTouchPoint;
    CGPoint endTouchPoint;
    NSDate *date_start;
}

@property (nonatomic, strong) NSMutableArray *dataShopBody;

+(CCScene *) scene;



@end
